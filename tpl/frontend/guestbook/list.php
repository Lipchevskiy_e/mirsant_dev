			<div class="c_list">
			
	<?php foreach ($guestbook as $obj): ?>
						
			<div class="c_item">
				<div class="c_head"><strong><?php echo $obj->name; ?></strong> (<?php echo $obj->city; ?>) <span class="c_info"><?php echo system::show_data($obj->created_at); ?></span></div>
				<div class="c_content"><?php echo $obj->text; ?></div>
			
				<?php if($obj->answer!=''): ?>
				<div class="c_item">
					<div class="c_head"><strong>Администратор:</strong> <span class="c_info"><?php echo system::show_data($obj->updated_at); ?></span></div>
					<div class="c_content"><?php echo $obj->answer; ?></div>
				</div>
				<?php endif ?>

			</div>			
						
	<?php endforeach; ?>
	
		</div>	
	
	<?php 
	// pager
	echo guestbook::count_guestbook()>GUESTBOOK_AT_PAGE ? pager::pager_J("guestbook",GUESTBOOK_AT_PAGE,guestbook::count_guestbook(),intval($_GET['page'])) : '';
	?>	
	
	<?php
		// добавление отзыва
		echo system::show_tpl(array(),'/frontend/guestbook/add.php');
	?>