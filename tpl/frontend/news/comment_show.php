<hr style="border:0; border-bottom: dashed 1px gray; padding: 10px 0 10px 0;">

<?php foreach ($comment as $obj): ?>

					<div class="news">
                    	<div class="news_item">
                        	<div class="item_header">
                                <span class="news_data"><?php echo system::show_data($obj->created_at); ?></span>
                            </div>
                            <div class="round-sides"><img src="/pic/round-sides-r.png" width="6" class="r"><img src="/pic/round-sides-l.png" width="6" class="l">
                                <div class="c">
                                
                                    <div class="bold"><?php echo $obj->fio; ?></div>
                                    <?php echo $obj->text; ?>
                                    
                                </div>
                            </div>
                        </div>
					</div>

<?php endforeach; ?>					
					
					
	<?php 
	// pager
	echo news::count_comment_news(news::get_news_id($_GET['url']))>COMMENT_NEWS_AT_PAGE ? pager::pager_J("news/url/".$_GET['url'],COMMENT_NEWS_AT_PAGE,news::count_comment_news(news::get_news_id($_GET['url'])),intval($_GET['page'])) : '';
	?>	
	
	<?php /*
		begin 		<?php foreach ($comment as $obj): ?>
		end  		<?php endforeach; ?>
		
		fio  		<?php echo $obj->fio; ?>
		data  		<?php echo system::show_data($obj->created_at); ?>
		text 		<?php echo $obj->text; ?>		
		answer 	<?php echo news::count_comment($obj->answer); ?>
	*/?>					