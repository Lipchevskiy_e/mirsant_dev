<div class="n_list">
	<?php foreach ($news as $obj): ?>
					
				<div class="n_item">
					<a href="<?php echo '/news/'.$obj->url; ?>"><?php echo images::isset_image("/images/_news/_".$obj->id.".jpg",$obj->zag,'n_pic'); ?></a>
					<div class="n_txt">
						<div class="n_head"><a class="n_name" href="<?php echo '/news/'.$obj->url; ?>"><?php echo $obj->zag; ?></a></div>
						<div class="n_content"><?php echo $obj->text_short; ?></div>
						<div><a class="pop_link size11" href="<?php echo '/news/'.$obj->url; ?>">Читать дальше</a></div>
					</div>
				</div>	
                	
	<?php endforeach; ?>
</div>	
					
    
<div class="tac">
    <div class="page_listbot">
    	<?php 
    	// pager
    	echo news::count_news()>NEWS_AT_PAGE ? pager::pager_J(isset($_GET['type']) ? "news/type/".$_GET['type'] : "news",NEWS_AT_PAGE,news::count_news(),intval($_GET['page'])) : '';
    	?>	
    </div>
</div>	
	<?php /*
		begin 		<?php foreach ($news as $obj): ?>
		end  		<?php endforeach; ?>
		
		url  		<?php echo '/news/'.$obj->url; ?>
		data  		<?php echo system::show_data($obj->date_news); ?>
		img   		<?php echo images::isset_image("/images/_news/_".$obj->id.".jpg",$obj->zag,''); ?>
		zag  		<?php echo $obj->zag; ?>
		text_short	<?php echo $obj->text_short; ?>
		text 		<?php echo $obj->text; ?>		
		comments 	<?php echo news::count_comment($obj->id); ?>
	*/?>