﻿                    <div class="blocks_link">
                    	<a class="show_1" href="#">Новый покупатель</a>
                        <a class="show_2" href="#">Я уже зарегистрирован</a>
                    </div>

<div class="blocks_form">
	
	<!-- первый ТАБ -->
	<div id="show_1">

<p>Поля, обозначенные <span class="red">*</span>, обязательны к заполнению.</p>
<p class="red">Внимание! Необходимо указывать достоверную информацию. В противном случае заказ не будет принят.</p>

<br>

<form class="validat form_style" method="post" action="/basket">

		<h2>Общая информация:</h2>

		<div class="row">
			<label>ФИО</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[name]" value="<?php echo isset($user->name) ? $user->name : '' ;?>" class="valid" minlength="2" />
		</div>
		
		<div class="row">
			<label>Адрес</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[address]" value="<?php echo isset($user->address) ? $user->address : '' ;?>" class="" minlength="2" />
		</div>
	    	    
		<div class="row">
			<label>Телефон</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[phone]" value="<?php echo isset($user->phone) ? $user->phone : '' ;?>" class="valid digits" minlength="2" />
		</div>			
		
	    <div class="row">
			<label>E-Mail</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[email]" value="<?php echo isset($user->email) ? $user->email : '' ;?>" class="valid email" />
		</div>
		
		<div class="row">
			<label>Способ оплаты</label>
			<span class="r">&nbsp;</span>
			<select name="oplata" style="width:350px;">
				<option value="0"></option>
				<option value="1">Оплатить через LiqPay</option>
				<option value="2">Другой способ оплаты</option>
			</select>
		</div>
		
	    <div class="row">
			<label for="cnote">Примечание</label>
			<span class="r">&nbsp;</span>
			<textarea id="cnote" name="FORM[note]" class="" style="width:340px; height: 120px;"></textarea>
		</div>		
				
		<div class="row sdvig">
			<input type="submit" onclick="yaCounter36911050.reachGoal('zakazoform');ga('send', 'event', 'button', 'zakazoform'); return true;" value="Оформить заказ" class="but">
		</div>				
	
</form>




	</div>
	<!-- первый ТАБ -->

	<!-- второй ТАБ -->
	<div id="show_2" style="display:none;">

	<form class="validat form_style" method="post" action="/registration/formregistration/enter">
	
			<h2>Данные для авторизации:</h2>


			<div class="row">
				<label>Логин</label>
				<span class="r">&nbsp;</span>
				<input name="FORM[login]" class="valid" />
				<!-- validate[required,ajax[ajaxLogin]] -->
			</div>

			<div class="row">
				<label>Пароль</label>
				<span class="r">&nbsp;</span>
				<input name="FORM[pasword]" class="valid" type="password" style="width: 340px;" />
			</div>
			
		
			<div class="row sdvig">
				<input type="submit" value="Войти" class="but">
			</div>		
		
		<input type="hidden" name="basket" value="1">
		
	</form>	
	
	</div>
	<!-- второй ТАБ -->
	
</div>


		<link rel="stylesheet" type="text/css" href="/css/liValidForm.css" />
		<script type="text/javascript" src="/js/liValidForm.js"></script>
		<script type="text/javascript">
			$(function(){
				$('.validat').liValidForm()
			})
			//.valid [.mail, .url, .digits]
		</script>