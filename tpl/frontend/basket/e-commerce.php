<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-53134412-1']);
_gaq.push(['_trackPageview']);


ga('require', 'ecommerce', 'ecommerce.js');

ga('ecommerce:addTransaction', {
  'id': '<?php echo $ecommerce['orderNumber']; ?>',                     // Transaction ID. Required.
  'affiliation': 'Med-Nature',   // Affiliation or store name.
  'revenue': '<?php echo $ecommerce['sum']; ?>',               // Grand Total.
  'shipping': '0',                  // Shipping.
  'tax': '0' ,                    // Tax.
  'currency': 'UAH' 
});
<?php foreach ($ecommerce['goods'] as $good) { ?>
ga('ecommerce:addItem', {
  'id': '<?php echo $ecommerce['orderNumber']; ?>',                     // Transaction ID. Required.
  'name': '<?php echo $good['name']; ?>',    // Product name. Required.
  'sku': '<?php echo $good['id']; ?>',                 // SKU/code.
  'category': '<?php echo $good['category']; ?>',         // Category or variation.
  'price': '<?php echo $good['cost']; ?>',                 // Unit price.
  'quantity': '<?php echo $good['number']; ?>'                   // Quantity.
});
<?php } ?>
ga('ecommerce:send');
</script>