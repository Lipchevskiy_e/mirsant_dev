<div id="basket_show_all_goods" class="form_style" >

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <th align="left"><span>Товар</span></th>
    <th align="center">Цена</th>
    <th align="center">Наим<span>енование</span></th>
    <th class="kol-vo" align="center">Кол<span>личество</span></th>
	<th align="center">Сумма</th>
</tr>	

	<?php foreach ($result as $key => $value): ?>
		
		<tr style="text-align: center;">
			<td class="cart_photo">
				<div style="position:relative;">
                	<?php if(file_exists(HOST.IMG_CATALOG_PATH.'/02/'.$value['id'].'_1.jpg')): ?>
                		<p><a href="/catalog/goods/<?php echo mirsant::get_url_by_id_goods($value['id']); ?>"><img src="<?php echo IMG_CATALOG_PATH.'/02/'.$value['id'].'_1.jpg'; ?>" target="_blank"></a>
                    <?else:?>
                        <a href="/catalog/goods/<?php echo mirsant::get_url_by_id_goods($value['id']); ?>"><img src="/pic/no_photo2.jpg" target="_blank"></a>
                	<?php endif; ?>		
                	<br /></p>
                    <a class="delete_from_busket" title="Удалить" alt="Удалить" href="#" id="<?php echo $value['id']; ?>" data-id="<?php echo $key;?>">Удалить</a> 
				</div>
			</td>
            <td class="cart_cost">
				<div class="cart_name_cost"><?php echo mirsant::format_cost($value['cost']); ?> <span class="rub">грн</span></div>
			</td>
			<td class="cart_name">
				<div class="row"><a href="/catalog/goods/<?php echo mirsant::get_url_by_id_goods($value['id']); ?>"><?php echo basket::get_Good_Data($value['id'])->name; ?></a></div>
			</td>
			<td class="cart_kolvo">
				<input type="text" style="width:150px;" class="cart_kolvo_input change_kolvo_basket" id="<?php echo $value['id']; ?>" value="<?php echo $value['kolvo']; ?>" />
            </td>
			<td class="cart_summa">
				<div class="cart_summa_wrap"><?php echo $value['cost']*$value['kolvo']; ?> <span class="rub">грн</span></div>
			</td>
		</tr>		
		
	<?php endforeach; ?>
	
</table>

	<div class="itogo_wrap">
		Итого: <span class="itogo_sum"><?php echo mirsant::format_cost(basket::format_Cost(basket::get_Total_Goods())); ?></span><span class="rub"> грн</span><br />
		<a href="/catalog" class="floatleft size12">Продолжить покупки?</a>
	</div>

</div>