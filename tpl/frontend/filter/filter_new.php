<style>.s_inp .filter-link{text-decoration:none; border:none;}</style>
<div class="fltr news_after">
    <div class="l_title js-filter-title">
        <p>Фильтр</p>
    </div>
    <div class="fltr_con js-filter-block">
        <div class="sort_block">
            <div class="title">Сортировать:
                <div class="s_inp">
                    <a href="<?php echo mirsant::sort(1, $_SERVER['REQUEST_URI']); ?>">по возрастанию</a>
                    <a href="<?php echo mirsant::sort(2, $_SERVER['REQUEST_URI']); ?>">по убыванию</a>
                </div>
            </div>
            <div class="sort">
                <div class="razm">
                    <div class="t_sort">Цена:</div>
                    <div class="s_inp">
                        <label for="from">от</label>
                        <input type="text" id="from" value="<?php if($chosen['cost'][0]){ echo $chosen['cost'][0]; } ?>" />
                    </div>
                    <div class="s_inp">
                        <label for="do">до</label>
                        <input type="text" id="do" value="<?php if($chosen['cost'][1]){ echo $chosen['cost'][1]; } ?>" />
                    </div>
                </div>
                <?php if($category->has_weight){ ?>
                    <div class="razm">
                        <div class="t_sort" style="margin-left:0px;">Размер (см):</div>
                        <div class="s_inp">
                            <label for="">ширина</label>
                            <input type="text" id="width" value="<?php if($chosen['width'][0]){ echo $chosen['width'][0]; } ?>" />
                        </div>
                        <div class="s_inp">
                            <label for="">длина</label>
                            <input type="text" id="length" value="<?php if($chosen['length'][0]){ echo $chosen['length'][0]; } ?>" />
                        </div>
                    </div>
                <?php } ?>
                <a href="javascript:void(0)" id="btn_cost" class="buy_btn" style="margin-left:27px;margin-top:-4px;">OK</a>
            </div><!-- .sort -->

            <?php if(count($filters)){
                foreach($filters as $key => $filter){ ?>
					<?php if($_GET['grupa'] == 'akrilovie_vanni'):?>
						<?php if($filter['name'] <> 'Серия'):?>
						<div class="filter_block sort showed_list">
							<div class="t_sort"><?php echo $filter['name']; ?>:</div>
							<div class="brend fHide">
								<?php foreach($filter['features'] as $feature){
									echo catalog::generateInput($key, $feature, $category->url, $chosen);
								} ?>
							</div>
						</div>
						<?php endif; ?>
					<?php else: ?>
                    <div class="filter_block sort showed_list">
                        <div class="t_sort"><?php echo $filter['name']; ?>:</div>
                        <div class="brend fHide">
                            <?php foreach($filter['features'] as $feature){
                                echo catalog::generateInput($key, $feature, $category->url, $chosen);
                            } ?>
                        </div>
                    </div>
					<?php endif; ?>
                <?php }
            } ?>

        </div>
    </div>
</div>
<script>
    $('.brandAjax').on('mouseenter',function(){
        var el = $(this).find('.vsplivayka');
        if(el.text()==''){
            $.post('/ajax/getBrandInfo.php', {'id': $(this).attr('data-id')}, function(data) {
                if (data) {
                    el.html(data.str);
                }
            },'json');
        }
    });

    $(document).ready(function(){

        $('#btn_cost').live('click', function(){
            var from = $('#from').val();
            var to = $('#do').val();
            var width = $('#width').val();
            var length = $('#length').val();
            var id_parent = '<?php echo $category->id; ?>';
            var link = '<?php echo $_SERVER['REQUEST_URI']; ?>';
            var link_wo_filter = '/catalog/grupa/<?php echo $category->url; ?>';
            var filter = '<?php echo $_GET['filter']; ?>';

            jQuery.post('/ajax/filter.php', {
                'length':length,
                'width':width,
                'id_parent':id_parent,
                'from': from,
                'to':to,
                'link':link,
                'link_wo_filter':link_wo_filter,
                'filter':filter
            }, function(data) {
                if(data) {
                    window.location.href = data.link;
                }
            },'json');
        });

        $('.checkboxFilter').click(function(){
            var path = $(this).parent().attr('href');
            window.location.href = path;
        });

    });
</script>
