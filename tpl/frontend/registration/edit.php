<p>Поля, обозначенные <span class="red">*</span>, обязательны к заполнению.</p>

<form class="validat form_style regf" method="post" action="/registration/formregistration/edit">

		<h2>Общая информация:</h2>

		<div class="row">
			<label>ФИО</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[name]" value="<?php echo $obj->name; ?>" class="valid" minlength="2" />
		</div>
		
		<div class="row">
			<label>Адрес, индекс</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[address]" value="<?php echo $obj->address; ?>" class="valid" minlength="2" />
		</div>
	    	    

		<div class="row">
			<label>Телефон</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[phone]" value="<?php echo $obj->phone; ?>" class="valid digits" minlength="2" />
		</div>			
		
	    <div class="row">
			<label>E-Mail</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[email]" value="<?php echo $obj->email; ?>" class="valid email" />
		</div>
				

		<h2>Дополнительная информация:</h2>
		
		<div class="row">
			<label>Логин</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[login]" value="<?php echo $obj->login; ?>" class="valid"  />
			<!-- validate[required,ajax[ajaxLogin]] -->
		</div>

		<div class="row">
			<label>Пароль</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[pasword]" class="valid" value="<?php echo $obj->pasword; ?>"  minlength="2" type="password" style="width: 340px;" />
		</div>
		
		<div class="row sdvig">
			<input class="but" value="Сохранить изменения" type="submit">                    
		</div>	
	    

	
</form>
