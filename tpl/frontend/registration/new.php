<p>Поля, обозначенные <span class="red">*</span>, обязательны к заполнению.</p>
<p class="red">Внимание! При регистрации необходимо указывать достоверную информацию. В противном случае Ваш аккаунт не будет активирован.</p>

<form class="validat form_style regf" method="post" action="/registration/formregistration/new">

		<h2>Общая информация:</h2>


		<div class="row">
			<label>ФИО</label>
			<span class="r">&nbsp;</span>
			<input id="cfio" name="FORM[name]" value="<?php echo isset($_POST['FORM']['name']) ? $_POST['FORM']['name'] : '' ;?>" class="valid" minlength="2" />
		</div>
		
		<div class="row">
			<label>Адрес, индекс</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[address]" value="<?php echo isset($_POST['FORM']['address']) ? $_POST['FORM']['address'] : '' ;?>" class="valid" minlength="2" />
		</div>
	    	    

		<div class="row">
			<label>Телефон</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[phone]" value="<?php echo isset($_POST['FORM']['phone']) ? $_POST['FORM']['phone'] : '' ;?>" class="valid digits" minlength="2" />
		</div>			
		
	    <div class="row">
			<label>E-Mail</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[email]" value="<?php echo isset($_POST['FORM']['email']) ? $_POST['FORM']['email'] : '' ;?>" class="valid email" />
		</div>
				

		<h2>Дополнительная информация:</h2>
		
		<div class="row">
			<label>Логин</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[login]" value="<?php echo isset($_POST['FORM']['login']) ? $_POST['FORM']['login'] : '' ;?>" class="valid" />
			<!-- validate[required,ajax[ajaxLogin]] -->
		</div>

		<div class="row">
			<label>Пароль</label>
			<span class="r">&nbsp;</span>
			<input name="FORM[pasword]" class="valid" minlength="2" type="password" style="width: 340px;" />
		</div>
		
		<div class="row">
			<label>Введите код с картинки</label>
			<span class="r">&nbsp;</span>
			<?php echo captcha::get_captcha(); ?>
		</div>
		<br />	
		

		<div class="row sdvig">
			<input class="but" value="Зарегистрироваться" type="submit">                    
		</div>		
	    

</form>
