<!DOCTYPE html>
<html>
    <!-- (c) Wezom web-студия | www.wezom.com.ua -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Запрашиваемая страница не найдена!</title>
        <meta name="google-site-verification" content="pHW8zKRTWpQTwbqODU0swQHDNw2Xzaz1D8Y8WoLSmhE" />
        <base href="http://mirsant.com.ua">
        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="target-densitydpi=device-dpi">
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/colorbox.css">
        <link rel="stylesheet" href="/css/liValidForm.css">
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <script src="/js/jquery-1.8.0.min.js"></script>
        <script src="/js/copyright.min.js"></script>
        <script src="/js/liValidForm.v6.js"></script>
        <script src="/js/cufon-yui.js"></script>
        <script src="/js/jquery.colorbox-min.js"></script>
        <script src="/js/jquery.magnific-popup.min.js"></script>
        <script src="/js/Franklin_Gothic_Heavy_400.font.js"></script>
        <script src="/js/jquery.carouFredSel-6.1.0-packed.js"></script>
        <script src="/js/jquery.liHarmonica.js"></script>
        <script src="/js/main.js"></script>
        <script src="/js/plugins.js"></script>
        <script src="/js/swfobject.js"></script>
        <link rel="shortcut icon" href="favicon.ico">
        <!--[if IE 9]><style type="text/css">.top_menu a {padding: 10px 13px 11px 14px;}</style><![endif]-->
        <!--[if IE 6]><script src="/js/DD_belatedPNG_0.0.8a-min.js"></script><script>DD_belatedPNG.fix('.png, img');</script><![endif]-->
        <!--[if IE]><script>document.createElement('header');document.createElement('nav');document.createElement('section');document.createElement('article');document.createElement('aside');document.createElement('footer');</script><![endif]-->

        <link rel="stylesheet" type="text/css" media="all" href="/css/jquery/jquery.alerts.css" />
        <link rel="stylesheet" type="text/css" media="all" href="/css/shop.css" />
        <script type="text/javascript" src="/js/shop-ru.js"></script>
        <script type="text/javascript" src="/js/jquery/jquery.alerts.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-43877801-1', 'mirsant.com.ua');
            ga('send', 'pageview');

        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('body').copyright({
                    extratxt: '&copy; %source%',
                    sourcetxt: 'Интернет-магазин сантехники «СантехМир» ✔ Большой выбор акриловых ванн http://mirsant.com.ua/',
                    length: 5,
                    hide: true
                });
            });
        </script>
    </head>
    <body>
        <script type="text/javascript"> (function (d, w, c) {
                (w[c] = w[c] || []).push(function () {
                    try {
                        w.yaCounter36911050 = new Ya.Metrika({id: 36911050, clickmap: true, trackLinks: true, accurateTrackBounce: true, webvisor: true, trackHash: true});
                    } catch (e) {
                    }
                });
                var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
                    n.parentNode.insertBefore(s, n);
                };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";
                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript>
            <div>
                <img src="https://mc.yandex.ru/watch/36911050" style="position:absolute; left:-9999px;" alt="" />
            </div>
        </noscript>
        <!-- /Yandex.Metrika counter -->
        <section class="site_size">
            <script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" src="https://web.redhelper.ru/service/main.js?c=santmir"></script> <!--/Redhelper -->
            <header>
                <a href="/" class="logo"><img src="/pic/logo.png" alt="Интернет магазин – «Мир Сантехники»" title="Интернет магазин – «Мир Сантехники»" /></a>
                <div class="menu_search">
                    <ul class="top_menu">
                        <noindex><li><a  href="/" rel="nofollow">Главная</a></li></noindex>
                        <li><a rel="nofollow"  href="/o_kompanii">О компании</a></li>
                        <li><a rel="nofollow"  href="/contact">Контакты</a></li>
                        <li><a rel="nofollow"  href="/oplatadostavka">Оплата/Доставка</a></li>
                        <li><a rel="nofollow"  class="cur" href="/news">Новости</a></li>
                        <li><a rel="nofollow"  href="/video">Видео информация</a></li>
                    </ul>
                    <div class="search_block">
                        <div class="phone" itemscope itemtype="schema.org/PostalAddress">
                            <div>
                                <p><span itemprop = "telephone" >(095) 101-77-26</span></p>
                                <p><span itemprop = "telephone" >(097) 017-16-27</span></p>
                            </div>
                            <a href="#" class="call_order but_reg1" onclick="yaCounter36911050.reachGoal('zakazvonka'); ga('send', 'event', 'button', 'zakazvonka'); return true;">Заказать звонок</a>
                            <a href="#inline_example3" class="foundpure">Нашли дешевле?</a>
                            <div style="display:none">

                                <div id='inline_example2' style="margin: 0 auto; width: 450px; border: solid 0px red;">

                                    <div class="form_style">

                                        <form class="validat form_style" method="post">

                                            <div class="row">
                                                <label for="tel">Ваш номер</label>
                                                <span class="r">&nbsp;</span>
                                                <input id="tel" type="text" name="FORM[tel]" class="valid" value="" >
                                            </div>

                                            <div class="row">
                                                <label for="cq">Ваше имя</label>
                                                <span class="r">&nbsp;</span>
                                                <input id="name" type="text" name="FORM[name]" class="valid" value="">
                                            </div> 

                                            <div class="row">
                                                <label for="cq">Описание ситуации</label>
                                                <span class="r">&nbsp;</span>
                                                <textarea id="text" name="FORM[text]" class="" style="width: 250px;"></textarea>
                                            </div>

                                            <div class="row input_but" style="margin-top: 10px;">
                                                <input onclick="yaCounter36911050.reachGoal('zvonokotpr');ga('send', 'event', 'button', 'zvonokotpr'); return true;" onclick="" class="but" value="Отправить" type="submit" style="margin-left:163px;" id="button_1">                    
                                            </div>

                                            <input type="hidden" name="senttt" id="senttt"/>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div style="display:none">
                                <div id='inline_example3' style="margin: 0 auto; max-width: 500px; border: solid 0px red;">
                                    <div class="form_style">
                                        <form class="validat form_style" method="post">
                                            <div class="row"  style="padding: 0 20px 10px; text-align:justify;">
                                                <p>Если Вы хотите приобрести товар, но цену нашли на другом интернет-магазине сантехники ниже чем у нас - напишите нам ссылку и мы сделаем Вам цену, самую интересную в интернете.</p>
                                                <p>Данная услуга действует только на актуальные цены, на сантехнический товар в указанных интернет магазинах</p>
                                            </div>
                                            <div class="row">
                                                <label for="tel">Ваш номер телефона</label>
                                                <span class="r">&nbsp;</span>
                                                <input id="tel_2" type="text" name="FORM[tel_2]" class="valid" value="" >
                                            </div>
                                            <div class="row">
                                                <label for="url1">Ссылка на товар</label>
                                                <span class="r">&nbsp;</span>
                                                <input id="url1" name="FORM[url]" class="valid min_width" style="width: 250px;">
                                            </div>
                                            <div class="row input_but" style="margin-top: 10px;">
                                                <input onclick="" class="but" value="Отправить" type="submit" style="margin-left:163px;" id="button_11">                    
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <script>
                                $(document).ready(function () {
                                    $('#button_1').click(function () {
                                        var name = $("#name").val();
                                        var tel = $("#tel").val();
                                        var text = $("#text").val();
                                        var senttt = $('#senttt').val();
                                        jQuery.post('/ajax/send_to_callform.php', {'name': name, 'phone': tel, 'message': text, 'senttt': senttt}, function (data) {
                                            if (data)
                                            {
                                                var txt = data.msg;
                                                $.colorbox({html: '<p align="center" style="padding: 25px 50px 0">' + txt + '</p>'});
                                            }
                                        }, 'json');

                                        return false;
                                    });

                                    $('#button_11').click(function () {
                                        var tel = $("#tel_2").val();
                                        var url = $("#url1").val();
                                        jQuery.post('/ajax/found_cheaper.php', {'phone': tel, 'url': url}, function (data) {
                                            if (data)
                                            {
                                                var txt = data.msg;
                                                $.colorbox({html: '<p align="center" style="padding: 25px 50px 0">' + txt + '</p>'});
                                                $.magnificPopup.close();
                                            }
                                        }, 'json');
                                        return false;
                                    })
                                });
                            </script>                        </div>
                        <div class="search">
                            <form action="/search" method="get">
                                <input type="text" class="search_input" name="word" placeholder="Поиск товаров">
                                <input type="submit" class="search_but">
                            </form>
                            <div class="searchList" style="display: none;" data-s="0">

                            </div>
                        </div>
                    </div><!-- .search_block -->
                    <div class="login_block">
                        <a href="/basket" class="basket">
                            <div>Ваша корзина</div>
                            <p>Товаров: <span id='basket_count'>0</span></p>
                        </a>
                        <div class="login">
                            <form method="post" action="/registration/formregistration/enter">
                                <div>
                                    <p class="enter"><span>Вход</span></p>
                                    <span class="enter_block">
                                        <span>Логин</span>
                                        <input type="text" name="FORM[login]" />
                                        <span>Пароль</span>
                                        <input type="password" name="FORM[pasword]" />
                                        <a class="remember_pass" href="/registration/formregistration/fpasword">Забыли пароль?</a>
                                        <input type="submit" class="submit" value="Вход" />
                                    </span>
                                </div>
                                <noindex><a href="/registration/formregistration/new" class="reg" rel="nofollow">Регистрация</a></noindex>
                            </form>
                        </div>
                        <!-- .login -->
                    </div>
                    <!-- .login_block -->
                    <div class="clear"></div>
                </div>
                <!-- .menu_search -->
                <div class="clear"></div>
                <img class="mob-menu" src="images/show-menu-icon.png">
            </header>
            <div id="conteiner">
                <div class="left_block">
                    <div class="l_title mb15 n_tit">
                        <p>Новости</p>
                    </div>
                    <noindex>
                        <div class="news">
                            <a href="/news/besplatnaja_dostavka_smesitelej_po_ukraine" rel="nofollow">
                                <img src="/images/_news/_94.jpg" alt="" style="width:93px;height:93px;" />
                            </a>
                            <div class="news_info">
                                <a href="/news/besplatnaja_dostavka_smesitelej_po_ukraine" rel="nofollow">БЕСПЛАТНАЯ ДОСТАВКА СМЕСИТЕЛЕЙ ПО УКРАИНЕ</a>
                                <p style="overflow:hidden;"><p><span style="font-family: arial; font-size: x-small;"><span style="font-size: small;">При официальной оплате заказа через расчетный счет или карту Приват Банка, на любой смеситель представленный в нашем интернет магазине, доставка по всей Украине БЕСПЛАТНА.</span></span></p></p>
                            </div>
                        </div>
                        <div class="news">
                            <a href="/news/pokupaj_installjatsiju_poluchaj_skidku_5" rel="nofollow">
                                <img src="/images/_news/_93.jpg" alt="" style="width:93px;height:93px;" />
                            </a>
                            <div class="news_info">
                                <a href="/news/pokupaj_installjatsiju_poluchaj_skidku_5" rel="nofollow">Покупай инсталляцию получай скидку -5%</a>
                                <p style="overflow:hidden;"><p><span style="font-family: arial; font-size: small;">При покупке инсталляции любого производителя представленого в нашем интернет-магазине, Вы получаете купон -5% на весь ассортимент сантехники в ванную комнату. Выгода на глаза: покупка инсталляции дает самую выгодную цену на сантехнику в ванную комнату во ВСЕМ ИНТЕРНЕТЕ.</span></p></p>
                            </div>
                        </div>

                        <div class="news">
                            <a href="/news/mgnovennaja_rassrochka_ot_privat_banka" rel="nofollow">
                                <img src="/images/_news/_91.jpg" alt="" style="width:93px;height:93px;" />
                            </a>
                            <div class="news_info">
                                <a href="/news/mgnovennaja_rassrochka_ot_privat_banka" rel="nofollow">Мгновенная рассрочка от Приват Банка</a>
                                <p style="overflow:hidden;"><p><span style="font-family: arial; font-size: x-small;">Для клиентов Приват Банка у которых есть карты, в нашем интернет-магазине есть мгновенная рассрочка. Мгновенная рассрочка - это своего рода кредит с минимальным процентом. Оплата товара которую можно разбить на несколько месяцев.</span></p></p>
                            </div>
                        </div>
                    </noindex>	
                </div>
                <!-- .left_block -->
                <div class="right_block">
                    <h1>Запрашиваемая страница не найдена!</h1>
                    <p><span style="font-family: arial; font-size: small;"><strong><br />Запрашиваемая страница не найдена!</strong></span></p>	
                </div>
                <!-- .right_block -->
                <div class="clearfix"></div>
            </div>
            <!-- #conteiner-->
        </section>
        <footer id="footer">
            <div class="site_size">
                <div class="footer_left">
                    <div class="footer_left_ind">
                        <p>© 2006-2016 Интернет- магазин сантехники. Купить сантехнику в Киеве.</p>
                    </div>
                </div>
                <!-- .footer_left -->
                <noindex>
                    <div class="footer_right">
                        <img src="/pic/wezom_logo.png" alt="Продвижение сайтов">
                        <a title="Продвижение сайтов" href="http://wezom.com.ua" target="_blank" rel="nofollow" style="margin-left: -5px;">Продвижение сайтов<br></a>
                        <span style="margin-left: -10px;">web-студия Wezom</span></span>
                    </div>
                    <!-- .footer_right -->
                    <div align="center" class="footer_center_ind">
                    </div>
                    <!-- .footer_center_ind -->
                </noindex>
            </div>
            <!-- .site_size -->
        </footer>
        <script>
            $(document).ready(function () {
                $(".but_reg1").colorbox({width: "500px", height: "280px", href: "#inline_example2", inline: true, maxWidth: "95%", maxHeight: "95%"});
                // $(".foundpure").colorbox({width:"500px", height:"300px", href:"#inline_example3", inline:true, maxWidth: "95%", maxHeight: "95%"  });
                // $(".buy_btn_click").colorbox({width:"500px", height:"230px", href:"#inline_example_buy", inline:true, maxWidth: "95%", maxHeight: "95%"  });
                $(window).resize(function () {
                    $.colorbox.close();
                    $.colorbox.resize();
                });

                $(function () {
                    $('.foundpure').magnificPopup({
                        type: 'inline',
                        preloader: false
                    });
                });

                $('.search_input').keyup(function (e) {

                    if (e.keyCode == 27) {
                        $(this).val('');
                        $('.searchList').empty();
                        $('.searchList').hide();
                        return false;
                    }
                    var word = $(this).val();

                    if (word.length < 3) {
                        $('.searchList').empty();
                        return false;
                    }

                    $.ajax({
                        url: '/ajax/search.php',
                        type: 'POST',
                        data: {word: word},
                        success: function (data) {
                            $('.searchList').empty().html(data);
                            $('.searchList').show();
                        }
                    });
                });

                $('.searchList').mouseenter(function () {
                    $(this).attr('data-s', 1);
                });

                $('.searchList').mouseleave(function () {
                    $(this).attr('data-s', 0);
                });

                $('body').click(function () {

                    if ($('.searchList').attr('data-s') != 1 && $('.search_input').is(':focus') == false) {
                        $('.searchList').hide();
                    }

                })
                $('.search_input').focusin(function () {
                    $(this).attr('data-s', 1);

                    if ($(this).val() != '') {
                        $('.searchList').show();
                    }
                });

                $(document).keyup(function (e) {
                    if (e.keyCode == 27) {
                        $('.searchList').hide();
                    }
                });
            });
        </script>
    </body>
</html>