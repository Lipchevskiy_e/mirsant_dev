<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- (c) Wezom web-студия | http://www.wezom.com.ua/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<!--<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> -->
<title>Web-студия «Wezom»</title>
<script type="text/javascript" src="/backend/js/jquery-1.5.min.js"></script>
<script type="text/javascript" src="/backend/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/backend/js/main.js"></script>
<script type="text/javascript" src="/backend/js/jquery.litree_cms.js"></script>
<!--<script type="text/javascript" src="/backend/js/general.js"></script>-->
<script type="text/javascript" src="/backend/js/jquery.synctranslit.min.js"></script>
<link type="text/css" rel="stylesheet" href="/backend/css/style.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/litree_cms.css" />

<script type="text/javascript" src="/backend/js/jquery.validationEngine-ru.js"></script>
<script type="text/javascript" src="/backend/js/jquery.validationEngine.js"></script>
<link type="text/css" rel="stylesheet" href="/backend/css/validationEngine.jquery.css" />

<link type="text/css" rel="stylesheet" href="/backend/css/colorbox.css" />
<script type="text/javascript" src="/backend/js/jquery.colorbox-min.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js"></script>

<script src="/backend/js/i18n/jquery-ui-i18n.js" type="text/javascript"></script>
<link type="text/css" href="/backend/css/smoothness/jquery-ui-1.8.2.custom.css" rel="stylesheet" />

<script type="text/javascript" src="/js/swfobject.js"></script>


<!--[if IE 6]>
		<script src="/backend/js/DD_belatedPNG_0.0.8a-min.js"></script>
		<script>
			DD_belatedPNG.fix('.png, img');
		</script>  
<![endif]-->
</head>
				<div style="margin: 50px;">
                    
                    <h3>Информация о заказе №<?php echo $obj->id; ?></h3>
                    <img src="/pic/logo.png" alt="" width="200" class="logo_2" />
                    <p><?php echo NAME_FIRMS; ?></p>
                    
                    <strong>Информация о заказе</strong>
                    <table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="50%">Номер заказа:</td>
                        <td><?php echo $obj->id; ?></td>
                      </tr>
                      <tr>
                        <td>Дата заказа:</td>
                        <td><?php echo system::show_data($obj->created_at); ?></td>
                      </tr>
                      <tr>
                        <td>Статус заказа:</td>
                        <td><?php echo $select[$obj->status]; ?></td>
                      </tr>
                    </table>
                    <br />
                    <strong>Информация о клиенте</strong>
					
                    <table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="50%">Имя</td>
                        <td><?php echo $obj->name; ?></td>
                      </tr>
                      <tr>
                        <td>Адрес</td>
                        <td><?php echo $obj->address; ?></td>
                      </tr>
                      <tr>
                        <td>Телефон</td>
                        <td><?php echo $obj->phone; ?></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><?php echo $obj->email; ?></td>
                      </tr>
                      <tr>
                        <td>Примечание</td>
                        <td><?php echo $obj->note; ?></td>
                      </tr>
                    </table>
                    <br />
					<strong>Содержание заказа</strong>
					
                    <table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <th>Артикул</th>
                        <th>Название</th>
                        <th>Цена за штуку</th>
                        <th>Кол-во</th>
                        <th>Промежуточный итог</th>
                      </tr>
					  
					<?php $_total=0; ?>

					<?php foreach ($result0 as $d_ord): ?>
						
						<tr>
                            <td><?php echo $d_ord->artikul; ?></td>
							<td><?php echo $d_ord->name; ?></td>
							<td><?php echo mirsant::format_Cost($d_ord->cost); ?> грн.</td>
                            <td><?php echo $d_ord->kolvo; ?></td>
							<td><?php echo mirsant::format_Cost($d_ord->kolvo*$d_ord->cost); ?> грн.</td>
						</tr>

						<?php $_total+=$d_ord->kolvo*$d_ord->cost; ?>					  
						
					<?php endforeach; ?>
					  
                    </table>
                    
					<br />
					
                    <div align="right">
                        <strong>Итого:</strong>  <?php echo mirsant::format_Cost($_total) ?> грн.
                    </div>
					
				</div>				
					
					
