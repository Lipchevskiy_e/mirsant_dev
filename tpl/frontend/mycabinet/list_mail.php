<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th><strong>Артикул</strong></th>
		<th><strong>Наименование товара</strong></th>
		<th align="center"><strong>Количество</strong></th>
		<th align="center"><strong>Сумма</strong></th>
	</tr>	
	
<?php foreach ($result as $obj): ?>

	<tr>
		<td>
			<?php echo basket::get_Good_Data($obj->id_good)->artikul; ?>
		</td>	
		<td>
			<?php echo basket::get_Good_Data($obj->id_good)->name; ?><br>
			<?php echo $obj->cost; ?>
		</td>
		<td align="center">
			<?php echo $obj->kolvo; ?>
		</td>
		<td align="center">
			<?php echo basket::format_Cost($obj->cost*$obj->kolvo); ?>
		</td>
	</tr>		

<?php endforeach; ?>	

	<tr>
		<td colspan="3"><strong>Итого:</strong></td>
		<td align="center"><strong><?php echo mycabinet::get_Total_Order($obj->number_order)->total; ?></strong></td>
	</tr>
    
	</table>