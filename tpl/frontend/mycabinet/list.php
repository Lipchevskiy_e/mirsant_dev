<?php if($result): ?>
    <?php foreach ($result as $obj): ?>
    	<tr style="text-align: center;">
    		<td class="cart_photo">
    			<div style="position:relative;">
                	<?php if(file_exists(HOST.IMG_CATALOG_PATH.'/02/'.$obj->id_good.'_1.jpg')): ?>
                		<a href="/catalog/goods/<?php echo mirsant::get_url_by_id_goods($obj->id_good); ?>"><img src="<?php echo IMG_CATALOG_PATH.'/02/'.$obj->id_good.'_1.jpg'; ?>" target="_blank"></a>
                    <?else:?>
                        <a href="/catalog/goods/<?php echo mirsant::get_url_by_id_goods($obj->id_good); ?>"><img src="/pic/no_photo2.jpg" target="_blank"></a>
                	<?php endif; ?> 
				</div>
    		</td>
    		<td class="cart_name">
    			<div class="row"><a href="/catalog/goods/<?php echo mirsant::get_url_by_id_goods($obj->id_good); ?>"><?php echo basket::get_Good_Data($obj->id_good)->name; ?></a></div>
    		</td>
            <td class="cart_kolvo">
    			<div class="cart_name_cost"><?php echo mirsant::format_cost($obj->cost); ?> <span class="rub">грн</span></div>
    		</td>
    		<td class="cart_kolvo">
    			<?php echo $obj->kolvo; ?>
    		</td>
    		<td class="cart_summa">
    			<div class="cart_summa_wrap"><?php echo mirsant::format_cost(basket::format_Cost($obj->cost*$obj->kolvo)); ?> <span class="rub">грн</span></div>
    		</td>
    	</tr>		
    <?php endforeach; ?>
<?php endif; ?>