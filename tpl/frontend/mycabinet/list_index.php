<?php foreach ($result as $obj): ?>

	<div class="row" align="right" style="margin: 0 20px 0 0;"><a href="/mycabinet/ctrl/print/id/<?php echo $obj->id; ?>" target="_blank">Распечатать заказ</a></div>

	<div style="padding:5px 11px;-moz-border-radius: 20px 20px 0 0; -webkit-border-radius:20px 20px 0 0; -khtml-border-radius: 20px 20px 0 0; border-radius: 20px 20px 0 0; background:#D4F2FA; overflow:hidden;">
		<div style="font:12px/1.2em Tahoma, Geneva, sans-serif; float:left">
			№<?php echo $obj->id; ?> -
			<span class="red" style="font:18px/1.2em Arial, Helvetica, sans-serif;">
				<?php echo $status_order[$obj->status]; ?>
			</span>
		</div>
		<div style="padding:4px 0 0 0; float:right;">
			<?php echo system::show_data($obj->created_at); ?>
		</div>
	</div>

	<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        <th align="left"><span>Товар</span></th>
        <th>Наим<span>енование</span></th>
        <th>Цена</th>
        <th class="kol-vo" align="center">Кол<span>личество</span></th>
    	<th align="center">Сумма</th>
    </tr>	
	
	<?php echo system::show_tpl(array('result'=>mycabinet::get_Order($obj->id),'status_order'=>$status_order),'/frontend/mycabinet/list.php'); ?>
	

	</table>
	
	<div class="itogo_wrap" style="-moz-border-radius: 0 0 20px 20px; -webkit-border-radius: 0 0 20px 20px; -khtml-border-radius: 0 0 20px 20px; border-radius: 0 0 20px 20px; background:#D4F2FA; overflow:hidden; padding:5px 11px 8px 0; margin:0 0 20px 0">
		Итого:
		<span class="itogo_sum">
			<?php echo mirsant::format_cost(mycabinet::get_Total_Order($obj->id)->total); ?>
		</span>
		<span class="rub">
			грн
		</span>
	</div>	
	
	
<?php endforeach; ?>	

