<div class="h2ForGB">Оставьте свой отзыв о товаре:</div>

<?php if(!empty($errors)): ?>
	<div class="errorsPlace">
		<?php foreach($errors AS $error): ?>
			<div class="errorPlace"><?php echo $error; ?></div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>

<?php if(isset($_SESSION['messageGB'])): ?>
	<div class="errorsPlace">
		<div class="goodPlace"><?php echo $_SESSION['messageGB']; ?></div>
	</div>
	<?php unset($_SESSION['messageGB']); ?>
<?php endif; ?>

<form action="" name="contact" method="post" class="validat form_style">
	<div class="row">
		<label>ФИО</label>
		<span class="r">&nbsp;</span>
		<input type="text" name="FORM[name]" class="valid" value="<?php echo (!empty($errors) AND isset($post['name'])) ? $post['name'] : ''; ?>" id="name" maxlength="36" style="width:340px;" />
	</div>
	<div class="row">
		<label>E-Mail</label>
		<span class="r">&nbsp;</span>
		<input type="text" name="FORM[email]"  class="valid email" value="<?php echo (!empty($errors) AND isset($post['email'])) ? $post['email'] : ''; ?>" id="email" style="width:340px;" />
	</div>
	<div class="row">
		<label>Отзыв</label>
		<span class="r">&nbsp;</span>
		<textarea name="FORM[text]" class="valid" id="text" style="width: 340px; height: 100px;"><?php echo (!empty($errors) AND isset($post['text'])) ? $post['text'] : ''; ?></textarea>
	</div>
	<div class="row sdvig">
		<input type="submit" value="Оставить отзыв" class="but">
	</div>
	<input type="hidden" name="sendGB" value="yes">
</form>