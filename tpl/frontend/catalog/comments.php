<?php if(count($result) > 0): ?>
	<div class="h2ForGB">Отзывы о данном продукте</div>
	<div class="cList">
		<?php foreach($result AS $obj): ?>
			<div class="cBlock">
				<div class="cHead">
					<span class="cName"><?php echo $obj->name; ?></span>
					<span class="cDate"><?php echo date('d.m.Y H:i',$obj->created_at); ?></span>
				</div>
				<div class="cContent"><?php echo nl2br($obj->text); ?></div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>