<?if(count($result)):?>

    <div class="similar">
        <div class="s_title">
            <p>Похожие товары</p>
        </div>
        <div class="tovar_content">        
            <?php foreach ($result as $obj): ?>
                <?php if($obj->url!=$_GET['goods']): ?>
                    <div class="tovar_block">
                        <div class="tovar_img">
                            <a href="<?php echo general::link('catalog/goods/'.$obj->url,$obj->name); ?>">
                                <?if($obj->block_new == 1):?>
                                    <span class="new_stiker"></span>
                                <?endif?>
                                <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/02/'.$obj->id.'_1.jpg')): ?>
                				    <img src="<?php echo IMG_CATALOG_PATH.'/02/'.$obj->id.'_1.jpg'; ?>" alt="<?php echo $obj->name; ?>">
                                <?else:?>
                                    <img src="/pic/no_photo2.jpg" alt="<?php echo $obj->name; ?>" />
                				<?php endif; ?>
                            </a>
                        </div>
                        <div class="tovar_info">
                            <a class="name_t" href="<?php echo general::link('catalog/goods/'.$obj->url,$obj->name); ?>"><?php echo $obj->name; ?></a>
                            <p>Артикул: <span><?php echo $obj->artikul; ?></span></p>
                            <p>Бренд: <span><?=mirsant::get_brand_by_id($obj->brand)?></span></p>
                            <span class="price"><?=mirsant::format_cost($obj->cost)?> грн</span>
                            <a href="#" class="add_tovar add_to_busket buy_btn" id="<?=$obj->id?>">Купить</a>
                        </div>
                	</div> 
            	<?php endif ?>
            <?php endforeach; ?>
        </div>
    </div>	

<?endif?>