﻿<?php $time = catalog::get_night_time();?>                
                <div class="tovar_left_block">
					<div class="one_tovar_img">
						<?if($obj->block_new == 1):?>
                              <span class="new_stiker"></span>
                        <?endif?>
                        <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/03/'.$obj->id.'_1.jpg')): ?>
		                      <a rel="example1" href="<?=IMG_CATALOG_PATH.'/original/'.$obj->id.'_1.jpg'?>"><img itemprop="image" src="<?php echo IMG_CATALOG_PATH.'/03/'.$obj->id.'_1.jpg'; ?>" alt="<?php echo $obj->name; ?>"></a>
                        <?else:?>
                              <img itemprop="image" src="/pic/no_photo_big2.jpg" alt="<?php echo $obj->name; ?>" />
  				        <?php endif; ?>
					</div>
                    <?for($i = 2; $i <= 5; $i++):?>
                        <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/01/'.$obj->id.'_'.$i.'.jpg')): ?>
		                      <a rel="example1" href="<?=IMG_CATALOG_PATH.'/original/'.$obj->id.'_'.$i.'.jpg'?>"><img itemprop="image" src="<?php echo IMG_CATALOG_PATH.'/01/'.$obj->id.'_'.$i.'.jpg'; ?>" alt="<?php echo $obj->name; ?>"></a>
                        <?endif?>
                    <?endfor?>
				</div><!-- .tovar_left_block -->
				<div class="tovar_right_block">
					<div class="price_buy fl_l">
						<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
							<p><span style="font-size: 20px;">Цена:</span> <span itemprop="price" id="c_<?php echo $obj->id; ?>">
							
							<?php if ($time['night'] == 1 AND $time['time_from'] > 0 AND time() > $time['time_from'] AND $time['time_to'] > 0 AND time() < $time['time_to']):?> 
								<?php if ($obj->valuta==2) $cost=$obj->cost_night*EVRO; elseif ($obj->valuta==3) $cost=$obj->cost_night*DOLLAR; else $cost=$obj->cost_night; ?>
									<?php if (!isset($_SESSION['skidka'][$obj->id])) echo mirsant::format_cost($cost); else echo  mirsant::format_cost($cost-($cost*$_SESSION['skidka'][$obj->id]/100));?>
									</span> грн
							<?php else:?>
							<?php if ($obj->valuta==2) $cost=$obj->cost*EVRO; elseif ($obj->valuta==3) $cost=$obj->cost*DOLLAR; else $cost=$obj->cost; ?>
								<?php if (!isset($_SESSION['skidka'][$obj->id])) echo mirsant::format_cost($cost); else echo  mirsant::format_cost($cost-($cost*$_SESSION['skidka'][$obj->id]/100));?>
								</span> грн
							<?php endif;?>	
							</p>
							<meta itemprop="priceCurrency" content="UAH"/>
							<link itemprop="availability" href="http://schema.org/InStock"/>
						</span>
						<div class="clear">	</div>
						<a href="#" class="add_tovar add_to_busket buy_btn_big" id="<?=$obj->id?>" onclick="yaCounter36911050.reachGoal('kupitcart');ga('send', 'event', 'button', 'kupitcart'); return true;">Купить</a>
						<?php if ($obj->brand_skidka>0 or $obj->catalog_skidka>0 and (!isset($_SESSION['skidka'][$obj->id]))): ?><a href="#" class="get_skidka" id="<?=$obj->id?>">Получить скидку</a><?php endif; ?>
					    <div class="clear"></div>
                        <div class="tal">
                            <a href="#inline_example_buy" class="buy_btn_click foundpure_style fl_n" onclick="yaCounter36911050.reachGoal('kupitoneclick');ga('send', 'event', 'button', 'kupitoneclick'); return true;">Купить в один клик</a>
                        </div>
                    </div>
					<!-- <div class="description"> -->
						<?php //echo $obj->text_short; ?>
					<!-- </div> -->

					<!--Раскомментировать и стилизовать Блок информации-->
					
					<div class="characteristic_top">
						<ul class="characteristic_top_list">
							<?php if( $obj->payment ): ?>
								<li class="payment">
									<div class="value">
										<a href="#popup_payment" class="magnificPopup_open"><?php echo $payment;?></a>
									</div>
									<div class="name">
										<span>Оплата:</span>
									</div>
									<div class="clear"></div>
								</li>
							<?php endif; ?>
							<?php if( $obj->delivery ): ?>
								<li class="delivery">
									<div class="value">
										<a href="#popup_delivery" class="magnificPopup_open"><?php echo $delivery;?></a>
									</div>
									<div class="name">
										<span>Доставка:</span>
									</div>
									<div class="clear"></div>
								</li>
							<?php endif; ?>
							<?php if( $obj->services ): ?>
								<li class="climb">
									<div class="value">
										<a href="#popup_climb" class="magnificPopup_open"><?php echo $services;?></a>
									</div>
									<div class="name">
										<span>Поднятие на этаж:</span>
									</div>
									<div class="clear"></div>
								</li>
							<?php endif; ?>
							<?php if( $obj->cost_services ): ?>
								<li class="installation">
									<div class="value">
										<a href="#popup_installation" class="magnificPopup_open"><?php echo $cost_services;?></a>
									</div>
									<div class="name">
										<span>Монтаж и Установка:</span>
									</div>
									<div class="clear"></div>
								</li>
							<?php endif; ?>
							<?php if( $obj->warranty ): ?>
								<li class="warranty">
									<div class="value">
										<a href="#popup_warranty" class="magnificPopup_open"><?php echo $warranty;?></a>
									</div>
									<div class="name">
										<span>Информация о гарантии:</span>
									</div>
									<div class="clear"></div>
								</li>
							<?php endif; ?>
						</ul>
					</div>

					

				</div><!-- .tovar_right_block -->

				<div class="clear"></div>

				<div class="tovar_left_block">
					
					<div class="characteristic">
						<h2 class="title">Характеристики</h2>
						<?//=$obj->charstik?>
						<p><span>Артикул:</span><ins><span itemprop="productID"><?=$obj->artikul?></span></ins></p>
						<p><span>Бренд:</span><ins><span itemprop="brand" itemscope itemtype="http://schema.org/Organization"><span itemprop="name"><?=mirsant::get_brand_by_id($obj->brand)?></span></span></ins></p>
						<?if($obj->has_type != '' and $obj->has_type != 0 and $obj->has_type != NULL):?><p><span>Тип:</span><ins><?=mirsant::get_type_tovar($obj->has_type)?></ins></p><?endif?>
						<?if($obj->width != '' and $obj->width != 0 and $obj->width != NULL):?><p><span>Ширина:</span><ins><?=$obj->width?> см</ins></p><?endif?>
						<?if($obj->height != '' and $obj->height != 0 and $obj->height != NULL):?><p><span>Высота:</span><ins><?=$obj->height?> см</ins></p><?endif?>
						<?if($obj->length != '' and $obj->length != 0 and $obj->length != NULL):?><p><span>Длина:</span><ins><?=$obj->length?> см</ins></p><?endif?>
                        <?php if( $obj->tray_depth ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_tray_depth WHERE id = "'.$obj->tray_depth.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Глубина поддона:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->cabin_form ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_cabin_form WHERE id = "'.$obj->cabin_form.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Форма кабины:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->color ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_colors WHERE id = "'.$obj->color.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Цвет:</span><ins><?php echo $__res->color; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->mounting_type ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_mounting_type WHERE id = "'.$obj->mounting_type.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Тип монтажа:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->mixer_style ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_mixer_style WHERE id = "'.$obj->mixer_style.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Стиль смесителя:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->mixer_type ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_mixer_type WHERE id = "'.$obj->mixer_type.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Тип смесителя:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>

						<?php if( $obj->toilet_mount ): ?>
							<?php $__res = mysql::query_one('SELECT name FROM dict_toilet_mount WHERE id = "'.$obj->toilet_mount.'"'); ?>
							<?php if( $__res ): ?>
								<p><span>Тип монтажа:</span><ins><?php echo $__res->name; ?></ins></p>
							<?php endif; ?>
						<?php endif; ?>
						<?php if( $obj->bathtub_form ): ?>
							<?php $__res = mysql::query_one('SELECT name FROM dict_bathtub_form WHERE id = "'.$obj->bathtub_form.'"'); ?>
							<?php if( $__res ): ?>
								<p><span>Форма ванны:</span><ins><?php echo $__res->name; ?></ins></p>
							<?php endif; ?>
						<?php endif; ?>
					</div>

					<div class="soc_add">
						<script type="text/javascript" src="//yandex.st/share/share.js"charset="utf-8"></script>
                        <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,odnoklassniki,lj,gplus"></div> 
					</div>

				</div><!-- .tovar_left_block -->
				
				
				<?php echo $info?>
				
				
				<div class="current_tovar_model">
					<h3 class="title">Описание</h3>
					<?php  $new_text_full = preg_replace('/<iframe.*?iframe>/is', '', $obj->text_full);?>
					<div class="current_tovar_model_cont">
						<div class="content" itemprop="description"><?php echo $new_text_full?$new_text_full:$obj->text_full; ?></div>
					</div>
				</div><!-- .current_tovar_model -->
				<?php if($arr[0] || $obj->video){?>
					<div class="current_tovar_model">
						<h3 class="title">Видео</h3>
						<div class="tovar_video">
						<?php preg_match('|<iframe[^>]*?>(.*?)</iframe>|sei', $obj->text_full, $arr);  ?>
							<?php if($arr[0]):?>

								<?php echo $arr[0]?>
							<?php else:?>
								<iframe src="<?php echo $obj->video?>" width="420" frameborder="0" height="315"></iframe>
							<?php endif;?>
						</div>
					</div>
				<?php }?>

                <?php echo catalog::get_catalog_ass_new("/frontend/catalog/show_ass.php", 2, mirsant::get_id_goods_by_url()); ?>

	<script type="text/javascript">
    	$(document).ready(function(){
    	   $("a[rel='example1']").colorbox({width:false, height:false, maxWidth: "95%", maxHeight: "95%"  });		
    	});
	</script>

<div class="similar_items_wrap">
	<div class="similar_items_title">
		<span>Похожие товары</span>
	</div>
	<div class="similar_items_overflow">
		<?php foreach ($same as $value): ?>
			<div class="tovar_block">
				<div class="tovar_img">
					<a href="<?php echo general::link('catalog/goods/'.$value->url,$value->name); ?>">
			            <?if($value->block_new == 1):?>
			                <span class="new_stiker"></span>
			            <?endif?>
			            <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/02/'.$value->id.'_1.jpg')): ?>
						    <img src="<?php echo IMG_CATALOG_PATH.'/02/'.$value->id.'_1.jpg'; ?>" alt="<?php echo $value->name; ?>">
			            <?else:?>
			                <img src="/pic/no_photo2.jpg" alt="<?php echo $value->name; ?>" />
						<?php endif; ?>
			        </a>
				</div>
				<div class="tovar_info">
					<a class="name_t" href="<?php echo general::link('catalog/goods/'.$value->url,$value->name); ?>"><?php echo $value->name; ?></a>
					<p>Артикул: <span><?php echo $value->artikul; ?></span></p>
					<p>Бренд: <span><?=mirsant::get_brand_by_id($value->brand)?></span></p>
					
					<?php if ($time['night'] == 1 AND $time['time_from'] > 0 AND time() > $time['time_from'] AND $time['time_to'] > 0 AND time() < $time['time_to']):?> 
						<span class="price">
							<span style="font-size: 12px;">Цена:</span> 
							<span id="c_<?php echo $value->id;?>">
								<?php if ($value->valuta==2) $cost=$value->cost_night*EVRO; elseif ($value->valuta==3) $cost=$value->cost_night*DOLLAR; else $cost=$value->cost_night; ?>
								<?php if (!isset($_SESSION['skidka'][$value->id])) echo mirsant::format_cost($cost); else echo  mirsant::format_cost($cost-($cost*$_SESSION['skidka'][$value->id]/100));?>
							</span> грн
						</span>
					<?php else:?>
						<span class="price"><span style="font-size: 12px;">Цена:</span> <span id="c_<?php echo $value->id;?>">
						<?php if ($value->valuta==2) $cost=$value->cost*EVRO; elseif ($value->valuta==3) $cost=$value->cost*DOLLAR; else $cost=$value->cost; ?>
						<?php if (!isset($_SESSION['skidka'][$value->id])) echo mirsant::format_cost($cost); else echo  mirsant::format_cost($cost-($cost*$_SESSION['skidka'][$value->id]/100));?>
						</span> грн</span>
					<?php endif;?>
					
					<a href="#" class="add_tovar add_to_busket buy_btn" id="<?=$value->id?>" onclick="yaCounter36911050.reachGoal('kupitpohozh');ga('send', 'event', 'button', 'kupitpohozh'); return true;">Купить</a>
					<?php if (($value->brand_skidka>0 or $value->catalog_skidka>0) and (!isset($_SESSION['skidka'][$value->id]))): ?><a href="#" class="get_skidka_min" id="<?=$value->id?>">Получить скидку</a><?php endif; ?>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>

<div style="display:none">
	<div id="inline_example_buy" class="fast_buy" style="margin: 0 auto; max-width: 500px; border: solid 0px red; padding: 20px 0;">
		<p class="title" align="center">Купить в один клик</p>
		<form class="validat form_style" method="post">
			<div class="row">
				<label for="qbe">Ваш E-Mail</label>
				<span class="r">&nbsp;</span>
				<input id="quick_buy_email" type="text" name="email" class="valid" value="" />
			</div>
			<div class="row">
				<label for="tel">Ваш телефон</label>
				<span class="r">&nbsp;</span>
				<input id="quick_buy_phone" type="text" name="phone" class="valid" value="" />
			</div>
			<br />
			<div class="row input_but" style="margin-top:10px;">
				<input id="buy_one_click_button" onclick="yaCounter36911050.reachGoal('podtverzhdone');ga('send', 'event', 'button', 'podtverzhdone'); return true;" onclick="" class="but" value="Подтвердить" type="submit" style="margin-left:163px;">
			</div>
			<input id="quick_buy_product" type="hidden" name="id_product" value="<?php echo $obj->id; ?>">
		</form>
	</div>

	<div id="popup_payment" class="popap_wrap">
		<div class="wTxt">
			<?php echo $obj->payment; ?>
		</div>
	</div>

	<div id="popup_climb" class="popap_wrap">
		<div class="wTxt">
			<?php echo $obj->services; ?>

		</div>
	</div>

	<div id="popup_installation" class="popap_wrap">
		<div class="wTxt">
			<?php echo $obj->cost_services; ?>

		</div>
	</div>

	<div id="popup_warranty" class="popap_wrap">
		<div class="wTxt">
			<?php echo $obj->warranty; ?>
		</div>
	</div>

	<div id="popup_delivery" class="popap_wrap">
		<div class="wTxt">
			<?php echo $obj->delivery; ?>
		</div>
	</div>

</div>

<?php echo $form; ?>
<?php echo $comments; ?>

<script>   
	// Include the ecommerce plugin
	ga('require', 'ecommerce', 'ecommerce.js');
	ga('create', 'UA-53134412-1', 'auto');
    ga('send', 'pageview');

    $(document).ready(function(){
        $('.get_skidka').click(function(){
            var id = $(this).attr('id');
            jQuery.post('/ajax/get_skidka.php', {'id':id}, function(data) {
                if(data){
                    $('#c_'+id).html(data.new_cost);
                    $('.get_skidka').hide()
                }
            },'json');
            return false;
        });
        $(function () {
			$('.buy_btn_click').magnificPopup({
				type: 'inline',
				preloader: false
			});
		});

        $('#buy_one_click_button').click(function(){
            var email = $("#quick_buy_email").val();
            var phone = $("#quick_buy_phone").val();
            var id_product = $("#quick_buy_product").val();
            jQuery.post('/ajax/buyoneclick.php', {
                'email': email,
                'phone': phone,
                'id_product': id_product
            }, function(data) {
                if(data){
                    var txt = data.msg;
                    $.colorbox({html:'<p align="center" style="padding: 25px 50px 0">'+txt+'</p>'});
                    $.magnificPopup.close();
					//console.log(data.cost_p);
					ga('ecommerce:addTransaction', {
								'id': 'fast_'+data.id_order,   
					   'affiliation': 'Mirsant', 
						   'revenue': "'"+data.cost_p+"'",      
						  'shipping': '0',        
							   'tax': '0'  
					});
					// Add item
					ga('ecommerce:addItem', {
							 'id': 'fast_'+data.id_order,          
							'sku': "'"+data.product.id+"'",       
						   'name': "'"+data.product.name+"'", 
					   'category': "'"+data.category+"'",  
						  'price': "'"+data.cost_p+"'",       
					   'quantity': '1' 
					});
					ga('ecommerce:send');
                }
            },'json');
            return false;
        });

    });
</script>

