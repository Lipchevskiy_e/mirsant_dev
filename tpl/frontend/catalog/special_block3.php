
	<?php foreach ($result as $obj): ?>
                        <div class="list_item">
                            <a href="<?php echo general::link('catalog/goods/'.$obj->id,$obj->name); ?>" class="list_item_link">
                                <span class="list_item_name"><?php echo $obj->name; ?></span>
                                <span class="list_item_photo">
                                    <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/01/'.$obj->id.'_1.jpg')): ?>
									<img src="<?php echo IMG_CATALOG_PATH.'/01/'.$obj->id.'_1.jpg'; ?>" alt="<?php echo $obj->name; ?>">
									<?php endif; ?>
                                    <span class="helper"></span>
                                </span>
                            </a>
							<span class="list_item_cost line-through gray"><?php echo $obj->cost; ?><span class="rub">a</span></span>
							<span class="list_item_cost red"><?php echo $obj->cost_spec; ?><span class="rub">a</span></span>
                            <a class="but_1 add_to_busket" href="#" id="<?php echo $obj->id; ?>">Купить</a>
                        </div>
	<?php endforeach; ?>
	