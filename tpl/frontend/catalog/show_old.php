                
                <div class="tovar_left_block">
					<div class="one_tovar_img">
						<?if($obj->block_new == 1):?>
                              <span class="new_stiker"></span>
                        <?endif?>
                        <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/03/'.$obj->id.'_1.jpg')): ?>
		                      <a rel="example1" href="<?=IMG_CATALOG_PATH.'/original/'.$obj->id.'_1.jpg'?>"><img itemprop="image" src="<?php echo IMG_CATALOG_PATH.'/03/'.$obj->id.'_1.jpg'; ?>" alt="<?php echo $obj->name; ?>"></a>
                        <?else:?>
                              <img itemprop="image" src="/pic/no_photo_big2.jpg" alt="<?php echo $obj->name; ?>" />
  				        <?php endif; ?>
					</div>
                    <?for($i = 2; $i <= 5; $i++):?>
                        <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/01/'.$obj->id.'_'.$i.'.jpg')): ?>
		                      <a rel="example1" href="<?=IMG_CATALOG_PATH.'/original/'.$obj->id.'_'.$i.'.jpg'?>"><img itemprop="image" src="<?php echo IMG_CATALOG_PATH.'/01/'.$obj->id.'_'.$i.'.jpg'; ?>" alt="<?php echo $obj->name; ?>"></a>
                        <?endif?>
                    <?endfor?>
				</div><!-- .tovar_left_block -->
				<div class="tovar_right_block">
					<div class="price_buy">
						<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
							<p><span style="font-size: 20px;">Цена:</span> <span itemprop="price" id="c_<?php echo $obj->id; ?>">
							<?php if ($obj->valuta==2) $cost=$obj->cost*EVRO; elseif ($obj->valuta==3) $cost=$obj->cost*DOLLAR; else $cost=$obj->cost; ?>
								<?php if (!isset($_SESSION['skidka'][$obj->id])) echo mirsant::format_cost($cost); else echo  mirsant::format_cost($cost-($cost*$_SESSION['skidka'][$obj->id]/100));?>
								</span> грн
							</p>
							<meta itemprop="priceCurrency" content="UAH"/>
							<link itemprop="availability" href="http://schema.org/InStock"/>
						</span>
						<a href="#" class="add_tovar add_to_busket buy_btn_big" id="<?=$obj->id?>">Купить</a>
						<?php if ($obj->brand_skidka>0 or $obj->catalog_skidka>0 and (!isset($_SESSION['skidka'][$obj->id]))): ?><a href="#" class="get_skidka" id="<?=$obj->id?>">Получить скидку</a><?php endif; ?>
					    <div class="clear"></div>
                        <div class="tal">
                            <a href="#" class="buy_btn_click foundpure_style">Купить в один клик</a>
                        </div>
                    </div>
					<div class="description">
						<?php //echo $obj->text_short; ?>
					</div>
					<div class="characteristic">
						<div class="title">Характеристики</div>
						<?//=$obj->charstik?>
						<p><span>Артикул:</span><ins><span itemprop="productID"><?=$obj->artikul?></span></ins></p>
						<p><span>Бренд:</span><ins><span itemprop="brand" itemscope itemtype="http://schema.org/Organization"><span itemprop="name"><?=mirsant::get_brand_by_id($obj->brand)?></span></span></ins></p>
						<?if($obj->has_type != '' and $obj->has_type != 0 and $obj->has_type != NULL):?><p><span>Тип:</span><ins><?=mirsant::get_type_tovar($obj->has_type)?></ins></p><?endif?>
						<?if($obj->width != '' and $obj->width != 0 and $obj->width != NULL):?><p><span>Ширина:</span><ins><?=$obj->width?> см</ins></p><?endif?>
						<?if($obj->height != '' and $obj->height != 0 and $obj->height != NULL):?><p><span>Высота:</span><ins><?=$obj->height?> см</ins></p><?endif?>
						<?if($obj->length != '' and $obj->length != 0 and $obj->length != NULL):?><p><span>Длина:</span><ins><?=$obj->length?> см</ins></p><?endif?>
                        <?php if( $obj->tray_depth ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_tray_depth WHERE id = "'.$obj->tray_depth.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Глубина поддона:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->cabin_form ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_cabin_form WHERE id = "'.$obj->cabin_form.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Форма кабины:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->color ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_colors WHERE id = "'.$obj->color.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Цвет:</span><ins><?php echo $__res->color; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->mounting_type ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_mounting_type WHERE id = "'.$obj->mounting_type.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Тип монтажа:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->mixer_style ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_mixer_style WHERE id = "'.$obj->mixer_style.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Стиль смесителя:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if( $obj->mixer_type ): ?>
                            <?php $__res = mysql::query_one('SELECT name FROM dict_mixer_type WHERE id = "'.$obj->mixer_type.'"'); ?>
                            <?php if( $__res ): ?>
                                <p><span>Тип смесителя:</span><ins><?php echo $__res->name; ?></ins></p>
                            <?php endif; ?>
                        <?php endif; ?>

						<?php if( $obj->toilet_mount ): ?>
							<?php $__res = mysql::query_one('SELECT name FROM dict_toilet_mount WHERE id = "'.$obj->toilet_mount.'"'); ?>
							<?php if( $__res ): ?>
								<p><span>Тип монтажа:</span><ins><?php echo $__res->name; ?></ins></p>
							<?php endif; ?>
						<?php endif; ?>
						<?php if( $obj->bathtub_form ): ?>
							<?php $__res = mysql::query_one('SELECT name FROM dict_bathtub_form WHERE id = "'.$obj->bathtub_form.'"'); ?>
							<?php if( $__res ): ?>
								<p><span>Форма ванны:</span><ins><?php echo $__res->name; ?></ins></p>
							<?php endif; ?>
						<?php endif; ?>
					</div>
					<div class="soc_add">
						<script type="text/javascript" src="//yandex.st/share/share.js"charset="utf-8"></script>
                        <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,odnoklassniki,lj,gplus"></div> 
					</div>
				</div><!-- .tovar_right_block -->
				
				<div class="current_tovar_model">
					<div class="title">Описание</div>
					<div class="current_tovar_model_cont">
						<span class="osntext" itemprop="description"><?php echo $obj->text_full; ?></span>
					</div>
				</div><!-- .current_tovar_model -->

                <?php echo catalog::get_catalog_ass_new("/frontend/catalog/show_ass.php", 2, mirsant::get_id_goods_by_url()); ?>

	<script type="text/javascript">
    	$(document).ready(function(){
    	   $("a[rel='example1']").colorbox({width:false, height:false, maxWidth: "95%", maxHeight: "95%"  });		
    	});
	</script>

<div class="similar_items_wrap">
	<div class="similar_items_title">
		<span>Похожие товары</span>
	</div>
	<div class="similar_items_overflow">
		<?php foreach ($same as $value): ?>
			<div class="tovar_block">
				<div class="tovar_img">
					<a href="<?php echo general::link('catalog/goods/'.$value->url,$value->name); ?>">
			            <?if($value->block_new == 1):?>
			                <span class="new_stiker"></span>
			            <?endif?>
			            <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/02/'.$value->id.'_1.jpg')): ?>
						    <img src="<?php echo IMG_CATALOG_PATH.'/02/'.$value->id.'_1.jpg'; ?>" alt="<?php echo $value->name; ?>">
			            <?else:?>
			                <img src="/pic/no_photo2.jpg" alt="<?php echo $value->name; ?>" />
						<?php endif; ?>
			        </a>
				</div>
				<div class="tovar_info">
					<a class="name_t" href="<?php echo general::link('catalog/goods/'.$value->url,$value->name); ?>"><?php echo $value->name; ?></a>
					<p>Артикул: <span><?php echo $value->artikul; ?></span></p>
					<p>Бренд: <span><?=mirsant::get_brand_by_id($value->brand)?></span></p>
					<span class="price"><span style="font-size: 12px;">Цена:</span> <span id="c_<?php echo $value->id;?>">
					<?php if ($value->valuta==2) $cost=$value->cost*EVRO; elseif ($value->valuta==3) $cost=$value->cost*DOLLAR; else $cost=$value->cost; ?>
					<?php if (!isset($_SESSION['skidka'][$value->id])) echo mirsant::format_cost($cost); else echo  mirsant::format_cost($cost-($cost*$_SESSION['skidka'][$value->id]/100));?>
					</span> грн</span>
					<a href="#" class="add_tovar add_to_busket buy_btn" id="<?=$value->id?>">Купить</a>
					<?php if (($value->brand_skidka>0 or $value->catalog_skidka>0) and (!isset($_SESSION['skidka'][$value->id]))): ?><a href="#" class="get_skidka_min" id="<?=$value->id?>">Получить скидку</a><?php endif; ?>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>

<div style="display:none">
	<div id="inline_example_buy" class="fast_buy" style="margin: 0 auto; width: 450px; border: solid 0px red;">
		<p class="title" align="center">Купить в один клик</p>
		<form class="validat form_style" method="post">
			<div class="row">
				<label for="qbe">Ваш E-Mail</label>
				<span class="r">&nbsp;</span>
				<input id="quick_buy_email" type="text" name="email" class="valid" value="" />
			</div>
			<div class="row">
				<label for="tel">Ваш телефон</label>
				<span class="r">&nbsp;</span>
				<input id="quick_buy_phone" type="text" name="phone" class="valid" value="" />
			</div>
			<br />
			<div class="row input_but" style="margin-top:10px;">
				<input id="buy_one_click_button" onclick="" class="but" value="Подтвердить" type="submit" style="margin-left:163px;">
			</div>
			<input id="quick_buy_product" type="hidden" name="id_product" value="<?php echo $obj->id; ?>">
		</form>
	</div>
</div>

<?php echo $form; ?>
<?php echo $comments; ?>

<script>   
    $(document).ready(function(){
        $('.get_skidka').click(function(){
            var id = $(this).attr('id');
            jQuery.post('/ajax/get_skidka.php', {'id':id}, function(data) {
                if(data){
                    $('#c_'+id).html(data.new_cost);
                    $('.get_skidka').hide()
                }
            },'json');
            return false;
        });

        $('#buy_one_click_button').click(function(){
            var email = $("#quick_buy_email").val();
            var phone = $("#quick_buy_phone").val();
            var id_product = $("#quick_buy_product").val();
            jQuery.post('/ajax/buyoneclick.php', {
                'email': email,
                'phone': phone,
                'id_product': id_product
            }, function(data) {
                if(data){
                    var txt = data.msg;
                    $.colorbox({html:'<p align="center" style="padding: 25px 50px 0">'+txt+'</p>'});
                }
            },'json');
            return false;
        });

    });
</script>