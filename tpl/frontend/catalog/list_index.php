﻿<div class="tovar_block">

	<div class="tovar_img">
    
		
		<a href="<?php echo general::link('catalog/goods/'.$obj->url,$obj->name); ?>">
            <?if($obj->block_new == 1):?>
                <span class="new_stiker"></span>
            <?endif?>
            <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/02/'.$obj->id.'_1.jpg')): ?>
			    <img src="<?php echo IMG_CATALOG_PATH.'/02/'.$obj->id.'_1.jpg'; ?>" alt="<?php echo $obj->name; ?>">
            <?else:?>
                <img src="/pic/no_photo2.jpg" alt="<?php echo $obj->name; ?>" />
			<?php endif; ?>
        </a>
        
	</div>
    
	<div class="tovar_info">
		<a class="name_t" href="<?php echo general::link('catalog/goods/'.$obj->url,$obj->name); ?>"><?php echo $obj->name; ?></a>
		<p>Артикул: <span><?php echo $obj->artikul; ?></span></p>
		<p>Бренд: <span><?=mirsant::get_brand_by_id($obj->brand)?></span></p>
		
		<?php if ($time['night'] == 1 AND $time['time_from'] > 0 AND time() > $time['time_from'] AND $time['time_to'] > 0 AND time() < $time['time_to']):?>
			<span class="price">
				<span style="font-size: 12px;">Цена:</span> 
				<span id="c_<?php echo $obj->id;?>">
				<?php if ($obj->valuta==2) $cost=$obj->cost_night*EVRO; elseif ($obj->valuta==3) $cost=$obj->cost_night*DOLLAR; else $cost=$obj->cost_night; ?>
				<?php if (!isset($_SESSION['skidka'][$obj->id])) echo mirsant::format_cost($cost); else echo  mirsant::format_cost($cost-($cost*$_SESSION['skidka'][$obj->id]/100));?>
				</span> грн
			</span>
		<?php else:?>
			<span class="price"><span style="font-size: 12px;">Цена:</span> <span id="c_<?php echo $obj->id;?>">
			<?php if ($obj->valuta==2) $cost=$obj->cost*EVRO; elseif ($obj->valuta==3) $cost=$obj->cost*DOLLAR; else $cost=$obj->cost; ?>
			<?php if (!isset($_SESSION['skidka'][$obj->id])) echo mirsant::format_cost($cost); else echo  mirsant::format_cost($cost-($cost*$_SESSION['skidka'][$obj->id]/100));?>
			</span> грн</span>
		<?php endif;?>
		
		<a href="#" class="add_tovar add_to_busket buy_btn" " id="<?=$obj->id?>" onclick="yaCounter36911050.reachGoal('kupitrek');ga('send', 'event', 'button', 'kupitrek'); return true;">Купить</a>
		<?php if (($obj->brand_skidka>0 or $obj->catalog_skidka>0) and (!isset($_SESSION['skidka'][$obj->id]))): ?><a href="#" class="get_skidka_min" id="<?=$obj->id?>">Получить скидку</a><?php endif; ?>
	</div>
    
</div><!-- .tovar_content -->	