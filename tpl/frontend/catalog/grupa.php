<div class="cat_n" style="margin:0 auto; ">	
    <?php $arr = array(); ?>
    <?php // print_r(count($result)); ?>
    <?php foreach ($result as $obj): ?>

        <div class="tovar_img spis" style="margin:0 25px 50px 25px;">
            <a href="/catalog/grupa/<?php echo $obj->url; ?>">
                <?php if (file_exists(HOST . IMG_CATALOG_TREE_PATH . '/' . $obj->id . '.jpg')): ?>
                    <img src="<?php echo IMG_CATALOG_TREE_PATH . '/' . $obj->id . '.jpg'; ?>" alt="<?php echo $obj->pole; ?>">
                <? else: ?>
                    <img src="/pic/no_photo2.jpg" alt="<?php echo $obj->pole; ?>" />
                <?php endif; ?>
            </a>
            <p style="text-align:center;margin-top:5px;"><?php echo $obj->pole; ?></p>
        </div>
        <?php $arr[] = $obj->id; ?>
    <?php endforeach; ?>
    <div class="clear"></div>
    <?php echo $items_tpl;?>
    <div class="clear"></div>
    <?php if ($_GET['grupa'] == 'vanni'): ?>
        <div class="similar_items_title">
            <span>Рекомендуемы товары</span>
        </div>
        <?php
        $_sql = 'SELECT catalog.*, brand.pole as brand_name, brand.skidka as brand_skidka, catalog_tree.skidka as catalog_skidka,  catalog.cost*valuta.kurs as sort_cost
                    FROM catalog
                    LEFT JOIN brand ON catalog.brand=brand.id
                    LEFT JOIN catalog_tree ON catalog_tree.id=catalog.id_parent
                    LEFT JOIN valuta ON valuta.id=catalog.valuta
                    WHERE catalog.id_parent IN (' . implode(',', $arr) . ') and catalog.status=1 order by rand() LIMIT 10';

        $result = mysql::query($_sql);

        echo system::show_tpl(array('result' => $result), 'frontend/catalog/list.php');
        ?>
    <?php endif; ?>

    <div class="content"><?php echo getSeoText(); ?></div>
</div>	