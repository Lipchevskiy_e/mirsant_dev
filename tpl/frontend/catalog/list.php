<?php
if (isset($_GET['filter'])) {
    $filter = '/filter/' . $_GET['filter'];
    $arr = explode(';', $_GET['filter']);
    $k = 0;
    $k1 = 0;
    $k2 = 0;
    $k3 = 0;
    $k4 = 0;
    foreach ($arr as $a) {
        $_arr = explode('_', $a);
        if ($_arr[0] == 'cost') {
            $k = 1;
            $__arr = explode('-', $_arr[1]);
            $from = $__arr[0];
            $to = $__arr[1];
        }
        if ($_arr[0] == 'width') {
            $k1 = 1;
            $width = $_arr[1];
        }
        if ($_arr[0] == 'length') {
            $k2 = 1;
            $length = $_arr[1];
        }
        if ($_arr[0] == 'brands') {
            $k3 = 1;
            $brands = $_arr[1];
        }
        if ($_arr[0] == 'series') {
            $k4 = 1;
        }
    }
} else
    $filter = '';
?>
<div class="tac">
    <div class="page_listbot">
        <?php
            echo catalog::get_count() > CATALOG_AT_PAGE ? pager::pager_J("catalog/grupa/" . $_GET['grupa'] . str_replace(';', '/', $filter), CATALOG_AT_PAGE, catalog::get_count(), intval($_GET['page'])) : '';
        ?>
    </div>
</div>

<div class="tovar_content">
    <?php if(count($result)): ?>
		<?php $time = catalog::get_night_time();?>
        <?php foreach ($result as $obj): ?>
            <?php echo system::show_tpl(array('obj' => $obj, 'level' => '', 'time' => $time), '/frontend/catalog/list_index.php'); ?>
        <?php endforeach; ?>
    <?php else: ?>
        <?php if(isset($_GET['filter'])): ?>
            <p style="margin: 30px 0 0 15px;">К сожалению нет товаров, подходящим под данные параметры.</p>
        <?php else: ?>
            <p style="margin: 30px 0 0 15px;">К сожалению в данной группе нет товаров.</p>
        <?php endif; ?>
    <?php endif; ?>
    <div class="clear"></div>
</div>
<!-- .tovar_content-->	

<div class="tac">
    <div class="page_listbot">
        <?php 
            echo catalog::get_count() > CATALOG_AT_PAGE ? pager::pager_J("catalog/grupa/" . $_GET['grupa'] . $filter, CATALOG_AT_PAGE, catalog::get_count(), intval($_GET['page'])) : '';
        ?>
        
    </div>
</div>

<div class="content"><?php echo getSeoText(); ?></div>

<script>
    $(document).ready(function () {
        $('.get_skidka_min').click(function () {
            var id = $(this).attr('id');
            var link = $(this);

            jQuery.post('/ajax/get_skidka.php', {'id': id}, function (data) {
                if (data)
                {
                    $('#c_' + id).html(data.new_cost);
                    link.hide()
                }
            }, 'json');

            return false;
        })
    });
</script>