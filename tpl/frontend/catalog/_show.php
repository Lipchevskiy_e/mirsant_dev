                
                <div class="tovar_left_block">
					<div class="one_tovar_img">
						<?if($obj->block_new == 1):?>
                              <span class="new_stiker"></span>
                        <?endif?>
                        <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/03/'.$obj->id.'_1.jpg')): ?>
		                      <a rel="example1" href="<?=IMG_CATALOG_PATH.'/original/'.$obj->id.'_1.jpg'?>"><img itemprop="image" src="<?php echo IMG_CATALOG_PATH.'/03/'.$obj->id.'_1.jpg'; ?>" alt="<?php echo $obj->name; ?>"></a>
                        <?else:?>
                              <img itemprop="image" src="/pic/no_photo_big2.jpg" alt="<?php echo $obj->name; ?>" />
  				        <?php endif; ?>
					</div>
                    <?for($i = 2; $i <= 5; $i++):?>
                        <?php if(file_exists(HOST.IMG_CATALOG_PATH.'/01/'.$obj->id.'_'.$i.'.jpg')): ?>
		                      <a rel="example1" href="<?=IMG_CATALOG_PATH.'/original/'.$obj->id.'_'.$i.'.jpg'?>"><img itemprop="image" src="<?php echo IMG_CATALOG_PATH.'/01/'.$obj->id.'_'.$i.'.jpg'; ?>" alt="<?php echo $obj->name; ?>"></a>
                        <?endif?>
                    <?endfor?>
				</div><!-- .tovar_left_block -->
				<div class="tovar_right_block">
					<div class="price_buy">
						<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
							<p><span style="font-size: 20px;">Цена:</span> <span itemprop="price"><?=mirsant::format_cost($obj->cost)?></span> грн</p>
							<meta itemprop="priceCurrency" content="UAH"/>
							<link itemprop="availability" href="http://schema.org/InStock"/>
						</span>
						<a href="#" class="add_tovar add_to_busket buy_btn_big" id="<?=$obj->id?>">Купить</a>
					</div>
					<div class="description">
						<?php //echo $obj->text_short; ?>
					</div>
					<div class="characteristic">
						<div class="title">Характеристики</div>
						<?//=$obj->charstik?>
						<p><span>Артикул:</span><ins><span itemprop="productID"><?=$obj->artikul?></span></ins></p>
						<p><span>Бренд:</span><ins><span itemprop="brand" itemscope itemtype="http://schema.org/Organization"><span itemprop="name"><?=mirsant::get_brand_by_id($obj->brand)?></span></span></ins></p>
						<?if($obj->has_type != '' and $obj->has_type != 0 and $obj->has_type != NULL):?><p><span>Тип:</span><ins><?=mirsant::get_type_tovar($obj->has_type)?></ins></p><?endif?>
						<?if($obj->width != '' and $obj->width != 0 and $obj->width != NULL):?><p><span>Ширина:</span><ins><?=$obj->width?> см</ins></p><?endif?>
						<?if($obj->height != '' and $obj->height != 0 and $obj->height != NULL):?><p><span>Высота:</span><ins><?=$obj->height?> см</ins></p><?endif?>
						<?if($obj->length != '' and $obj->length != 0 and $obj->length != NULL):?><p><span>Длина:</span><ins><?=$obj->length?> см</ins></p><?endif?>
					</div>
					<div class="soc_add">
						<script type="text/javascript" src="//yandex.st/share/share.js"charset="utf-8"></script>
                        <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,odnoklassniki,lj,gplus"></div> 
					</div>
				</div><!-- .tovar_right_block -->
				
				<div class="current_tovar_model">
					<div class="title">Описание</div>
					<div class="current_tovar_model_cont">
						<span itemprop="description"><?php echo $obj->text_full; ?></span>
					</div>
				</div><!-- .current_tovar_model -->

                <?php echo catalog::get_catalog_ass_new("/frontend/catalog/show_ass.php", 2, mirsant::get_id_goods_by_url()); ?>

	<script type="text/javascript">
    	$(document).ready(function(){
    	   $("a[rel='example1']").colorbox();		
    	});
	</script>

<?php echo $form; ?>
<?php echo $comments; ?>