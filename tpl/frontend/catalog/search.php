<?php if (count($list) > 0): ?>
    <ul>
        <?php foreach ($list as $item): ?>
            <li>
                <a href="<?php echo '/catalog/goods/' . $item->url; ?>">
                    <span class="srchPic">
                        <?php if (file_exists(HOST . IMG_CATALOG_PATH . '/02/' . $item->id . '_1.jpg')): ?>
                            <img src="<?php echo IMG_CATALOG_PATH . '/02/' . $item->id . '_1.jpg'; ?>" alt="<?php echo $item->name; ?>">
                        <? else: ?>
                            <img src="/pic/no_photo2.jpg" alt="<?php echo $item->name; ?>" />
                        <?php endif; ?>
                    </span>
                    <span class="srchInfo">
                        <p><?php echo $item->name; ?></p>
                        <span class="srchPrice">
                            <?php if ($item->valuta == 2) $cost = $item->cost * EVRO;
                            elseif ($item->valuta == 3) $cost = $item->cost * DOLLAR;
                            else $cost = $item->cost; ?>
        <?php if (!isset($_SESSION['skidka'][$item->id])) echo mirsant::format_cost($cost);
        else echo mirsant::format_cost($cost - ($cost * $_SESSION['skidka'][$item->id] / 100)); ?> грн
                        </span>
                    </span>
                </a>
            </li>                        
    <?php endforeach; ?>
    </ul>
<?php else: ?>
    По данному запросу ничего не найдено...
<?php endif; ?>