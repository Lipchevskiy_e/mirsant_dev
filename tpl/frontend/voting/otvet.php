		<?php foreach ($otvet as $obj): ?>

			<div class="d-str">
				<strong class="d-label"><?= $obj->text; ?></strong>
				<div class="overflow">
					<div class="d-wrap">
						<div class="d-size" data-pr="<?= round($obj->summa); ?>">
							<div class="d-color"></div>
							<b class="d-pr"></b>
						</div>
					</div>
				</div>
			</div>

		<?php endforeach; ?>
