<form action="/voting" name="voting" method="post" class="validat form_style">

	<?php foreach ($vopros as $obj): ?>
	
		<div class="d-title "><?= $obj->text; ?></div>
		<div class="d-wrap-size">
	
			<?= voting::get_otvet('/frontend/voting/otvet_first_page.php',$obj->id); ?>

			<div class="row sdvig_btn">
				<input type="submit" value="Проголосовать" class="but">
			</div>				

			<div class="d-num d-num_right"><a href="/voting/arhiv">Архив опросов</a></div>
			
		</div>	
		
	<?php break; ?>
										
	<?php endforeach; ?>

</form>