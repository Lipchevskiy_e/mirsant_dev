<script type="text/javascript">
$(document).ready(function(){
	
	var prMax = 0;
	$('.d-wrap-size').each(function(){
		var dWrapSize = $(this);
		$('.d-str',dWrapSize).each(function(){
			var str = $(this);
			var pr = $('.d-size',str).data('pr');
			if(prMax<pr){
				prMax = pr;
			}
		})
		$('.d-str',dWrapSize).each(function(){
			var str = $(this);
			var pr = $('.d-size',str).data('pr');
			var prW = pr*100/prMax;
			$('.d-size',str).width(prW+'%')
			$('.d-color',str).css({opacity:prW/100});
			$('.d-pr',str).html(pr+'%');
		})
	})
	

});

</script>

	<?php foreach ($vopros as $obj): ?>
	
		<div class="d-title "><?= $obj->text; ?></div>
		<div class="d-wrap-size">

			<?= voting::get_otvet('/frontend/voting/otvet.php',$obj->id); ?>
			
			<div class="d-num">В данному опитуванні прийняло участь: <?= voting::get_kolvo($obj->id); ?> чоловік</div>
			<?php if (!isset($_GET['arhiv'])): ?>
				<div class="d-num"><a href="/voting/arhiv">Здесь Вы можете просмотреть архив опросов</a></div>
			<?php endif ?>
			
		</div>	
										
	<?php endforeach; ?>
