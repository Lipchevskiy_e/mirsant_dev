<h2><?php echo __('form_title_contact') ?>:</h2>

<form action="/contact" name="contact" method="post" class="validat form_style">
	<div class="row">
		<label>ФИО</label>
		<span class="r">&nbsp;</span>
		<input type="text" name="FORM[name]" class="valid" value="<?php echo $name ?>" id="name" maxlength="36" style="width:340px;" />
	</div>
	<div class="row">
		<label><?php echo __('email') ?></label>
		<span class="r">&nbsp;</span>
		<input type="text" name="FORM[email]"  class="valid email" value="<?php echo $email ?>" id="email" style="width:340px;" />
	</div>
	<div class="row">
		<label><?php echo __('phone') ?> </label>
		<span class="r">&nbsp;</span>
		<input type="text" name="FORM[phone]" class="" value="<?php echo $phone ?>" id="phone" style="width:340px;" />
	</div>
	<div class="row">
		<label><?php echo __('country') ?></label>
		<span class="r">&nbsp;</span>
		<input type="text" name="FORM[country]" class="" value="<?php echo $country ?>" maxlength="36" id="country" style="width:340px;" />
	</div>
	<div class="row">
		<label><?php echo __('message') ?> </label>
		<span class="r">&nbsp;</span>
		<textarea name="FORM[message]" class="valid" id="message" style="width: 340px; height: 100px;"><?php echo $message ?></textarea>
	</div>
	<div class="row">
		<label><?php echo __('captcha_input_number') ?></label>
		<span class="r">&nbsp;</span>
		<?php echo captcha::get_captcha(); ?>
	</div>
	<br />
	<div class="row sdvig">
		<input type="submit" value="<?php echo __('send') ?>" class="but">
	</div>
	<input type="hidden" name="send" value="yes">
</form>
