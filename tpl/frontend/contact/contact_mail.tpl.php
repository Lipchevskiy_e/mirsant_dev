<strong>Добрый день</strong><br />
Администрация сайта благодарит Вас за проявленный интерес.<br />
Ниже приведен текст Вашего сообщения:<br />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?php echo __('your_name') ?></div></td>
		<td><?php echo $name ?></td>
	</tr>
	<tr>
		<td><?php echo __('email') ?></td>
		<td><?php echo $email ?></td>
	</tr>
	<tr>
		<td><?php echo __('phone') ?></td>
		<td><?php echo $phone ?></td>
	</tr>
	<tr>
		<td><?php echo __('country') ?></td>
		<td><?php echo $country ?></td>
	</tr>
	<tr>
		<td><?php echo __('message') ?></td>
		<td><?php echo $message ?></td>
	</tr>
</table>

<br><br>

<strong>Ответ будет получен Вами в ближайшее время!</strong>
