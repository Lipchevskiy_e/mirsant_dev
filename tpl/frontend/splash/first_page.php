<?if(count($splash)):?>
    <div class="list_carousel">
        <ul id="slider">
            <?foreach($splash as $obj):?>
                <?if(file_exists(HOST.IMG_SPLASH_PATH.'/'.$obj->id.'.jpg')):?>
                    <li>
                        <?if($obj->url != ''):?>
                            <a href="<?=$obj->url?>" class="slid_txt">
                                <p class="fgh"><?=$obj->name?></p>
                                <p>перейти в раздел</p>
                                <span class="slid_txt_arrow"></span>
                            </a>
                        <?else:?>
                            <span class="slid_txt">
                                <p class="fgh"><?=$obj->name?></p>
                            </span>
                        <?endif?>
                        
                        <img src="<?=IMG_SPLASH_PATH.'/'.$obj->id.'.jpg'?>" />
                    </li>
                <?endif?>
            <?endforeach?>
        </ul>
        
        
        <div class="clearfix"></div>
        <div id="pager" class="pager"></div>
    </div>
<?endif?>