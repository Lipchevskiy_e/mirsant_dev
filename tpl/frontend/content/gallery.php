<!--
<link type="text/css" rel="stylesheet" href="/css/colorbox.css" />
<script type="text/javascript" src="/js/jquery.colorbox-min.js"></script>
-->

<div class="thumbnails">

	<?php foreach ($gallery_grupa as $gallery_grupa): ?>

	
		<div align="center"><?php echo $gallery_grupa->pole; ?></div>

		<?php 
			// получаем потомков галереи
			$gallery=dbh::get_gallery($gallery_grupa->id_gallerygrupa);
		?>
		
			<?php foreach ($gallery as $gallery): ?>
				
					<ins class="thumbnail">
                			<div class="r">
				
					<?php if(file_exists(HOST.IMG_GALLERY_PATH.'/'.$gallery->id.'.jpg')): ?>
						<a href="<?php echo IMG_GALLERY_PATH; ?>/<?php echo $gallery->id; ?>.jpg" rel="example1"><img src="<?php echo IMG_GALLERY_PATH; ?>/_<?php echo $gallery->id; ?>.jpg"></a>
					<?php endif; ?>
					
					</div><!-- .r-->
				</ins><!-- .thumbnail-->
				
			<?php endforeach; ?>
			
			<div clas="clear"></div>
			
	<?php endforeach; ?>
	
</div>

<script type="text/javascript">
$(document).ready(function(){
	<!-- colorbox -->
  $("a[rel='example1']").colorbox();
});
</script>	