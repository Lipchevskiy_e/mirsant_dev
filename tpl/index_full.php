﻿<!DOCTYPE html>
<html>
    <!-- (c) Wezom web-студия | www.wezom.com.ua -->
    <head>
        <?php
        if ($_SERVER['REQUEST_URI'] == '/catalog/grupa/akrilovie_vanni/filter/brands_32') {
            $statik['TITLE'] = 'Акриловые ванны Kolpa San – купить акриловую ванну Kolpa San по доступной цене в Киеве | «Мир Сантехники»';
            $statik['DESCRIPTION'] = 'Акриловые ванны Kolpa San – интернет-магазин сантехники «СантехМир» ✔Большой выбор акриловых ванн Kolpa San по доступной цене в Киеве – mirsant.com.ua ✔ Доставка по Киеву и Украине ✔ ☎ +38 (095) 101-77-26';
            $statik['H1'] = 'Акриловые ванны Kolpa San ';
        } else {
            $statik['H1'] = get_h1();
        }

        $seo = mysql::query_one('SELECT `h1`, `title`, `description`, `keywords` FROM `seo_links` WHERE `status` = 1 AND `link` = "' . $_SERVER['REQUEST_URI'] . '"', 0);
        if ($seo) {
            if (!empty($seo->title)) {
                $statik['TITLE'] = $seo->title;
            }
            if (!empty($seo->description)) {
                $statik['DESCRIPTION'] = $seo->description;
            }
            if (!empty($seo->h1)) {
                $statik['H1'] = $seo->h1;
            }
            if (!empty($seo->keywords)) {
                $statik['KEYWORDS'] = $seo->keywords;
            }
        } else {
            $seo = SEO::factory()->_seo;
            if ($seo) {
                $statik['TITLE'] = $seo['title'] ? $seo['title'] : NULL;
                $statik['DESCRIPTION'] = $seo['description'] ? $seo['description'] : NULL;
                $statik['KEYWORDS'] = $seo['keywords'] ? $seo['keywords'] : NULL;
                $statik['H1'] = $seo['h1'] ? $seo['h1'] : NULL;
            }
        }
        
        $filter_color = mysql::query_one('SELECT `h1`, `title`, `description`, `keywords` FROM `seo_templates` WHERE `id` = 5', 0);

        if (strripos($_SERVER['REQUEST_URI'], 'color') !== false) {
            $groups = explode('/grupa/', $_SERVER['REQUEST_URI']);
            $search = explode('/', $groups[1]);
            $grupa = mysql::query_one('SELECT * FROM `catalog_tree` WHERE `url` LIKE "' . $search[0] . '" ', 0);

            $data = explode('color_', $_GET['filter']);
            if (strripos($data[1], ';') !== false) {
                $colors = explode(',', strstr($data[1], ';', true));
            } else {
                $colors = explode(',', $data[1]);
            }

            print_r($_GET['filter']);
            
            foreach ($colors as $col) {
                switch ($col) {
                    case '1':
                        $color = 'Хром';
                        break;
                    case '2':
                        $color = 'Белый';
                        break;
                    case '3':
                        $color = 'Черный';
                        break;
                    case '4':
                        $color = 'Бронза';
                        break;
                    case '5':
                        $color = 'Золото';
                        break;
                    case '6':
                        $color = 'Матовый';
                        break;
                    case '7':
                        $color = 'Бел/Хром';
                        break;
                    case '8':
                        $color = 'Хром/Золото';
                        break;
                    case '9':
                        $color = 'Медь';
                        break;
                    case '10':
                        $color = 'Латунь';
                        break;
                    default :
                        break;
                }
            }

            $from = array('{{group}}', '{{color}}', '{{filter}}', '{{phone}}');
            $to = array($grupa->pole, $color, $color, $statik['tel1']);

            $statik['TITLE'] = str_replace($from, $to, $filter_color->title);
            $statik['DESCRIPTION'] = str_replace($from, $to, $filter_color->description);
            $statik['H1'] = str_replace($from, $to, $filter_color->h1);
            $statik['KEYWORDS'] = str_replace($from, $to, $filter_color->keywords);
        }

        $special_groups = array(
            'akrilovie_vanni',
            'iskusstvennyj_mramor',
            'stalnie_vanni',
            'chugunnie_vanni',
            'dushevie_boksi',
            'dushevye_kabiny',
            'mebel_dlya_vannoi',
            'unitazi',
            'plitka',
            'smesiteli',
        );
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><?= $statik['TITLE'] ?></title>
        <meta name="description" content="<?= $statik['DESCRIPTION'] ?>" />
        <meta name="keywords" content="<?= $statik['KEYWORDS'] ?>" />
        <meta name="google-site-verification" content="pHW8zKRTWpQTwbqODU0swQHDNw2Xzaz1D8Y8WoLSmhE" />
        <base href="<?= MAIN_PATH ?>">
        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="target-densitydpi=device-dpi">
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <?php
        if ($_GET['grupa']) {
            if ((strpos($_GET['filter'], ','))) { ?>
                <link rel="cannonical" href="http://mirsant.com.ua/catalog/grupa/<?php echo $_GET['grupa']; ?>">
            <?php }
        } elseif ($_SERVER['REQUEST_URI'] == '/catalog/goods/modul_s_rakovinoj_valente_taljari_t3_91_134_sm_new') { ?>
            <link rel="cannonical" href="/catalog/goods/modul_s_rakovinoj_valente_taljari_t3_91_134_sm">
        <?php } elseif ($_SERVER['REQUEST_URI'] == '/catalog/goods/akrilovaja_gidromassazhnaja_vanna_ravak_rosa_ii_160105_lr') { ?>
            <link rel="cannonical" href="/catalog/goods/akrilovaja_gidromassazhnaja_vanna_ravak_rosa_ii_160_105_lr">
        <?php } ?>
        <?php $tmp = explode('/', $_SERVER['REQUEST_URI']); ?>
        <?php if ((in_array('sort_max-min', $tmp)) OR (in_array('sort_min-max', $tmp))): ?>
            <meta name="robots" content="noindex,nofollow">
        <?php endif; ?>
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/colorbox.css">
        <link rel="stylesheet" href="/css/liValidForm.css">
        <link rel="stylesheet" href="/css/mmenu.css">
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <script src="/js/jquery-1.8.0.min.js"></script>
        <script src="/js/copyright.min.js"></script>
        <script src="/js/liValidForm.v6.js"></script>
        <script src="/js/cufon-yui.js"></script>
        <script src="/js/jquery.colorbox-min.js"></script>
        <script src="/js/jquery.magnific-popup.min.js"></script>
        <script src="/js/Franklin_Gothic_Heavy_400.font.js"></script>
        <script src="/js/jquery.carouFredSel-6.1.0-packed.js"></script>
        <script src="/js/jquery.liHarmonica.js"></script>
        <script src="/js/mmenu.min.js"></script>
        <script src="/js/main.js"></script>
        <script src="/js/plugins.js"></script>
        <script src="/js/swfobject.js"></script>
        <link rel="shortcut icon" href="favicon.ico">
        <!--[if IE 9]><style type="text/css">.top_menu a {padding: 10px 13px 11px 14px;}</style><![endif]-->
        <!--[if IE 6]><script src="/js/DD_belatedPNG_0.0.8a-min.js"></script><script>DD_belatedPNG.fix('.png, img');</script><![endif]-->
        <!--[if IE]><script>document.createElement('header');document.createElement('nav');document.createElement('section');document.createElement('article');document.createElement('aside');document.createElement('footer');</script><![endif]-->
        <?= $statik['HEAD'] ?>
        <!-- <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-53134412-1', 'auto');
          ga('send', 'pageview');
        
        </script> -->

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-43877801-1', 'mirsant.com.ua');
            ga('send', 'pageview');

        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('body').copyright({
                    extratxt: '&copy; %source%',
                    sourcetxt: 'Источник: mirsant.com.ua',
                    length: 50,
                    hide: true
                });
            });
        </script>

<?php if ($_GET['action'] == 'basket' and isset($_GET['finish'])) echo $ecommerce; ?>

    </head>
    <body>
		<!-- RedHelper --> <script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" src="https://web.redhelper.ru/service/main.js?c=santmir"> </script> <!--/Redhelper -->
        <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) {
                (w[c] = w[c] || []).push(function () {
                    try {
                        w.yaCounter36911050 = new Ya.Metrika({id: 36911050, clickmap: true, trackLinks: true, accurateTrackBounce: true, webvisor: true, trackHash: true});
                    } catch (e) {
                    }
                });
                var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
                    n.parentNode.insertBefore(s, n);
                };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";
                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(document, window, "yandex_metrika_callbacks");</script> <noscript><div><img src="https://mc.yandex.ru/watch/36911050" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
        <div class="section site_size">
            <!--
            <script type="text/javascript"> _shcp = []; _shcp.push({widget_id : 572872, widget : "Chat", side : "right", position : "center", template : "blue" }); (function() { var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true; hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://widget.siteheart.com/apps/js/sh.js"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(hcc, s.nextSibling); })(); </script>
            -->
            <header>
                <a href="/" class="logo"><img src="/pic/logo.png" alt="Интернет магазин – «Мир Сантехники»" title="Интернет магазин – «Мир Сантехники»" /></a>
                <div class="menu_search">
                    <ul class="top_menu">
                        <noindex><li><a <?php echo general::active_menu('index'); ?> href="/" rel="nofollow">Главная</a></li></noindex>
                        <li><a rel="nofollow" <?php echo general::active_menu('o_kompanii'); ?> href="/o_kompanii">О компании</a></li>
                        <li><a rel="nofollow" <?php echo general::active_menu('contact'); ?> href="/contact">Контакты</a></li>
                        <li><a rel="nofollow" <?php echo general::active_menu('oplatadostavka'); ?> href="/oplatadostavka">Оплата/Доставка</a></li>
                        <li><a rel="nofollow" <?php echo general::active_menu('news'); ?> href="/news">Новости</a></li>
                        <li><a rel="nofollow" <?php echo general::active_menu('video'); ?> href="/video">Видео информация</a></li>
                    </ul>
                    <div class="search_block">
                        <div class="phone" itemscope itemtype="schema.org/PostalAddress">
                            <div>
                                <p><span itemprop="telephone"><?= $statik['tel1'] ?></span></p>
                                <p><span itemprop="telephone"><?= $statik['tel2'] ?></span></p>
                            </div>
                            <a href="#" class="call_order but_reg1" onclick="yaCounter36911050.reachGoal('zakazvonka');
                ga('send', 'event', 'button', 'zakazvonka'); return true;">Заказать звонок</a>
                            <a href="#inline_example3" class="foundpure">Нашли дешевле?</a>
<?php echo system::show_tpl(array(), '/frontend/catalog/ask_q2.php'); ?>
                        </div>
                        <div class="search">
                            <form action="/search" method="get">
                                <input type="text" class="search_input" name="word" placeholder="Поиск товаров">
                                <input type="submit" class="search_but">
                            </form>
                            <div class="searchList" style="display: none;" data-s="0">

                            </div>
                        </div>
                    </div><!-- .search_block -->
                    <div class="login_block">
                        <a href="/basket" class="basket">
                            <div>Ваша корзина</div>
                            <p>Товаров: <span id='basket_count'><?php echo basket::get_Count_Goods(); ?></span></p>
                        </a>
                        <div class="login">

                            <?if(!isset($_SESSION['user_id'])): ?>
                            <form method="post" action="/registration/formregistration/enter">
                                <div>
                                    <p class="enter"><span>Вход</span></p>
                                    <span class="enter_block">
                                        <span>Логин</span>
                                        <input type="text" name="FORM[login]" />
                                        <span>Пароль</span>
                                        <input type="password" name="FORM[pasword]" />
                                        <a class="remember_pass" href="/registration/formregistration/fpasword">Забыли пароль?</a>
                                        <input type="submit" class="submit" value="Вход" />
                                    </span>
                                </div>
                                <noindex><a href="/registration/formregistration/new" class="reg" rel="nofollow">Регистрация</a></noindex>
                            </form>
                            <?else:?>
                            <div>
                                <p class="enter"><span>Личный кабинет</span></p>
                                <span class="enter_block">
                                    <a href="/registration/formregistration/edit" class="submit_href" style="margin-bottom:4px;text-decoration:none;text-align:center;color:white;">Мои данные</a>
                                    <a href="/mycabinet/ctrl/myorders" class="submit_href" style="margin-bottom:4px;text-decoration:none;text-align:center;color:white;">Мои заказы</a>
                                    <a href="/registration/formregistration/exit" class="submit_href" onclick="return confirm('Вы уверены?')" style="text-decoration:none;text-align:center;color:white;">Выход</a>
                                </span>
                            </div>	
                            <?endif?>

                        </div><!-- .login -->
                    </div><!-- .login_block -->
                    <div class="clear"></div>
                </div><!-- .menu_search -->
                <div class="clear"></div>
                <a href="#mobMenu" class="mobMenuLink"></a>
                <!--<img class="mob-menu" src="images/show-menu-icon.png">-->
            </header>
            <?if($_GET['action'] == 'index'):?>
            <div class="slider_block">
                <div class="left_bs">
                    <div id="globus"></div>
                    <a href="<?= PIC_1 ?>" class="left_bsM1">
                            <span>Ванны<ins></ins></span>
                        </a>
                        <a href="<?= PIC_2 ?>" class="left_bsM2">
                            <span>Мебель<ins></ins></span>
                        </a>
                        <a href="<?= PIC_3 ?>" class="left_bsM3">
                            <span>Боксы<ins></ins></span>
                        </a>
                        <a href="<?= PIC_4 ?>" class="left_bsM4">
                            <span>Умывальники<ins></ins></span>
                        </a>
                        <a href="<?= PIC_5 ?>" class="left_bsM5">
                            <span>Унитазы<ins></ins></span>
                        </a>
                        <a href="<?= PIC_6 ?>" class="left_bsM6">
                            <span>Смесители<ins></ins></span>
                        </a>
                    </div>

                    <script type="text/javascript">
                        var so = new SWFObject("/pic/globus.swf", "mymovie", "460", "430", "7", "transparent");
                        so.addParam("wmode", "transparent");
                        so.write("globus");
                    </script>

    <?= splash::get_slider(10, "/frontend/splash/first_page.php"); ?>

                    <div class="clear"></div>
                </div><!-- .slider_block -->
                <?endif?>
                <div id="conteiner">
                    <div class="left_block">
                        <?php
                    if ($_GET['grupa'] && $_GET['grupa'] != 'vanni') {
                        if (in_array($_GET['grupa'], $special_groups)) {
                            echo catalog::get_filter_block("/frontend/filter/filter_new.php");
                        } else {
                            ?>
                            <div class="fltr news_after">
                                <div class="l_title js-filter-title">
                                    <p>Фильтр</p>
                                </div>
                                <div class="fltr_con js-filter-block">
                            <?php include 'tpl/frontend/filter/filter.php'; ?>
                                </div>
                            </div>
                        <?php
                        }
                    }
                    ?>
                        <?php if(!$_GET['goods']):?>
                            <div class="l_title mb15 n_tit">
                                <p>Новости</p>
                            </div>
                            <?php echo news::show_news($statik['news_at_first'], "/frontend/news/first_page.php"); ?>
                        <?php endif; ?>
                </div><!-- .left_block -->
                <div class="<?php echo $_GET['goods']?'right_block_new':'right_block'?>">
                    <div class="l_title js_catalog_open_btn">
                        <p>Каталог продукции</p>
                    </div>
                    <ul class="acc js_catalog_open_list" <?php if($_SERVER['REQUEST_URI'] != '/'):?> style="display: none;" <?php endif; ?>>
                        <?php echo catalog::get_catalog_menu("/frontend/catalog/menu_ind.php"); ?>
                    </ul>
                    <?if($_GET['action'] == 'index'):?>
                    <div class="r_title mb15">
                        <p>Лучшие товары</p>
                    </div>
                    <div class="tovar_content">

    <?php echo catalog::get_goods_for_special_block("/frontend/catalog/special_block2.php", $statik['lider_at_first'], 'block_lider=1'); ?>

                            <div class="clear"></div>
                        </div><!-- .tovar_content -->
                        <?else:?>
                        <div class="navigator" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
<?= get_navigator() ?>
                        </div>
                            <?php if (strpos(SEO_S::$uri, '/goods/')) : ?>
                            <div class="product-block" itemscope="" itemtype="http://schema.org/Product">
                                <h1 itemprop="name"><?= $statik['H1'] ?></h1>
                            <?php else: ?>
                                <h1><?= $statik['H1'] ?></h1>
                            <?php endif; ?>

                            <?php
                            if ($_GET['grupa'] && $_GET['grupa'] != 'vanni') {
                                if (in_array($_GET['grupa'], $special_groups)) {
                                    include 'tpl/frontend/filter/filter_chosen_new.php';
                                } else {
                                    include 'tpl/frontend/filter/filter_chosen.php';
                                }
                            }
                            ?>
                        
                        <?= $str ?>
                 
    <?php if (strpos(SEO_S::$uri, '/goods/')) : ?>
                            </div>
    <?php endif; ?>
                        <?endif?>    

                    </div><!-- .right_block -->
                    <?if($_GET['action'] == 'index'):?>
                    <div class="clear"></div>
                    <div class="content">
                        <h1><?= $statik['H1'] ?></h1>
    <?= $str ?>
                    </div> <!-- .content -->
                    <?endif?>
                    <div class="clearfix"></div>
                </div><!-- #conteiner-->
            </div>
            <div id="footer">
                <div class="site_size">
                    <div class="footer_left">
                        <div class="footer_left_ind">
                            <p><?= $statik['copy'] ?></p>
                    </div>
                    <div class="soc_ico">
                      <a href="https://www.facebook.com/mirsantcomua/" target="_blank">
                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                               xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                               width="24px" height="24px" viewBox="0 0 512 512" enable-background="new 0 0 512 512"
                               xml:space="preserve">
                              <g>
                                  <path d="M223.22,71.227c16.066-15.298,38.918-20.465,60.475-21.109c22.799-0.205,45.589-0.081,68.388-0.072
		c0.09,24.051,0.098,48.111-0.009,72.161c-14.734-0.026-29.478,0.036-44.212-0.026c-9.343-0.582-18.937,6.5-20.635,15.762
		c-0.224,16.093-0.081,32.195-0.072,48.289c21.61,0.089,43.22-0.027,64.829,0.054c-1.582,23.281-4.47,46.456-7.858,69.541
		c-19.088,0.179-38.187-0.018-57.274,0.099c-0.17,68.665,0.089,137.33-0.134,205.995c-28.352,0.116-56.721-0.054-85.072,0.08
		c-0.537-68.674,0.044-137.383-0.295-206.066c-13.832-0.144-27.672,0.099-41.503-0.116c0.053-23.085,0.018-46.169,0.026-69.246
		c13.822-0.169,27.654,0.036,41.477-0.098c0.42-22.442-0.421-44.91,0.438-67.333C203.175,101.384,209.943,83.493,223.22,71.227z"/>
                              </g>
                        </svg>
                      </a>
                      <a href="https://ok.ru/group/54458648428783" target="_blank">
                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                               version="1.1" x="0px" y="0px" width="21px" height="21px"
                               viewBox="0 0 95.481 95.481" style="enable-background:new 0 0 95.481 95.481;"
                               xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M43.041,67.254c-7.402-0.772-14.076-2.595-19.79-7.064c-0.709-0.556-1.441-1.092-2.088-1.713    c-2.501-2.402-2.753-5.153-0.774-7.988c1.693-2.426,4.535-3.075,7.489-1.682c0.572,0.27,1.117,0.607,1.639,0.969    c10.649,7.317,25.278,7.519,35.967,0.329c1.059-0.812,2.191-1.474,3.503-1.812c2.551-0.655,4.93,0.282,6.299,2.514    c1.564,2.549,1.544,5.037-0.383,7.016c-2.956,3.034-6.511,5.229-10.461,6.761c-3.735,1.448-7.826,2.177-11.875,2.661    c0.611,0.665,0.899,0.992,1.281,1.376c5.498,5.524,11.02,11.025,16.5,16.566c1.867,1.888,2.257,4.229,1.229,6.425    c-1.124,2.4-3.64,3.979-6.107,3.81c-1.563-0.108-2.782-0.886-3.865-1.977c-4.149-4.175-8.376-8.273-12.441-12.527    c-1.183-1.237-1.752-1.003-2.796,0.071c-4.174,4.297-8.416,8.528-12.683,12.735c-1.916,1.889-4.196,2.229-6.418,1.15    c-2.362-1.145-3.865-3.556-3.749-5.979c0.08-1.639,0.886-2.891,2.011-4.014c5.441-5.433,10.867-10.88,16.295-16.322    C42.183,68.197,42.518,67.813,43.041,67.254z"/>
                                    <path d="M47.55,48.329c-13.205-0.045-24.033-10.992-23.956-24.218C23.67,10.739,34.505-0.037,47.84,0    c13.362,0.036,24.087,10.967,24.02,24.478C71.792,37.677,60.889,48.375,47.55,48.329z M59.551,24.143    c-0.023-6.567-5.253-11.795-11.807-11.801c-6.609-0.007-11.886,5.316-11.835,11.943c0.049,6.542,5.324,11.733,11.896,11.709    C54.357,35.971,59.573,30.709,59.551,24.143z"/>
                                </g>
                            </g>
                            </svg>
                      </a>
                      <a href="https://www.instagram.com/mirsant_com_ua/" target="_blank">
                          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                               width="22px" height="22px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M459,0H51C22.95,0,0,22.95,0,51v408c0,28.05,22.95,51,51,51h408c28.05,0,51-22.95,51-51V51C510,22.95,487.05,0,459,0z
                                         M255,153c56.1,0,102,45.9,102,102c0,56.1-45.9,102-102,102c-56.1,0-102-45.9-102-102C153,198.9,198.9,153,255,153z M63.75,459
                                        C56.1,459,51,453.9,51,446.25V229.5h53.55C102,237.15,102,247.35,102,255c0,84.15,68.85,153,153,153c84.15,0,153-68.85,153-153
                                        c0-7.65,0-17.85-2.55-25.5H459v216.75c0,7.65-5.1,12.75-12.75,12.75H63.75z M459,114.75c0,7.65-5.1,12.75-12.75,12.75h-51
                                        c-7.65,0-12.75-5.1-12.75-12.75v-51c0-7.65,5.1-12.75,12.75-12.75h51C453.9,51,459,56.1,459,63.75V114.75z"/>
                                </g>
                            </g>
                            </svg>
                      </a>
                    </div>
                </div><!-- .footer_left -->
                <noindex>
                    <div class="footer_right">
                        <img src="/pic/wezom_logo.png" alt="Продвижение сайтов">
                        <a title="Продвижение сайтов" href="http://wezom.com.ua" target="_blank" rel="nofollow" style="margin-left: -5px;">Продвижение сайтов<br></a>
                        <span style="margin-left: -10px;">web-студия Wezom</span></span>
                    </div><!-- .footer_right -->
                    <div align="center" class="footer_center_ind">
<?php /* <script src="http://www.100bannerov.com/banner.php?id_d=1258&type_banner=0"></script> */ ?>
                    </div><!-- .footer_center_ind -->
                </noindex>
            </div> <!-- .site_size -->
        </div>

        <!-- сообщения -->
<?php echo general::global_massage(); ?>
        <!-- сообщения -->

        <script>
            $(document).ready(function () {
                $(".but_reg1").colorbox({width: "500px", height: "280px", href: "#inline_example2", inline: true, maxWidth: "95%", maxHeight: "95%"});
                // $(".foundpure").colorbox({width:"500px", height:"300px", href:"#inline_example3", inline:true, maxWidth: "95%", maxHeight: "95%"  });
                // $(".buy_btn_click").colorbox({width:"500px", height:"230px", href:"#inline_example_buy", inline:true, maxWidth: "95%", maxHeight: "95%"  });
                $(window).resize(function () {
                    $.colorbox.close();
                    $.colorbox.resize();
                });

                $(function () {
                    $('.foundpure').magnificPopup({
                        type: 'inline',
                        preloader: false
                    });
                });

                $('.search_input').keyup(function (e) {

                    if (e.keyCode == 27) {
                        $(this).val('');
                        $('.searchList').empty();
                        $('.searchList').hide();
                        return false;
                    }
                    var word = $(this).val();

                    if (word.length < 3) {
                        $('.searchList').empty();
                        return false;
                    }

                    $.ajax({
                        url: '/ajax/search.php',
                        type: 'POST',
                        data: {word: word},
                        success: function (data) {
                            $('.searchList').empty().html(data);
                            $('.searchList').show();
                        }
                    });
                });

                $('.searchList').mouseenter(function () {
                    $(this).attr('data-s', 1);
                });

                $('.searchList').mouseleave(function () {
                    $(this).attr('data-s', 0);
                });

                $('body').click(function () {

                    if ($('.searchList').attr('data-s') != 1 && $('.search_input').is(':focus') == false) {
                        $('.searchList').hide();
                    }

                })
                $('.search_input').focusin(function () {
                    $(this).attr('data-s', 1);

                    if ($(this).val() != '') {
                        $('.searchList').show();
                    }
                });

                $(document).keyup(function (e) {
                    if (e.keyCode == 27) {
                        $('.searchList').hide();
                    }
                });
            });
        </script>

<!--<script type="text/javascript" src="/ss/def.cont.js"></script>-->
<?php if ($_GET['action'] == 'basket'): ?>
            <!-- Google Code for &#1044;&#1086;&#1073;&#1072;&#1074;&#1083;&#1077;&#1085;&#1080;&#1077; &#1074; &#1082;&#1086;&#1088;&#1079;&#1080;&#1085;&#1091; Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 965672959;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "x5MhCMmJ1AoQ__-7zAM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/965672959/?label=x5MhCMmJ1AoQ__-7zAM&amp;guid=ON&amp;script=0"/>
            </div>
            </noscript>
<?php endif; ?>
<nav id="mobMenu" class="mob-menu">
    <ul>

        <li><a <?php echo general::active_menu('index'); ?> href="/" rel="nofollow">Главная</a></li>
        <li>
            <span>Каталог продукции</span>
            <ul>
                <?php echo catalog::get_catalog_menu("/frontend/catalog/menu_ind.php"); ?>
            </ul>

        </li>
        <li><a rel="nofollow" <?php echo general::active_menu('o_kompanii'); ?> href="/o_kompanii">О компании</a></li>
        <li><a rel="nofollow" <?php echo general::active_menu('contact'); ?> href="/contact">Контакты</a></li>
        <li><a rel="nofollow" <?php echo general::active_menu('oplatadostavka'); ?> href="/oplatadostavka">Оплата/Доставка</a></li>
        <li><a rel="nofollow" <?php echo general::active_menu('news'); ?> href="/news">Новости</a></li>
        <li><a rel="nofollow" <?php echo general::active_menu('video'); ?> href="/video">Видео информация</a></li>
    </ul>
</nav>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'SyP7k1fmbT';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
s.src = '//code.jivosite.com/script/widget/'+widget_id
; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->

    </body>
</html>