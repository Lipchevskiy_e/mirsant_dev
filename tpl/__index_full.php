<!DOCTYPE html>
<!-- (c) Wezom web-студия | http://www.wezom.com.ua/ -->
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?= $statik['TITLE'] ?></title>
<meta name="description" content="<?= $statik['DESCRIPTION'] ?>" />
<meta name="keywords" content="<?= $statik['KEYWORDS'] ?>" />
<base href="<?= MAIN_PATH  ?>">
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<link type="text/css" rel="stylesheet" href="/css/colorbox.css" />
<link rel="stylesheet" type="text/css" href="css/LiGManuLight.css" />
<link rel="stylesheet" type="text/css" href="css/liValidForm.css" />
<link rel="stylesheet" type="text/css" href="css/litree.css" />
<link rel="stylesheet" type="text/css" href="css/paginator3000.css" />
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/liValidForm.v6.js"></script>


<script type="text/javascript" src="/js/jquery.LiGManuLight.js"></script>
<script type="text/javascript" src="/js/jquery.litree.js"></script>
<script type="text/javascript" src="/js/paginator3000.js"></script>
<script type="text/javascript" src="/js/jquery.colorbox-min.js"></script>

<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<!--<script type="text/javascript" src="js/swfobject.js"></script> -->
<link rel="shortcut icon" href="favicon.ico" />
<!--[if IE 6]><script src="js/DD_belatedPNG_0.0.8a-min.js"></script><script>DD_belatedPNG.fix('.png, img');</script><![endif]-->
<!--[if IE]><script>document.createElement('header');document.createElement('nav');document.createElement('section');document.createElement('article');document.createElement('aside');document.createElement('footer');</script><![endif]-->
<?= $statik['HEAD'] ?>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<div class="h_left_coll">
			<a href="/" class="logo">weZom</a>
			<!--
			<ul class="menu_lang">
				<li><a href="#">ru</a></li>
				<li><a href="#">en</a></li>
				<li><a href="#">ua</a></li>
			</ul>
			-->
			
            <?php if (!isset($_SESSION['user_id'])): ?>
				<div class="enter_wrap">
					<a href="/registration/formregistration/enter" class="pad_0_5">Вход</a>
					<a href="/registration/formregistration/new" class="pad_0_5">Регистрация</a>
					<a href="/registration/formregistration/fpasword" class="pad_0_5">Забыли пароль?</a>
				</div><!-- .enter_wrap-->
			<?php endif; ?>			
			
		</div><!-- .h_left_coll-->
		<div class="h_right_coll">
			<div class="basket_wrap">
				В корзине <br/> товаров: <strong><span id="basket_count"><?php echo basket::get_Count_Goods(); ?></span> шт</strong><br />
				На сумму: <strong><span id="basket_total"><?php echo basket::get_Total_Goods(); ?></span> грн.</strong><br/>
				<a href="/basket">Оформить заказ</a>
			</div><!-- .basket_wrap-->
		</div><!-- .h_right_coll-->
		<div class="h_center_coll">
			<div class="h_center_coll_size">
				<div class="h_center_left">
					<form action="/search" class="s_form" method="POST">
						<label class="s_label">Поиск по сайту:</label><br>
						<input type="text" class="w_100p" name="search" />
						<input type="submit" class="but_1" value="Найти" />
					</form><!-- .s_form-->
				</div><!-- .h_center_left-->
				<div class="h_center_right">
					<form action="" class="s_form">
						<label class="s_label">Поиск по бренду:</label><br>
						<select>
							<option>Бренд 1</option>
							<option>Бренд 2</option>
							<option>Бренд 3</option>
						</select>
					</form><!-- .s_form-->
				</div><!-- .h_center_right-->
			</div><!-- .h_center_coll_size-->
		</div><!-- .h_center_coll-->
		<div class="clear"></div>
	</header><!-- #header-->
	<nav class="menu_top_wrap">
		<div class="menu_top_wrap2">
			<ul class="menu_top">
				<li><a <?php echo general::active_menu('index'); ?> href="/">Главная</a></li>
				<li><a <?php echo general::active_menu('kak_zakazat'); ?> href="/kak_zakazat">Как заказать</a>
					<?php echo general::get_all_punkt_menu(399); ?>
				</li>
				<li><a href="/catalog">Каталог</a>
					<?php echo catalog::get_catalog_menu("/frontend/catalog/menu_drop_down.php"); ?>
				</li>
				<li><a <?php echo general::active_menu('gallery'); ?> href="/gallery">Фотогалерея</a></li>
				<li><a <?php echo general::active_menu('indguestbookex'); ?> href="/guestbook">Отзывы</a></li>
				<li><a <?php echo general::active_menu('news'); ?> href="/news">Новости</a></li>
				<li><a href="#">Карта сайта</a></li>
				<li><a href="#">Корзина</a></li>
				<li><a href="#">Личный кабинет</a></li>
				<li><a <?php echo general::active_menu('contact'); ?> href="/contact">Контакты</a></li>
				<li><a <?php echo general::active_menu('voting'); ?> href="/voting">Голосование</a></li>
			</ul><!-- .menu_top-->
		</div><!-- .menu_top_wrap2-->
	</nav><!-- .menu_top_wrap-->
	<div id="conteiner">
		<div class="left_coll">
			<div class="left_coll_size">
				
				<?php if (isset($_SESSION['user_id'])): ?>
				<div class="box">
					<p><strong>Добро пожаловать «<?php echo $_SESSION['user_name']; ?>»</strong></p>
					<a href="/registration/formregistration/edit">Личные данные</a><br />
					<a href="/mycabinet/ctrl/myorders">История покупок</a><br />
					<a href="/registration/formregistration/exit" onclick="return confirm('Вы уверены?')">Выход</a>
				</div>
                <?php endif; ?>						
				
				<div class="box">
					<p><strong>Каталог</strong></p>
					<ul class="menu_tree">
						<?php echo catalog::get_catalog_menu("/frontend/catalog/menu_ind.php"); ?>
					</ul>
				</div>
				
				<div class="box">
					<p><strong>Бренды</strong></p>
					<div class="brands">
						<?php echo catalog::get_brand('/frontend/catalog/brand/brand_list.php',$statik['brand_at_first']); ?>
					</div><!-- .brands-->
				</div>
				
				<div class="box">
					<p><strong>Новости</strong></p>
					<?php echo news::show_news($statik['news_at_first'],"/frontend/news/first_page.php"); ?>
					<a href="/news">Все новости</a>
				</div>
				
					

			</div><!-- .left_coll_size-->
		</div><!-- .left_coll-->
		<div class="right_coll">
			<div class="right_coll_size">
			
				<div class="box">
					<p><strong>Фотогалерея</strong></p>
					<p align="center">
						<?= gallery::get_gallery_first_page("/frontend/gallery/first_page.php",1); ?>
					</p>
				</div>
	
				<div class="box">
					<p><strong>Баннерная система</strong></p>
					<p align="center">
						<?php echo banners::get_banner(1); ?>
					</p>
				</div>
				
				<div class="box">
					<p><strong>Голосование</strong></p>
					<p align="center">
						<?php  echo voting::get_vopros('/frontend/voting/vopros_first_page.php'); ?>
					</p>
				</div>				
				
			</div><!-- .right_coll_size-->
		</div><!-- .right_coll-->
		<div class="center_coll">
			<div class="center_coll_size content">
			
				<div class="navigator">
					<?php echo get_navigator(); ?>
				</div><!-- .navigator-->
				
				<h1><?= $statik['H1'] ?></h1>
				
				<?php echo $str; ?>
				
			</div><!-- .center_coll_size-->
		</div><!-- .center_coll-->
    </div><!-- #conteiner-->
</div><!-- #wrapper-->
<footer id="footer">
	<div class="footer_line"></div>
	<div class="footer_left">
		<div class="footer_left_ind">
			© 2011 Company
			<div style="padding:10px 0 0 0;">
				<?php echo counters::get_counter(); ?>
			</div>
		</div>
	</div><!-- .footer_left-->
	<div class="footer_right">
		<a target="_blank" href="http://wezom.com.ua" title="Разработка сайтов в Херсоне">
		<img src="/pic/wezom_logo.png" alt="Разработка сайтов в Херсоне" /><br />
		<span class="wezom_txt">Разработка сайта —<br />
		<span>web-студия Wezom</span></span>
		</a>
	</div><!-- .footer_right-->
	<div align="center" class="footer_center_ind">
		<?= _BANNER ?>
	</div><!-- .footer_center_ind-->
</footer><!-- #footer-->

<!-- сообщения -->
<?php echo general::global_massage(); ?>
<!-- сообщения -->

</body>
</html>