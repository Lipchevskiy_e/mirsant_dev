<?php

class SEO_S {

    static public $uri;   # /index.php?page=1
    static public $host;  # site.ru
    static public $url;   # http://site.ru/index.php?page=1
    static public $home;  # http://site.ru
    static public $uri_count; # Количество подзапросов в uri
    static public $uri_array; # uri в массиве
    static public $seo_dir;  # /var/www/ss
    static public $charset;  # UTF-8
    static public $db_charset;
    static public $db_host;
    static public $db_user;
    static public $db_pass;
    static public $db_name;

    static function loader() {
        header('Content-type: text/html; charset=' . self::$charset . '');
        self::$uri = $_SERVER['REQUEST_URI'];
        self::$host = $_SERVER['HTTP_HOST'];
        self::$url = 'http://' . $_SERVER['HTTP_HOST'] . self::$uri;
        self::$home = 'http://' . $_SERVER['HTTP_HOST'];
        self::$uri_count = self::uri_count();
        self::$uri_array = self::uri_array();
        self::$seo_dir = $_SERVER['DOCUMENT_ROOT'] . '/ss';
        self::$db_charset = 'utf8';
    }

    static public function validate($data) {
        $data = trim($data);
        return $data;
    }

    static public function get_connect() {
        if (mysql_connect(self::$db_host, self::$db_user, self::$db_pass)) {
            if (mysql_select_db(self::$db_name)) {
                mysql_query("SET CHARSET utf8");
                return TRUE;
            } else {
                self::msg("Ошибка в соединении с БД”");
            }
        } else {
            self::msg("Ошибка в соединении с БД");
        }
    }

    static public function msg($msg, $type = 1) {
        if ($_SERVER['HTTP_X_FORWARDED_FOR'] == '217.76.201.90' ||
                $_SERVER['HTTP_X_FORWARDED_FOR'] == '213.108.75.92' ||
                $_SERVER['HTTP_X_FORWARDED_FOR'] == '93.75.81.213' ||
                $_SERVER['REMOTE_ADDR'] == '217.76.201.90' ||
                $_SERVER['REMOTE_ADDR'] == '213.108.75.92' ||
                $_SERVER['REMOTE_ADDR'] == '93.75.81.213') {
            $msg = $msg;
            if ($type == 0) {
                ?>
                <pre style="
                     text-align:left !important;
                     background-color: #FFF !important;
                     color: #000 !important;
                     font-size:12px !important;
                     display:block !important;
                     font-family: Courier new !important;
                     "><?php print_r($msg); ?></pre>		
                <?php
            } elseif ($type == 1) {
                ?>
                <textarea style="
                          resize:both !important; 
                          float:left !important; 
                          position:fixed;
                          top:300px;
                          left:30px;
                          width:300px;
                          height:50px;
                          z-index:999999 !important; 
                          border: 1px dotted #000 !important
                          "><?php print_r($msg) ?>
                </textarea>	
            <?php
            }
        }
    }

    static public function db_redirects() {
        $url = self::query("
							SELECT new_url 
							FROM seo_redirects 
							WHERE old_url = '" . mysql_real_escape_string(self::$uri) . "'
						  ");
        if ($url) {
            $url['new_url'] = self::validate($url['new_url']);
            self::rd301(self::$site . $url['new_url']);
        }
    }

    static public function index($i = '0', $last = '1') {
        if ($i == '0') {
            if (self::$uri == '/index.php' || self::$uri == '/index.html') {
                self::rd301(self::$home);
            }
        }
        if ($i == '1') {
            if (self::$uri == '/') {
                self::rd301(self::$home . '/index.php');
            }
        }

        if ($last == '0') {
            if (self::$uri_array[self::$uri_count] == 'index.php' ||
                    self::$uri_array[self::$uri_count] == 'index.html'
            ) {
                $new_url = str_replace(array('index.html', 'index.php'), '', self::$url);
                self::rd301($new_url);
            }
        }
    }

    static public function www($i = '0') {
        if ($i == 0) {
            if (substr(self::$host, 0, 3) == 'www') {
                self::rd301(str_replace('www.', '', self::$url));
            }
        }

        if ($i == 1) {
            if (substr(self::$host, 0, 3) != 'www') {
                self::rd301('http://www.' . self::$host . self::$uri);
            }
        }
    }

    static public function get_seo_optimise() {
        $result = self::query("SELECT * FROM seo_optimise WHERE uri = '" . mysql_real_escape_string(self::$uri) . "'");
        if (isset($result)) {
            $seo_optimise['title'] = trim($result['title']);
            $seo_optimise['desc'] = trim($result['desc']);
            $seo_optimise['h1'] = trim($result['h1']);
            $seo_optimise['text'] = trim($result['text']);
        }
        return $seo_optimise;
    }

    static public function uri_count() {
        $uri_path = explode('/', self::$uri);
        $uri_count = count($uri_path) - 1;
        return $uri_count;
    }

    static public function uri_array($id = '-1', $id_end = 0) {
        $uri_path = explode('/', self::$uri);
        if ($id == '-1') {
            return $uri_path;
        } elseif ($id_end == 0) {
            return $uri_path[$id];
        } else {
            $count = $id_end - $id + 1;
            while ($id <= $count) {
                $uri .= '/' . $uri_path[$id];
                $id++;
            }
            return $uri;
        }
    }

    static public function rd301($new_url) {
        $new_url = self::validate($new_url);
        header("HTTP/1.1 301 Moved Permanently");
        header("Location:" . $new_url);
        exit();
    }

    static public function rd404($uri404 = 0) {
        $uri404 = self::validate($uri404);
        if (self::$uri == $uri404) {
            header('Content-type: text/html; charset=' . self::$charset . '');
            header('HTTP/1.1 404 Not Found');
            include('404.html');
            die();
        }
        if ($uri404 === 0) {
            header('Content-type: text/html; charset=' . self::$charset . '');
            header('HTTP/1.1 404 Not Found');
            include('404.html');
            die();
        }
    }

    static public function rewrite($old_uri, $new_uri = '0') {
        $old_uri = self::validate($old_uri);
        $new_uri = self::validate($new_uri);
        if ($new_uri == '0') {
            if ($old_uri == self::$uri) {
                self::rd301(self::$home);
            }
        } else {
            if ($old_uri == self::$uri) {
                self::rd301($new_uri);
            }
        }
    }

    static public function query($query) {
        $query = self::validate($query);
        $seo_data = mysql_fetch_array(mysql_query($query));
        return $seo_data;
    }

    static public function optimise() {
        include self::$seo_dir . '/seo_optimise.php';
        $seo_optimise = array();
        $seo_optimise['title'] = trim($seo_title);
        $seo_optimise['desc'] = trim($seo_desc);
        $seo_optimise['h1'] = trim($seo_h1);
        $seo_optimise['text'] = trim($seo_text);
        return $seo_optimise;
    }

    static public function canonical($url, $type = 0) {

        if ($type == 1) {
            $canonical = explode(',', $url);
            $i = 0;
            while ($i < count($canonical)) {

                $result = explode('=>', $canonical[$i]);
                $in = trim($result[0]);
                $out = trim($result[1]);

                if ($in == self::$uri) {
                    return '<link rel="canonical" href="' . self::$home . $out . '" />';
                }
                $i++;
            }
        } else {
            return '<link rel="canonical" href="' . self::$home . $url . '" />';
        }
    }

    static public function robots($index = '0', $follow = '0', $uri_robots = '0') {
        $uri_robots = self::validate($uri_robots);
        if ($index == '0') {
            $page_index = 'noindex';
        } elseif ($index == '1') {
            $page_index = 'index';
        }

        if ($follow == '0') {
            $page_follow = 'nofollow';
        } elseif ($follow == '1') {
            $page_follow = 'follow';
        }
        $robots = "\n" . '<meta name="robots" content="' . $page_index . ',' . $page_follow . '">' . sprintf("\n");

        if ($uri_robots == '0') {
            return $robots;
        } else {
            if (self::$uri == $uri_robots) {
                return $robots;
            } else {
                return false;
            }
        }
    }

    static public function cyr2lat($cyr) {
        $cyr = mb_strtolower(self::validate($cyr), 'UTF-8');

        $cyr2lat = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ь' => '', 'ы' => 'y', 'ъ' => '', 'э' => 'e', 'ю' => 'ju',
            'я' => 'ja', ' ' => '-', 'a' => 'a', 'b' => 'b', 'c' => 'c', 'd' => 'd', 'e' => 'e', 'f' => 'f',
            'g' => 'g', 'h' => 'h', 'i' => 'i', 'j' => 'j', 'k' => 'k', 'l' => 'l', 'm' => 'm', 'n' => 'n',
            'o' => 'o', 'p' => 'p', 'q' => 'q', 'r' => 'r', 's' => 's', 't' => 't', 'u' => 'u', 'v' => 'v',
            'w' => 'w', 'x' => 'x', 'y' => 'y', 'z' => 'z', '0' => '0', '1' => '1', '2' => '2', '3' => '3',
            '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '-' => '-', '(' => '-',
            ')' => '-', ':' => '-', '.' => '-', ';' => '-', '_' => '-', '\'' => '-', '"' => '-'
        );
        $i = 0;
        $result = NULL;

        while ($i < mb_strlen($cyr, 'UTF-8')) {
            $str = mb_substr($cyr, $i, 1, 'UTF-8');

            if (isset($cyr2lat[$str])) {
                $result .= $cyr2lat[$str];
            }
            $i++;
        }
        return str_replace('--', '-', $result);
    }

    static public function more($first_text, $second_text, $button_name = 'Подробнее') {
        $result = '
		<script language="javascript" type="text/javascript">
			function popUp($id){
				$div=document.getElementById($id);
				if ($div.style.display==\'none\') $div.style.display=\'block\';
				else $div.style.display=\'none\';
			}
		</script>
		
		<div class="ns_hdr">
			<p>'
                . $first_text . ' 
				<a href="javascript:popUp(\'seo_text\')" class="test" title="">' . $button_name . '</a>
			</p>
			<div id="seo_text">
				' . $second_text . '
			</div>
		</div>
		
		<script language="javascript" type="text/javascript">
			popUp(\'seo_text\');
		</script>';
        return $result;
    }

    static public function paginator_next($next) {
        return '<link rel="next" href="' . self::$home . $next . '" />';
    }

    static public function paginator_prev($prev) {
        return '<link rel="prev" href="' . self::$home . $prev . '" />';
    }

    static public function db_config($db_host, $db_user, $db_pass, $db_name, $conf_file) {
        $seo_conf = explode("\n", file_get_contents($conf_file));
        $test_seo = array();
        foreach ($seo_conf as $key => $value) {
            if (!strpos($value, $db_host) === false ||
                    !strpos($value, $db_user) === false ||
                    !strpos($value, $db_pass) === false ||
                    !strpos($value, $db_name) === false
            ) {
                $test_seo[] = $key;
            }
        }

        $data_conf = array();
        foreach ($test_seo as $key => $value) {
            $data = explode('=', $seo_conf[$value]);
            $data[1] = str_replace(array('\'', ';', '`'), '', $data[1]);
            $data_conf[] = $data[1];
        }

        return $data_conf;
    }

    static public function lastSlash($action = 1) {
        if (self::$uri == '/') {
            return false;
        }

        if ($action == 1) {
            if (substr(self::$uri, -1) == '/') {
                return false;
            }

            $url = 'http://' . self::$host . self::$uri . '/';
            self::rd301($url);
        } elseif ($action == -1) {
            if (substr(self::$uri, -1) != '/') {
                return false;
            }

            $url = 'http://' . self::$host . substr_replace(self::$uri, '', strlen(self::$uri) - 1);
            self::rd301($url);
        }
    }

}
