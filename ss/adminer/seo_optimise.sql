CREATE TABLE `seo_optimise` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uri` varchar(255) COLLATE 'utf8_general_ci' NOT NULL UNIQUE,
  `title` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `desc` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `h1` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `text` text COLLATE 'utf8_general_ci' NOT NULL
) COMMENT='' COLLATE 'utf8_general_ci';

CREATE TABLE `seo_redirects` (
  `old_url` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
  `new_url` varchar(255) COLLATE 'utf8_general_ci' NOT NULL
) COMMENT='' COLLATE 'utf8_general_ci';
