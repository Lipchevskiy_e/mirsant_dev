<?php
ini_set('display_errors', 0);

$conf = array();

require_once $_SERVER['DOCUMENT_ROOT'].'/ss/seo_class.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/ss/db_con.php';

$conf['host'] = SEO_S::$db_host;
$conf['user'] = SEO_S::$db_user;
$conf['pass'] = SEO_S::$db_pass;
$conf['db'] = SEO_S::$db_name;

$conf['charset'] = 'utf8';

$god['full_path'] = 'http://'.$_SERVER['HTTP_HOST'].'/ss/god/index.php';

header('Content-Type: text/html; charset='.$conf['charset'].'');

try {
	if (!$delim = mysql_connect($conf['host'], $conf['user'], $conf['pass'])) {
		throw new Exception("Program has not connected. Call admin please.");
	} else {
		mysql_select_db($conf['db'], $delim);
		mysql_set_charset($conf['charset']);
	}
} catch(Exception $e) {
	echo $e -> getMessage();
}

$serv = $_GET;
$upd = $_POST;
$width = 400;
function table_exists($table, $db) {
	$tables = mysql_list_tables($db);
	while (list($temp) = mysql_fetch_array($tables)) {
		if ($temp == $table) {
			return TRUE;
		}
	}
	return FALSE;
}

if (!table_exists('seo_users', $conf['db'])) {

	$chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
	$max = 10;
	$size = strlen($chars) - 1;
	$password = null;

	while ($max--) {
		$password .= $chars[rand(0, $size)];
	}

	mysql_query("CREATE TABLE `seo_users` (
              `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `login` varchar(255) COLLATE 'utf8_general_ci' NOT NULL UNIQUE KEY,
              `pwd` varchar(100) COLLATE 'utf8_general_ci' NOT NULL,
              `user_type` int(2) COLLATE 'utf8_general_ci' NOT NULL,
              `active` int(1) COLLATE 'utf8_general_ci' NOT NULL
            ) COMMENT='' COLLATE 'utf8_general_ci';") or die('error! Cant create seo_users.');

	mysql_query("CREATE TABLE `seo_user_types` (
              `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `type_name` varchar(150) COLLATE 'utf8_general_ci' NOT NULL       
            ) COMMENT='' COLLATE 'utf8_general_ci';") or die('error! Cant create seo_user_types.');

	mysql_query("INSERT INTO `seo_user_types` (type_name) VALUES ('Куратор'), ('Менеджер'), ('Разработчик')") or die('error! We cant write to database.');

	mysql_query("INSERT INTO seo_users(login, pwd, user_type, active) 
    VALUES 
    ('kurator', '" . md5($password) . "', 1, 1),
    ('den', '519c84155964659375821f7ca576f095', 3, 1),
    ('alex', 'fbc44786b921455839353465f7b04ad0', 3, 1)");

	echo "Login: kurator<br>
	New password: <b>" . $password . "</b><br>";
}

if (!table_exists('seo_optimise', $conf['db'])) {
	$query = "CREATE TABLE `seo_optimise` (
              `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `uri` varchar(255) COLLATE 'utf8_general_ci' NOT NULL UNIQUE KEY,
              `title` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
              `desc` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
              `h1` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
              `text` text COLLATE 'utf8_general_ci' NOT NULL
            ) COMMENT='' COLLATE 'utf8_general_ci';";

	if (mysql_query($query)) {
		die('Init OK. Tables created.');
	}
}
if (isset($upd['up'])) { // Сохранение записи
	$query = "UPDATE seo_optimise SET 
            uri = '" . $upd['uri'] . "', 
            title = '" . $upd['title'] . "', 
            `desc` = '" . $upd['desc'] . "', 
            h1 = '" . $upd['h1'] . "', 
            text = '" . $upd['text'] . "'
            WHERE id = " . $serv['update'];
	mysql_query($query) or die(mysql_error($delim));
	header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	exit();
}
if (isset($upd['add']) || isset($upd['add_insert'])) { // Добавление записи
	$query = "INSERT INTO seo_optimise 
                (
                    uri, 
                    title, 
                    `desc`, 
                    h1, 
                    text
                )
              VALUES
                (
                    '" . trim($upd['uri']) . "', 
                    '" . $upd['title'] . "', 
                    '" . $upd['desc'] . "', 
                    '" . $upd['h1'] . "', 
                    '" . $upd['text'] . "'
                )";
	mysql_query($query);
	($upd['add_insert'] ? header('Location: '. $god['full_path'].'?action=add') : header('Location: ' . $god['full_path']));
	exit();
} // Конец Добавление записи
if (isset($serv['delete'])) {// Удаление записей
	mysql_query("DELETE FROM seo_optimise WHERE id = " . intval($serv['delete']));
	header('Location: ' . $god['full_path']);
	exit();
} // Конец Удаление записей


session_start();

if (isset($upd['logout'])) {
	unset($_SESSION['login']);
}

// Авторизация
if (isset($upd['enter'])) {
	$query = mysql_query("SELECT * FROM seo_users WHERE login = '" . mysql_real_escape_string($upd['login']) . "'");
	if (mysql_num_rows($query) == 1) {
		$control = mysql_fetch_object($query);
		if ($control -> pwd == md5($upd['pass'])) {
			$_SESSION['login'] = $upd['login'];
			$_SESSION['first_login'] = 0;
			header('Location: ' . $god['full_path']);
		} else {
			$error = '<div id="error" class="alert alert-error fade in" style="margin: 0 auto; margin-top: 5px; width: 20%; text-align:center">
                	<button type="button" class="close" data-dismiss="alert" id="close_button">×</button>
                	Ошибка! Неверный логин или пароль!</div>';
		}
	} else {
		$error = '<div id="error" class="alert alert-error fade in" style="margin: 0 auto; margin-top: 5px; width: 20%; text-align:center">
                	<button type="button" class="close" data-dismiss="alert" id="close_button">×</button>
                	Ошибка! Неверный логин или пароль!</div>';
	}
}

if (isset($_SESSION['login'])) {
	$query = mysql_query("SELECT `seo_user_types`.`type_name` AS `name`, `seo_user_types`.`id` AS `id` FROM `seo_users` INNER JOIN `seo_user_types` ON seo_user_types.id = seo_users.user_type WHERE seo_users.login = '" . mysql_real_escape_string($_SESSION['login']) . "'");
	$types = mysql_fetch_array($query);

	$_SESSION['type']['id'] = $types['id'];
	$_SESSION['type']['name'] = $types['name'];
}
?>
<!doctype html>
<html>
    <head>
    	<title>Seo Studio Uniq Pages</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.css" rel="stylesheet">
        
        <script type="text/javascript" src="http://twitter.github.io/bootstrap/assets/js/bootstrap-alert.js"></script>
        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        
        <script type="text/javascript">
			$(document).ready(function() {
				$('#close_button').click(function() {
					$('#error').css('display', 'none');
				});


				tinymce.PluginManager.load('moxiemanager', 'http://www.tinymce.com/js/moxiemanager/plugin.min.js');
				tinymce.init({
					selector : "textarea",
					plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste moxiemanager"],
					toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
					autosave_ask_before_unload : false,
					width : 500
				});

				var dataSend = {
					logout : true
				}
				$('#logout').click(function() {
					$.ajax({
						type : "POST",
						url : "<?php echo $god['ful_path']?>",
						data : dataSend,
						success : function(data) {
							location.reload();
						}
					});
				});
				


				$("#searchQuery").click(function() {
					var info = $('#data').val();
					if (info == '') {
						alert('Вы ничего не ввели!');
					} else {
						$('#content').empty();
						$.ajax({
							type : "POST",
							url : "<?php echo $god['ful_path']?>",
							data : {
								search : true,
								data : info
							},
							success : function(data) {
								$('#content').append(data);
								$('#data').val(info);
							}
						});
						return false;
					}
				});

				$('#refresh').click(function() {
					location.reload();
				});
			});
        </script>
    </head>
<body style="background: #e5e5e5 url('concrete_wall_3.png') repeat; height: 100%">
	<span id="alert"></span>
<div class="body">
    <?php 
    if (isset($error)) echo $error;
    if (!isset($_SESSION['login'])): ?>
        <table style="background-color: #f5f5f5; width: 0; margin: auto; margin-top: 35px;" class="table table-bordered">
        <form method="post" class="form-horizontal">
          <tr><td>
              <div class="control-group">
                <div class="controls">
                  <input name="login" type="text" id="inputEmail" placeholder="Логин">
                </div>
              </div>
          </td></tr>
          <tr><td>
              <div class="control-group">
                <div class="controls">
                  <input name="pass" type="password" id="inputPassword" placeholder="Пароль">
                </div>
              </div>
          </td></tr>
          <tr><td>
              <div class="control-group">
                <div class="controls">
                  <input name="enter" type="submit" class="btn" value="Вход">
                </div>
              </div>
          </td></tr>
        </form>
        </table>
        
    <?php else: ?>
    <div id="content">
	<div style="margin: 5px; text-align: right;"><button class="btn btn-primary" id="logout">Выход</button></div>
   	
   	<?php
		if (isset($upd['search'])) ://ajax search
		$ajax .= '<center>
		  <div class="input-append">
		    <input id="data" style="width: 500px;" type="text" class="span2">
		    <button class="btn" id="refresh" type="button">Сброс результатов</button>
		    <button id="searchQuery" name="submit" type="button" class="btn">Поиск</button>
		  </div>
	    </center>
	    <table style="width: 0; max-width: 1000px; margin: auto; margin-top: 35px; margin-bottom: 15px; background-color: #f5f5f5;" class="table table-bordered">
	        <thead>
	            <tr>
	                <th>#</th>
	                <th>url</th>
	                <th>name</th>
	                <th colspan="2" style="text-align: right;"><span class="label label-info">Seo Optimise</span></th>
	            </tr>
	        </thead>
	        <tbody>';

			$query = mysql_query("SELECT * FROM `seo_optimise` WHERE (`uri` LIKE '%" . $upd['data'] . "%' OR `title` LIKE '%" . $upd['data'] . "%' OR `desc` LIKE '%" . $upd['data'] . "%' OR `h1` LIKE '%" . $upd['data'] . "%' OR `text` LIKE '%" . $upd['data'] . "%')");
			while ($doc = mysql_fetch_object($query)) {
				$ajax .= '<tr>';
				$ajax .= '<td>' . $doc -> id . '</td>';
				$ajax .= '<td title="перейти по данной ссылке&#010Title: ' . $doc -> title . '&#010Description: ' . $doc -> desc . '&#010H1: ' . $doc -> h1 . '"><a target="blank" href="http://' . $_SERVER['HTTP_HOST'] . $doc -> uri . '">' . ($doc -> uri == '/' ? $doc -> uri : substr($doc -> uri, 1)) . '</td>';
				$ajax .= '<td>' . ($doc -> h1 ? $doc -> h1 : $doc -> title) . '</td>';
				$ajax .= '<td><a href="' . ($_SESSION['type']['id'] != 2 ? $god['full_path'].'?update=' . $doc -> id : '#') . '" id="red"><p><nobr>Редактировать <span class="icon-pencil"></span></nobr></p></td>';
				$ajax .= '<td><a href="' . ($_SESSION['type']['id'] != 2 ? $god['full_path'].'?delete=' . $doc -> id : '#') . '" id="del" onclick="return confirm(\'Хотите удалить запись?\')"><nobr>Удалить <i class="icon-remove"></i></nobr></td>';
				$ajax .= '</tr>';
			}

			$ajax .= '</tbody>
	    </table></div>';
			echo $ajax;
		endif;
 ?>
    <?php if (empty($serv) && !isset($upd['search'])) : // Просмотр всех записей ?>
    	
    <?php if ($_SESSION['first_login'] === 1){ ?>
    <div id="error" class="alert fade in" style="margin: 0 auto; width: 25%; margin-bottom: 10px; text-align:center">
    <button type="button" class="close" data-dismiss="alert" id="close_button">×</button>
    Вы: <?php echo $_SESSION['type']['name'] ?></div>
    <?php 
    }

	if (isset($_SESSION['first_login'])){
		$_SESSION['first_login']++;
	}
	else{
		$_SESSION['first_login'] = 1;	
	}
    ?>
    <center>
		  <div class="input-append">
		    <input id="data" style="width: 500px;" type="text" class="span2">
		    <button id="searchQuery" name="submit" type="button" class="btn">Поиск</button>
		  </div>
	</center>
    <table style="width:0; max-width: 1000px; margin: auto; margin-top: 35px; margin-bottom: 15px; background-color: #f5f5f5;" class="table table-bordered">
        <thead>
        	<tr>
        		<td colspan="5">
        			<?php $count = mysql_query("SELECT COUNT(*) FROM seo_optimise"); $out = mysql_fetch_row($count); ?>
        			<a href="<?php echo $god['full_path']?>?action=add">Добавить запись <i class="icon-plus-sign"></i></a>
        			<p style="float: right">Всего записей: <b><?php echo $out[0]; ?></b></p>
        		</td>
        	</tr>
            <tr>
                <th>#</th>
                <th>url</th>
                <th>name</th>
                <th colspan="2" style="text-align: right;"><span class="label label-info">Seo Optimise</span></th>
            </tr>
        </thead>
        <tbody> 
                <?php
				$query = mysql_query("SELECT * FROM seo_optimise");
				while ($doc = mysql_fetch_object($query)) {
					echo '<tr>';
					echo '<td>' . $doc -> id . '</td>';
					echo '<td title="перейти по данной ссылке&#010Title: ' . $doc -> title . '&#010Description: ' . $doc -> desc . '&#010H1: ' . $doc -> h1 . '"><a target="blank" href="http://' . $_SERVER['HTTP_HOST'] . $doc -> uri . '">' . ($doc -> uri == '/' ? $doc -> uri : substr($doc -> uri, 1)) . '</td>';
					echo '<td>' . ($doc -> h1 ? $doc -> h1 : $doc -> title) . '</td>';
					echo '<td valign="middle"><a href="' . ($_SESSION['type']['id'] != 2 ? $god['full_path'].'?update=' . $doc -> id : '#') . '" id="red"><p><nobr>Редактировать <span class="icon-pencil"></span></nobr></p></td>';
					echo '<td valign="middle"><a href="' . ($_SESSION['type']['id'] != 2 ? $god['full_path'].'?delete=' . $doc -> id : '#') . '" id="del" onclick="return confirm(\'Хотите удалить запись?\')"><nobr>Удалить <i class="icon-remove"></i></nobr></td>';
					echo '</tr>';
				}
                ?>
                  
        </tbody>
    </table>
    </div>
    <?php endif; // Конец Просмотр всех записей ?>
    <?php if (isset($serv['update'])) : // Редактирование записей ?>
        <?php
		$query = mysql_query("SELECT * FROM seo_optimise WHERE id = " . intval($serv['update']));
		$res = mysql_fetch_object($query);
        ?>
        <form action="" method="post" id="form">
            <table style="margin: auto; width: 0; margin-top: 35px; background-color: #f5f5f5;" class="table table-bordered">
                <tr>
                    <td colspan="2"><span class="label label-info">Редактирование записи</span></td>
                </tr>
                <tr>
                    <td><a href="<?php echo $god['full_path']?>" class="btn dropdown-toggle" />Назад</a></td>
                    <td style="text-align: right;"><input type="submit" name="up" value="Сохранить" class="btn btn-success" /></td>
                </tr>
                <tr>
                    <td>Uri</td>
                    <td><input style="width: <?php echo $width; ?>px;" name="uri" type="text" value="<?php echo $res -> uri; ?>" /></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td><input style="width: <?php echo $width; ?>px;" name="title" type="text" value="<?php echo $res -> title; ?>" /></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><input style="width: <?php echo $width; ?>px;" name="desc" type="text" value="<?php echo $res -> desc; ?>" /></td>
                </tr>
                <tr>
                    <td>H1</td>
                    <td><input style="width: <?php echo $width; ?>px;" name="h1" type="text" value="<?php echo $res -> h1; ?>" /></td>
                </tr>
                <tr>
                    <td>Text</td>
                    <td><textarea style="width: <?php echo $width; ?>px;" name="text" rows="6"><?php echo $res -> text; ?></textarea></td>
                </tr>
            </table>
        </form>
    
    <?php endif; // Конец Редактирование записей ?>
    
    <?php if (isset($serv['action']) && $serv['action'] == 'add') : // Добавление записей ?>
        <form action="" method="post" id="form">
            <table style="margin: auto; width: 0; margin-top: 35px; background-color: #f5f5f5;" class="table table-bordered">
            	
                <tr>
                    <td colspan="2"><span class="label label-info">Добавление записи</span></td>
                </tr>
                <tr>
                    <td><a href="<?php echo $god['full_path']?>" class="btn dropdown-toggle" /><span class="icon-arrow-left"></span> Назад</a></td>
                    <td style="text-align: right;">
                    	<input style="float: left" type="submit" name="add_insert" value="Сохранить и добавить еще" class="btn btn-primary" />
                    	<input type="submit" name="add" value="Сохранить" class="btn btn-success" />
                    </td>
                </tr>
                <tr>
                    <td>Uri</td>
                    <td><input style="width: <?php echo $width; ?>px;" name="uri" type="text" value="" /></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td><input style="width: <?php echo $width; ?>px;" name="title" type="text" value="" /></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><input style="width: <?php echo $width; ?>px;" name="desc" type="text" value="" /></td>
                </tr>
                <tr>
                    <td>H1</td>
                    <td><input style="width: <?php echo $width; ?>px;" name="h1" type="text" value="" /></td>
                </tr>
                <tr>
                    <td>Text</td>
                    <td><textarea style="width: <?php echo $width; ?>px;" name="text" rows="6"></textarea></td>
                </tr>
                
            </table>
        </form>
    <?php endif; ?> 
</div>
<?php endif; ?>
</body>
</html>
