<?php
    /************************************
    / Модуль склнения с обращением к API
    / -----------------------------------
    / Ver 2.3.1
    / Дата 25.02.2015
    / Авторы: Зинченко Никита, 
    /         Соколенко Владислав, 
    /         Сукач Юрий
    /*************************************/
    class MorphyClient {
        private $morphy      = null;
        private $data        = array();
        private $wordsRegister = array();
        
        private $template    = null;
        private $words       = null;
        private $declension  = null;
        private $plural      = false;
        
        private $filter_cyrillic = false; // Нужно ли фильтровать запросы и отправлять только кириллицу?
        private $cache       = array();
        private $withcache   = true;
        private $page_reset  = false;
        private $cache_reset = false;
        private $phase_reset = false;
        
        
        private $time   = 0;
        
        private $key    = '4907bbd257f6';
        private $url    = 'http://api.seo-studio.ua/morphy/';
        private $params = array();
        private $ident  = 'local'; // local|client идентификатор запроса
        
        const TIMEOUT                       = 1;   // Таймаут ожидания ответа в секундах
        const CONNECT_TIMEOUT               = 1;   // Таймаут соединения в секундах
        const MIN_TIME_FOR_REPEAT_REQUEST   = 3600;// Период времени в секундах для максимального количества запросов по фразе
        const MAX_REQUESTS_PER_MIN_TIME     = 3;   // Максимальное количество запросов по фразе за период времени
        
        const ERROR_NOT_VALID               = 'The request is not a valid';
        
        public function __construct($needMorphyLib = true) {
            if (strpos(__FILE__,'api.seo-studio.ua')) $this->ident = 'local'; else $this->ident = 'client';
            if ($needMorphyLib == true) {
                try {
                    require_once(dirname(__FILE__) . '/lib/phpmorphy/src/common.php');
                    $this->morphy = new phpMorphy(dirname(__FILE__) . '/lib/phpmorphy/dicts', 'ru_RU', array(
                        'storage' => PHPMORPHY_STORAGE_FILE,
                        'predict_by_suffix'     => true,
                        'predict_by_db'         => true,
                        'graminfo_as_text'      => true,
                        'common_source'         => null
                    ));
                } catch(phpMorphy_Exception $e) {
                    printError('Error occured while creating phpMorphy instance: ' . PHP_EOL . print_R($e, true)); die();
                }
            }
        }
        
        public function __destruct() {
            $this->morphy = null;
        }
        
        private function _ucfirst($word) {
           
            if(!function_exists('mb_ucfirst')) { 
                $first = mb_strtoupper(mb_substr($word, 0, 1,  'utf-8'), 'utf-8') . mb_substr($word, 1, mb_strlen($word, 'utf-8') - 1, 'utf-8'); 
            } else {
                $first = mb_ucfirst($word, 'urf-8');
            }
            return $first;
        }
        
        private function checkWordRegister($word) {
            $word = trim($word); 
            $word = str_replace(array(',', '(', ')', '.', ':', ';', '!', '?', '%', '$', '"'), '', $word);
            $upper = mb_strtoupper($word, 'utf-8');
            $lower = mb_strtolower($word, 'utf-8');
            $first = $this->_ucfirst($word);
            if ($word == $upper) return 'upper';
            elseif ($word == $lower) return 'lower';
            elseif ($word == $first) return 'first';
            else return 'other';
        }
        
        public function init($template, $words, $declension, $plural) {
            $response = array();
            $this->time = time();
            $this->data = array();
            $this->declension = '';
            $this->template      = $template;
            $this->words         = $words;
            $this->page_reset    = isset($_REQUEST['page_reset'])    ? true : false; // Сброс по странице
            if (isset($_REQUEST['page_reset'])) $this->page_reset = true; // Alias page_reset
            $this->cache_reset   = isset($_REQUEST['cache_reset'])   ? true : false; // Сброс всего кеша
            $this->phase_reset   = isset($_REQUEST['phase_reset'])   ? mb_strtolower($_REQUEST['phase_reset'], 'utf-8') : false; //Сброс по фразе
            
            $result = $this->setWords();
            
            if ($this->withcache) $this->loadCache();
            
            $this->setParams();
            if (!empty($response['params']) && !empty($response['key']) && $response['key'] == $this->key) {
                $this->changeParams($response['params']);
            }
            // Установка фразы в кеш
            if (isset($_REQUEST['set_cache_to']) && isset($_REQUEST['cache_value']) && isset($_REQUEST['cache_plural']) && isset($_REQUEST['cache_declension'])) {
                    $phase = mb_strtolower($_REQUEST['set_cache'], 'utf-8');   $prase = mb_substr($prase, 0 , 1024, 'utf-8');
                    $cache = mb_strtolower($_REQUEST['cache_value'], 'utf-8'); $cache = mb_substr($cache, 0 , 1024, 'utf-8');
                    $plural = $_REQUEST['cache_plural']; $plural = mb_substr($plural, 0 , 2, 'utf-8');
                    $declension = $_REQUEST['cache_declension']; $declension = mb_substr($declension, 0 , 2, 'utf-8');
                    $this->cache['data'][$phase][$declension][$plural] = $cache;
            } else {   
            // Склонение             
                if ($result === true) {
                    $this->template   = $template;
                    $this->declension = $declension;
                    $this->plural     = $plural !== false ? true : false;
                    $this->getPhrasesData();
                    $response = array('phrases' => $this->data);
                } else {
                    $response = array('error'   => $result);
                }
            }
            
            if ($this->withcache) $this->saveCache();
            
            return $response;
        }
        
        // Слова для обрабоки
        private function setWords() {
            $result = self::ERROR_NOT_VALID;
            if (isset($this->words)) {
                $words = $this->words;
                if (isset($words) && is_array($words)) {
                    foreach ($words as $key => $word) {
                        $wordLower = mb_strtolower(trim($word), 'utf-8');
                        $infWords = explode(' ', $word);
                        $_num = 0;
                        foreach ($infWords  as $wpart) {
                            $wpartLower = mb_strtolower(trim($wpart), 'utf-8');
                            $this->wordsRegister[$wordLower][$wpartLower] = $this->checkWordRegister($wpart);
                            $this->wordsRegister[$wordLower]['_' . $_num] = $this->checkWordRegister($wpart);
                            $_num++;
                        }
                        $this->data[$wordLower] = array('word' => $wordLower);
                    }
                    $result = true;
                }
            }
            return $result;
        }
        
        // Чтение данных о словах
        public function getWordInfo($word) {
            $result = array();
            $grammar = $this->morphy->getGramInfo(mb_strtoupper($word, 'utf-8'));
            $codes = implode(',', (array)$grammar[0][0]['grammems']);
            $result['word']   = $word;
            $result['pos']    = $grammar[0][0]['pos'];
            $result['code']   = $codes;
            $result['plural'] = strpos($codes, 'ЕД') !== false ? 'ЕД' : 'МН';
            $result['genus']  =
                strpos($codes, 'МР') !== false ? 'МР' :
                (strpos($codes, 'ЖР') !== false ? 'ЖР' : 'СР')
            ;
            $result['declension'] =
                strpos($codes, 'ИМ') !== false ? 'ИМ' :
                (strpos($codes, 'РД') !== false ? 'РД' :
                (strpos($codes, 'ДТ') !== false ? 'ДТ' :
                (strpos($codes, 'ВН') !== false ? 'ВН' :
                (strpos($codes, 'ТВ') !== false ? 'ТВ' :
                (strpos($codes, 'ПР') !== false ? 'ПР' : 'ЗВ')))))
            ;
            return $result;
        }
        
        // Получаем форму слова
        public function getWordForm($word, $info, $declension, $plural) {
            $forms = $this->morphy->getAllFormsWithGramInfo(mb_strtoupper($word, 'utf-8'));
            if (is_array($forms)) {
                $forms = $forms[0];
                foreach ((array)$forms['all'] as $key => $code) {
                    $info['plural'] = $plural === false ? 'ЕД' : 'МН';
                    if (strpos($code, $info['plural']) !== false &&
                        strpos($code, $info['genus']) !== false &&
                        strpos($code, $declension) !== false
                    ) {
                        return mb_strtolower($forms['forms'][$key], 'utf-8');
                    }
                }
            }
            return $word;
        }
        
        // Обработка слов
        private function getPhrasesData() {
            foreach ($this->data as $key => $word) {
                
                //unset($this->cache['data'][$word['word']][$this->declension][$this->plural]);
                if (!isset($this->cache['data'][$word['word']][$this->declension][$this->plural]) || !$this->withcache || $this->page_reset
                || ($this->phase_reset !== false && $this->phase_reset == $word['word'])
                ) {
                    /* Обработка фразы не с кеша*/
                    $apiSucess = false;
                    
                    /* Нет кириллицы, нет запроса к API */
                    if ($this->filter_cyrillic) {
                        if (!preg_match('/[а-яА-Я]+/UiS', $word['word'])) {
                            $this->cache['data'][$word['word']][$this->declension][$this->plural] = $word['word'];
                            $this->data[$key]['before'] = $word['word'];
                            $this->data[$key]['after'] = str_replace('{fraze}', $word['word'], $this->template);
                            $apiSucess = true;
                            continue;
                        }
                    }
                    
                    $this->data[$key]['before'] = $word['word'];
                    
                    /* Формируем запрос к API */                    
                    $can_send_request = true;
                    if (isset($this->cache['requests'][$word['word']][$this->declension][$this->plural])) {
                        if ($this->cache['requests'][$word['word']][$this->declension][$this->plural]['count'] >= $this->params['max_requests']
                        && $this->cache['requests'][$word['word']][$this->declension][$this->plural]['time'] > time() - $this->params['min_time_request']
                        ) $can_send_request = false;
                    }    
                    
                    if ($this->withcache && $can_send_request) { 
                        $this->params['request'] = array(
                            'action' => 'words',
                            'key' => $this->key,
                            'data' => array(
                                'word' => $word['word'],
                                'declension' => $this->declension,
                                'plural' => empty($this->plural) ? 0 : 1,
                                'host' => (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''),
                                'url' => (isset($_SERVER['HTTP_HOST']) && isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '')
                            )
                        );
                        $response = json_decode($this->getUrl(), true);
                        
                        if (!empty($response['success']) && !empty($response['data']['after'])) {
                            // OK       
                            $apiSucess = true;
                            $phrase = $response['data']['after'];
                            // Register of letters
                            $phraseWords = explode(' ', $phrase);
                            $_num = 0;
                            $newPhrase = $phrase;
                            foreach ($phraseWords as $partOfPhrase) {                            
                                if (isset($this->wordsRegister[mb_strtolower($word['word'], 'utf-8')]['_' . $_num])) {
                                    $checkWordRegister = $this->wordsRegister[mb_strtolower($word['word'], 'utf-8')]['_' . $_num];
                                } else {
                                    $checkWordRegister = 'first';
                                }                                
                                $phraseWordsClean = str_replace(array(',', '(', ')', '.', ':', ';', '!', '?', '%', '$', '"'), '', $partOfPhrase);
                                if ($checkWordRegister == 'upper') $phraseWordsClean = mb_strtoupper($phraseWordsClean, 'utf-8');
                                elseif ($checkWordRegister == 'lower') $phraseWordsClean = mb_strtolower($phraseWordsClean, 'utf-8');
                                else $phraseWordsClean = $this->_ucfirst($phraseWordsClean);
                                $newPhrase = str_replace( $partOfPhrase, $phraseWordsClean, $newPhrase);
                                $_num++;
                            }
                            $this->cache['data'][$word['word']][$this->declension][$this->plural] = $newPhrase;
                            $phrase = $newPhrase;
                            // reset temp cache counter
                            if (isset($this->cache['requests'][$word['word']][$this->declension][$this->plural])) {
                                unset($this->cache['requests'][$word['word']][$this->declension][$this->plural]);
                            }
                        } else {
                            // False. Set temp cache counter
                            if (isset($this->cache['requests'][$word['word']][$this->declension][$this->plural])) {
                                if ($this->cache['requests'][$word['word']][$this->declension][$this->plural]['time'] > time() - $this->params['min_time_request']) {
                                    $this->cache['requests'][$word['word']][$this->declension][$this->plural]['count']++;
                                } else {
                                    $this->cache['requests'][$word['word']][$this->declension][$this->plural]['count'] = 1;
                                    $this->cache['requests'][$word['word']][$this->declension][$this->plural]['time'] = time();
                                }
                            } else {
                                $this->cache['requests'][$word['word']][$this->declension][$this->plural]['count'] = 1;
                                $this->cache['requests'][$word['word']][$this->declension][$this->plural]['time'] = time();
                            }
                        }
                    }
                    // Обработаем локально. API не вернул результатов
                    if (!$apiSucess) {
                        $words = (array)explode(' ', $word['word']);
                        $wordForms = array();
                        foreach ($words as $value) { 
                            $checkWordRegister = $this->wordsRegister[$word['word']][$value];//$this->data[$value]['type'];
                            $wordClean = str_replace(array(',', '(', ')', '.', ':', ';', '!', '?', '%', '$', '"'), '', $value);
                            
                            $wordInfo = $this->getWordInfo($wordClean);
                            $wordForm = $this->getWordForm($wordClean, $wordInfo, $this->declension, $this->plural); 
                            // Буковки
                            if ($checkWordRegister == 'upper') $wordForm = mb_strtoupper($wordForm, 'utf-8');
                            elseif ($checkWordRegister == 'lower') $wordForm = mb_strtolower($wordForm, 'utf-8');
                            else $wordForm = $this->_ucfirst($wordForm);
                            $wordForm = str_replace($wordClean, $wordForm, $value);
                            $wordForms[] = $wordForm; 
                        }
                        $phrase = implode(' ', $wordForms);
                    }
                    
                    $this->data[$key]['after'] = str_replace('{fraze}', $phrase, $this->template); 
                } else {
                    /* есть кеш */
                    $this->data[$key]['before'] = $word['word'];
                    $this->data[$key]['after'] = str_replace('{fraze}', $this->cache['data'][$word['word']][$this->declension][$this->plural], $this->template);    
                    $phraseWords = explode(' ', $this->cache['data'][$word['word']][$this->declension][$this->plural]);
                    $_num = 0;
                    $newPhrase = $phrase;
                    foreach ($phraseWords as $partOfPhrase) {                            
                        if (isset($this->wordsRegister[mb_strtolower($word['word'], 'utf-8')]['_' . $_num])) {
                            $checkWordRegister = $this->wordsRegister[mb_strtolower($word['word'], 'utf-8')]['_' . $_num];
                        } else {
                            $checkWordRegister = 'first';
                        }                                
                        $phraseWordsClean = str_replace(array(',', '(', ')', '.', ':', ';', '!', '?', '%', '$', '"'), '', $partOfPhrase);
                        if ($checkWordRegister == 'upper') $phraseWordsClean = mb_strtoupper($phraseWordsClean, 'utf-8');
                        elseif ($checkWordRegister == 'lower') $phraseWordsClean = mb_strtolower($phraseWordsClean, 'utf-8');
                        else $phraseWordsClean = $this->_ucfirst($phraseWordsClean);
                        $this->data[$key]['after'] = str_replace( $partOfPhrase, $phraseWordsClean, $this->data[$key]['after']);
                        $_num++;
                    }          
                    $this->data[$key]['cache'] = true;
                }
                
                unset($this->data[$key]['word']);
            }
        }
        
        // Изменение параметров
        private function changeParams($params) {
            if (is_array($params)) {
                foreach ($params as $key => $param) {
                    if (!empty($param) && isset($this->params[$key])) {
                        $this->params[$key] = $param;
                        $this->cache['params'][$key] = $param;
                    }
                }
            }
        }
        
        // Установка параметров
        private function setParams() {
            $this->params['url'] = $this->url;
            $this->params['timeout']            = self::TIMEOUT;
            $this->params['connect_timeout']    = self::CONNECT_TIMEOUT;
            $this->params['min_time_request']   = self::MIN_TIME_FOR_REPEAT_REQUEST;
            $this->params['max_requests']       = self::MAX_REQUESTS_PER_MIN_TIME;
            
            if (isset($this->cache['params']) && is_array($this->cache['params'])) {
                foreach ($this->cache['params'] as $key => $param) {
                    if (!empty($param) && isset($this->params[$key])) {
                        $this->params[$key] = $param;
                    }
                }
            }
        }
        
        // Запрос УРЛа
        private function getUrl() {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $this->params['url'] . '?ident=' . $this->ident . '&c=' . base64_encode(json_encode($this->params['request'])));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->params['connect_timeout']);
            curl_setopt($curl, CURLOPT_MAXREDIRS, 0);
            curl_setopt($curl, CURLOPT_TIMEOUT, $this->params['timeout']);
            
            if ($data = curl_exec($curl)) {
                curl_close($curl);
                return $data;
            } else {
                return false;
            }
        }
        // Вывод дампа
        private function dump($data) {
            echo '<pre>', print_r($data, 1), '</pre>';
        }
        
        // Вывод ошибки
        private function printError($text) {
            echo ('<div style="position: absolute; left: 0; top: 0; padding: 20px 10px; background-color: #FAA; border: 1px dotted #f00; color: 600; z-index: 9999999;">' . $text . '</div>');
        }
        
        
        /* ---------- КЕШИРОВАНИЕ --------------*/
        
        public function getCache() {
            return $this->cache;
        }
        
        // Загрузка кеша
        public function loadCache() {
            if ($this->cache_reset == true) {
                $this->cache = array();
            } else {
                if (file_exists(dirname(__FILE__) . '/cache.db')) {
                    $this->cache = json_decode(trim(file_get_contents(dirname(__FILE__) . '/cache.db')), true);
                } else {
                    $this->printError('Error phpMorphy cache read');
                }
            }
        }
        // Сохранение кеша
        public function saveCache() {
            if (!file_put_contents(dirname(__FILE__) . '/cache.db', json_encode($this->cache) . "\n")) {
                $this->printError('Error phpMorphy cache write');
            }
        }
        // Полная чистка кеша
        public function cleanCache() {
            if (!file_put_contents(dirname(__FILE__) . '/cache.db', '')) {
                $this->printError('Error phpMorphy cache write');
            }
        }
        // Обновление данных кеша с сервера API по фразе
        public function refreshCache($phrase, $declension, $plural) {
            $this->setParams();
            $this->params['request'] = array(
                            'action' => 'words',
                            'key' => $this->key,
                            'data' => array(
                                'word' => $phrase,
                                'declension' => $declension,
                                'plural' => $plural,
                                'host' => (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''),
                                'url' => (isset($_SERVER['HTTP_HOST']) && isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '')
                            )
                        ); 
                        $response = json_decode($this->getUrl(), true); 
                        if (!empty($response['success']) && !empty($response['data']['after'])) {
                            $res = $response['data']['after'];
                            $this->cache['data'][$phrase][$declension][$plural] = $res;
                            if (isset($this->cache['manualchange'][$phrase][$declension][$plural])) unset($this->cache['manualchange'][$phrase][$declension][$plural]);
                            $this->saveCache();
                            return array('result' => 'success', 'after' => $res);
                        } else {
                            return array('result' => 'error');
                        }
        }
        // Установка кеша для фразы по параметрам
        public function setCachePhrase($phrase, $setcache, $declension, $plural) {
                $this->cache['data'][$phrase][$declension][$plural]         = $setcache;
                $this->cache['manualchange'][$phrase][$declension][$plural] = array($setcache,date("Y-m-d H:i:s"));
                $this->saveCache();
                
        }
        // Сброс данных кеша по фразе и параметрам
        public function resetCachePhrase($phrase, $declension, $plural) {
                unset($this->cache['data'][$phrase][$declension][$plural]);
                $this->saveCache();
                
        }
        // Сброс данных кеша по фразе и параметрам
        public function resetTempCachePhrase($phrase, $declension, $plural) {
                unset($this->cache['requests'][$phrase][$declension][$plural]);
                $this->saveCache();  
        }
        
    }
