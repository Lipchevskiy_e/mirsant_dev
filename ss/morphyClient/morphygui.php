<?php
    error_reporting(0);
    ini_set('max_execution_time', 0);
    mb_internal_encoding('UTF-8');
    
    require_once(dirname(__FILE__) . '/lib/phpmorphy/src/common.php');
    require_once(dirname(__FILE__) . '/MorphyClient.php');
    
    $morphy = new MorphyClient();
?>
<!DOCTYPE html>
<html>
            <head>
                <title>Cache Viewer :: phpMorphy Client</title>
                <meta charset="utf-8">
                <meta name="robots" content="noindex, nofollow">
                <style>
                    body { padding: 5px; font-family: Arial, Verdana, Tahoma, Trebuchet; font-size: 11px; }
                    h1 { font-size: 16px; padding: 5px; margin: 0px 0px 15px 0px; border-bottom: 1px dotted #999; color: #1E90FF;}
                    div.footer { border-bottom: 1px dotted #777;}
                    
                    table {font-size: 11px; border:0px; margin:5px 0px; }
                    table.main { border-collapse: collapse; font-size: 11px; border:0px; margin:5px 0px; }
                    table.full { width: 99%;}
                    table.main td { vertical-align: top; }
                    table.main td, table.main th { padding: 2px; border: 1px solid #BFBFBF; font-size: 11px; }
                    table.main tr:hover, .main_tr_gray {background: #E5E5E5; font-size: 12px; }
                    table.main   .main_tr_darkgray {background: #a9a9a9; text-align:center;}
                    table.main td, table.main th, .main_tr_gray { text-align:center; }
                    table.main td.left {text-align: left;}
                    table.main tr.header { background-color: #1E90FF; font-weight: bold; font-size: 12px; text-align: center; color: #fff;}
                    table.main tr.header:hover { background-color: #42A2FF; font-weight: bold;} 
                    table.main tr.gray td { color: #555;}
                    select.option {width: 180px;}
                    p.exp{margin: 0px; padding: 0px; font-size: 9px; color: #777; }
                    textarea {width: 99%; height: 140px;}
                    
                    .error {position: fixed; z-index: 99999; width: 500px; padding: 20px; background-color: #EFE; border: 2px solid #f00; left: 50%; top: 200px; margin-left: -270px; opacity: 0.8;}
                </style>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                <script>window.jQuery || alert('No jQuery!')</script>
                <script>
                   
                </script>
            </head>
            <body>
            <h1>GUI :: phpMorphy Client</h1>
            
    <form action="" method="post">
        <div class="separator"></div>
        <table>
            <tr>
             <td>Выберите падеж:</td><td> <select name="declension" class="option">
        <?php
            $declensions = array(
                'ИМ' => 'Именительный',
                'РД' => 'Родительный',
                'ДТ' => 'Дательный',
                'ВН' => 'Винительный',
                'ТВ' => 'Творительный',
                'ПР' => 'Предложный',
            );
            
            $declension = (!empty($_POST['declension']) ? $_POST['declension'] : '');
            
            foreach ($declensions as $code => $title) {
                echo '<option value="' . $code . '"' . ($code == $declension ? ' selected' : '') . '>' . $title . '</option>';
            }
        ?>
        </select>
        </td><tr>
            
        <tr>
            <td>    
                Выберите число: 
            </td><td>
                <select name="plural" class="option">
        <?php
            $plurals = array(
                '0' => 'Единственное',
                '1' => 'Множественное',
            );
            
            $plural = (!empty($_POST['plural']) ? 1 : 0);
            
            foreach ($plurals as $code => $title) {
                echo '<option value="' . $code . '"' . ($code == $plural ? ' selected' : '') . '>' . $title . '</option>';
            }
        ?>
        </select>
        </td></td>
        
        <?php
            $template = !empty($_POST['template']) ? $_POST['template'] : '{fraze}';
        ?>
         <tr>
            <td> 
                Шаблон:
            </td><td>    <input type="text" name="template" value="<?php echo $template; ?>" style="width:300px;">
            <p class="exp">Например: Купить <b>{fraze}</b> в Киеве со скидкой.</p>
            </td>
        <td>
        <tr>
            <td> 
                Фразы:
            </td><td>
            <textarea name="words"><?php echo (!empty($_POST['words']) ? $_POST['words'] : ''); ?></textarea>
            <p class="exp">Каждая фраза с новой строки.</p>
            <p class="exp">Пример фразы: Мужская обувь</p>
        </td>
        </tr>
        <tr>
            <td> 
                
            </td><td>
                <input type="submit" name="do" value="Генерировать"/>
             </td>
        </tr>
        </table>
        
        <div class="separator"></div>
        <?php
            echo '<br>';
            if (isset($_POST['do'])) {
                if (!empty($_POST['words'])) {
                    echo '<h1>Результат склонения :: phpMorphy Client</h1>';
                    $declension = $_POST['declension'];
                    
                    $getWordsList = explode("\n", str_replace("\r", '', $_POST['words']));
                    
                    foreach ($getWordsList as $index => $word) {
                        $getWordsList[$index] = trim($word);
                        if ($getWordsList[$index] == '') { unset($getWordsList[$index]); continue; }
                    }
                    
                    $request = array(
                        'template' => empty($template) ? '{fraze}' : $template,
                        'words' => array_unique($getWordsList),
                        'declension' => $declension,
                        'plural' => !empty($plural) ? true : false
                    );
                    
                    $result = $morphy->init(
                        $request['template'],
                        $request['words'],
                        $request['declension'],
                        $request['plural']
                    );
                    
                    echo '<table class="main">
                            <tr class="header"><th>#</th><th>До</th><th>После</th></tr>';
                            $n = 1;
                    foreach ($result['phrases'] as $item) {
                        echo '<tr><td>' . $n . '</td><td>' . $item['before'] . '</td><td>' . $item['after'] . '</td></tr>';
                        $n++;
                    }
                    echo '</table>';
                }
            }
        ?>
    </form>
    <?
        $year = 2015;
        if ($year < (int)date('Y')) $year .= '-' . date('Y');
        echo '<div class="footer">&nbsp;</div><center>©  <a href="http://seo-studio.ua" target="_blank">SEO-Studio</a>, ' . $year . '</center>';
    ?>
    
</body>
</html>
