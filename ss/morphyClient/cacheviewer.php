<?php
// Читалка кеша 

//error_reporting(0);
ini_set('max_execution_time', 0);
mb_internal_encoding('UTF-8');
    
   
    
    

class cacheviewer {
    private $phpMorphy = false;
    private $inc = 1;
    private $login = 'seo-morphy';
    private $pass  = 'morphy2015';
    
    public function __construct() {
        if (file_exists(dirname(__FILE__) . '/lib/phpmorphy/src/common.php')) {
            require_once(dirname(__FILE__) . '/lib/phpmorphy/src/common.php');
        } else die('Not found file: ' . dirname(__FILE__) . '/lib/phpmorphy/src/common.php');
        if (file_exists(dirname(__FILE__) . '/MorphyClient.php')) {
            require_once(dirname(__FILE__) . '/MorphyClient.php');
        } else die('Not found file:' . dirname(__FILE__) . '/MorphyClient.php');
        
        $this->phpMorphy = new MorphyClient(false);
    }
    
    public function run() {
        $this->phpMorphy->loadCache();
        if (isset($_REQUEST['action'])) {
            if ($_REQUEST['action'] == 'delete') {
                $phrase     = $_REQUEST['phrase'];
                $plural     = mb_strtoupper(trim($_REQUEST['plural'])) == 'ЕД' ? false : true;
                $declension = $_REQUEST['declension'];
                $this->phpMorphy->resetCachePhrase($phrase, $declension, $plural);
                echo 'true';
                die();
            }
            elseif ($_REQUEST['action'] == 'deletetemp') {
                $phrase     = $_REQUEST['phrase'];
                $plural     = mb_strtoupper(trim($_REQUEST['plural'])) == 'ЕД' ? false : true;
                $declension = $_REQUEST['declension'];
                $this->phpMorphy->resetTempCachePhrase($phrase, $declension, $plural);
                echo 'true';
                die();
            }
            elseif ($_REQUEST['action'] == 'refresh') {
                $phrase     = $_REQUEST['phrase'];
                $plural     = mb_strtoupper(trim($_REQUEST['plural'])) == 'ЕД' ? false : true;
                $declension = $_REQUEST['declension'];
                $result = $this->phpMorphy->refreshCache($phrase, $declension, $plural);
                if ($result['result'] == 'success') {
                    echo $result['after'];
                } else {
                    echo 'error';
                }
                die();
            }
            elseif ($_REQUEST['action'] == 'update') {
                $phrase     = $_REQUEST['phrase'];
                $plural     = mb_strtoupper(trim($_REQUEST['plural'])) == 'ЕД' ? false : true;
                $declension = $_REQUEST['declension'];
                $setcache   = $_REQUEST['setcache'];
                $result = $this->phpMorphy->setCachePhrase($phrase, $setcache, $declension, $plural);
                echo 'true';
                die();
            } elseif ($_REQUEST['action'] == 'resend') {
                $this->phpMorphy = new MorphyClient(true);
                $this->phpMorphy->loadCache();
                $phrase     = $_REQUEST['phrase'];
                $plural     = mb_strtoupper(trim($_REQUEST['plural'])) == 'ЕД' ? false : true;
                $declension = $_REQUEST['declension'];
                $this->phpMorphy->resetTempCachePhrase($phrase, $declension, $plural);
                $response = $this->phpMorphy->init('{fraze}', array($phrase), $declension, $plural);
                if (isset($response['phrases'][$phrase])) {
                    echo '<b>ДО</b>:' . $response['phrases'][$phrase]['before'] . '<br><b>ПОСЛЕ</b>: ' . $response['phrases'][$phrase]['after'];
                } else {
                    echo 'error';
                }
                die();
            } else {
                die('Unknown action requested!');
            }
            
        } else {
            $this->echoCache();
        }
    }
    
    private function echoCache() {
        $buffer  = $this->echoHeader();
        $buffer .= $this->echoTableHeader();
        $buffer .= $this->echoData();
        $buffer .= $this->echoTableFooter();
        
        $buffer .= $this->echoHeader2();
        $buffer .= $this->echoTableHeader2();
        $buffer .= $this->echoData2();
        $buffer .= $this->echoTableFooter2();
        
        $buffer .= $this->echoInfo();
        $buffer .= $this->echoFooter();
        echo $buffer;
    }
    
    private function echoData2() {
        $buffer = '';
        //$this->cache = $this->phpMorphy->getCache();
        if (isset($this->cache['requests']) && count($this->cache['requests'])) { 
            foreach ($this->cache['requests'] as $phrase => $data) {
                $buffer .= $this->echoTableLines2($phrase, $data); 
            }
        }
        return $buffer;
    }
    
    private function echoData() {
        $buffer = '';
        $this->inc = 1;
        $this->cache = $this->phpMorphy->getCache();
        if (isset($this->cache['data']) && count($this->cache['data'])) { 
            foreach ($this->cache['data'] as $phrase => $data) {
                $buffer .= $this->echoTableLines($phrase, $data); 
            }
        }
        return $buffer;
    }
    
    private function echoTableLines2($phrase, $data) {
        foreach ($data as $declension => $_data) {
            foreach ($_data as $plural => $_inf) {
                $buffer  = '<tr id="_line_' . $this->inc . '">';
                $buffer .= '<td>' . $this->inc . '</td>';
                $buffer .= '<td id="_before_' . $this->inc . '">' . $phrase . '</td>';
                $buffer .= '<td id="_declension_' . $this->inc . '">' . $declension . '</td>';
                $buffer .= '<td id="_plural_' . $this->inc . '">' . ($plural ? 'МН' : 'ЕД') . '</td>';
                $buffer .= '<td>[ <a href="javascript:resend(' . $this->inc . ');">resend</a> ] [ <a href="javascript:delr(' . $this->inc . ');">del</a> ]</td>';
                $buffer .= '<td>' . $_inf['count'] . '</td>';
                $buffer .= '<td id="_time_' . $this->inc . '">' . date('d.m.Y H:i:s', $_inf['time']) . '</td>';
                $buffer .= '</tr>';
                $this->inc++;
            }
        }
        return $buffer;
    }
    private function echoTableLines($phrase, $data) {
        foreach ($data as $declension => $_data) {
            foreach ($_data as $plural => $after) {
                if (isset($this->cache['manualchange'][$phrase][$declension][$plural])) {
                        $inf = $this->cache['manualchange'][$phrase][$declension][$plural][1];
                } else {
                        $inf = ' - ';
                }
                $buffer  = '<tr id="line_' . $this->inc . '">';
                $buffer .= '<td>' . $this->inc . '</td>';
                $buffer .= '<td id="before_' . $this->inc . '">' . $phrase . '</td>';
                $buffer .= '<td id="after_' . $this->inc . '">' . $after . '</td>';
                $buffer .= '<td id="declension_' . $this->inc . '">' . $declension . '</td>';
                $buffer .= '<td id="plural_' . $this->inc . '">' . ($plural ? 'МН' : 'ЕД') . '</td>';
                $buffer .= '<td>[ <a href="javascript:edit(' . $this->inc . ');">edit</a> ] [ <a href="javascript:refresh(' . $this->inc . ');">refresh</a> ] [ <a href="javascript:del(' . $this->inc . ');">del</a> ]</td>';
                $buffer .= '<td id="inf_' . $this->inc . '">' . $inf . '</td>';
                $buffer .= '</tr>';
                $this->inc++;
            }
        }
        return $buffer;
    }
    
    private function echoInfo() {
        $cache = $this->phpMorphy->getCache();
        if (isset($cache['params'])) {
            return '
                Таймаут соединения: ' . $cache['params']['connect_timeout'] . '<br>
                Таймаут ожидания ответа: ' . $cache['params']['timeout'] . '<br>
                API url: ' . $cache['params']['url'] . '<br>
                ';
        } else {
            return 'Параметры настройки библиотеки хранимые в файле кеша не найдены. Библиотека использует параметры по умолчанию!';
        }
    }
    
    private function echoTableFooter2() {
        return '</table>';
    }
    private function echoTableFooter() {
        return '</table>';
    }
    private function echoTableHeader() {
        return '
            <table class="main full">
                <tr class="header">
                    <td>#</td>
                    <td>Оригинал фразы</td>
                    <td>Склоненная фраза</td>
                    <td>Падеж</td>
                    <td>Число</td>
                    <td>Ф-ции</td>
                    <td>Инфо</td>
                </tr>
        ';
    }
    private function echoTableHeader2() {
        return '
            <table class="main full">
                <tr class="header">
                    <td>#</td>
                    <td>Оригинал фразы</td> 
                    <td>Падеж</td>
                    <td>Число</td>
                    <td>Ф-ции</td>
                    <td>Время запроса</td>
                    <td>Число запросов</td>
                </tr>
        ';
    }
    
    private function echoFooter() { 
        $year = 2015;
        if ($year < (int)date('Y')) $year .= '-' . date('Y');
        return '<div class="footer">&nbsp;</div><center>©  <a href="http://seo-studio.ua" target="_blank">SEO-Studio</a>, ' . $year . '</center>';
    }
    
    public function auth() {
        
        session_start();
        if (isset($_REQUEST['exit'])) {unset($_SESSION['_morphy_login']);}
        
        if (isset($_POST['_login_m']) && isset($_POST['_pass_m']) && isset($_SERVER['HTTP_HOST'])) {
                if (!isset($_SESSION['_block']) || ($_SESSION['_block'] < time() - 3600)) {
                    $_SESSION['_morphy_login'] = $_POST['_login_m'];
                    $_SESSION['_morphy_pass'] = $_POST['_pass_m'];
                }
        }
        
        if (
                   isset($_SESSION['_morphy_login']) && $_SESSION['_morphy_login'] == $this->login
                && isset($_SESSION['_morphy_pass'])  && $_SESSION['_morphy_pass'] == $this->pass
                ) {
            return true;
        } else {
            echo $this->echoHeader() . '<br><br><br><br><br><br>';
            if (isset($_POST['_login_m'])) {
                if ($_POST['_login_m'] != $this->login) {
                    $_SESSION['try']++;
                    if ($_SESSION['try'] > 5) $_SESSION['block'] = time();
                    echo "<center style=\"color: red\">неверные данные авторизации!</center>";
                }
            }
            echo '<center><form method="POST" action="">
                    <table class="main" align="center">
                        <tr class="header">
                            <td colspan="2">&nbsp;<br>Авторизация:<br>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Логин:</td><td> <input type="text" name="_login_m" value="" style="width: 200px;"></td>
                        </tr>
                        <tr>
                            <td>Пароль:</td><td> <input type="password" name="_pass_m" value="" style="width: 200px;"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit" name="goAuth" value="Авторизироваться"></td>
                        </tr>
                    </table>
                  </form><br><br><br><br><br><br>';
            echo $this->echoFooter();
            return false;
        }
    }
    
    
    private function echoHeader() {
        return '
        <html>
            <head>
                <title>Cache Viewer :: phpMorphy Client</title>
                <meta charset="utf-8">
                <meta name="robots" content="noindex, nofollow">
                <style>
                    body { padding: 5px; font-family: Arial, Verdana, Tahoma, Trebuchet; font-size: 11px; }
                    h1 { font-size: 16px; padding: 5px; margin: 0px 0px 15px 0px; border-bottom: 1px dotted #999; color: #1E90FF;}
                    div.footer { border-bottom: 1px dotted #777;}
                    
                    table {font-size: 11px; border:0px; margin:5px 0px; }
                    table.main { border-collapse: collapse; font-size: 11px; border:0px; margin:5px 0px; }
                    table.full { width: 99%;}
                    table.main td { vertical-align: top; }
                    table.main td, table.main th { padding: 2px; border: 1px solid #BFBFBF; font-size: 11px; }
                    table.main tr:hover, .main_tr_gray {background: #E5E5E5; font-size: 12px; }
                    table.main   .main_tr_darkgray {background: #a9a9a9; text-align:center;}
                    table.main td, table.main th, .main_tr_gray { text-align:center; }
                    table.main td.left {text-align: left;}
                    table.main tr.header { background-color: #1E90FF; font-weight: bold; font-size: 12px; text-align: center; color: #fff;}
                    table.main tr.header:hover { background-color: #42A2FF; font-weight: bold;} 
                    table.main tr.gray td { color: #555;}
                    
                    .error {position: fixed; z-index: 99999; width: 500px; padding: 20px; background-color: #EFE; border: 2px solid #f00; left: 50%; top: 200px; margin-left: -270px; opacity: 0.8;}
                </style>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                <script>window.jQuery || alert(\'No jQuery!\')</script>
                <script>
                    // редактирование
                    var temp = false;
                    function edit(_id) {
                        var fraze = $("#after_" + _id).html();
                        temp = fraze;
                        $("#after_" + _id).html(\'<input type="text" value="\' + fraze + \'" id="edit_\' + _id + \'"> [ <a href="javascript:savephrase(\' + _id + \')">save</a> ] [ <a href="javascript:backphrase(\' + _id + \')">cancel</a> ]\');
                    }
                    
                    function backphrase(_id, fraze) {
                        $("#after_" + _id).html(temp);
                    }
                    
                    // Обновить
                    function savephrase(_id) {
                       var phrase     = $("#before_" + _id).html();
                       var setcache     = $("#edit_" + _id).val();
                       var plural     = $("#plural_" + _id).html();
                       var declension = $("#declension_" + _id).html();
                       
                       if (confirm("Вы действительно хотите обновить кеш по фразе: \n\n" + phrase)) {
                            $("#line_" + _id).fadeTo(100, 0.5);
                            $.ajax({
                                 type: "POST",	url: "cacheviewer.php" , dataType: "html",
                                 data: ({ action: "update", phrase: phrase,  setcache: setcache, plural: plural,  declension: declension}),
                                 success: function(response){  
                                                                if (response == "true") {
                                                                    $("#line_" + _id).fadeTo(500, 1); 
                                                                    $("#after_" + _id).html(setcache);
                                                                    $("#inf_" + _id).html(\'<span style="color: green;">Изменено</span>\');
                                                                }
                                                                else { 
                                                                        $("body").prepend(\'<div class="error" onClick="$(this).remove();">\' + response + \'</div>\');
                                                                        setTimeout(function(){ $(".error").fadeOut(1000, function(){ $(this).remove();} ); }, 4000);
                                                                      }
                                                                
                                                                 },
                                 error:   function(response){  alert(\'Не удалось выполнить операцию!\');}
                             });
                       }
                    }
                    
                    // Удалить
                    function del(_id) {
                       var phrase     = $("#before_" + _id).html();
                       var plural     = $("#plural_" + _id).html();
                       var declension = $("#declension_" + _id).html();
                       
                       if (confirm("Вы действительно хотите удалить кеш по фразе: \n\n" + phrase)) {
                            $("#line_" + _id).fadeTo(100, 0.5);
                            $.ajax({
                                 type: "POST",	url: "cacheviewer.php" , dataType: "html",
                                 data: ({ action: "delete", phrase: phrase,  plural: plural,  declension: declension}),
                                 success: function(response){  
                                                                if (response == "true") $("#line_" + _id).fadeOut(500, function(){ $(this).remove(); }); 
                                                                else { 
                                                                        $("body").prepend(\'<div class="error" onClick="$(this).remove();">\' + response + \'</div>\');
                                                                        setTimeout(function(){ $(".error").fadeOut(1000, function(){ $(this).remove();} ); }, 4000);
                                                                      }
                                                                
                                                                 },
                                 error:   function(response){  alert(\'Не удалось выполнить операцию!\');}
                             });
                       }
                    }
                    
                    // Удалить TEMP
                    function delr(_id) {
                       var phrase     = $("#_before_" + _id).html();
                       var plural     = $("#_plural_" + _id).html();
                       var declension = $("#_declension_" + _id).html();
                       
                       if (confirm("Вы действительно хотите удалить временынй кеш запроса по фразе: \n\n" + phrase)) {
                            $("#_line_" + _id).fadeTo(100, 0.5);
                            $.ajax({
                                 type: "POST",	url: "cacheviewer.php" , dataType: "html",
                                 data: ({ action: "deletetemp", phrase: phrase,  plural: plural,  declension: declension}),
                                 success: function(response){  
                                                                if (response == "true") $("#_line_" + _id).fadeOut(500, function(){ $(this).remove(); }); 
                                                                else { 
                                                                        $("body").prepend(\'<div class="error" onClick="$(this).remove();">\' + response + \'</div>\');
                                                                        setTimeout(function(){ $(".error").fadeOut(1000, function(){ $(this).remove();} ); }, 4000);
                                                                      }
                                                                
                                                                 },
                                 error:   function(response){  alert(\'Не удалось выполнить операцию!\');}
                             });
                       }
                    }
                    
                    
                    // Обновить
                    function refresh(_id) {
                        var phrase     = $("#before_" + _id).html();
                        var plural     = $("#plural_" + _id).html();
                        var declension = $("#declension_" + _id).html(); 
                        
                        $("#line_" + _id).fadeTo(100, 0.5);
                        $.ajax({
                             type: "POST",	url: "cacheviewer.php" , dataType: "html",
                             data: ({ action: "refresh", phrase: phrase,  plural: plural,  declension: declension}),
                             success: function(response){  
                                                            if (response != "error") {
                                                                $("#line_" + _id).fadeTo(100, 1, function(){ 
                                                                        $("#after_" + _id).html(response);
                                                                        $("#inf_" + _id).html(\'<span style="color: green;">Обновлено</span>\');
                                                                        
                                                                 }); 
                                                            }
                                                            else { 
                                                                    $("#inf_" + _id).html(\'<span style="color: red;">Ошибка</span>\');
                                                                    $("body").prepend(\'<div class="error" onClick="$(this).remove();">Кеш не удалось обновить...</div>\');
                                                                    setTimeout(function(){ $(".error").fadeOut(1000, function(){ $(this).remove();} ); }, 4000);
                                                                  }
                                                            
                                                             },
                             error:   function(response){  alert(\'Не удалось выполнить операцию!\');}
                         });
                        
                    }
                    
                    // Обновить
                    function resend(_id) {
                        var phrase     = $("#_before_" + _id).html();
                        var plural     = $("#_plural_" + _id).html();
                        var declension = $("#_declension_" + _id).html(); 
                        
                        $("#line_" + _id).fadeTo(100, 0.5);
                        $.ajax({
                             type: "POST",	url: "cacheviewer.php" , dataType: "html",
                             data: ({ action: "resend", phrase: phrase,  plural: plural,  declension: declension}),
                             success: function(response){  
                                                            if (response != "error") {
                                                                $("#_line_" + _id).fadeTo(100, 1, function(){ 
                                                                       $("#_time_" + _id).html(response);
                                                                 }); 
                                                            }
                                                            else { 
                                                                    $("body").prepend(\'<div class="error" onClick="$(this).remove();">Не удалось обновить...</div>\');
                                                                    setTimeout(function(){ $(".error").fadeOut(1000, function(){ $(this).remove();} ); }, 4000);
                                                                  }
                                                            
                                                             },
                             error:   function(response){  alert(\'Не удалось выполнить операцию!\');}
                         });
                        
                    }
                </script>
            </head>
            <body>
            <h1>Cache Viewer :: phpMorphy Client</h1>
        ';
    }
    private function echoHeader2() {
        return '
            <h1>Temp requests cache Viewer :: phpMorphy Client</h1>
        ';
    }
        
        
}


$cacheviewer = new cacheviewer();
if ($cacheviewer->auth()) $cacheviewer->run();
unset($cacheviewer);
