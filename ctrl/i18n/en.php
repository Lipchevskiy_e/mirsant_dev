<?php

	$_i18n_ = array(
		'error500' => 'Server can\'t processed your request. Try again latter.',
		'error404' => 'Server can\'t find requested page.',

	// DB
		'debug_tab_mysql'		=> 'MySQL',
		'mysql_connect_faild' 	=> 'DB server connect failed',
		'mysql_db_select_faild' => 'Db select failed',

	// UPLOAD
		'upload_error_file_size' 			=> 'File size',
		'upload_error_invalid_file_type' 	=> 'Wrong file type',
		'upload_error_moving_file_failed' 	=> 'File copy failed',
		'upload_error_file_not_uploaded' 	=> 'File upload failed',
		'upload_error_empty_file_field' 	=> 'File not selected',

	// MESSAGES
		'message_error_captcha' 	=> 'You input the wrong security code. <br> Repeat, please, send data, carefully pointing out the code.',
		'message_add_comment_to_news' 	=> 'Thank you for leaving a comment. <br> With your help, our site becomes more interesting and useful. <br> Site Administration must review your materials and publish them soon.',
		'message_after_registration' 	=> 'Thank you for registering on our site! Enjoy!',
		'message_text_after_registration_user_at_site'=>'Thank you for registering on our site! <br /> In your email, specified at registration, notification sent the data for input into your cabinet on the site. <br /> Enjoy!',
		'message_after_autorisation' 	=> 'Data is entered correctly! Enjoy!',
		'message_error_autorisation' 	=> 'Data is not entered correctly! Be careful!',
		'message_text_after_autorisation_user_at_site'=>'Welcome to our site! <br /> Use personal cabinet for: Edit your data and view your purchase history. <br /> Enjoy!',

		'message_after_exit' 	=> 'Come back again!',
		'message_text_after_exit' 	=> 'Site administration thanks you for your time spent on our site. See you soon!',

		'message_error_login' 	=> 'This login already exists. <br> Repeat, please, changing the login.',

		'message_text_after_edit_data' 	=> 'Thank you for your attention to our site. <br /> In your email, specified at registration, notified to the changed data. <br /> Enjoy!',

		'message_after_edit_data' 	=> 'Your data changed. Nice work.',

		'message_after_q_about_good' 	=> 'Your question is accepted. Manager will respond to you shortly.',

		'message_after_oformleniya_basket' 	=> 'Thank you for your order. <br /> Manager will contact you shortly.',

		'message_add_contact' 			=> 'Thank you for your message!',
		

		
		// подписка
		'subscribe_refuse' 				=> 'You refused to send online!',
		'subscribe_refuse_error' 		=> 'You are not subscribed to our newsletter on the site!',
		'subscribe_already01' 			=> 'already a subscriber to the site!',
		'subscribe_already02' 			=> 'enter a different email to sign up.',
		'subscribe_done' 			=> 'You are subscribed to the site',		

		'message_add_comment_to_guestbook' 	=> 'Благодарим вас за оставленный отзыв. <br>Администрация сайта обязательно рассмотрит ваши материалы и опубликует их в ближайшее время.',		
		

	// CAPTCHA
		'captcha_input_number'  => 'Enter the number shown in the picture',
		'captcha_refresh_image' => 'Refresh image',

	// PAGER
		'pager_page'	=>	'Pages',

	// BACKEND

		'after_save'	=>	'Data saved!',
		'backend_after_delete'	=>	'Record deleted!',
		'backend_after_delete_photo'	=>	'Photo deleted!',
		'backend_orror_access'	=>	'Access denied!',
		'backend_after_delete_file'	=>	'File deleted!',
		'backend_upload_reklama_error_type'	=>	'Invalid format of downloadable banner!',


	// ФОРМЫ

		// Контакты
		'form_title_contact' => 'Send message',
		'form_subject_from'  => 'Section - Contact',
		'mail_from_site'     => 'Mail send from',
		'your_name' => 'Your name',
		'email'     => 'Your e-mail',
		'phone'     => 'Phone',
		'country'   => 'City (country)',
		'message'   => 'Message',
		'send'      => 'Send',
		'currency'	=> 'uah',
		'price'		=> 'Price',
		'buy_itm'	=> 'Buy',
		
		// Timing

		'today' => 'Today',
		'yesterday' => 'Yesterday',
		'tomorrow' => 'Tomorrow',

		// Dates

		'January' => 'January',
		'February' => 'February',
		'March' => 'March',
		'April' => 'April',
		'May' => 'May',
		'June' => 'June',
		'July' => 'July',
		'August' => 'August',
		'September' => 'September',
		'October' => 'October',
		'November' => 'November',
		'December' => 'December',	
		
		'year' => 'year',		
		
	);