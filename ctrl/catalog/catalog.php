<?php

global $_str;
global $_sklad, $_sklad_color;

$deletedBrands = array(120, 121, 35, 154, 148, 106);
$deletedItems = array(
    '_dushevaja_kabina_eger_sarvar_599_503_95_95',
    'akrilovaja_vanna_cersanit_cersania_150_150_46_',
    'akrilovaja_vanna_cersanit_santana_140_70',
    'akrilovaja_vanna_cersanit_santana_150_70_41',
    'akrilovaja_vanna_cersanit_santana_160_70_41',
    'akrilovaja_vanna_cersanit_santana_170_70_41',
    'akrilovaja_vanna_ravak_evolution_120_102_lr',
    'bide_ido_mozaik_51019_65_001_napolnoe',
    'bide_ido_pozzi_ginori_easy_51221_01_napolnoe',
    'bide_roca_doma_senso_357515000_podvesnoe',
    'boks_gidromassazhnyj_sansa_z_002mrz_90_90',
    'dushevaja_gidromassazhnaja_panel_valentin_buggy_521400_001_00_',
    'dushevaja_kabina_am_pm_bliss_round_120_80_r',
    'dushevaja_kabina_aquaform_enta_105_14099_120_85_l',
    'dushevaja_kabina_aquaform_variabel_101_26911_80_90',
    'dushevaja_kabina_kolo_first_zkpg90214003_90_90',
    'dushevaja_shtorka_dlja_vanny_aquaform_standard_3_170_04000',
    'dushevoj_boks_atlantis_akl_150_87_218',
    'dushevoj_boks_dusrux_a071_115_85_216_lr',
    'dushevoj_boks_esk_1011_100_100_215_',
    'dushevoj_boks_esk_8911_90_90_215',
    'dushevoj_boks_golston_g_u6810_163_163_220_chernyj',
    'dushevoj_boks_miracle_100_80',
    'dushevoj_poddon_cersanit_90_90_16',
    'dushevoj_poddon_kolo_terra_xbk1710_100_100',
    'dushevoj_ugol_cersanit_tesalia_90_90',
    'gidroboks_atlantis_akl_60p_7_90_90_215',
    'gidroboks_atlantis_akl_60pb_90_90_215',
    'gidroboks_esk_9924lt_90_90_215',
    'gidroboks_grandehome_ws103rs6_120_90_224_lr_s_parom',
    'gidroboks_sansa_z_002mrz_100_100',
    'gidromassazhnyj_boks_atlantis_akl_60p_16_90_90_215',
    'gidromassazhnyj_boks_atlantis_akl_60p_5_90_90_215',
    'gidromassazhnyj_boks_atlantis_akl_60p_t_90_90_215',
    'gidromassazhnyj_boks_atlantis_bh_3010_120_82_215_lr',
    'gidromassazhnyj_boks_caribe_x161_lr_rz_110_80_215_lr',
    'gidromassazhnyj_boks_eco_san_z52_90_90_215',
    'gidromassazhnyj_boks_fpollo_a_0886_120_80_215',
    'gidromassazhnyj_boks_golston_g_u6810_163_163_220_belyj',
    'gidromassazhnyj_boks_golston_g_u696_115_115_220_belyj_tv',
    'installjatsija_s_unitazom_siamp_complect_soft',
    'installjatsija_unitaz_siamp_trend',
    'installjatsionnaja_sistema_cersanit_aqua_dlja_unitaza_',
    'nitaz_napolnyj_cersanit_pure_',
    'penal_kolo_twins_88322',
    'penal_mojdodyr_nika_28_sm',
    'penal_mojdodyr_nika_33_sm',
    'poddon_kolo_terra_xbn1710_100_100',
    'poddon_kolo_terra_xbp1729_120_90',
    'rakovina_ido_trevi_11185_01_56_sm',
    'rakovina_laufen_mylife_10947_95_sm',
    'rakovina_laufen_vienna_10038_80_sm',
    'rakovina_laufen_vienna_comfort_13478_80_sm',
    'rakovina_villeroy_boch_century_7350_50_r1_50_sm',
    'rakovina_villeroy_boch_subway_6169_55_01_55_sm',
    'rakovina_villeroy_boch_verity_51797001_70_sm',
    'sansa_z002rz_gidromassazhnyj_boks_90_90',
    'shkaf_podvesnoj_mojdodyr_parma_',
    'shkafchik_podvesnoj_mojdodyr_blondi_36_sm',
    'shtorka_dlja_vanny_aquaform_standard_3_170_04010',
    'smesitel_dlja_dusha_s_termostatom_am_pm_serenity_f4240000_hrom',
    'smesitel_dlja_kuhni_grohe_eurodisk_33334001_2_v_1_',
    'smesitel_dlja_kuhni_kludi_mix_33_906_36_62_belyj',
    'smesitel_dlja_kuhni_kludi_trendo_star_33_976_96_75_nerzhavejuschaja_stal',
    'smesitel_dlja_kuhni_kludi_trendo_star_33_977_05_75_hrom',
    'smesitel_dlja_kuhni_welle_fm56087d_hrom',
    'smesitel_dlja_rakoviny_elektronnyj_kludi_balance_52_100_05_hrom_new',
    'smesitel_dlja_rakoviny_elektronnyj_kludi_balance_52_202_05_hrom_new',
    'smesitel_dlja_rakoviny_s_gidrozatvorom_grohe_arabesk_21155000_hrom',
    'umyvalnik_laufen_gallery_14176_102_sm',
    'umyvalnik_laufen_gallery_16165_47_sm_uglovoj',
    'umyvalnik_laufen_lb3_12683_55_sm',
    'umyvalnik_laufen_lb3_13681105_80_sm',
    'umyvalnik_laufen_vienna_15304_50_sm',
    'umyvalnik_roca_ibis_320841001_44_sm',
    'unitaz_kolo_ego_k13000_pristavnoj',
    'unitaz_kompakt_am_pm_bliss_c558607wh_s_sideniem',
    'unitaz_kompakt_gustavsberg_artic_11_4310_napolnyj_',
    'unitaz_kompakt_ido_trevi_37092_01_napolnyj',
    'unitaz_kompakt_roca_america_342497000_napolnyj',
    'unitaz_kompakt_roca_khroma_34265700080165af3t_napolnyj_belyjkrasnyj',
    'unitaz_laufen_lb3_24684_napolnyj',
    'unitaz_roca_hall_346627000801622004_podvesnoj_kompakt',
    'unitaz_villeroy_boch_bellevua_566510r1_napolnyj',
    'vanna_akrilovaja_aquaform_arabella_169_75_',
    'vanna_akrilovaja_cersanit_virgo_140_90_lr',
    'vanna_akrilovaja_fibrex_water_nory_125_125',
    'vanna_akrilovaja_kolo_mirra_xwp3340000_140_75',
    'vanna_akrilovaja_villeroy_boch_sabway_18090',
    'vanna_assimetrichnaja_aquaform_simi_150_80_l',
    'vanna_assimetrichnaja_aquaform_simi_150_80_r',
    'vanna_stalnaja_kolo_princessa_220470001_150_75',
    'zerkalo_mojdodyr_bogatyr_120_100',
);
$deletedGroups = array(
    'bojlery',
);

	/*ночное время для цен товаров*/
	$time_from = strtotime(mysql::query_findpole('select zna from config where id=1022','zna'));
	$time_to = strtotime(mysql::query_findpole('select zna from config where id=1023','zna'));
	$time_night = mysql::query_findpole('select zna from config where id=1024','zna');

if (isset($_GET['grupa'])) {
    if (in_array($_GET['grupa'], $deletedGroups)) {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: http://" . $_SERVER['HTTP_HOST']);
        exit();
    }

    //var_dump($_GET['grupa']);
    //system::isset_numeric($_GET['grupa']);

    /* Фильтры */
    $_s = 0;
    $_c = 0;
    $_b = 0;
    $_w = 0;
    $_h = 0;
    $_t = 0;
    $_ser = 0;

    $_tray_depth = 0;
    $_cabin_form = 0;
    $_color = 0;
    $_mounting_type = 0;
    $_mixer_style = 0;
    $_mixer_type = 0;
    $_bathtub_form = 0;
    $_toilet_mount = 0;


    if (isset($_GET['filter'])) {
        $tmp = explode('?', $_GET['filter']);
        $_GET['filter'] = $tmp[0];
        $filter = explode(';', $_GET['filter']);
        foreach ($filter as $f) {
            $arr = explode('_', $f);

            switch ($arr[0]) {
                case 'sort':
                    $sort = ' ORDER BY sort_cost ';
                    if ($arr[1] == 'max-min')
                        $sort .= ' DESC';
                    $_s = 1;
                    break;

                case 'cost':
                    $_c = 1;
                    $c = explode('-', $arr[1]);
                    $cost = ' AND ((catalog.cost>=' . $c[0] . ' and catalog.cost<=' . $c[1] . ' and catalog.valuta=1) OR (catalog.cost>=' . $c[0] / EVRO . ' and catalog.cost<=' . $c[1] / EVRO . ' and catalog.valuta=2) OR (catalog.cost>=' . $c[0] / DOLLAR . ' and catalog.cost<=' . $c[1] / DOLLAR . ' and catalog.valuta=3)) ';
                    break;

                case 'brand':

                    $uri = $_SERVER['REQUEST_URI'];
                    $newUri = preg_replace('/brand_/', 'brands_', $uri);

                    header("HTTP/1.1 301 Moved Permanently");
                    header("Location: http://" . $_SERVER['HTTP_HOST'] . $newUri);
                    exit();

                    break;

                case 'brands':
                    $_b = 1;

                    $tmpArr = explode(',', $arr[1]);

                    $brands_chosen_arr = $tmpArr; //explode(',', $brands);
                    if (count($brands_chosen_arr)) {
                        $category_alias = $_GET['grupa'];
                        if ($category_alias == 'dushevie_boksi') {
                            $brands_redirect_arr = array(22, 23, 24, 36, 167);
                        }
                        if ($category_alias == 'dushevye_kabiny') {
                            $brands_redirect_arr = array(23, 73);
                        }
                        if ($category_alias == 'smesiteli') {
                            $brands_redirect_arr = array(41, 149);
                        }
                        if ($category_alias == 'mebel_dlya_vannoi') {
                            $brands_redirect_arr = array(99, 108, 152);
                        }
                        if ($category_alias == 'unitazi') {
                            $brands_redirect_arr = array(45, 48, 67);
                        }
                        if ($category_alias == 'umivalniki') {
                            $brands_redirect_arr = array(45, 48, 67);
                        }

                        if (isset($brands_redirect_arr)) {
                            $wrong_brand = false;
                            foreach ($brands_chosen_arr as $chosen_brand) {
                                if (in_array($chosen_brand, $brands_redirect_arr)) {
                                    $wrong_brand = true;
                                }
                            }

//                            if ($wrong_brand) {
//                                header("HTTP/1.1 301 Moved Permanently");
//                                header("Location: http://mirsant.com.ua/catalog/grupa/" . $category_alias);
//                                exit();
//                            }
                        }
                    }

                    $inDeleted = false; // flag
                    foreach ($tmpArr as $key => $value) {
                        if (in_array($value, $deletedBrands)) {
                            // Delete from filter
                            unset($tmpArr[$key]);
                            $inDeleted = true;
                        }
                    }

                    // Redirect if need
                    if ($inDeleted == true) {
                        $uri = $_SERVER['REQUEST_URI'];
                        $newUri = preg_replace('/brands_[0-9,]+/', 'brands_' . implode(',', $tmpArr), $uri);

                        header("HTTP/1.1 301 Moved Permanently");
                        header("Location: http://" . $_SERVER['HTTP_HOST'] . $newUri);
                        exit();
                    }

                    $bb = mysql::query('SELECT id FROM brand WHERE id IN(' . $arr[1] . ') AND brand.status=1');

                    if (count($bb) <> count($tmpArr)) {
                        header('HTTP/1.1 404 Not Found');
                        include '404.html';
                        exit;
                    }

                    $brands = ' AND catalog.brand IN(' . $arr[1] . ') ';
                    break;

                case 'series':
                    $_ser = 1;
                    $series = ' AND catalog.serie IN(' . $arr[1] . ') ';
                    break;

                case 'width':
                    $_w = 1;
                    $width = ' AND catalog.width=' . $arr[1] . ' ';
                    break;

                case 'length':
                    $_h = 1;
                    $length = ' AND catalog.length=' . $arr[1] . ' ';
                    break;

                case 'type':
                    $_t = 1;
                    $type = ' AND catalog.has_type IN(' . $arr[1] . ') ';
                    break;

                case 'tray-depth':
                    $_tray_depth = 1;
                    $tray_depth = ' AND catalog.tray_depth IN (' . $arr[1] . ') ';
                    break;

                case 'cabin-form':
                    $_cabin_form = 1;
                    $cabin_form = ' AND catalog.cabin_form IN(' . $arr[1] . ') ';
                    break;

                case 'color':
                    $_color = 1;
                    $color = ' AND catalog.color IN (' . $arr[1] . ') ';
                    break;

                case 'mounting-type':
                    $_mounting_type = 1;
                    $mounting_type = ' AND catalog.mounting_type IN(' . $arr[1] . ') ';
                    break;

                case 'mixer-style':
                    $_mixer_style = 1;
                    $mixer_style = ' AND catalog.mixer_style IN (' . $arr[1] . ') ';
                    break;

                case 'mixer-type':
                    $_mixer_type = 1;
                    $mixer_type = ' AND catalog.mixer_type IN(' . $arr[1] . ') ';
                    break;

                case 'bathtub-form':
                    $_bathtub_form = 1;
                    $bathtub_form = ' AND catalog.bathtub_form IN(' . $arr[1] . ') ';
                    break;

                case 'toilet-mount':
                    $_toilet_mount = 1;
                    $toilet_mount = ' AND catalog.toilet_mount IN(' . $arr[1] . ') ';
                    break;

                case 'forma-gidromassazhnoj-vanny':
                    $_gidr_van = 1;
                    $gidr_van = ' AND catalog.gidr_van IN(' . $arr[1] . ') ';
                    break;

                case 'tip-montazha-tumby-dlja-vannoj':
                    $_tumb_van = 1;
                    $tumb_van = ' AND catalog.tumb_van IN(' . $arr[1] . ') ';
                    break;

                case 'tip-montazha-penalov-dlja-vannoj':
                    $_penal_van = 1;
                    $penal_van = ' AND catalog.penal_van IN(' . $arr[1] . ') ';
                    break;

                case 'tip-montazha-bide':
                    $_bide_mont = 1;
                    $bide_mont = ' AND catalog.bide_mont IN(' . $arr[1] . ') ';
                    break;

                case 'tip-montazha-kuhonnyh-moek':
                    $_kuh_mont = 1;
                    $kuh_mont = ' AND catalog.kuh_mont IN(' . $arr[1] . ') ';
                    break;

                case 'kolichestvo-sektsij-kuhonnoj-mojki':
                    $_kuh_count = 1;
                    $kuh_count = ' AND catalog.kuh_count IN(' . $arr[1] . ') ';
                    break;

                default:
                    header('HTTP/1.1 404 Not Found');
                    include '404.html';
                    exit;
            }
        }
    }

    if ($_s == 0)
        $sort = ' ORDER BY sort ';

    if ($_c == 0)
        $cost = ' ';

    if ($_b == 0)
        $brands = ' ';

    if ($_t == 0)
        $type = ' ';

    if ($_w == 0)
        $width = ' ';

    if ($_h == 0)
        $length = ' ';

    if ($_ser == 0)
        $series = ' ';

    if ($_tray_depth == 0)
        $tray_depth = ' ';
    if ($_cabin_form == 0)
        $cabin_form = ' ';
    if ($_color == 0)
        $color = ' ';
    if ($_mounting_type == 0)
        $mounting_type = ' ';
    if ($_mixer_style == 0)
        $mixer_style = ' ';
    if ($_mixer_type == 0)
        $mixer_type = ' ';
    if ($_bathtub_form == 0)
        $bathtub_form = ' ';
    if ($_toilet_mount == 0)
        $toilet_mount = ' ';
    /* Конец фильтра */

    $__id = mirsant::get_id_grupa_by_url();

    // строим запрос
    $_sql = 'SELECT * FROM catalog_tree where id_parent=' . $__id . ' and status=1 ORDER BY sort';

    // выполняем запрос + при необходимости выводим сам запрос
    $result = mysql::query($_sql, 0);

    if ($result) {
        // check for count news at page
        catalog::count_at_page();
        $limit = pager::pager_limit(catalog::get_count(), CATALOG_AT_PAGE);

        $_sql = 'SELECT catalog.*, brand.pole as brand_name, brand.skidka as brand_skidka, catalog_tree.skidka as catalog_skidka,  catalog.cost*valuta.kurs as sort_cost
					FROM catalog
					LEFT JOIN brand ON catalog.brand=brand.id
					LEFT JOIN catalog_tree ON catalog_tree.id=catalog.id_parent
					LEFT JOIN valuta ON valuta.id=catalog.valuta
					WHERE catalog.id_parent=' . $__id . ' and catalog.status=1' . $cost . $brands . $width . $series . $length . $type . $tray_depth . $cabin_form . $color . $mounting_type . $mixer_style . $mixer_type . $bathtub_form . $toilet_mount . $gidr_van . $tumb_van . $penal_van . $bide_mont . $kuh_mont . $kuh_count. $sort . '
					LIMIT ' . $limit;

        $items = mysql::query($_sql, 0);

        $items_tpl = '';
        if(count($items)){
            $items_tpl .= system::show_tpl(array('result' => $items, 'time_from' => $time_from, 'time_to' => $time_to, 'time_night' => $time_night) , 'frontend/catalog/list.php');
        }

        // выводим группы товаров
        $_str.=system::show_tpl(array('result' => $result, 'items_tpl' => $items_tpl), 'frontend/catalog/grupa.php');
    } else {

        // check for count news at page
        catalog::count_at_page();

        // get limit 
        $limit = pager::pager_limit(catalog::get_count(), CATALOG_AT_PAGE);

        // сортировка
        //	$_sort=catalog::get_sort_at_page();
        // фильтрация
        //$_filter=catalog::get_filter();
        // строим запрос
        $_sql = 'SELECT catalog.*, brand.pole as brand_name, brand.skidka as brand_skidka, catalog_tree.skidka as catalog_skidka,  catalog.cost*valuta.kurs as sort_cost
					FROM catalog
					LEFT JOIN brand ON catalog.brand=brand.id
					LEFT JOIN catalog_tree ON catalog_tree.id=catalog.id_parent
					LEFT JOIN valuta ON valuta.id=catalog.valuta
					WHERE catalog.id_parent=' . $__id . ' and catalog.status=1' . $cost . $brands . $width . $series . $length . $type . $tray_depth . $cabin_form . $color . $mounting_type . $mixer_style . $mixer_type . $bathtub_form . $toilet_mount . $gidr_van . $tumb_van . $penal_van . $bide_mont . $kuh_mont . $kuh_count .  $sort . '
					LIMIT ' . $limit;

        // die($_sql);
        // выполняем запрос + при необходимости выводим сам запрос

        /* if($__id == 17 && $_SERVER['REMOTE_ADDR'] == '178.136.229.251'){
          $result=mysql::query($_sql, 1);
          } else {
          $result=mysql::query($_sql);
          } */
        $result = mysql::query($_sql);

        $sql__ = "SELECT * FROM catalog_tree WHERE url='" . $_GET['grupa'] . "'";
        $res__ = mysql::query_one($sql__);

        if ($result) {
            $_str.=system::show_tpl(array('result' => $result, 'grupa' => $res__, 'time_from' => $time_from, 'time_to' => $time_to, 'time_night' => $time_night), 'frontend/catalog/list.php');
            unset($_SESSION['no_result']);
        } else {
            $_SESSION['no_result'] = 1;
            $_str.=system::show_tpl(array('result' => $result, 'grupa' => $res__, 'time_from' => $time_from, 'time_to' => $time_to, 'time_night' => $time_night), 'frontend/catalog/list.php');
        }
    }
} else if (isset($_GET['goods'])) {
    //var_dump($_GET['goods']);
    if (in_array($_GET['goods'], $deletedItems)) {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: http://" . $_SERVER['HTTP_HOST']);
        exit();
    }


    $__id = mirsant::get_id_goods_by_url();

    /*     * ***********************
     * Check for redirect
     * ************************ */
    // Get current Item
    $currItem = mysql::query_one('SELECT id, brand, id_parent FROM catalog WHERE id = ' . $__id, 0);
    if (in_array($currItem->brand, $deletedBrands)) {
        // Get parrent alias
        $parentUrl = mysql::query_findpole('SELECT url FROM catalog_tree WHERE id = ' . $currItem->id_parent, 'url', 0);
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: http://" . $_SERVER['HTTP_HOST'] . "/catalog/grupa/" . $parentUrl);
        exit();
    }



    $post = array();
    $errors = array();
    if (isset($_POST['sendGB'])) {
        $post = $_POST['FORM'];
        if (!empty($post)) {
            if (!isset($post['name']) OR trim($post['name']) == '')
                $errors[] = 'Поле "ФИО" не может быть пустым!';
            if (!isset($post['email']) OR trim($post['email']) == '')
                $errors[] = 'Поле "E-Mail" не может быть пустым!';
            if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL))
                $errors[] = 'Вы ввели некоректный E-Mail!';
            if (!isset($post['text']) OR trim($post['text']) == '')
                $errors[] = 'Поле "Отзыв" не может быть пустым!';
            if (mb_strlen(trim($post['text'])) < 10)
                $errors[] = 'Отзыв не может быть на столько коротким!';

            if (empty($errors)) {
                $res = mysql::query_only('INSERT INTO catalog_comments (status,created_at,name,email,text,id_goods) VALUES (0,"' . time() . '","' . $post['name'] . '","' . $post['email'] . '","' . $post['text'] . '","' . $__id . '")');
                if ($res) {
                    $_SESSION['messageGB'] = 'Спасибо за ваш отзыв. Он поступил администратору для проверки на спам и вскоре будет опубликован.';
                    header('Location:' . $_SERVER['REQUEST_URI']);
                    exit(0);
                } else {
                    $errors[] = 'К сожалению, данный отзыв не может быть оставлен.';
                }
            }
        } else {
            $errors[] = 'Вы не ввели никаких данных!';
        }
    }



    // кол-во просмотров + 1
    $_sql = "UPDATE catalog SET kolvoview=kolvoview+1 WHERE id=" . $__id;
    $result = mysql::just_query($_sql, 0);

    // строим запрос
    $_sql = 'SELECT catalog.*, 
                    brand.pole as brand_name, 
                    brand.skidka as brand_skidka, 
                    catalog_tree.skidka as catalog_skidka, 
                    catalog_tree.payment,
                    catalog_tree.delivery,
                    catalog_tree.services,
                    catalog_tree.cost_services,
                    catalog_tree.warranty
				FROM catalog
				LEFT JOIN brand ON catalog.brand=brand.id
				LEFT JOIN catalog_tree ON catalog_tree.id=catalog.id_parent
				WHERE catalog.id=' . $__id . ' and catalog.status=1';
    // выполняем запрос + при необходимости выводим сам запрос
    $result = mysql::query_one($_sql);
    /* echo '<pre>';
      print_r($result);
      echo '</pre>';
      die; */

    if ($result) {

        $_sql = 'SELECT catalog.*, 
                    brand.pole as brand_name, 
                    brand.skidka as brand_skidka, 
                    catalog_tree.skidka as catalog_skidka
					FROM catalog
					LEFT JOIN brand ON catalog.brand=brand.id
					LEFT JOIN catalog_tree ON catalog_tree.id=catalog.id_parent
					WHERE catalog.id != "' . $__id . '" and catalog.id_parent = "' . $result->id_parent . '" and catalog.status=1
					LIMIT 4';
        $same = mysql::query($_sql);
		
		$_sql = 'SELECT catalog_tree.*
					FROM catalog_tree
					WHERE catalog_tree.id = "' . $result->id_parent . '"';
        $group = mysql::query_one($_sql);
	

        $form = system::show_tpl(array('post' => $post, 'errors' => $errors), 'frontend/catalog/form.php');
        $comments = mysql::query('SELECT * FROM catalog_comments WHERE status = 1 AND id_goods = ' . $__id . ' ORDER BY created_at DESC');
        $comments = system::show_tpl(array('result' => $comments), 'frontend/catalog/comments.php');
        $payment = $group->text_oplata ? $group->text_oplata :  mysql::query_findpole('select zna from config where id=1010','zna');
        $delivery = $group->text_dostavka ? $group->text_dostavka : mysql::query_findpole('select zna from config where id=1011','zna');
        $services = $group->text_etag ? $group->text_etag : mysql::query_findpole('select zna from config where id=1012','zna');
        $cost_services = $group->text_gruz ? $group->text_gruz : mysql::query_findpole('select zna from config where id=1013','zna');
        $warranty = $group->text_garant ? $group->text_garant : mysql::query_findpole('select zna from config where id=1014','zna');
        $info = mysql::query_findpole('select zna from config where id=1015','zna');
        $_str.=system::show_tpl(array('warranty' => $warranty, 'info' => $info, 'cost_services' => $cost_services, 'services' => $services, 'delivery' => $delivery, 'payment' => $payment, 'same' => $same, 'obj' => $result, '_sklad' => $_sklad, 'sklad_color' => $_sklad_color, 'form' => $form, 'comments' => $comments), 'frontend/catalog/show.php');
    } else {
        header('HTTP/1.1 404 Not Found');
        include '404.html';
        exit;
        $_str.='Выбранный вами товар отсутствует.';
    }
} else if (isset($_POST['id'])) {

    if ($_POST) {

        if (!registration::is_Autorize() and ! captcha::check_captcha()) {
            //выводим сообщение
            Message::GetMessage(0, v::getI18n('message_error_captcha'));

            echo "<meta http-equiv='refresh' content='2;URL=/catalog/goods/" . $_POST['id'] . "'>";
        } else {

            //---------------------------------------------------------//
            //---------- Отправляем мыло админу------------------------//
            //---------------------------------------------------------//
            // парсим тело письма
            $_arr = ___findarray('select * from message where id=7');
            // елементы для замены
            $mass_element_for_parsing = array('%fio%', '%email%', '%q%', '%id%', '%link%', '%name_good%');
            // заменяеміе значения
            // имя товара
            $_arr1 = ___findarray('select * from catalog where id=' . intval($_POST['id']));

            // если неавторизован
            if (!registration::is_Autorize()) {

                $mass_result_for_parsing = array($_POST['FORM']['fio'], $_POST['FORM']['email'], $_POST['FORM']['q'], $_POST['id'], MAIN_PATH . '/catalog/goods/' . $_POST['id'], $_arr1['name']);
                $_email = $_POST['FORM']['email'];
            } else {

                $_user = registration::get_User($_SESSION['log']);
                $mass_result_for_parsing = array($_user->name, $_user->email, $_POST['FORM']['q'], $_POST['id'], MAIN_PATH . '/catalog/goods/' . $_POST['id'], $_arr1['name']);
                $_email = $_user->email;
            }
            // парсим данные
            $message = parsing_data($mass_element_for_parsing, $mass_result_for_parsing, $_arr['text']);

            // парсим заголовок письма
            $subject = replace_data($_arr['zag'], '%site%', ADRESS_SITE);

            // отправляем мыло
            sent_email_new($GLOBALS["mailadmin"], nl2br($message), "", $subject, $_email);

            //---------------------------------------------------------//
            //---------------------------------------------------------//
            //---------------------------------------------------------//				
            //выводим сообщение
            Message::GetMessage(1, v::getI18n('message_after_q_about_good'));

            echo "<meta http-equiv='refresh' content='2;URL=/catalog/goods/" . $_POST['id'] . "'>";

            // очищаем POST
            unset($_POST);
        }
    }
} else {

    // выводим все группы верхнего уровня
    // строим запрос
    $_sql = 'SELECT * FROM catalog_tree where id_parent=0 and status=1 order by sort';
    // выполняем запрос + при необходимости выводим сам запрос
    $result = mysql::query($_sql, 0);

    if ($result) {
        // выводим группы товаров
        $_str.=system::show_tpl(array('result' => $result), 'frontend/catalog/grupa.php');
    } else {
        $_str.='В данной группе товары отсутствуют.';
    }
}
