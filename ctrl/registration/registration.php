<?php

global $_str;
		
//если регистрируемся
if (system::isset_ctrl('formregistration')) {

	switch ($_GET['formregistration']) {
	
	
		// новый пользователь
		case "fpasword":
				
	
			// добавление
			if ($_POST) {

				if (!captcha::check_captcha()) {
					//выводим сообщение
			    	Message::GetMessage(0, v::getI18n('message_error_captcha'));
			    	
					// форма регистрации
					$_str=system::show_tpl(array('city'=>dbh::get_city()),"frontend/registration/fpasword.php");
			    
				} else if (!registration::is_Mail($_POST['FORM']['email'])) {	
					
					//выводим сообщение
			    	Message::GetMessage(0, v::getI18n('mail_error_login'));					
					
					// убиваем логин
					unset($_POST['FORM']['email']);
					
					// форма регистрации
					$_str=system::show_tpl(array('city'=>dbh::get_city()),"frontend/registration/fpasword.php");						
						
			    } else {
				    	
					// получаем данные пользователя
					$_user=registration::get_User_By_Email($_POST['FORM']['email']);
					
					// авторизуем пользователя
					//registration::autorize_User($_user);
					
					//---------------------------------------------------------//
					//---------- Отправляем мыло useru ------------------------//
					//---------------------------------------------------------//

					// парсим тело письма
					$_arr=___findarray('select * from message where id=9');
					// елементы для замены
					$mass_element_for_parsing=array('%login%','%password%','%site%','%name%','%hash%');
					// заменяеміе значения
					$mass_result_for_parsing=array($_user->login,$_user->pasword,ADRESS_SITE,$_user->name,'http://'.ADRESS_SITE.'/registration/formregistration/enter/id/'.$_user->hash);
					// парсим данные
					$message=parsing_data($mass_element_for_parsing,$mass_result_for_parsing,$_arr['text']);					
			
					// парсим заголовок письма
			        $subject=replace_data($_arr['zag'],'%site%',ADRESS_SITE);
			        
			        // отправляем мыло
					sent_email_new($_user->email,nl2br($message),"",$subject,$GLOBALS["mailadmin"]);
			
					//---------------------------------------------------------//
					//---------------------------------------------------------//
					//---------------------------------------------------------//					
						
					// выводим мообщение
					Message::GetMessage(1, v::getI18n('email_after_registration'));
					
					// форма регистрации
					$_str=system::show_tpl(
						array(
							'text'=>v::getI18n('mail_text_after_registration_user_at_site')
						),"frontend/registration/welcome.php");
						
					// очищаем POST
					unset($_POST);
					
				//	echo "<meta http-equiv='refresh' content='5;URL=/index.php'>";

						
		    	 }				
				

			} else {
						
				// форма регистрации
				$_str=system::show_tpl(array('city'=>dbh::get_city()),"frontend/registration/fpasword.php");
				
			}
			break;		
	
		
		// новый пользователь
		case "new":
				
	
			// добавление
			if ($_POST) {

				if (!captcha::check_captcha()) {
					//выводим сообщение
			    	Message::GetMessage(0, v::getI18n('message_error_captcha'));
			    	
					// форма регистрации
					$_str=system::show_tpl(array('city'=>dbh::get_city()),"frontend/registration/new.php");
			    
				} else if (registration::is_Login($_POST['FORM']['login'])) {	
					
					// если такой логин уже существует
					
					//выводим сообщение
			    	Message::GetMessage(0, v::getI18n('message_error_login'));					
					
					// убиваем логин
					unset($_POST['FORM']['login']);
					
					// форма регистрации
					$_str=system::show_tpl(array('city'=>dbh::get_city()),"frontend/registration/new.php");						
						
			    } else {
				    	
					// генерим ХЕШ для директ ИД
					$_POST['FORM']['hash']=md5($_POST['FORM']['login'].getrandmax());
					// пишем в базу
					Forms::MultyInsertForm('users',0);
					
					// получаем данные пользователя
					$_user=registration::get_User($_POST['FORM']['login']);
					
					// авторизуем пользователя
					registration::autorize_User($_user);
					
					//---------------------------------------------------------//
					//---------- Отправляем мыло useru ------------------------//
					//---------------------------------------------------------//

					// парсим тело письма
					$_arr=___findarray('select * from message where id=2');
					// елементы для замены
					$mass_element_for_parsing=array('%login%','%password%','%site%','%name%','%hash%');
					// заменяеміе значения
					$mass_result_for_parsing=array($_user->login,$_user->pasword,ADRESS_SITE,$_user->name,'http://'.ADRESS_SITE.'/registration/formregistration/enter/id/'.$_user->hash);
					// парсим данные
					$message=parsing_data($mass_element_for_parsing,$mass_result_for_parsing,$_arr['text']);					
			
					// парсим заголовок письма
			        $subject=replace_data($_arr['zag'],'%site%',ADRESS_SITE);
			        
			        // отправляем мыло
					sent_email_new($_user->email,nl2br($message),"",$subject,$GLOBALS["mailadmin"]);
			
					//---------------------------------------------------------//
					//---------------------------------------------------------//
					//---------------------------------------------------------//					
						
					// выводим мообщение
					Message::GetMessage(1, v::getI18n('message_after_registration'));
					
					// форма регистрации
					$_str=system::show_tpl(
						array(
							'text'=>v::getI18n('message_text_after_registration_user_at_site')
						),"frontend/registration/welcome.php");
						
					// очищаем POST
					unset($_POST);
					
				//	echo "<meta http-equiv='refresh' content='5;URL=/index.php'>";

						
		    	 }				
				

			} else {
						
				// форма регистрации
				$_str=system::show_tpl(array('city'=>dbh::get_city()),"frontend/registration/new.php");
				
			}
			break;
			
			
		// вход на сайт
		case "enter":
				
			// добавление
			if ($_POST or isset($_GET['id'])) {

				// проверяем на логин и пароль
				if (registration::enter_User ($_POST['FORM']['login'], $_POST['FORM']['pasword']) or registration::enter_User_open_id ($_GET['id'])) {
					
					if ($_POST) {
						// получаем данные пользователя
						$_user=registration::get_User($_POST['FORM']['login']);
					} else if (isset($_GET['id'])) {
						// получаем данные пользователя
						$_user=registration::get_User_open_id($_GET['id']);
					}
					
					// авторизуем пользователя
					registration::autorize_User($_user);
					
					
					// выводим мообщение
					Message::GetMessage(1, v::getI18n('message_after_autorisation'));
					
					// форма регистрации
					$_str=system::show_tpl(
						array(
							'text'=>v::getI18n('message_text_after_autorisation_user_at_site')
						),"frontend/registration/welcome.php");
							
					// проверяем откуда авторизовались
				/*	if (isset($_POST['basket'])) {
						echo "<meta http-equiv='refresh' content='5;URL=".$_SERVER["HTTP_REFERER"]."'>";
					} else {
						echo "<meta http-equiv='refresh' content='5;URL=/index.php'>";
					}*/
					
					// очищаем POST
					unset($_POST);
			
				} else {
					
					// выводим мообщение
					Message::GetMessage(1, v::getI18n('message_error_autorisation'));

					// форма регистрации
					$_str=system::show_tpl(array(),"frontend/registration/enter.php");
					
				}

			} else {
						
				// форма регистрации
				$_str=system::show_tpl(array(),"frontend/registration/enter.php");
				
			}
			break;		
			

		// выход с сайта
		case "exit":
				
			registration::exit_User ();
					
			// выводим мообщение
			Message::GetMessage(1, v::getI18n('message_after_exit'));
					
			// форма регистрации
			$_str=system::show_tpl(
				array(
					'text'=>v::getI18n('message_text_after_exit')
				),"frontend/registration/welcome.php");
				
		//	echo "<meta http-equiv='refresh' content='5;URL=/index.php'>";
							
				
			break;		

			
		// редактируем данные
		case "edit":
				
	
			// добавление
			if ($_POST) {

		    
				if (registration::is_Login_while_edit($_POST['FORM']['login'], $_SESSION['user_id'])) {	
					
					// если такой логин уже существует
					
					//выводим сообщение
			    	Message::GetMessage(0, v::getI18n('message_error_login'));					
					
					// возвращаем логин
					$_POST['FORM']['login']=$_SESSION['log'];
					
					// получаем данные пользователя
					$_user=registration::get_User($_SESSION['log']);						
					
					// форма регистрации
					$_str=system::show_tpl(
						array('obj'=>$_user)
					,"frontend/registration/edit.php");					
						
			    } else {
			    	
					// проверка на авторизованного юзера
					if (!registration::is_Autorize()) system::error(404);
				    	
					// пишем в базу
					Forms::multy_update_form('users',$_SESSION['user_id'],0);
					
					// возвращаем логин
					$_SESSION['log']=$_POST['FORM']['login'];
					
					// получаем данные пользователя
					$_user=registration::get_User($_SESSION['log']);
					
					// авторизуем пользователя
					registration::autorize_User($_user);
					
					//---------------------------------------------------------//
					//---------- Отправляем мыло useru ------------------------//
					//---------------------------------------------------------//

					// парсим тело письма
					$_arr=___findarray('select * from message where id=6');
					// елементы для замены
					$mass_element_for_parsing=array('%login%','%password%','%site%','%name%','%hash%');
					// заменяеміе значения
					$mass_result_for_parsing=array($_user->login,$_user->pasword,ADRESS_SITE,$_user->name,'http://'.ADRESS_SITE.'/registration/formregistration/enter/id/'.$_user->hash);
					// парсим данные
					$message=parsing_data($mass_element_for_parsing,$mass_result_for_parsing,$_arr['text']);					
			
					// парсим заголовок письма
			        $subject=replace_data($_arr['zag'],'%site%',ADRESS_SITE);
			        
			        // отправляем мыло
					sent_email_new($_user->email,nl2br($message),"",$subject,$GLOBALS["mailadmin"]);
			
					//---------------------------------------------------------//
					//---------------------------------------------------------//
					//---------------------------------------------------------//					
						
					// выводим мообщение
					Message::GetMessage(1, v::getI18n('message_after_edit_data'));
					
					// форма регистрации
					$_str=system::show_tpl(
						array(
							'text'=>v::getI18n('message_text_after_edit_data')
						),"frontend/registration/welcome.php");
						
					// очищаем POST
					unset($_POST);
					
				//	echo "<meta http-equiv='refresh' content='5;URL=/registration/formregistration/edit'>";

						
		    	 }				
				

			} else {
				
				// проверка на авторизованного юзера
				if (!registration::is_Autorize()) system::error(404);				
					
				// получаем данные пользователя
				$_user=registration::get_User($_SESSION['log']);				
				
				// форма регистрации
				$_str=system::show_tpl(
					array('obj'=>$_user,'city'=>dbh::get_city())
				,"frontend/registration/edit.php");
				
			}
			break;					
					
	}	
			
}
		
?>