<?php

 	global $_str;
 	
	$id=false;
	if (isset($_GET['id'])) { 
		$id=$_GET['id'];
	}
	if (isset($_POST['id'])) { 
		$id=$_POST['id'];
	}
	

 	if ($id) {
 		
 		system::isset_numeric($id);
 		if (isset($_GET['type_sort'])) system::isset_numeric($_GET['type_sort']);
 		if (isset($_GET['at_page'])) system::isset_numeric($_GET['at_page']);
 		
	 	// check for count news at page
		catalog::count_at_page();				
					
		// get limit 
		$limit=pager::pager_limit(brand::get_count(),CATALOG_AT_PAGE);				
			
		// сортировка
		$_sort=catalog::get_sort_at_page();
			
		// фильтрация
		$_filter=brand::get_filter();

			
		// строим запрос
		$_sql='SELECT catalog.*, brand.pole as brand_name 
					FROM catalog, brand 
					WHERE catalog.brand=brand.id and catalog.status=1 and brand.status=1 '.$_filter.' 
					ORDER BY '.$_sort.' 
					LIMIT '.$limit;
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		// наименование бренда
		$_brand=___findpole('select pole from brand where id='.$id,'pole');
		
		if ($result) {
			$_str.=system::show_tpl(array('result'=>$result,'_brand'=>$_brand), 'frontend/catalog/brand/list.php');
		} else {
			$_str.='У данного бренда товары отсутствуют.';
		}
 	
	} else {
		
		// выводим все бренды
		
		// get limit 
		$limit=pager::pager_limit(brand::get_count(),mysql::query_findpole('select zna from config where id=47','zna'));		
		
		// строим запрос
		$_sql='SELECT
					brand.*,
					count(catalog.id) as count
				FROM
					brand
				Left Outer Join catalog ON brand.id=catalog.brand
				WHERE brand.id>0 and brand.status=1
				GROUP BY
					brand.id
				order by brand.sort
				LIMIT '.$limit;
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		$_str.=system::show_tpl(array('result'=>$result),'frontend/catalog/brand/all_brands.php');		
		
	}
	
?>