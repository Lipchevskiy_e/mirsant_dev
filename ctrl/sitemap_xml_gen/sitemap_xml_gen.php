<?php
$domain = 'http://'.$_SERVER['HTTP_HOST'];
$last_mod = '2015-04-03T11:50:59+00:00';
$r_last_mod = date(DATE_ATOM, time());
$def_priority = '0.6400';

$special_groups = array(
    'akrilovie_vanni',
    'iskusstvennyj_mramor',
    'stalnie_vanni',
    'chugunnie_vanni',

    'dushevie_boksi',
    'dushevye_kabiny',
    'mebel_dlya_vannoi',
    'unitazi',
    'plitka',
    'smesiteli',
);

/*$cats = array(
    'vanni' => 0.5,
	'mebel_dlya_vannoi' => 0.6400,
    'dushevie_boksi' => 0.6400,
    'umivalniki' => 0.6400,
    'unitazi' => 0.6400,
    'smesiteli' => 0.6400,
    'akrilovie_vanni' => 0.6400,
    'stalnie_vanni' => 0.6400,
    'chugunnie_vanni' => 0.6400,
    'gidromassajnie_vanni' => 0.6400,
    'installyacii' => 0.6400,
    'shtorki_na_vannu' => 0.6400,
    'gidromassazhnye_paneli' => 0.6400,
    'plitka' => 0.6400,
    'dushevye_kabiny' => 0.6400,
    'iskusstvennyj_mramor' => 0.6400,
);*/

$xml = '';
$xml.= '<?xml version="1.0" encoding="UTF-8"?>'."\n";
$xml.= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

    $_sql = 'SELECT id, action FROM content WHERE status = 1 ORDER BY id ASC';
    $content = mysql::query($_sql,0);

    $_sql = 'SELECT id, url as action,
            has_type,
            has_mixer,
            has_shower,
            has_toilet,
            has_bathtub
            FROM catalog_tree WHERE status = 1 ORDER BY id_parent ASC, sort ASC';
    $categories = mysql::query($_sql,0);

    $_sql = 'SELECT id, url as action, id_parent,
            brand,
            serie,
            has_type,
            tray_depth,
            cabin_form,
            color,
            mounting_type,
            mixer_style,
            mixer_type,
            toilet_mount,
            bathtub_form
            FROM catalog WHERE status = 1 ORDER BY id DESC';
    $products = mysql::query($_sql,0);


    $content[]->action = 'news';
    if(count($content)){
        foreach($content as $content_item){
            if($content_item->action == 'index'){ $content_item->action = ''; }
            $xml.= '<url>'."\n";
                $xml.= '<loc>'.$domain.'/'.$content_item->action.'</loc>'."\n";
                $xml.= '<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                $xml.= '<changefreq>monthly</changefreq>'."\n";
                $xml.= '<priority>1.0000</priority>'."\n";
            $xml.= '</url>'."\n";
        }
    }

    if(count($categories)){
        foreach($categories as $categories_item){
            if($categories_item->action == 'index'){ $categories_item->action = ''; }
            $xml.= '<url>'."\n";
                $xml.= '<loc>'.$domain.'/catalog/grupa/'.$categories_item->action.'</loc>'."\n";
                $xml.= '<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                $xml.= '<changefreq>monthly</changefreq>'."\n";
                $xml.= '<priority>0.8000</priority>'."\n";
            $xml.= '</url>'."\n";
        }

        foreach($categories as $categories_item){
            if(in_array($categories_item->action, $special_groups)){
                $something = catalog::getCatFeatures($categories_item);
                if(count($something)){
                    $origins_link = $domain.'/catalog/grupa/'.$categories_item->action.'/filter';
                    foreach($something as $feature_alias => $little_something){
                        if($feature_alias != 'series'){
                            foreach($little_something as $tiny_something){
                                $xml.= '<url>'."\n";
                                $xml.= "\t".'<loc>'.$origins_link.'/'.$feature_alias.'_'.$tiny_something->id.'</loc>'."\n";
                                $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                $xml.= '</url>'."\n";
                            }
                        }
                    }
                }
            }
        }
/*
 * brands
 * series
 * type
 * bathtub-form
 * toilet-mount
 * tray-depth
 * cabin-form
 * color
 * mounting-type
 * mixer-style
 * mixer-type
 * */
        foreach($categories as $categories_item){
            if(in_array($categories_item->action, $special_groups) && $categories_item->action != 'smesiteli'){
                $something = catalog::getCatFeatures($categories_item);
                if(count($something)){
                    $origins_link = $domain.'/catalog/grupa/'.$categories_item->action.'/filter';

                    if($something['brands']){
                        foreach($something['brands'] as $brand){
                            if($something['series']){
                                foreach($something['series'] as $level1){
                                    foreach($level1 as $brand_id => $series){
                                        if($series->id_brand == $brand->id){
                                            $xml.= '<url>'."\n";
                                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'</loc>'."\n";
                                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                            $xml.= '</url>'."\n";
                                            /**********brand > series > feature ***************** */
                                            if($something['type']){
                                                foreach($something['type'] as $level2){
                                                    $xml.= '<url>'."\n";
                                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/type_'.$level2->id.'</loc>'."\n";
                                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                    $xml.= '</url>'."\n";
                                                }
                                            }
                                            if($something['bathtub-form']){
                                                foreach($something['bathtub-form'] as $level2){
                                                    $xml.= '<url>'."\n";
                                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/bathtub-form_'.$level2->id.'</loc>'."\n";
                                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                    $xml.= '</url>'."\n";
                                                }
                                            }
                                            if($something['toilet-mount']){
                                                foreach($something['toilet-mount'] as $level2){
                                                    $xml.= '<url>'."\n";
                                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/toilet-mount_'.$level2->id.'</loc>'."\n";
                                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                    $xml.= '</url>'."\n";
                                                }
                                            }
                                            if($something['tray-depth']){
                                                foreach($something['tray-depth'] as $level2){
                                                    $xml.= '<url>'."\n";
                                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/tray-depth_'.$level2->id.'</loc>'."\n";
                                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                    $xml.= '</url>'."\n";
                                                    if($something['cabin-form']){
                                                        foreach($something['cabin-form'] as $level3){
                                                            $xml.= '<url>'."\n";
                                                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/tray-depth_'.$level2->id.'/cabin-form_'.$level3->id.'</loc>'."\n";
                                                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                            $xml.= '</url>'."\n";
                                                        }
                                                    }
                                                }
                                            }
                                            if($something['cabin-form']){
                                                foreach($something['cabin-form'] as $level2){
                                                    $xml.= '<url>'."\n";
                                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/cabin-form_'.$level2->id.'</loc>'."\n";
                                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                    $xml.= '</url>'."\n";
                                                }
                                            }
                                            if($something['color']){
                                                foreach($something['color'] as $level2){
                                                    $xml.= '<url>'."\n";
                                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/color_'.$level2->id.'</loc>'."\n";
                                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                    $xml.= '</url>'."\n";
                                                }
                                            }
                                            if($something['mounting-type']){
                                                foreach($something['mounting-type'] as $level2){
                                                    $xml.= '<url>'."\n";
                                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/mounting-type_'.$level2->id.'</loc>'."\n";
                                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                    $xml.= '</url>'."\n";
                                                }
                                            }
                                            if($something['mixer-style']){
                                                foreach($something['mixer-style'] as $level2){
                                                    $xml.= '<url>'."\n";
                                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/mixer-style_'.$level2->id.'</loc>'."\n";
                                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                    $xml.= '</url>'."\n";
                                                }
                                            }
                                            if($something['mixer-type']){
                                                foreach($something['mixer-type'] as $level2){
                                                    $xml.= '<url>'."\n";
                                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/series_'.$series->id.'/mixer-type_'.$level2->id.'</loc>'."\n";
                                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                                    $xml.= '</url>'."\n";
                                                }
                                            }
                                            /*************************** */
                                        }
                                    }
                                }
                            }
                            /****** brand > feature ********************* */
                            if($something['type']){
                                foreach($something['type'] as $level1){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/type_'.$level1->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                            if($something['bathtub-form']){
                                foreach($something['bathtub-form'] as $level1){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/bathtub-form_'.$level1->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                            if($something['toilet-mount']){
                                foreach($something['toilet-mount'] as $level1){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/toilet-mount_'.$level1->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                            if($something['tray-depth']){
                                foreach($something['tray-depth'] as $level1){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/tray-depth_'.$level1->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                            if($something['cabin-form']){
                                foreach($something['cabin-form'] as $level1){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/cabin-form_'.$level1->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                            if($something['color']){
                                foreach($something['color'] as $level1){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/color_'.$level1->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                            if($something['mounting-type']){
                                foreach($something['mounting-type'] as $level1){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/mounting-type_'.$level1->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                            if($something['mixer-style']){
                                foreach($something['mixer-style'] as $level1){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/mixer-style_'.$level1->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                            if($something['mixer-type']){
                                foreach($something['mixer-type'] as $level1){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/mixer-type_'.$level1->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                            /*************************** */

                        }
                    }


                    /****** feature > feature ********************* */
                    /*if($something['type']){
                        foreach($something['type'] as $level1){
                            $xml.= '<url>'."\n";
                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/type_'.$level1->id.'</loc>'."\n";
                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                            $xml.= '</url>'."\n";
                        }
                    }*/
                    /*if($something['bathtub-form']){
                        foreach($something['bathtub-form'] as $level1){
                            $xml.= '<url>'."\n";
                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/bathtub-form_'.$level1->id.'</loc>'."\n";
                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                            $xml.= '</url>'."\n";
                        }
                    }*/
                   /* if($something['toilet-mount']){
                        foreach($something['toilet-mount'] as $level1){
                            $xml.= '<url>'."\n";
                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/toilet-mount_'.$level1->id.'</loc>'."\n";
                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                            $xml.= '</url>'."\n";
                        }
                    }*/
                    if($something['tray-depth'] && $something['cabin-form']){
                        foreach($something['tray-depth'] as $level1){
                            $xml.= '<url>'."\n";
                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/tray-depth_'.$level1->id.'</loc>'."\n";
                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                            $xml.= '</url>'."\n";
                            if($something['cabin-form']){
                                foreach($something['cabin-form'] as $level2){
                                    $xml.= '<url>'."\n";
                                    $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/tray-depth_'.$level1->id.'/cabin-form_'.$level2->id.'</loc>'."\n";
                                    $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                                    $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                                    $xml.= "\t".'<priority>0.8000</priority>'."\n";
                                    $xml.= '</url>'."\n";
                                }
                            }
                        }
                    }
                    /*if($something['cabin-form']){
                        foreach($something['cabin-form'] as $level1){
                            $xml.= '<url>'."\n";
                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/cabin-form_'.$level1->id.'</loc>'."\n";
                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                            $xml.= '</url>'."\n";
                        }
                    }*/
                    /*if($something['color']){
                        foreach($something['color'] as $level1){
                            $xml.= '<url>'."\n";
                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/color_'.$level1->id.'</loc>'."\n";
                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                            $xml.= '</url>'."\n";
                        }
                    }*/
                    /*if($something['mounting-type']){
                        foreach($something['mounting-type'] as $level1){
                            $xml.= '<url>'."\n";
                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/mounting-type_'.$level1->id.'</loc>'."\n";
                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                            $xml.= '</url>'."\n";
                        }
                    }*/
                    /*if($something['mixer-style']){
                        foreach($something['mixer-style'] as $level1){
                            $xml.= '<url>'."\n";
                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/mixer-style_'.$level1->id.'</loc>'."\n";
                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                            $xml.= '</url>'."\n";
                        }
                    }*/
                    /*if($something['mixer-type']){
                        foreach($something['mixer-type'] as $level1){
                            $xml.= '<url>'."\n";
                            $xml.= "\t".'<loc>'.$origins_link.'/brands_'.$brand->id.'/mixer-type_'.$level1->id.'</loc>'."\n";
                            $xml.= "\t".'<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                            $xml.= "\t".'<changefreq>monthly</changefreq>'."\n";
                            $xml.= "\t".'<priority>0.8000</priority>'."\n";
                            $xml.= '</url>'."\n";
                        }
                    }*/
                    /*************************** */


                }
            }
        }
    }


    /*
    if(count($products)){
        foreach($products as $products_item){
            if($products_item->action == 'index'){ $products_item->action = ''; }
            $xml.= '<url>'."\n";
                $xml.= '<loc>'.$domain.'/catalog/goods/'.$products_item->action.'</loc>'."\n";
                $xml.= '<lastmod>'.$r_last_mod.'</lastmod>'."\n";
                $xml.= '<changefreq>monthly</changefreq>'."\n";
                $xml.= '<priority>'.$def_priority.'</priority>'."\n";
            $xml.= '</url>'."\n";
        }
    }
    */

$xml.= '<!-- Generated by The Dark side -->'."\n";
$xml.= '</urlset>';

$fp = fopen(HOST.'/sitemap_new.xml','w+');
fwrite($fp, $xml);
fclose($fp);

header('Content-Type: text/html; charset=utf-8', true);
echo '<a href="/sitemap_new.xml"">Карта сайта</a>';
die();
/*
<url>
   <loc>http://mirsant.com.ua/</loc>
   <lastmod>2015-04-03T11:50:59+00:00</lastmod>
   <changefreq>monthly</changefreq>
   <priority>1.0000</priority>
</url>
*/