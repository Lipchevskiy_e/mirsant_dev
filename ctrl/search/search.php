<?php

require_once HOST . "/classes/search.class.php";

global $_str;

$search = '';
if (isset($_GET['word']) and ! empty($_GET['word'])) {
    $search = new search($_GET['word']);
    $_SESSION['word'] = serialize($search);
}


if (is_object($search)) {
    $_str = $search->get_content();
} else {
    $_str = search::no_search();
}