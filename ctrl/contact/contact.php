<?php

	$_str = '';

	// Контентовая страница "Контакты"
	if (if_content($_GET['action'])) {
		$_str .= get_content($_GET['action']);
	}

	// Обработка нажатия кнопки
	if (isset($_POST['send'])) {
		// Подключаем капчу
		require_once  HOST.'/captcha/securimage.php';
		$image = new Securimage();

		// Если капча не пройдена
		if (!$image->check($_POST['hislo'])) {
			 // Выводим сообщение
			Message::GetMessage(0, v::getI18n('message_error_captcha'));
		}

		// Если пройдена - отправляем письмо
		else {
			// проверяем заказан ли товар
			if (isset($_SESSION['id_goods'])) {
				// ищем даннеы о товаре
				$_data_good="<br><br>Заказан следующий товар:<br>".$_SESSION['name_goods']." - ".$_SESSION['name_goods']."$";
				} else {
				$_data_good="";
			}

			// Массив для данных из формы
			// (его значения подставляются в шаблоны для отправки сообщений)
			$data = array();

			// Получаем данные из формы
			foreach($_POST['FORM'] as $key => $value) {
				if ($key == 'message') {
					$data[$key] = nl2br(filter::varpost($value));
				}
				else {
					$data[$key] = filter::varpost($value);
				}
			}

			//----------------------------------------------
			// формируем письмо и отправляем его АДМИНУ
			//----------------------------------------------
			$to      = $GLOBALS['mailadmin'];
			$from    = $data['email'];
			$subject = ADRESS_SITE.', '.__('form_subject_from');
			$message = system::show_tpl($data, 'frontend/contact/contact_mail_admin.tpl.php');
			sent_email_new($to, $message, "", $subject,$from);

			//----------------------------------------------
			// формируем письмо и отправляем его ЮЗЕРУ
			//----------------------------------------------
			$to      = $data['email'];
			$from    = $GLOBALS['mailadmin'];
			$subject = __('mail_from_site').' '.ADRESS_SITE.', '.__('form_subject_from');
			$message = system::show_tpl($data, 'frontend/contact/contact_mail.tpl.php');
			sent_email_new($to, $message, "", $subject,$from);

			////////////////////////////////////////////////////////////////////
			Message::GetMessage(1, v::getI18n('message_add_contact'));
			
		}

		// Удаляем переменные сесси товара
		unset($_SESSION['id_goods']);
		unset($_SESSION['name_goods']);
		unset($_SESSION['cost_goods']);
	}

////////////////////////////////////////////////////////////////////////////////

	// Инициализация переменных для формы
	$data = array(
		'name'    => '',
		'email'   => '',
		'phone'   => '',
		'country' => '',
		'message' => '',
		'imggg'   => captcha::get_captcha()
	);

	// Если уже вводили данные и не прошли капчу
	if (isset($_POST['FORM']) AND !$image->check($_POST['hislo'])) {
		// Заполняем массив значениями из формы
		foreach($_POST['FORM'] as $key => $value) {
			$data[$key] = filter::varpost($value);
		}
	}

	// Рендерим шаблон
	$_str .= system::show_tpl($data, 'frontend/contact/contact.tpl.php');
?>