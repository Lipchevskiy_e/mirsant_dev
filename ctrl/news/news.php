 <?php
 
 		global $_str;

		// check for crack!
		if (isset($_GET['page'])) { 
			system::count_parametr(4);
			system::isset_numeric($_GET['page']); 
		} else {
			system::count_parametr(3);
		 	$_GET['page']=1;
		} 

		// добавление новости
		if (system::IsPost()) {
			
			if (!captcha::check_captcha()) {
				//выводим сообщение
		    	Message::GetMessage(0, v::getI18n('message_error_captcha'));
				
		    } else {
		    	
				// пишем в базу
				Forms::MultyInsertForm('news_gb',0);
				
				// очищаем POST
				unset($_POST);

				// выводим мообщение
				Message::GetMessage(1, v::getI18n('message_add_comment_to_news'));
				
	    	 }	

		} 
			

		if (isset($_GET['url'])) {
				
			// выводим нужную новость
			$_str.= news::show_url('/frontend/news/show.php');	
			
			// Записываем тип новости в $_GET для правильной подсветки меню, когда находимся в новости
			$_GET['type'] = news::get_news_type($_GET['url']);

			// вывод комментариев + формы для добавления при необходимости
			if (COMMENT_NEWS==1)  {

		 		// check for count news at page
				news::count_comment_news_at_page();				
					
				// get limit 
				$limit=pager::pager_limit(news::count_comment_news(news::get_news_id($_GET['url'])),COMMENT_NEWS_AT_PAGE);						
				// вывод формы для добавления комментов к новости
				$_str.= news::comment_show($limit,news::get_news_id($_GET['url']),'/frontend/news/comment_show.php');	
					
				// вывод формы для добавления комментов к новости
				$_str.= news::comment_add('/frontend/news/comment_add.php');	
			}
				
		} else {
		
	 		// check for count news at page
			news::count_news_at_page();				
				
			// параметры для поиска (НАЧАЛО)
			//$_get_param=get_dop_param_news();
				
			// get limit 
			$limit=pager::pager_limit(news::count_news(),NEWS_AT_PAGE);
				
			// show news with pager
			$_str.= news::show_news($limit,'/frontend/news/list.php');	
			
		}
				
		
?>