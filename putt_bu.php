<?php

// Наименование б/д
$dbname="mirsant";

// дата
$what_is_date=date('Y-m-d');
$what_is_time=date('G-i');

// Для баз
$hostname="localhost";
$username="mirsant";
$password="UJmfOXgc";

// коннектимся
mysql::connect();


// абсолютный путь
$putt="http://".$_SERVER [ 'HTTP_HOST'];


define ('MAIN_PATH',"http://".$_SERVER [ 'HTTP_HOST']);
define ('HOST',dirname(__FILE__));
define('CTRL_PATH',		HOST.'/ctrl');
define('TPL_PATH',		HOST.'/tpl');
define('MODEL_PATH',	HOST.'/classes');
define('PLAGINS_PATH',	HOST.'/plagin');
define('CACHE_PATH',	HOST.'/cache');

define('ADMIN_PATH',	HOST.'/backend');

define('IMG_NEWS_PATH',	'/images/_news');
define('IMG_GALLERY_GRUPA_PATH', '/images/_gallery_grupa');
define('IMG_GALLERY_PATH', '/images/_gallery');
define('IMG_CATALOG_TREE_PATH', '/images/_catalog_tree');
define('IMG_CATALOG_PATH', '/images/_catalog');
define('IMG_REKLAMA_PATH', '/images/_reklama');
define('FILES_PATH', '/files');
define('IMG_BRAND_PATH', '/images/_brand');
define('PRICE_LIST_PATH', '/files/price');
define('IMG_SPLASH_PATH', '/images/_splash');
define('IMG_VIDEO_PATH', '/images/_video');

//язык по умолчанию
define(LANGS,'ru');

// адрес сайта
define ('ADRESS_SITE',mysql::query_findpole('select zna from config where id=11','zna'));

// название фирмы
define ('NAME_FIRMS',mysql::query_findpole('select zna from config where id=12','zna'));

// путь к imagemagick
//define ('CONVERT','/usr/local/bin/');
define ('CONVERT','');

// дебаггер
define ('DEBUG_SITE','0');
define ('DEBUG_ADMIN','0');

// кеш
define ('USE_KEH', '0');

// debug кеш
define ('DEBUG_KEH', '0');

// код банера
define ('_BANNER', '<a target="_blank" href="http://www.100bannerov.com"><img src="/pic/banner.gif" alt="" /></a>');

// НАЧАЛЬНОЕ кол-во товаров на одной странице
define ('FIRST_COUNT_GOODS_AT_PAGE',mysql::query_findpole('select zna from config where id=3','zna'));

// идентификатор сети
define ('_PRINT','0');

// скидка для СПЕЦИАЛЬНОГО ПРЕДЛОЖНИЯ
define ('_SKIDKA',0);

// как работаем с image_magick
define ('_IMAGE_MAGICK','1');


// Мыло админа
$mailadmin=mysql::query_findpole('select zna from config where id=2','zna');

/*
* выбираем основные title , keyword, description
*/
$_arr=mysql::query_one('select title, description, keywords, h1 from content where id=1');
define ('_TITLE',$_arr->title);
define ('_DESCRIPTION',$_arr->description);
define ('_KEYWORDS',$_arr->keywords);
define ('_H1',$_arr->h1);

/*
* нововведения для новой админки
*/
define('__NAME_ADM_DIR','adm_tools_n');


/*
* 29 сентября 2010, кеш для css и js
*/
define ('__CACHE_CSS',0);


/*
* НАСТРОЙКИ ДЛ/ КАТАЛОГА
*/

// размері картинок для каталога
// ширина / высота
define ('_SIZE_01_W','74');
define ('_SIZE_01_H','74');

// ширина / высота
define ('_SIZE_02_W','150');
define ('_SIZE_02_H','108');

// ширина / высота
define ('_SIZE_03_W','1024');
define ('_SIZE_03_H','768');

// тип валюты по умолчанию при выводе, для каталога продукции 0-грн / 1-$
define ('SHOW_CURRENCY', '0');

// тип валюты по умолчанию в базе данных, для каталога продукции 0-грн / 1-$
define ('DB_CURRENCY', '0');

// колво фоток в разделе каталог
define ('COUNT_PHOTO_CATALOG',5);

// нужна картинка в каталоге без watermark: 0- нет / 1 - да
define ('SAVE_ORIGINAL_PHOTO', false);

// нужна картинка в каталоге без watermark: 0- нет / 1 - да
define ('WATERMARK_CATALOG', true);

// watermark для галереи: 0- нет / 1 - да
define ('WATERMARK_GALLERY', true);

// кол-во уровней в каталоге товаров: 1 - группа /2-подгруппа /3-подподгруппа
//define ('__COUNT_LEVEL_CATALOG', '3');

// НАЧАЛЬНОЕ кол-во товаров на одной странице
define ('FIRST_CATALOG_AT_PAGE',mysql::query_findpole('select zna from config where id=29','zna'));

// статусы заказов
$_status_order=array(
			'0'=>"Новый",
			'1'=>"В работе",
			'2'=>"Проведенные"
		);

// кол-во товаров на одной странице в каталоге В АДМИНЧАСТИ
define('ADMIN_CATALOG_AT_PAGE',20);

// сопутствующие товары для каталога есть/нет
define ('IS_CATALOG_ASS',true);

// кол-во фотографий на одной станице в фотогалерее
define('ADMIN_GALLERY_AT_PAGE',20);

// кол-во новостей на одной станице в фотогалерее
define('ADMIN_NEWS_AT_PAGE',20);



/**
 * NEWS
 */

// НАЧАЛЬНОЕ кол-во новостей на одной странице
define ('FIRST_NEWS_AT_PAGE',mysql::query_findpole('select zna from config where id=15','zna'));

// НАЧАЛЬНОЕ кол-во комментерией к новостям на одной странице
define ('FIRST_COMMENT_NEWS_AT_PAGE',mysql::query_findpole('select zna from config where id=18','zna'));

// показывать комментарии к новостям
define ('COMMENT_NEWS',0);

// типы новостей есть/нет
define ('TYPE_NEWS',0);
// массив типов новостей
$_type_news=array(
			'0'=>"Новости",
			'1'=>"Новости тип #1",
			'2'=>"Новости тип #2"
		);

// 2011

define ('DEFAULT_CTRL','page');
define ('DEFAULT_ACTION','index');


// грузим файлы к разделам?
define ('UPLOAD_FILES', false);

// соотносим галереи к разделом?
define ('IS_GALLERY_CONTENT', false);

define ('SUPER_ADMIN', 0);

// типы баннеров есть/нет
define ('TYPE_BANNER',1);
// массив типов баннеров
$_type_banner=array(
			'0'=>"Верхний блок",
			'1'=>"Боковой блок"
		);
		
/**
 * GALLERY
 */

// НАЧАЛЬНОЕ кол-во новостей на одной странице
define ('FIRST_GALLERY_AT_PAGE',mysql::query_findpole('select zna from config where id=6','zna'));		


/**
 * GUESTBOOK
 */

// НАЧАЛЬНОЕ кол-во отзывов на одной странице
define ('FIRST_GUESTBOOK_AT_PAGE',mysql::query_findpole('select zna from config where id=7','zna'));		

define ('VIDEO_AT_PAGE',mysql::query_findpole('select zna from config where id=1001','zna'));

		
?>