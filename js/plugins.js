$(document).ready(function() {
    $('.validat').liValidForm();
    $('.enter').loginPopup($('.enter_block'));
    if ($('#slider').length) {
        $('#slider').carouFredSel({
            auto: true,
            timeoutDuration: 3000,
            pagination: "#pager",
            responsive: true,
            width: "100%",
            scroll: {
                items: 1,
                fx: 'scroll'
            },
            items: {
                visible: {
                    min: 1,
                    max: 1
                }
            }
        });
    }

    /*var socData = [];
    $('.wHeader_right_bot .socLink li').each(function() {
        socData.push($(this).html().trim());
    });*/

    $('nav#mobMenu').mmenu({
        "navbar": {
            "title": "Меню"
        },
        "extensions": ["pageshadow"],
        "offCanvas": {
            pageNodetype: "div, section"
        }
        /*"navbars": [{
            "position": "bottom",
            "content": socData
        }]*/
    });



    /*if ($('.fHide').length) {
        $('.fHide').each(function() {
            var _self = $(this);
            if (_self.children().length > 5) {
                $('<div class="fHideMore"><span>+</span> Больше</div>').appendTo(_self);
            }
        });
        $('.fHide').on('click', '.fHideMore', function(event) {
            $(this).parent('.fHide').removeClass('fHide');
            $(this).remove();
        });
    }*/
    if ($('.acc').length) {
        $('.acc').liHarmonica({
            onlyOne: true,
            speed: 500
        });
    }
    $('a.harFull').on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        window.location.href = href;
        return false;
    }
    );
    $('a.harFull.cur').addClass('harOpen');
    $('a.harFull.cur').closest('li').find('ul').show();
    var width;
    width = window.innerWidth;
    if (width <= 980) {
        if ($(".content").length) {
            var title = $(".content").before($(".n_tit"));
            var news = $(".content").before($(".news"));
            $(".n_tit").addClass("padding-t");
        } else {
            var news = $(".right_block").after($(".news"));
            var title = $(".right_block").after($(".n_tit"));
            $(".n_tit").addClass("padding-t");
        }
    }
    if (width > 980) {
        var title = $(".news_after").after($(".news"));
        var news = $(".news_after").after($(".n_tit"));
        $(".n_tit").removeClass("padding-t");
    }
    /*$(".mob-menu").on("click", function() {
        if ($(".top_menu").hasClass("open")) {
            $(".open").animate({
                "left": "-190px"
            }, "slow");
            $(".mob-menu").animate({
                "left": "10px"
            }, "slow");
            $(".top_menu").removeClass("open");
        } else {
            $(".top_menu").addClass("open");
            $(".open").animate({
                "left": "0px"
            }, "slow");
            $(".mob-menu").animate({
                "left": "180px"
            }, "slow");
        }
    }
    );*/
    $(window).on("resize", function() {
        width = window.innerWidth;
        $(".open").css("left", "-190px");
        $(".top_menu").removeClass("open");
        $(".mob-menu").removeAttr("style");
        if (width <= 980) {
            if ($(".content").length) {
                var title = $(".content").before($(".n_tit"));
                var news = $(".content").before($(".news"));
                $(".n_tit").addClass("padding-t");
            } else {
                var news = $(".right_block").after($(".news"));
                var title = $(".right_block").after($(".n_tit"));
                $(".n_tit").addClass("padding-t");
            }
        }
        if (width > 980) {
            var title = $(".news_after").after($(".news"));
            var news = $(".news_after").after($(".n_tit"));
            $(".n_tit").removeClass("padding-t");
        }
    }
    );
}
);
