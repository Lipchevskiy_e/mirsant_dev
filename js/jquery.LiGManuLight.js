﻿/*
 * jQuery GMenu v 1
 * http://
 *
 * Copyright 2010, Linnik
 * Free to use
 * 
 * September 2010
 */
jQuery.fn.LiGManuLight = function(options){
	var options = jQuery.extend({
		//gh: 25 //Высота пунктов меню
	},options);
	return this.each(function() {
		var m_el = $(this),
			myTime;
		$('ul',m_el).each(function(){
			var ul_el = $(this),
				li_el = ul_el.closest('li').css({zIndex:'50'}),
				a_el = li_el.children('a');
			
			ul_el.hide();
			a_el.addClass('full')
			a_el.bind('mouseenter',function(){
				ul_el.slideDown(200);
				a_el.addClass('open');
				li_el.css({zIndex:'100'});
			})
			li_el.bind('mouseleave',function(){
				ul_el.hide();
				a_el.removeClass('open');
				li_el.css({zIndex:'50'})	
			})
		})
	});
};
