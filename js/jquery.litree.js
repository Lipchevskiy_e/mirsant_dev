﻿/*
 * jQuery Litree v 1.0
 * http://
 *
 * Copyright 2010, Linnik
 * Free to use
 * 
 * September 2010
 */
jQuery.fn.litree = function(options){
	var options = jQuery.extend({
		speed: 200, // Скорость анимации
		classActive: '.cur' // Класс активного элемента
	},options);
	return this.each(function() { 
		tree = jQuery(this).addClass('tree');
		jQuery(options.classActive,this).parents('ul').show(); //открываем активный пункт
		jQuery('li',this).prepend('<span></span>');
		jQuery('ul:visible',this).parent('li').addClass('tm'); //ставим маркер "минус" для открытых списков
		jQuery('ul:hidden',this).parent('li').addClass('tp'); //ставим маркер "плюс" для закрытых списков
		jQuery('span',this)
			.click(function(){  
				jQuery(this).parent().children('ul') 
					.slideToggle(options.speed,function(){  // по клику переключаем состояние вложеных списков		
							jQuery('ul:visible',jQuery(this).parents('.tree')).parent('li').removeClass('tp').addClass('tm');
							jQuery('ul:hidden',jQuery(this).parents('.tree')).parent('li').removeClass('tm').addClass('tp').find('ul').hide(); //закрываем все списки внутри закрытого списка
					});
			return false
			});
	});
};