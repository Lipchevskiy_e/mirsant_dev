// настройки

var collapse_section_id = "quotes"; // ID раздела с элементами
var title_tag = "span"; // имя тега для заголовков
var item_tag = "blockquote"; // имя тега для текста
var text_decotation = "none" // подчеркивание заголовков, если не нужно -- ставим "none" 
var highlight_text = " "; // загогулина перед заголовком при открытом тексте (нельзя использовать entities!)
var normal_text = " "; // загогулина перед заголовком при скрытом тексте (нельзя использовать entities!)

// главная ф-ция

function toggle(e){
var el = window.event ? window.event.srcElement : e.currentTarget;
var collapse_item = el.nextSibling;
while (collapse_item.nodeType != 1) collapse_item = collapse_item.nextSibling; // workaround (whitespace)!
curr_shown = collapse_item.style.display == "block";
collapse_item.style.display = (curr_shown) ? "none" : "block";
var title_text = el.firstChild;
title_text.nodeValue = (curr_shown) ? title_text.nodeValue.replace(highlight_text, normal_text) : title_text.nodeValue.replace(normal_text, highlight_text);
}

// ф-ция инициализации

function init(){
if (document.getElementById && document.createTextNode){
var collapse_section = document.getElementById(collapse_section_id);
var collapse_title = collapse_item = null;
for (var i = 0; ((collapse_title = collapse_section.getElementsByTagName(title_tag).item(i)) && (collapse_item = collapse_section.getElementsByTagName(item_tag).item(i))); i++){
collapse_title.style.textDecoration = text_decotation;
collapse_title.style.cursor = window.event ? "hand" : "pointer";
if (collapse_title.addEventListener) collapse_title.addEventListener("click", toggle, false);
else if (collapse_title.attachEvent) collapse_title.attachEvent("onclick", toggle);
collapse_title.firstChild.nodeValue = normal_text + collapse_title.firstChild.nodeValue;
collapse_item.style.display = "none";
}
}
}

// запускаем процесс по onload'у

var root = window.addEventListener || window.attachEvent ? window : document.addEventListener ? document : null;
if (root){
if (root.addEventListener) root.addEventListener("load", init, false);
else if (root.attachEvent) root.attachEvent("onload", init);
}



