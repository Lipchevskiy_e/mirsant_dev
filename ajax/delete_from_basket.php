<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
	$id_good=intval($_POST['id']);
	
	// если подстава
	if ($id_good==0) die('Товар не найден!');
	
	// если ве гуд
	require "../main.php";

	// закидываем товар в сессию
	if (isset($_SESSION['basket'][$id_good])) {
		foreach($_SESSION['basket'] as $key => $value) {
			if ($key==$id_good) {
				unset($_SESSION['basket'][$key]);
			}	
		}
	} 

	// результат
	die(json_encode(array(
		'count'=>basket::get_Count_Goods(),
		'total'=>basket::get_Total_Goods(),
		'basket'=>system::show_tpl(array('result'=>basket::show_Basket()), 'frontend/basket/list.php')
	)));

?>
