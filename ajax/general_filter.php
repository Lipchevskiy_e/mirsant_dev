<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

    $id = intval($_POST['id']);

    require "../main.php";

    $type = $_POST['type'];

    if($_POST['filter'] == '' or $_POST['filter'] == null or !isset($_POST['filter'])) {
        $link = $_POST['link_wo_filter'].'/filter/'.$type.'_'.$id;
    } else {
        $filter = explode(';', $_POST['filter']);

        $count = 0;
        $r = 0;
        foreach($filter as $c) {
            if($c != '')
                $count++;

            $fil = explode('_', $c);
            if($fil[0] == $type)
                $r = 1;
        }

        /* Если есть фильтр, но в нем нет type */
        if($count > 0 and $r == 0)
            $link = $_POST['link_wo_filter'].'/filter/'.str_replace(';', '/', $_POST['filter']).'/'.$type.'_'.$id;

        /* Если есть фильтр, и в нем есть type */
        if($count > 0 and $r == 1) {
            /* Предварительная работа */
            $_yes = 0;
            $mass = array();
            $filter_new = '';

            foreach($filter as $f)
            {
                $arr = explode('_', $f);
                $key = array();

                if($arr[0] == $type)
                {
                    $_arr = explode(',', $arr[1]);
                    $k = array();
                    $yes = 0;

                    foreach($_arr as $a)
                    {
                        if($a != $id)
                            $k[] = $a;
                        else
                            $yes = 1;
                    }

                    if($yes == 0)
                        $k[] = $id;

                    if(count($k))
                        $mass[] = $type.'_'.implode(',', $k);
                }
                else
                    $mass[] = $f;
            }

            $filter_new = implode('/', $mass);

            if($filter_new != '')
                $link = $_POST['link_wo_filter'].'/filter/'.$filter_new;
            else
                $link = $_POST['link_wo_filter'];
        }

    }

    die(json_encode(array(
        'link'=>$link
    )));