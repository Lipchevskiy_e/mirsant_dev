<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    
	$from=intval($_POST['from']);
	$to=intval($_POST['to']);
    $id_parent = intval($_POST['id_parent']);
    
	require "../main.php";

    $sql = "SELECT cost FROM catalog WHERE status=1 and id_parent=".$id_parent;
    $res = mysql::query($sql);
        
    $max = 0;
    foreach($res as $r)
    {
        if($r->cost > $max)
            $max = $r->cost;
    }
    $max = explode('.', $max);
    $max = $max[0];


    if($to > $max)
        $to = $max;
    
    if($to == 0)
        $to = $max;
    
    if($from > $max)
        $from = $max;

    if($from > $to)
    {
        $_from = $to;
        $to = $from;
        $from = $_from;
    }
    
	$_link = explode('/', $_POST['link']);
    $link = implode('/', $_link);
    
    if($_POST['filter'] == '' or $_POST['filter'] == null or !isset($_POST['filter']))
    {
        if(end($_link) == '')
            $link .= 'filter/cost_'.$from.'-'.$to;
        else
            $link .= '/filter/cost_'.$from.'-'.$to;
    }
    else
    {
        $link = $_POST['link_wo_filter'].'/filter/';
        
        $b = 0;
        $_f = explode(';', $_POST['filter']);
        $_arr = array();
        foreach($_f as $f)
        {
            $_b = 0;
            $arr = explode('_', $f);
            if($arr[0] == 'cost')
            {
                $_b = 1;
                $b = 1;
            }
            
            if($_b == 0)
                $_arr[] = $f;
            else
                $_arr[] = 'cost_'.$from.'-'.$to;
        }
        
        if($b == 0)
            $link .= $_POST['filter'].'/cost_'.$from.'-'.$to;
        else
            $link .= implode(';', $_arr);
    }

	/* Если есть высота или ширина */
    if($_POST['width'] != '' AND $_POST['width'] != null AND isset($_POST['width']))
    {
        $_link_w = explode('/', $link);
        $link_w = end($_link_w);
        
        $width = intval($_POST['width']);
        
        $_f_w = explode(';', $link_w);
        $k_w = 0;
        $l_w = array();
        foreach($_f_w as $f_w)
        {
            $arr_w = explode('_', $f_w);
            if($arr_w[0] == 'width')
            {
                $k_w = 1;
                $l_w[] = 'width_'.$width;
            }
            else
                $l_w[] = $f_w;
        }
        
        if($k_w == 0)
            $l_w[] = 'width_'.$width;
            
        $filter_w = implode(';', $l_w);
        
        $array_w = array();
        for($i = 0; $i < count($_link_w) - 1; $i++)
            $array_w[] = $_link_w[$i];
            
        $link = implode('/',$array_w).'/'.$filter_w;
    }
    else
    {
        $_link_w = explode('/', $link);
        $link_w = end($_link_w);

        $_f_w = explode(';', $link_w);
        $l_w = array();
        foreach($_f_w as $f_w)
        {
            $arr_w = explode('_', $f_w);
            if($arr_w[0] != 'width')
                $l_w[] = $f_w;
        }
            
        $filter_w = implode(';', $l_w);
        
        $array_w = array();
        for($i = 0; $i < count($_link_w) - 1; $i++)
            $array_w[] = $_link_w[$i];
            
        $link = implode('/',$array_w).'/'.$filter_w;
    }
    
    
    if($_POST['length'] != '' AND $_POST['length'] != null AND isset($_POST['length']))
    {
        $_link_h = explode('/', $link);
        $link_h = end($_link_h);
        
        $length = intval($_POST['length']);
        
        $_f_h = explode(';', $link_h);
        $k_h = 0;
        $l_h = array();
        foreach($_f_h as $f_h)
        {
            $arr_h = explode('_', $f_h);
            if($arr_h[0] == 'length')
            {
                $k_h = 1;
                $l_h[] = 'length_'.$length;
            }
            else
                $l_h[] = $f_h;
        }
        
        if($k_h == 0)
            $l_h[] = 'length_'.$length;
            
        $filter_h = implode('/', $l_h);
        
        $array_h = array();
        for($i = 0; $i < count($_link_h) - 1; $i++)
            $array_h[] = $_link_h[$i];
            
        $link = implode('/',$array_h).'/'.$filter_h;
    }
    else
    {
        $_link_h = explode('/', $link);
        $link_h = end($_link_h);
        
        $_f_h = explode(';', $link_h);
        $l_h = array();
        foreach($_f_h as $f_h)
        {
            $arr_h = explode('_', $f_h);
            if($arr_h[0] != 'length')
                $l_h[] = $f_h;
        }
        
        $filter_h = implode('/', $l_h);
        
        $array_h = array();
        for($i = 0; $i < count($_link_h) - 1; $i++)
            $array_h[] = $_link_h[$i];
            
        $link = implode('/',$array_h).'/'.$filter_h;
    }
    
	die(json_encode(array(
		'link'=>$link
	)));

?>
