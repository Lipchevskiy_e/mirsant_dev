<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    
    require "../main.php";
    
    @mysql::query('SET NAMES cp1251');
    
   
    preg_match('/[0-9+() -]*/',$_POST['phone'],$math); 
    if(empty($math[0]) or strlen($_POST['phone']) > 20 or strlen($_POST['phone']) < 6) {
        $msg = 'Вы ввели неверный номер телефона. Проверьте его пожалуйста.';
        die(json_encode(array('msg'=>$msg)));
    }

	$_arr=___findarray('select * from message where id=10');
	$mass_element_for_parsing=array('%phone%','%url%');
	$mass_result_for_parsing=array($_POST['phone'],$_POST['url']);
	$message=parsing_data($mass_element_for_parsing,$mass_result_for_parsing,$_arr['text']);					
    $subject=replace_data($_arr['zag'],'%site%',ADRESS_SITE);
	sent_email_new($GLOBALS["mailadmin"],nl2br($message),"",$subject,$_email);
    
    unset($_POST['phone'], $_POST['url']);

    $msg = 'Спасибо за сообщение, Вам скоро перезвонят! Пожалуйста ожидайте!';

    die(json_encode(array('msg'=>$msg)));
?>