<?php
header("Content-Type: text/html;charset=utf-8");

$num=intval($_GET['num']);

// если подстава
//if ($num==0) return 0;
	
// Подключаем капчу
require_once  '../captcha/securimage.php';
$image = new Securimage();

// Если капча не пройдена
if (!$image->check($num)) {
	echo 0;
} else {
	echo 1;
}
