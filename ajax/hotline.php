<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

set_time_limit(2000);

// если ве гуд
require "../main.php";

// Format
$bathtubObj = mysql::query('SELECT id, name FROM dict_bathtub_form');
$bathtubForm = array();
foreach($bathtubObj as $obj) {
    $bathtubForm[$obj->id] = $obj->name;
}
// Color
$colorsObj = mysql::query('SELECT id, name FROM dict_colors');
$colors = array();
foreach($colorsObj as $obj) {
    $colors[$obj->id] = $obj->name;
}
// Cabin forms
$cabinFormsObj = mysql::query('SELECT id, name FROM dict_cabin_form');
$cabinForms = array();
foreach($cabinFormsObj as $obj) {
    $cabinForms[$obj->id] = $obj->name;
}

function clearString($data) {
    $data = str_replace('&nbsp;',' ',strip_tags($data));
    $data = str_replace(array('&laquo;','&raquo;','&ldquo;','&rdquo;'),'&quot;',$data);
    $data = str_replace(array('&ndash;','&mdash;','&deg;','&hellip;','&not;'),'',$data);
    $data = str_replace(array('&'),'&amp;',$data);

    return $data;
}

function parsePattern($product, $pattern) {
    global $bathtubForm,
        $colors,
        $cabinForms;

    $assocRelation = array(
        '[item_name]' => clearString($product->name),
        '[item_size:XxY]' => ($product->length ? $product->length : $product->height).'x'.$product->width,
        '[item_size:XxYxZ]' => ($product->length ? $product->length : $product->height).'x'.$product->width.'x'.$product->height,
        '[execution]' => '',
        '[format]' => ($product->has_bathtub && array_key_exists($product->bathtub_form, $bathtubForm)
            ? $bathtubForm[$product->bathtub_form]
            : ($product->cabin_form && array_key_exists($product->cabin_form, $cabinForms)? $cabinForms[$product->cabin_form] : '')),
        '[model_number]' => $product->artikul,
        '[item_color]' => (array_key_exists($product->color, $colors) ? $colors[$product->color] : ''),
        '[item_type]' => '',
        '[item_view]' => '',
        '[item_assignment]' => '',
        '[item_surface]' => '',
        '[description]' => clearString($product->text_full),
    );

    $result = str_replace(array_keys($assocRelation), array_values($assocRelation), $pattern);
    return $result;
}

// Items structure by category
/*
 * Fields:
 * [item_name] - название товара
 * [item_size:XxY] - размеры изделия ШИРИНАxДЛИНА
 * [item_size:XxYxZ] - размеры изделия ШИРИНАxДЛИНАхВЫСОТА
 * [execution] - исполнение, левое или правое (L, R)
 * [format] - форма (душевой кабины или душевого поддона)
 * [model_number] - Код модели (артикул модели)
 * [item_color] - Цвет (в формате предусмотренном производителем)
 * [item_type] - Тип товара
 * [item_view] - Вид
 * [item_assignment] - Назначение
 * [item_surface] - Поверхность
 * [description] - Описание товара
 * */
$catStucture = array(
//    Ванны
    18 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
    20 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
    22 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
    31 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
//    Гидромассажные ванны
    25 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
//    Душевые боксы
    14 => array('name' => '[item_name] [item_size:XxY] [format]', 'code' => '[model_number]'),
//    Душевые кабины
    30 => array('name' => '[item_name] [item_size:XxY] [format]', 'code' => '[model_number]'),
//    Смесители
    17 => array('name' => '[item_name]', 'code' => '[model_number]'),
//    Мебель для ванной
    13 => array('name' => '[item_name] [item_color] [item_type]', 'code' => '[model_number]'),
//    Унитазы
    16 => array('name' => '[item_name]', 'code' => '[model_number]'),
//    Умывальники
    15 => array('name' => '[item_name]', 'code' => '[model_number]'),
//    Инсталляции
    26 => array('name' => '[item_name]', 'code' => '[model_number]'),
//    Шторки на ванну
    27 => array('name' => '[item_name]', 'code' => '[model_number]'),
//    Гидромассажные панели
    28 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
//    Плитка
    29 => array('name' => '[item_name] [item_type] [item_size:XxY] [execution]', 'code' => '[model_number]',
        'params' => array(
            'Вид' => '[item_view]',
            'Назначение' => '[item_assignment]',
            'Поверхность' => '[item_surface]',
            'Размеры' => '[item_size:XxYxZ]',
        ),
        'description' => '[description] [item_color]',
    ),
);

// строим запрос
$_sql="select * FROM config WHERE id = 1009";
// выполняем запрос + при необходимости выводим сам запрос
$firmId=mysql::query_one($_sql,0);

$firmId = $firmId->zna;

$products = mysql::query("SELECT catalog.*, brand.pole as brand_name FROM catalog LEFT JOIN brand ON brand.id=catalog.brand WHERE catalog.status = 1",0);
$categories = mysql::query("SELECT * FROM catalog_tree WHERE status = 1",0);

// Categories
$str  = '<?xml version="1.0" encoding="UTF-8"?>';
$str .= '<price>';
$str .= '<date>'.date('Y-m-d H:i').'</date>';
$str .= '<firmName>http://mirsant.com.ua/</firmName>';
$str .= '<firmId>'.$firmId.'</firmId>';
$str .= '<categories>';
foreach($categories as $category) {
    $str .= '<category><id>'.$category->id.'</id><name>'.$category->pole.'</name></category>';
}
$str .= '</categories>';

// Items
$str .= '<items>';
foreach($products as $product) {
    $str .= '<item>';
    $str .= '<id>'.$product->id.'</id>';
    $str .= '<categoryId>'.$product->id_parent.'</categoryId>';
    $str .= '<vendor>'.clearString($product->brand_name).'</vendor>';

    $product_name =  clearString($product->name);
    $description = clearString($product->text_full);

    $avi = 'В наличии';

    if ($product->valuta==2)
        $cost=$product->cost*EVRO;
    elseif ($product->valuta==3)
        $cost=$product->cost*DOLLAR;
    else $cost=$product->cost;

//    Regenerate fields by category templates
    if (array_key_exists($product->id_parent, $catStucture)) {
        $tags = $catStucture[$product->id_parent];

        foreach($tags as $tag => $pattern) {
            switch ($tag) {
                case "name":
                    $product_name = parsePattern($product, $pattern);
                    break;
                case "description":
                    $description = parsePattern($product, $pattern);
                    break;
                case "params":
                    foreach ((array)$pattern as $paramName => $paramPattern) {
                        $paramValue = parsePattern($product, $paramPattern);
                        if ($paramValue != '') {
                            $str .= '<param name="'.$paramName.'">'.parsePattern($product, $paramPattern).'</param>';
                        }
                    }
                    break;
                case "code":
                    $str .= '<code>'.parsePattern($product, $pattern).'</code>';
                    break;
            }
        }
    }
    $str .= '<name>'.htmlspecialchars($product_name).'</name>';
    $str .= '<description>'.htmlspecialchars($description).'</description>';
    $str .= '<url>'.'http://'.$_SERVER['HTTP_HOST'].'/catalog/goods/'.$product->url.'</url>';
    $str .= '<image>'.'http://'.$_SERVER['HTTP_HOST'].IMG_CATALOG_PATH.'/original/'.$product->id.'_1.jpg</image>';
    $str .= '<priceRUAH>'.ceil($cost).'</priceRUAH>';
    $str .= '<stock>'.$avi.'</stock>';
    $str .= '<guarantee>От производителя</guarantee>';
    $str .= '</item>';
}
$str .= '</items>';
$str .= '</price>';
$fp = fopen(HOST.'/xml/hotline.xml','w+');
fwrite($fp, $str);
fclose($fp);

echo "ok";die;





?>
