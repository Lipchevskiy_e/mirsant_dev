<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    
	$id=explode('_', $_POST['id']);
	$id = $id[1];

	require "../main.php";

    if($_POST['filter'] == '' or $_POST['filter'] == null or !isset($_POST['filter']))
        /* Если нет фильтра вообще */
        $link = $_POST['link_wo_filter'].'/filter/series_'.$id;
    else
    {
        $filter = explode(';', $_POST['filter']);
        
        $count = 0;
        $r = 0;
        foreach($filter as $c)
        {
            if($c != '')
                $count++;
            
            $fil = explode('_', $c);
            if($fil[0] == 'series')
                $r = 1;
        }
        
        /* Если есть фильтр, но в нем нет series */
        if($count > 0 and $r == 0)
            $link = $_POST['link_wo_filter'].'/filter/'.str_replace(';', '/', $_POST['filter']).'/series_'.$id;
        
        /* Если есть фильтр, и в нем есть series */
        if($count > 0 and $r == 1)
        {
            /* Предварительная работа */
            $_yes = 0;
            $mass = array();
            $filter_new = '';
            
            foreach($filter as $f)
            {
                $arr = explode('_', $f);
                $key = array();
                
                if($arr[0] == 'series')
                {
                    $_arr = explode(',', $arr[1]);
                    $k = array();
                    $yes = 0;
                    
                    foreach($_arr as $a)
                    {
                        if($a != $id)
                           $k[] = $a;
                        else
                           $yes = 1; 
                    }
                    
                    if($yes == 0)
                        $k[] = $id;
                    
                    if(count($k))
                        $mass[] = 'series_'.implode(',', $k);
                }
                else
                    $mass[] = $f;
            }
            
            $filter_new = implode('/', $mass);

            if($filter_new != '')
                $link = $_POST['link_wo_filter'].'/filter/'.$filter_new;
            else
                $link = $_POST['link_wo_filter'];
        }
        
    }
    
    
	die(json_encode(array(
		'link'=>$link
	)));

?>
