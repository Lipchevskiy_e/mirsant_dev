<?php
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
	header('Content-Type: text/html; charset=UTF-8');


    define ('APPLICATION','/backend');
    define ('_APPLICATION','backend');

	@session_start();	

	ob_start();	

	require "../main.php";
	
	$_SERVER['REQUEST_URI']=str_replace('/backend','',$_SERVER['REQUEST_URI']);
	
	// роутинг
	system::_route();
	
    // суперадмин / не суперадмин
	general::access();
	
	
	/*
	* CONTROLLER
	*/
	$ctrl=system::_get('ctrl');
	$action=system::_get('action');
				
	// get controller
	$ctrl_file = ADMIN_PATH."/ctrl/".$ctrl."Controller.php";
				
	//echo ($ctrl_file);
	
	if (is_file($ctrl_file))
	{
		// load core classes
		$class = $ctrl.'Controller';
		
		//echo ($class);
		
						
		// get action
		if (class_exists($class))
		{
			$class = new $class;
			$method = $action.'Action';
			
			//echo ($method);
							
			if (method_exists($class, $method))	{

				// execute action
				$class->_pre();
				$class->_post($class->$method());
			
			} else {
			
				system::error(404,'!method_exists: '.$method);		
			}
				
		} else {

			system::error(404,'!class_exists: '.$class);
			
		}
	}
	else
	system::error(404,'!controller: '.$ctrl_file);	
	/*
	* CONTROLLER
	*/
		

?>