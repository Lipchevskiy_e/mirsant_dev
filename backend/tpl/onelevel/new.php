	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Редактирование &rArr; <?php echo $obj->name; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new/tablename/'.$_GET['tablename']) ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index/tablename/'.$_GET['tablename'], 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
    	<div class="item_1">
    		<label class="block title_1">Наименование:</label>
			<input type=text name="FORM[pole]" value='<?php echo $obj->pole; ?>' class="validate[required] input_1" id="pole">    
	    </div>
		
	    <div class="item_1">
    		<label class="block title_1">Описание:</label>
			<textarea name="FORM[text]" id="content" rows="10" class="text_1 tiny"><?php echo $obj->text; ?></textarea>
	    </div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index/tablename/'.$_GET['tablename'], 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>