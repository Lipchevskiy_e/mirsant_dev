	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Добавление нового материала</h1>
	
	<?php echo $msg ? $msg : '' ?>	
	
	<form id="commentForm" name="form_add" method="post" action="<?php echo general::link($tpl_folder.'/add/tablename/'.$_GET['tablename']) ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index/tablename/'.$_GET['tablename'], 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
    	<div class="item_1">
    		<label class="block title_1">Наименование:</label>
			<input type=text name="FORM[pole]" class="validate[required] input_1" id="pole">    
	    </div>
		
	    <div class="item_1">
    		<label class="block title_1">Описание:</label>
			<textarea name="FORM[text]" id="content" rows="10" class="text_1 tiny"></textarea>
	    </div>
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать:</label>
			<input name="FORM[status]" type="checkbox" id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index/tablename/'.$_GET['tablename'], 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>