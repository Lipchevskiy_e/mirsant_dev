<h1>Управление материалами</h1>

<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index/tablename/'.$_GET['tablename']) ; ?>">
	
	<div class="search_sort">	
		<span class="search_sort_item">
			<label>Статус</label>	
			<select name='status' class="middle" id="status" onChange="commentForm.submit();">
				<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
				<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
				<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
			</select>
		</span>
	</div>
		
	</form>
</div>

<?php echo $msg ? $msg : '' ?>	

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th>Удаление</th>
		<th>Наименование</th>
		<th>Опубликован?</th>
		<th>Операции</th>
	</tR>

	
<?php foreach ($result as $obj): ?>
	
	<tr>
		<td><?php echo dbh::content_count_child($obj->id)==0 ? general::link_to($tpl_folder.'/delete/id/'.$obj->id.'/tablename/'.$_GET['tablename'],'Удалить?','onclick="return confirm(\'Вы уверены?\')"') : ''; ?></td>
		<td><?php echo $obj->pole; ?></td>
		<td><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></td>		
		<td><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id.'/tablename/'.$_GET['tablename'],'Редактировать'); ?></td>
	</tr>



<?php endforeach; ?>

</table>

<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
		/*Код изменения статуса*/	
		$('.setStatus').click(function(){
			 var id  = $(this).attr('data-id');
			 var it  = $(this);
			jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'<?php echo $_GET['tablename'] ?>'}, function(data) {
				//alert($(this).text());
				if (data=='1') 
				{
					it.html("<span style='color:#39C;'>Да</span>");
				}
				if (data=='0') 
				{
					it.html("<span style='color:red;'>Нет</span>");
				}
			}); 
			return false;
		})
		/*конец кода изменения статуса*/
		});
</script>	