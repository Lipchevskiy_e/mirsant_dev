<h1>Управление предназначениями товара</h1>

<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
	
	<div class="search_sort">	
		<span class="search_sort_item">
			<label>Статус</label>	
			<select name='status' class="middle" id="status" onChange="commentForm.submit();">
				<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
				<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
				<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
			</select>
		</span>
	</div>
		
	</form>
</div>

<div class="clear"></div>

<?php echo $msg ? $msg : '' ?>	

<form action="">
<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
  <thead>
	<tr>
		<th>Удаление</th>
		<th>Предназначение товара</th>
		<th>Опубликован?</th>
		<th>Операции</th>
	</tr>
  </thead>

	<tbody id="sortable">	
<?php foreach ($result as $obj): ?>
	
	<tr id="listItem_<?php echo $obj->id; ?>">
		<td><?php echo $obj->count==0 ? general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"') : ''; ?></td>		
		<td><?php echo $obj->name; ?></td>
		<td><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></td>	
		<td><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?></td>
	</tr>


<?php endforeach; ?>
  </tbody>
</table>
</form>



	<div id="info" align="center" style="color: red;">Меняйте порядок брендов перетаскивая картинки!</div>

	<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
			$("#sortable, .s").sortable({
				update : function () {
					var order = $('#sortable').sortable('serialize');
					//$("#info").load("<?php echo APPLICATION ?>/ajax/writesort.php?"+order+'&tablename=catalog');
					$.ajax({
						type: "POST",
						url: "<?php echo APPLICATION ?>/ajax/writesort.php",
						data: order+'&tablename=type_tovar'
					});				
				}
			});
			$('#sortable td').each(function(){
				$(this).width($(this).width())
			})
			
			
			/*Код изменения статуса*/	
			$('.setStatus').click(function(){
				 var id  = $(this).attr('data-id');
				 var it  = $(this);
				jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'type_tovar'}, function(data) {
					//alert($(this).text());
					if (data=='1') 
					{
						it.html("<span style='color:#39C;'>Да</span>");
					}
					if (data=='0') 
					{
						it.html("<span style='color:red;'>Нет</span>");
					}
				}); 
				return false;
			})
			/*конец кода изменения статуса*/
		});
	</script>