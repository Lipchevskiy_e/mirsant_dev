	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	
	<h1>Редактирование страницы &rArr; <?php echo $obj->name; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
    	<div class="item_1">
    		<label class="block title_1">Наименование страницы:</label>
			<input type=text name="FORM[name]" value='<?php echo $obj->name; ?>' class="validate[required] input_1" id="title">    
	    </div>
		
		<?if (general::sadmin()):?>
    	<div class="item_1">
    		<label class="block title_1">URL страницы:</label>
			<input type=text name="FORM[action]" value="<?php echo $obj->action; ?>" class="validate[required] input_1" id="action">    
	    </div>		
		<?endif;?>
		<?php if($obj->id_parent>0): ?>
    	<div class="item_1">
    		<label class="block title_1">Раздел:</label>
			<select name='FORM[id_parent]'>
				<?php foreach ($select as $select): ?>
					<option value="<?php echo $select->id; ?>"<?php echo $select->id==$obj->id_parent ? ' selected': '';?>><?php echo $select->name; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>	
		<?php endif; ?>
		
		<div class="item_1">
    		<label class="block title_1">Текстовое содержание раздела:</label>
			<textarea name="FORM[content]" id="content" rows="30" class="text_1 tiny"><?php echo $obj->content; ?></textarea>
	    </div>
		
		<?php if(IS_GALLERY_CONTENT): ?>
    	<div class="item_1">
    		<label class="block title_1">Галерея:</label>
			<?php foreach ($gallery_grupa as $gallery_grupa): ?>
				<input name="gallery_grupa[]" type="checkbox" value="<?php echo $gallery_grupa->id; ?>" <?php echo dbh::get_content_gallery($obj->id, $gallery_grupa->id)>0 ? 'checked' : ''; ?>>&nbsp;<?php echo $gallery_grupa->pole; ?>
			<?php endforeach; ?>			
	    </div>	
		<?php endif; ?>				
		
    	<div class="item_1">
    		<label class="block title_1">Заголовок страницы (h1):</label>
			<input type=text name="FORM[h1]" class="validate[required] input_1" value="<?php echo $obj->h1; ?>" id="h1">    
	    </div>	
		
        <div class="item_1">
    		<label class="block title_1">Title:</label>
			<input type="text" name="FORM[title]" id="title" value='<?php echo $obj->title; ?>' class="validate[required] input_1">        	
    	</div>
	    <div class="item_1">
    		<label class="block title_1">Keywords:</label>
    		<textarea name="FORM[keywords]" id="keywords" rows="7" class="validate[required] text_1"><?php echo $obj->keywords; ?></textarea>
    	</div>
	    <div class="item_1">
    		<label class="block title_1">Description:</label>
    		<textarea name="FORM[description]" id="description" rows="7" class="validate[required] text_1"><?php echo $obj->description; ?></textarea>
    	</div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
		
		<?php if(UPLOAD_FILES): ?>
			<div class="item_1">
				<?php if($_files): ?>
					<label for="status" class="block title_1">Загруженные файлы:</label>
					<?php foreach ($_files as $_files): ?>
						<?php echo general::get_icon_for_file($_files->name); ?>&nbsp;
						<?php echo $_files->name; ?> <span class="data1">[<?php echo system::show_data($_files->created_at); ?>]</span>
						<?php echo general::link_to($tpl_folder.'/deletefile/id_photo/'.$_files->id.'/id/'.$obj->id,'Удалить','onclick="return confirm(\'Вы уверены?\')"'); ?>
						<br/>			
					<?php endforeach; ?>
				<?php endif; ?>				
				<?php echo general::link_to('plagin/swfupload/files.php?id='.$_GET['id'], 'Загрузить файлы?','class="example"'); ?>
			</div>
		<?php endif; ?>				


        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>   
	
	<script type="text/javascript">
	$(document).ready(function(){
		<!-- colorbox -->
	  $(".example").colorbox({width:"450px", height:"350px", iframe:true,onClosed:function(){ window.location.reload(); }});
	});
	</script>