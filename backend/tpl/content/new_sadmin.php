	<h1>Редактирование подраздела &rArr; <?php echo $obj->name; ?></h1>

	<h2 align="center" style="color:red">Только для СуперАдмина!</h2>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
    	<div class="item_1">
    		<label class="block title_1">Наименование подраздела:</label>
			<input type=text name="FORM[name]" value="<?php echo $obj->name; ?>" class="validate[required] input_1" id="title">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">URL страницы:</label>
			<input type=text name="FORM[action]" value="<?php echo $obj->action; ?>" class="validate[required] input_1" id="action">    
	    </div>

		<?php if($obj->id_parent>0): ?>
		<div class="item_1">
    		<label class="block title_1">Раздел:</label>
			<select name='FORM[id_parent]'>
				<?php foreach ($select as $select): ?>
					<option value="<?php echo $select->id; ?>"<?php echo $select->id==$obj->id_parent ? ' selected': '';?>><?php echo $select->name; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>
		<?php endif; ?>

        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>

        <div class="item_1">
    		<label for="status" class="block title_1">Раздел с подразделами?:</label>
			<input name="FORM[has_podrasdel]" type="checkbox" <?php echo $obj->has_podrasdel==1 ? checked : ''; ?> id="status">
    	</div>

        <div class="item_1">
    		<label for="status" class="block title_1">Показывать в разделе "Карта сайта"?:</label>
			<input name="FORM[is_map]" type="checkbox" <?php echo $obj->is_map==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>    
	
	<script type="text/javascript">
	$(document).ready(function(){
	});
	</script>