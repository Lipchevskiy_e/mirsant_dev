	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Разделы сайта &rArr; <?php echo $obj->name; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/edit') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
    	<div class="item_1">
    		<label class="block title_1">Текстовое содержание раздела:</label>
			<textarea name="FORM[content]" id="content" rows="30" class="text_1 tiny"><?php echo $obj->content; ?></textarea>
	    </div>
			
		<!--
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		-->
		
        <div class="item_1">
    		<label class="block title_1">Title:</label>
			<input type="text" name="FORM[title]" id="title" value='<?php echo $obj->title; ?>' class="validate[required] input_1">        	
    	</div>
	    <div class="item_1">
    		<label class="block title_1">Keywords:</label>
    		<textarea name="FORM[keywords]" id="keywords" rows="7" class="validate[required] text_1"><?php echo $obj->keywords; ?></textarea>
    	</div>
	    <div class="item_1">
    		<label class="block title_1">Description:</label>
    		<textarea name="FORM[description]" id="description" rows="7" class="validate[required] text_1"><?php echo $obj->description; ?></textarea>
    	</div>
    	
        <div class="item_1 textcenter">
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>    