	<h1>Добавление нового раздела</h1>

	<h2 align="center" style="color:red">Только для СуперАдмина!</h2>
	
	<?php echo $msg ? $msg : '' ?>	
	
	<form id="commentForm" name="form_add" method="post" action="<?php echo general::link($tpl_folder.'/add') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
    	<div class="item_1">
    		<label class="block title_1">Наименование подраздела:</label>
			<input type=text name="FORM[name]" class="validate[required] input_1" id="title">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">URL страницы:</label>
			<input type=text name="FORM[action]" class="validate[required] input_1" id="action">    
	    </div>		

        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>

        <div class="item_1">
    		<label for="status" class="block title_1">Раздел с подразделами?:</label>
			<input name="FORM[has_podrasdel]" type="checkbox" <?php echo $obj->has_podrasdel==1 ? checked : ''; ?> id="status">
    	</div>

        <div class="item_1">
    		<label for="status" class="block title_1">Показывать в разделе "Карта сайта"?:</label>
			<input name="FORM[is_map]" type="checkbox" <?php echo $obj->is_map==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>    
	
	
	<script type="text/javascript">
	$(document).ready(function(){
		<!-- переводим в транслит -->
	  $('#title').syncTranslit({destination: 'action', /*caseStyle: 'upper',*/ urlSeparator: '_'});
	});
	</script>