<h1>Управление страницами &rArr; Все страницы</h1>

<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
	
	<div class="search_sort">	
		<span class="search_sort_item">
			<label>Статус</label>	
			<select name='status' class="middle" id="status" onChange="commentForm.submit();">
				<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
				<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
				<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
			</select>
		</span>
	</div>
		
	</form>
</div>

<?php echo $msg ? $msg : '' ?>	
	<form name='myfrm' action="" method='post'>
	 <input id='hinp' type='hidden' name='lang'>
	</form>
	<script type="text/javascript">
	function gopost(href,key,val)
	{
	 document.getElementById('hinp').name=key;
     document.getElementById('hinp').value=val;
	 document.myfrm.action=href;
	 document.myfrm.submit();	
	}
	</script>

	<div class="ps_table_title">
        <div class="coll_title">
			<span>Опубликован?</span>
            <span>Действие</span>
        </div>
        <div class="name_page">Наименование текущей страницы</div>
    </div>
	<div class="sort_block">
		<ul class="tree" id="sortable">
			<?php echo system::show_tpl(array('result'=>$result,'tpl_folder'=>$tpl_folder),$tpl_folder.'/menu.php'); ?>
		</ul>
	</div>

	<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
			
		/*Код изменения статуса*/	
		$('.setStatus').click(function(){
			 var id  = $(this).attr('data-id');
			 var it  = $(this);
			jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'content'}, function(data) {
				//alert($(this).text());
				if (data=='1') 
				{
					it.html("<span style='color:#39C;'>Да</span>");
				}
				if (data=='0') 
				{
					it.html("<span style='color:red;'>Нет</span>");
				}
			}); 
			return false;
		})
		/*конец кода изменения статуса*/
	
		});
	</script>	