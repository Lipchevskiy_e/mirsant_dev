	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	
	<h1>Добавление новой страницы</h1>
	
	<?php echo $msg ? $msg : '' ?>	
	
	<form id="commentForm" name="form_add" method="post" action="<?php echo general::link($tpl_folder.'/add') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
    	<div class="item_1">
    		<label class="block title_1">Наименование страницы:</label>
			<input type=text name="FORM[name]" class="validate[required] input_1" id="title">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">URL страницы:</label>
			<input type=text name="FORM[action]" class="validate[required] input_1" id="action">    
	    </div>		

    	<div class="item_1">
    		<label class="block title_1">Раздел:</label>
			<select name='FORM[id_parent]'>
				<?php foreach ($select as $select): ?>
				<?
				 if ($_POST['parid'])
				 {
				  if($_POST['parid']==$select->id)
				   echo " <option value=\"".$select->id."\">".$select->name."</option>";
				 }
				 else echo " <option value=\"".$select->id."\">".$select->name."</option>";
				?>		
				<?php endforeach; ?>				
			</select>
	    </div>	
		
    	<div class="item_1">
    		<label class="block title_1">Текстовое содержание раздела:</label>
			<textarea name="FORM[content]" id="content" rows="30" class="text_1 tiny"></textarea>
	    </div>

    	<div class="item_1">
    		<label class="block title_1">Заголовок страницы (h1):</label>
			<input type=text name="FORM[h1]" class="validate[required] input_1" id="h1">    
	    </div>	
		
        <div class="item_1">
    		<label class="block title_1">Title:</label>
			<input type="text" name="FORM[title]" id="title" class="validate[required] input_1">        	
    	</div>
	    <div class="item_1">
    		<label class="block title_1">Keywords:</label>
    		<textarea name="FORM[keywords]" id="keywords" rows="7" class="validate[required] text_1"></textarea>
    	</div>
	    <div class="item_1">
    		<label class="block title_1">Description:</label>
    		<textarea name="FORM[description]" id="description" rows="7" class="validate[required] text_1"></textarea>
    	</div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать:</label>
			<input name="FORM[status]" type="checkbox" id="status">
    	</div>		
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>    
	
	
	<script type="text/javascript">
	$(document).ready(function(){
		<!-- переводим в транслит -->
	  $('#title').syncTranslit({destination: 'action', /*caseStyle: 'upper',*/ urlSeparator: '_'});
	});
	</script>