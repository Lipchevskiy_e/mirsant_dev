<h1>Редактирование заказа &rArr; №<?php echo $obj->id; ?></h1>

<?php echo $msg ? $msg : '' ?>

<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link('orders/new') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to('orders/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">  
			<input type="submit" name="change_status" value="Сохранитьи уведомить заказчика о смене статуса заказа">    
			<?php echo general::link_to('orders/print/id/'.$obj->id, 'Распечатать заказ', 'target="_blank"'); ?>
        </div>
		
    	<div class="item_1">		
			<label class="block title_1">Данные заказа:</label>
			
			<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th>Имя получателя</th>
					<th>Город</th>
					<th>Примечание</th>
					<th>Доставка</th>
					<?php if ($obj->deliver == 'по Киеву'): ?>
						<th>Адрес</th>
					<?php endif ?>
					<th>Форма оплаты</th>
					<th>Как связаться</th>
					<th>Статус оплаты</th>
				</tr>
				
				<tr>
					<td><?php echo $obj->name; ?></td>
					<td><?php echo $obj->city; ?></td>
					<td><?php echo $obj->note; ?></td>
					<td><?php echo $obj->deliver.'<br>'.$obj->city_np.'<br>'.$obj->sklad; ?></td>
					<?php if ($obj->deliver == 'по Киеву'): ?>
						<td><?php echo $obj->order_address; ?></td>
					<?php endif ?>
					<td><?php echo $obj->form_payment; ?></td>
					<td><?php echo $obj->type_connect; ?></td>
					<td><?php echo $obj->status_payment; ?></td>
				</tr>				
				
			</table>		

			<label class="block title_1">Данные пользователя:</label>
			
			<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th>Имя</th>
					<th>Адрес</th>
					<th>Телефон</th>
					<th>Email</th>
				</tR>
				
				<tr>
					<td><?php echo $user->name; ?></td>
					<td><?php echo $user->address; ?></td>
					<td><?php echo $user->phone; ?></td>
					<td><?php echo $user->email; ?></td>
				</tr>
				
			</table>				

	    </div>			
		
    	<div class="item_1">

			<label class="block title_1">Данные о товаре:</label>
			<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th>Удаление</th>
					<th>Артикул</th>
					<th>Наименование товара</th>
					<th>Цвет</th>
					<th>Размер</th>
					<th>Цена товара</th>
					<th>Кол-во</th>
					<th>Итого</th>
				</tR>
				
			<?php $_total=0; ?>

			<?php foreach ($result0 as $d_ord): ?>
				
				<tr>
					<td><?php echo general::link_to('orders/deleteOrders/id_order/'.$d_ord->id.'/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>
					<td><?php echo $d_ord->artikul; ?></td>
					<td><?php echo $d_ord->name; ?></td>
					<td><?php echo $d_ord->color; ?></td>
					<td><?php echo $d_ord->size; ?></td>
					<td><?php echo $d_ord->cost; ?></td>
					<td><?php echo $d_ord->kolvo; ?></td>
					<td><?php echo $d_ord->kolvo*$d_ord->cost; ?></td>
				</tr>

				<?php $_total+=$d_ord->kolvo*$d_ord->cost; ?>
				
			<?php endforeach; ?>

				<tr>
					<th colspan="7">ВСЕГО:</th>
					<th><?php echo $_total; ?></th>
				</tR>			
				
			</table>

	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Статус товара:</label>
			<select name='FORM[status]' id="status">
				<?php foreach ($select as $key => $value): ?>
					<option value="<?php echo $key; ?>"<?php echo $key==$obj->status ? ' selected': '';?>><?php echo $value; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>	
		
        <div class="item_1 textcenter">
			<?php echo general::link_to('orders/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">    
			<input type="submit" name="change_status" value="Сохранитьи уведомить заказчика о смене статуса заказа">    
			<?php echo general::link_to('orders/print/id/'.$obj->id, 'Распечатать заказ', 'target="_blank"'); ?>
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
</form>	