<h1>Управление заказами</h1>

<!-- фильтр НАЧАЛО -->
<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link('orders/index') ; ?>">

		<div class="search_sort">	
			<span class="search_sort_item">
				<label>Дата заказа C </label><input type="text" name="date_s" id="datepicker">
			</span>
			<span class="search_sort_item">
				<label>Дата заказа ПО </label><input type="text" name="date_po" id="datepicker1">
			</span>
			<span class="item_last"><input type="submit" value="Найти"></span>

		</div><!-- .search_sort--> 
		
	</form>
</div>
<!-- фильтр КОНЕЦ -->


<?php echo $msg ? $msg : '' ?>	

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th>Удаление</th>
		<th>№ заказа</th>
		<th>E-mail</th>
		<th>Телефон</th>		
		<th>Товар</th>
		<th>Дата заказа</th>
	</tr>

    <?php foreach ($result as $obj): ?>
    	<tr>
    		<td><?php echo general::link_to('buyoneclick/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>
    		<td><?php echo $obj->id; ?></td>
			<td><?php echo $obj->email; ?></td>
    		<td><?php echo $obj->phone; ?></td>
    		<td><?php echo general::link_to('backend/catalog/new/id/'.$obj->id_product,$obj->name,'');?></td>
    		<td><?php echo date('d.m.Y H:i',$obj->created_at);?></td>
    	</tr>

    <?php endforeach; ?>
</table>

<script type="text/javascript">
	$(document).ready(function(){
    	  $.datepicker.setDefaults($.extend($.datepicker.regional["ru"]));
    	  $("#datepicker").datepicker();
    	  $("#datepicker1").datepicker();
	});
</script>