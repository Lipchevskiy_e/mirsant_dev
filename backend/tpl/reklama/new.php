	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Редактирование баннера &rArr; <?php echo $obj->name; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
		<?php if (TYPE_BANNER==1): ?>
    	<div class="item_1">
    		<label class="block title_1">Тип новостей:</label>
			<select name='FORM[category]' class="validate[required]" id="type">
				<?php foreach ($select as $key => $value): ?>
					<option value="<?php echo $key; ?>"<?php echo $key==$obj->category ? ' selected': '';?>><?php echo $value; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>	
		<?php endif; ?>	
		
    	<div class="item_1">
    		<label class="block title_1">Наименование:</label>
			<input type="text" name="FORM[name]" value='<?php echo $obj->name; ?>' class="validate[required] input_1" id="pole">    
	    </div>    	
		
		
    	<div class="item_1">
    		<label class="block title_1">Описание:</label>
			<textarea name="FORM[description]" id="content" rows="10" class="text_1 tiny"><?php echo $obj->description; ?></textarea>
	    </div>		

		<div class="item_1">
		<label class="block title_1">URL баннера:</label>
			<input type="text" name="FORM[url]" value='<?php echo $obj->url; ?>' class="input_1" id="pole">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Баннер:</label>
			<?php if($obj->image) : ?>
				<?php echo banners::get_reklama_backend($obj->id); ?>
				<?php echo general::link_to($tpl_folder.'/deletephoto/id/'.$obj->id,'Удалить баннер?','onclick="return confirm(\'Вы уверены?\')"'); ?>
			<?php else: ?>				
				<input type="file" name="file1" class="input_1">    				
			<?php endif; ?>
	    </div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>