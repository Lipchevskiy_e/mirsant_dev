	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Добавление нового баннера</h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/add') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
		<?php if (TYPE_BANNER==1): ?>
    	<div class="item_1">
    		<label class="block title_1">Расположение:</label>
			<select name='FORM[category]' class="validate[required]" id="type">
				<option></option>
				<?php foreach ($select as $key => $value): ?>
					<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>	
		<?php endif; ?>
		
    	<div class="item_1">
    		<label class="block title_1">Наименование:</label>
			<input type="text" name="FORM[name]" class="validate[required] input_1" id="pole">    
	    </div>    	
		

		
    	<div class="item_1">
    		<label class="block title_1">Описание:</label>
			<textarea name="FORM[description]" id="content" rows="10" class="text_1 tiny"></textarea>
	    </div>	

		<div class="item_1">
    		<label class="block title_1">URL баннера:</label>
			<input type="text" name="FORM[url]" class="input_1" id="pole">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Баннер:</label>
			<input type="file" name="file1" class="input_1">    
	    </div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>  