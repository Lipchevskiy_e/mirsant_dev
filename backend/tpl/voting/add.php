	<h1>Добавление нового вопроса</h1>
	
	<?php echo $msg ? $msg : '' ?>	
	
	<form id="commentForm" name="form_add" method="post" action="<?php echo general::link($tpl_folder.'/add') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
		<div class="item_1">
			<label class="block title_1">Текст вопроса:</label>
			<input type=text name="FORM[text]" class="validate[required] input_1" id="title">    
		</div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать:</label>
			<input name="FORM[status]" type="checkbox" id="status">
    	</div>		
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>    