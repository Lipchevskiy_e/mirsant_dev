	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Редактирование существующего опроса</h1>
	
	<?php echo $msg ? $msg : '' ?>
	
	<?php foreach ($result as $obj):?>
	
	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link('voting/editq') ; ?>" enctype='multipart/form-data'>
	
	    <div class="items_wrap">
	
			<div class="item_1 textcenter">
				<?php echo general::link_to('voting/index', 'Вернуться к общему списку'); ?>
				<input type="submit" name="submit" value="Сохранить">        	
			</div>
	
			<div class="item_1">
				<label class="block title_1">Вопрос:</label>
				<input type="text" name="FORM[q]" class="validate[required] input_1" id="q" value="<?=$obj->q?>" >    
			</div>
			
			<div class="item_1">
				<label for="status" class="block title_1">Публиковать?:</label>
				<input name="FORM[status]" type="checkbox" id="status" <?=$obj->status==1 ? checked : '' ?>>
    	    </div>
			
			<input type="hidden" name="id" value="<?=$obj->id?>" />
			
			<div class="item_1 textcenter">
				<?php echo general::link_to('voting/index', 'Вернуться к общему списку'); ?>
				<input type="submit" name="submit" value="Сохранить">        	
			</div>
		
		</div>
		
		<?php endforeach;?>
	
	</form>
	
	<script type="text/javascript">
		$(document).ready(function(){
			<!-- colorbox -->
		  $("a[rel='example1']").colorbox();
		});
	</script>	