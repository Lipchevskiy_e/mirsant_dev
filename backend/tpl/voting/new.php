	<h1>Редактирование вопроса/ответа&rArr; <?php echo $obj->text; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
    	<div class="item_1">
    		<label class="block title_1">Текст вопроса/ответа:</label>
			<input type=text name="FORM[text]" value='<?php echo $obj->text; ?>' class="validate[required] input_1" id="title">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Кол-во голосов:</label>
			<input type=text name="FORM[kolvo]" value='<?php echo $obj->kolvo; ?>' class="input_1" id="title">    
	    </div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>   