

	<h1>Редактирование ответа на вопрос</h1>
	
	<?php echo $msg ? $msg : '' ?>
	
	<?php foreach ($result as $obj):?>
	
	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link('voting/edita') ; ?>" enctype='multipart/form-data'>
	
	    <div class="items_wrap">
	
			<div class="item_1 textcenter">
				<?php echo general::link_to('voting/question', 'Вернуться к общему списку'); ?>
				<input type="submit" name="submit" value="Сохранить">        	
			</div>
	
			<div class="item_1">
				<label class="block title_1">Вопрос:</label>
				<select name="FORM[id_q]" class="validate[required]" id="id_q">
				<?php
					foreach ($select as $option)
					{
						if($obj->id_q == $option->id)
							echo "<option value='".$option->id."' selected='selected'>".$option->q."</option>";
						else
							echo "<option value='".$option->id."'>".$option->q."</option>";
					}
				?>
				</select>
			</div>
			
			<div class="item_1">
				<label class="block title_1">Ответ:</label>
				
				<input type="text" name="FORM[answer]" class="validate[required] input_1" id="answer" value="<?=$obj->answer?>" > 
				
			</div>
			
			
			<div class="item_1">
				<label class="block title_1">Кол-во голосов:</label>
				
				<input type="text" name="FORM[amount]" class="validate[required] input_1" id="amount" value="<?=$obj->amount?>" > 
				
			</div>
			
			<input type="hidden" name="id" value="<?=$obj->id?>" />
			
			<div class="item_1 textcenter">
				<?php echo general::link_to('voting/question', 'Вернуться к общему списку'); ?>
				<input type="submit" name="submit" value="Сохранить">        	
			</div>
		
		</div>
		
		<?php endforeach;?>
	
	</form>
	
	<script type="text/javascript">
		$(document).ready(function(){
			<!-- colorbox -->
		  $("a[rel='example1']").colorbox();
		});
	</script>