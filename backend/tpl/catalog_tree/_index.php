<h1>Управление группами товаров</h1>

<!-- фильтр НАЧАЛО -->
<div style="text-align:center; padding:0 0 20px;">
	
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
	
		<div class="search_sort">	
			<span class="search_sort_item">		
				<label for="name_group">Название</label><input type="text" name="name_group" value="<?php echo isset($_SESSION['name_group']) ? $_SESSION['name_group'] : ''; ?>">
			</span>
			<span class="item_last"><input type="submit" value="Найти"></span>
		
			<span class="search_sort_item">	
				<label for="status">Статус</label>
				<select name='status' class="middle" id="status" onChange="commentForm.submit();">
					<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
					<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
					<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
				</select>
			</span>
		</div>
		
	</form>
</div>
<!-- фильтр КОНЕЦ -->

<?php echo $msg ? $msg : '' ?>	

<form action="">
<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
  <thead>
	<tr>
		<th>Удаление</th>
		<th>Наименование группы</th>
		<th>Дата публикации</th>
		<th>Опубликован?</th>
		<th>Файл (обложка)?</th>
		<th>Операции</th>
	</tR>
  </thead>	

  <tbody id="sortable">
	<?php echo system::show_tpl(array('result'=>$result,'level'=>'&nbsp;','tpl_folder'=>$tpl_folder),$tpl_folder.'/menu.php'); ?>
  </tbody>
	
</table>
</form>


	<div id="info" align="center" style="color: red;">Манипулируйте строками для сортировки данных!</div>

	<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
			$("#sortable, .s").sortable({
			  update : function () {
				var order = $('#sortable').sortable('serialize');
				$("#info").load("<?php echo APPLICATION ?>/ajax/writesort.php?"+order+'&tablename=catalog_tree');
			  }
			});
			$('#sortable td').each(function(){
				$(this).width($(this).width())
			})
		});
	</script>	