<h1>Управление группами товаров</h1>

<!-- фильтр НАЧАЛО -->
<div style="text-align:center; padding:0 0 20px;">
	
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
	
		<div class="search_sort">	
			<span class="search_sort_item">		
				<label for="name_group">Название</label><input type="text" name="name_group" value="<?php echo isset($_SESSION['name_group']) ? $_SESSION['name_group'] : ''; ?>">
			</span>
			<span class="item_last"><input type="submit" value="Найти"></span>
		
			<span class="search_sort_item">	
				<label for="status">Статус</label>
				<select name='status' class="middle" id="status" onChange="commentForm.submit();">
					<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
					<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
					<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
				</select>
			</span>
		</div>
		
	</form>
</div>
<!-- фильтр КОНЕЦ -->

<?php echo $msg ? $msg : '' ?>	
	<form name='myfrm' action="" method='post'>
	 <input id='hinp' type='hidden' name='lang'>
	</form>
	<script type="text/javascript">
	function gopost(href,key,val)
	{
	 document.getElementById('hinp').name=key;
     document.getElementById('hinp').value=val;
	 document.myfrm.action=href;
	 document.myfrm.submit();	
	}
	</script>

	<div class="ps_table_title">
        <div class="coll_title">
			<span>Опубликован?</span>
			<span class="">Фото</span>			
			<span class="title_actions">Действие</span>
			

        </div>
        <div class="name_page">Наименование текущей страницы</div>
    </div>
	<div class="sort_block">
		<ul class="tree" id="sortable">
			<?php echo system::show_tpl(array('result'=>$result,'tpl_folder'=>$tpl_folder),$tpl_folder.'/menu.php'); ?>
		</ul>
	</div>

	<div id="info" align="center" style="color: red;">Манипулируйте строками для сортировки данных!</div>

	<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
			$('.sort_block ul').sortable({
			  update : function () {
				var sort_wrap = $(this);
					order = sort_wrap.sortable('serialize');
					$.ajax({
						type: "POST",
						url: "<?php echo APPLICATION ?>/ajax/writesort.php",
						data: order+'&tablename=catalog_tree'
					});
			  }
			});
			
			/*Код изменения статуса*/	
			$('.setStatus').click(function(){
				 var id  = $(this).attr('data-id');
				 var it  = $(this);
				jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'catalog_tree'}, function(data) {
					//alert($(this).text());
					if (data=='1') 
					{
						it.html("<span style='color:#39C;'>Да</span>");
					}
					if (data=='0') 
					{
						it.html("<span style='color:red;'>Нет</span>");
					}
				}); 
				return false;
			})
			/*конец кода изменения статуса*/
		});
	</script>	