	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Добавление новой группы товаров</h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/add') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		 
    	<div class="item_1">
    		<label class="block title_1">Наименование группы товаров:</label>
			<input type="text" name="FORM[pole]" class="validate[required] input_1" id="pole">    
	    </div>
        <div class="item_1">
            <label class="block title_1">Наименование группы товаров в единственном числе и винительном падеже:</label>
            <input type="text" name="FORM[vin]" class="validate[required] input_1" id="vin">    
        </div>
		<div class="item_1">
			<label class="block title_1">Наименование группы товаров для вывода в меню:</label>
			<input type="text" name="FORM[menu_name]" class="validate[required] input_1" id="vin">
		</div>
		<div class="item_1">
    		<label class="block title_1">URL группы товаров:</label>
			<input type=text name="FORM[url]" class="validate[required] input_1" id="url">    
	    </div>	
        
		<script type="text/javascript">
    	$(document).ready(function(){
    		<!-- переводим в транслит -->
    	  $('#pole').syncTranslit({destination: 'url', /*caseStyle: 'upper',*/ urlSeparator: '_'});
    	});
    	</script>

		<div class="item_1">
			<div class="inline">
				<label class="block title_1">Имеет объемные характеристики?</label>
				<select name="FORM[has_weight]" class="validate[required]" id="has_weight">
					<option value="1" selected>Да</option>
					<option value="0">Нет</option>
				</select>
			</div>
			<div class="inline">
				<label class="block title_1">Имеет тип?</label>
				<select name="FORM[has_type]" class="validate[required]" id="has_type">
					<option value="1" selected>Да</option>
					<option value="0">Нет</option>
				</select>
			</div>
			<div class="inline">
				<label class="block title_1">Имеет параметры для смесителей?</label>
				<select name="FORM[has_mixer]" class="validate[required]" id="has_mixer">
					<option value="0" selected>Нет</option>
					<option value="1">Да</option>
				</select>
			</div>
			<div class="inline">
				<label class="block title_1">Имеет параметры для душевых кабин и душевых боксов?</label>
				<select name="FORM[has_shower]" class="validate[required]" id="has_shower">
					<option value="0" selected>Нет</option>
					<option value="1">Да</option>
				</select>
			</div>

            <div class="inline">
                <label class="block title_1">Имеет параметры для ванн?</label>
                <select name="FORM[has_bathtub]" class="validate[required]" id="has_bathtub">
                    <option value="0" selected>Нет</option>
                    <option value="1">Да</option>
                </select>
            </div>
            <div class="inline">
                <label class="block title_1">Имеет параметры для унитазов?</label>
                <select name="FORM[has_toilet]" class="validate[required]" id="has_toilet">
                    <option value="0" selected>Нет</option>
                    <option value="1">Да</option>
                </select>
            </div>
			<div class="inline">
				<label class="block title_1">Имеет параметры для гидромассажных ванн?</label>
				<select name="FORM[has_gidr_van]" class="validate[required]" id="has_gidr_van">
					<option value="0">Нет</option>
					<option value="1">Да</option>
				</select>
			</div>
			<div class="inline">
				<label class="block title_1">Имеет параметры для тумб для ванн?</label>
				<select name="FORM[has_tumb_van]" class="validate[required]" id="has_tumb_van">
					<option value="0">Нет</option>
					<option value="1">Да</option>
				</select>
			</div>
			<div class="inline">
				<label class="block title_1">Имеет параметры для пеналов для ванн?</label>
				<select name="FORM[has_penal_van]" class="validate[required]" id="has_penal_van">
					<option value="0">Нет</option>
					<option value="1">Да</option>
				</select>
			</div>
			<div class="inline">
				<label class="block title_1">Имеет параметры для биде?</label>
				<select name="FORM[has_bide_mont]" class="validate[required]" id="has_bide_mont">
					<option value="0">Нет</option>
					<option value="1">Да</option>
				</select>
			</div>
			<div class="inline">
				<label class="block title_1">Имеет параметры для биде?</label>
				<select name="FORM[has_bide_mont]" class="validate[required]" id="has_bide_mont">
					<option value="0">Нет</option>
					<option value="1">Да</option>
				</select>
			</div>
			<div class="inline">
				<label class="block title_1">Имеет параметры монтажа кухонных моек?</label>
				<select name="FORM[has_kuh_mont]" class="validate[required]" id="has_kuh_mont">
					<option value="0">Нет</option>
					<option value="1">Да</option>
				</select>
			</div>
			<div class="inline">
				<label class="block title_1">Имеет параметры количество секций?</label>
				<select name="FORM[has_kuh_count]" class="validate[required]" id="has_kuh_count">
					<option value="0">Нет</option>
					<option value="1">Да</option>
				</select>
			</div>

		</div>
		<div class="clear"></div>
		
		<div class="item_1">
    		<label class="block title_1">Скидка (%):</label>
			<input type="text" name="FORM[skidka]" class=" input_1" id="skidka">    
	    </div>
		
		
    	<div class="item_1">
    		<label class="block title_1">Описание группы товаров:</label>
			<textarea name="FORM[text]" id="content" rows="10" class="text_1 tiny"></textarea>
	    </div>			

    	<div class="item_1">
    		<label class="block title_1">Группа товаров:</label>
			<select name='FORM[id_parent]' class="" id="id_parent">
				<option></option>
				<?php echo system::show_tpl(array('select'=>$select,'level'=>'&nbsp;','tpl_folder'=>$tpl_folder),$tpl_folder.'/select.php'); ?>
			</select>
	    </div>	
		
		
    	<div class="item_1">
    		<label class="block title_1">Файл (обложка):</label>
			<input type="file" name="file1" class="input_1">    
	    </div>
		
		<div class="item_1">
			<label class="block title_1">Текст кнопки "Оплата":</label>
			<input type="text" name="FORM[text_oplata]" class="input_1" id="vin">
		</div>
		
		<div class="item_1">
			<label class="block title_1">Способы оплаты:</label>
			<textarea name="FORM[payment]" id="content2" rows="10" class="text_1 tiny"></textarea>
		</div>
		
		<div class="item_1">
			<label class="block title_1">Текст кнопки "Доставка":</label>
			<input type="text" name="FORM[text_dostavka]" class="input_1" id="vin">
		</div>

		<div class="item_1">
			<label class="block title_1">Способы доставки:</label>
			<textarea name="FORM[delivery]" id="content3" rows="10" class="text_1 tiny"></textarea>
		</div>
		
		<div class="item_1">
			<label class="block title_1">Текст кнопки "Поднятие на этаж":</label>
			<input type="text" name="FORM[text_etag]" class="input_1" id="vin">
		</div>

		<div class="item_1">
			<label class="block title_1">Поднятие на этаж:</label>
			<textarea name="FORM[services]" id="content4" rows="10" class="text_1 tiny"></textarea>
		</div>
		
		<div class="item_1">
			<label class="block title_1">Текст кнопки "Стоимость доставки/установки, грузчиков:":</label>
			<input type="text" name="FORM[text_gruz]" class="input_1" id="vin">
		</div>

		<div class="item_1">
			<label class="block title_1">Стоимость доставки/установки,грузчиков:</label>
			<textarea name="FORM[cost_services]" id="content5" rows="10" class="text_1 tiny"></textarea>
		</div>
		
		<div class="item_1">
			<label class="block title_1">Текст кнопки "Информация о гарантии":</label>
			<input type="text" name="FORM[text_garant]" class="input_1" id="vin">
		</div>

		<div class="item_1">
			<label class="block title_1">Информация о гарантии:</label>
			<textarea name="FORM[warranty]" id="content6" rows="10" class="text_1 tiny"></textarea>
		</div>
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" id="status">
    	</div>
		
    	<div class="item_1">
    		<label class="block title_1">Заголовок H1 (для поисковых систем):</label>
			<input type="text" name="FORM[h1]" class="validate[required] input_1" id="h1">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Заголовок title (для поисковых систем):</label>
			<input type="text" name="FORM[title]" class="validate[required] input_1" id="title">    
	    </div>		
		
    	<div class="item_1">
    		<label class="block title_1">Заголовок keywords (для поисковых систем):</label>
			<textarea name="FORM[keywords]" id="content1" rows="6" class="text_1"></textarea>
	    </div>	

    	<div class="item_1">
    		<label class="block title_1">Заголовок description (для поисковых систем):</label>
			<textarea name="FORM[description]" id="content2" rows="6" class="text_1"></textarea>
	    </div>			
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>

	<style>
		.inline {
			display: inline-block;
			margin-right: 10px;
		}
	</style>