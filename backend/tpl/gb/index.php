<h1>Управление отзывами</h1>

<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
	
	<div class="search_sort">	
		<span class="search_sort_item">
			<label>Статус</label>	
			<select name='status' class="middle" id="status" onChange="commentForm.submit();">
				<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
				<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
				<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
			</select>
		</span>
	</div>
		
	</form>
</div>

<div class="clear"></div>

<?php echo $msg ? $msg : '' ?>	

<form id="commentForm1" name="commentForm1" method="post" action="<?php echo general::link($tpl_folder.'/authorize') ; ?>">

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0" id="kt_where">
	<tr>
		<th><input type="checkbox" class="color_reset" /></th>		
		<th>Удаление</th>
		<th>Кто оставил</th>
		<th>Email</th>
		<th>Город</th>
		<th>Дата публикации</th>
		<th>Опубликован?</th>
		<th>Операции</th>
	</tR>

	
<?php foreach ($result as $obj): ?>
	
	<tr>
		<td><input type="checkbox" name="kt_where[]" value="<?php echo $obj->id; ?>" /></td>
		<td><?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>
		<td><?php echo $obj->name; ?></td>
		<td><?php echo $obj->email; ?></td>
		<td><?php echo $obj->city; ?></td>
		<td><?php echo system::show_data($obj->created_at); ?></td>
		<td><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></td>		
		<td><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?></td>
	</tr>


<?php endforeach; ?>

	<tr>
		<th colspan="8">
		Выделенные: 
		<select name='st' class="" id="st">
			<option value="0">Что делаем?</option>
			<option value="1">Авторизовать</option>
			<option value="2">Удалить</option>
		</select>				
		<input type="submit" name="submit" id="go" value="Выполнить" onclick="return confirm('Вы уверены?')" disabled>
		</th>
	</tR>

</table>
</form>

	<script type="text/javascript">
	$(document).ready(function(){
		<!-- multyselect -->
		$('.color_reset').click(function(){
		if ($(this).attr('checked')==true) {
		  $('#kt_where input').attr('checked',true);
		} else {
		  $('#kt_where input').attr('checked',false);
		}
		})
		<!-- multyselect GO -->
		$('#st').change(function(){
			if ($(this).val()==1 || $(this).val()==2) {
				$('#go').attr('disabled','');
			} else {
				$('#go').attr('disabled','disabled');
			}
		})
		
		/*Код изменения статуса*/	
		$('.setStatus').click(function(){
			 var id  = $(this).attr('data-id');
			 var it  = $(this);
			jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'guestbook'}, function(data) {
				//alert($(this).text());
				if (data=='1') 
				{
					it.html("<span style='color:#39C;'>Да</span>");
				}
				if (data=='0') 
				{
					it.html("<span style='color:red;'>Нет</span>");
				}
			}); 
			return false;
		})
		/*конец кода изменения статуса*/
	});
	</script>	