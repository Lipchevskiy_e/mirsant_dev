	<h1>Редактирование фото &rArr; <?php echo $obj->pole; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Наименование фото:</label>
			<input type="text" name="FORM[pole]" value='<?php echo $obj->pole; ?>' class="input_1" id="pole">    
	    </div>
		
		<div class="item_1">
				<label class="block title_1">Фотоальбом:</label>
			<select name='FORM[id_parent]' class="" id="id_parent">
				<option></option>
				<?php echo system::show_tpl(array('select'=>$select,'level'=>'&nbsp;','curent_id_parent'=>$obj->id_parent,'tpl_folder'=>$tpl_folder),$tpl_folder.'/select.php'); ?>
			</select>	
		</div>		
		
    	<div class="item_1">
    		<label class="block title_1">Фото:</label>
			<?php if(file_exists(HOST.IMG_GALLERY_PATH.'/'.$obj->id.'.jpg')): ?>
				<a href="<?php echo IMG_GALLERY_PATH.'/'.$obj->id.'.jpg'; ?>" rel="example1"><img src="<?php echo IMG_GALLERY_PATH.'/_'.$obj->id.'.jpg'; ?>"></a>
				<?php echo general::link_to($tpl_folder.'/deletephoto/id/'.$obj->id,'Удалить фото?','onclick="return confirm(\'Вы уверены?\')"'); ?>
			<?php else: ?>				
				<input type="file" name="file1" class="input_1">    			
			<?php endif; ?>
	    </div>		

        <div class="item_1">
			<div class="item_1" style="height:40px;">
				<div style="float:left; width:30%;">
					<label for="status" class="block title_1">Показывать на главной?:</label>
					<input name="FORM[firstpage]" type="checkbox" <?php echo $obj->firstpage==1 ? checked : ''; ?> id="status">
				</div>		
				<div style="float:left; width:30%;">
					<label for="status" class="block title_1">Публиковать?:</label>
					<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
				</div>
				<div style="float:left; width:30%;">
					<label for="status" class="block title_1">Показывать наименование фото на сайте?:</label>
					<input name="FORM[alt]" type="checkbox" <?php echo $obj->alt==1 ? checked : ''; ?> id="alt">
				</div>
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>
	
	<script type="text/javascript">
	$(document).ready(function(){
		<!-- colorbox -->
	  $("a[rel='example1']").colorbox();
	});
	</script>	