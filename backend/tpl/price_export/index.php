<h1>Управление прайс генераторами</h1>

<?php echo $msg ? $msg : '' ?>	

<div class="price_wrap">
	<div class="price_ua">
		<h2>Price.ua</h2>
		<a href="/backend/price_export/priceUaGenerate" title="">Сгенерировать новый</a>

		<?php if (is_file(HOST.$folder.$priceUa['file_name'])): ?>
			<p>В последний раз прайс "<?php echo $priceUa['file_name'] ?>" был сгенерирован: <?php echo date ("d-m-Y H:i:s", filemtime(HOST.$folder.$priceUa['file_name'])) ?></p>
			<a href="/backend/price_export/getFile/regenerator/price_ua" target="_blank" title="">Скачать</a>
		<?php else: ?>
			<p class="danger">Прайс отсутствует</p>
		<?php endif ?>

	</div>
	<div class="hotline">
		<h2>Hotline</h2>
		<a href="/backend/price_export/hotlineGenerate" title="">Сгенерировать новый</a>

		<?php if (is_file(HOST.'/xml/hotline.xml')): ?>
			<p>В последний раз прайс "<?php echo $hotline['file_name'] ?>" был сгенерирован: <?php echo date ("d-m-Y H:i:s", filemtime(HOST.'/xml/hotline.xml')) ?></p>
			<a href="/backend/price_export/getFile/regenerator/hotline" target="_blank" title="">Скачать</a>
		<?php else: ?>
			<p class="danger">Прайс отсутствует</p>
		<?php endif ?>
	</div>
</div>


<style>
	.price_wrap > div {
		width: 48%; 
		border: 1px dashed grey;
		float: left; 
	}
	.price_wrap h2{
		border-bottom: 2px solid grey;
		padding-bottom: 5px;
	}
	p.danger {
		color: #FF2C2C;
		font-size: 16px;
	}
</style>
