	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Редактирование фотоальбома &rArr; <?php echo $obj->pole; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Наименование фотоальбома:</label>
			<input type="text" name="FORM[pole]" value='<?php echo $obj->pole; ?>' class="validate[required] input_1" id="pole">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Описание фотоальбома:</label>
			<textarea name="FORM[text]" id="content" rows="10" class="text_1 tiny"><?php echo $obj->text; ?></textarea>
	    </div>

    	<div class="item_1">
    		<label class="block title_1">Фотоальбом:</label>
			<select name='FORM[id_parent]' class="" id="id_parent">
				<option></option>
				<?php echo system::show_tpl(array('select'=>$select,'level'=>'&nbsp;','curent_id_parent'=>$obj->id_parent,'tpl_folder'=>$tpl_folder),$tpl_folder.'/select.php'); ?>
			</select>
	    </div>			
		
    	<div class="item_1">
    		<label class="block title_1">Файл (обложка):</label>
			<?php if(file_exists(HOST.IMG_GALLERY_GRUPA_PATH.'/'.$obj->id.'.jpg')): ?>
				<img src="<?php echo IMG_GALLERY_GRUPA_PATH.'/'.$obj->id.'.jpg'; ?>">
				<?php echo general::link_to($tpl_folder.'/deletephoto/id/'.$obj->id,'Удалить фото?','onclick="return confirm(\'Вы уверены?\')"'); ?>
			<?php else: ?>				
				<input type="file" name="file1" class="input_1">    				
			<?php endif; ?>
	    </div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>