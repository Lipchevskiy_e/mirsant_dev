		<?php foreach ($result as $obj): ?>
		
        <li id="listItem_<?php echo $obj->id; ?>">
            <span class="p_wr">
                <span class="control_link">
					<span class="ps_coll"><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></span>
					<span class="ps_coll">
						<?php if(file_exists(HOST.IMG_GALLERY_GRUPA_PATH.'/'.$obj->id.'.jpg')): ?>
							<span class="trigger_thumb"><img src="<?php echo IMG_GALLERY_GRUPA_PATH.'/'.$obj->id.'.jpg'; ?>"></span>
						<?php else: ?>
							<span style="color:red;">Нет</span>
						<?php endif; ?>					
					</span>
					<span class="ps_coll"><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?></span>
					<span class="ps_coll"><?php if (dbh::gallery_grupa_count_child($obj->id)==0 and dbh::gallery_grupa_count_child($obj->id)==0): ?>
					<?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?>
					<?php endif; ?></span>
					
				</span>
                <span class="name"><?php echo $level.$obj->pole; ?></span>
            </span>
			
			<?php echo dbh::gallery_grupa_count_child($obj->id)>0 ? '<ul>' : ''; ?>
				<?php echo system::show_tpl(array('result'=>dbh::gallery_grupa_get_child($obj->id),'tpl_folder'=>$tpl_folder),$tpl_folder.'/menu.php'); ?>
			<?php echo dbh::gallery_grupa_count_child($obj->id)>0 ? '</ul>' : ''; ?>
			
		</li>						
			
		<?php endforeach; ?>		