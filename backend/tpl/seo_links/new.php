<h1>Добавление</h1>

<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder . '/new/id/' . $obj->id); ?>"  enctype='multipart/form-data'>

    <div class="items_wrap">

        <div class="item_1 textcenter">
            <?php echo general::link_to($tpl_folder . '/index', 'Вернуться к общему списку'); ?>
            <input type="submit" name="submit" value="Сохранить">           
        </div>

        <div class="item_1">
            <label class="block title_1">Ссылка ( Все что анходится после домена, включая "/" в начале ):</label>
            <input type="text" name="FORM[link]" class="validate[required] input_1" id="link" value="<?php echo $obj->link; ?>">    
        </div>

        <div class="item_1">
            <label class="block title_1">Title:</label>
            <input type="text" name="FORM[title]" value="<?php echo $obj->title; ?>" class="validate[required] input_1" id="title">    
        </div>
        <div class="item_1">
            <label class="block title_1">H1:</label>
            <input type="text" name="FORM[h1]" value="<?php echo $obj->h1; ?>" class="validate[required] input_1" id="h1">    
        </div>
        <div class="item_1">
            <label class="block title_1">Keywords:</label>
            <textarea name="FORM[keywords]" id="keywords" rows="10" class="text_1"><?php echo $obj->keywords; ?></textarea>
        </div>
        <div class="item_1">
            <label class="block title_1">Description:</label>
            <textarea name="FORM[description]" id="description" rows="10" class="text_1"><?php echo $obj->description; ?></textarea>
        </div>
        
        <div class="item_1">
            <label class="block title_1">SEO текст:</label>
            <textarea name="FORM[seo_text]" id="seo_text" rows="10" class="text_1"><?php echo $obj->seo_text; ?></textarea>
        </div>

        <div class="item_1">
            <label for="status" class="block title_1">Публиковать?:</label>
            <input name="FORM[status]" type="checkbox" id="status" <?php echo $obj->status == 1 ? checked : ''; ?> />
        </div>

        <div class="item_1 textcenter">
            <?php echo general::link_to($tpl_folder . '/index', 'Вернуться к общему списку'); ?>
            <input type="submit" name="submit" value="Сохранить">           
        </div>

        <input name="id" type="hidden" value="<?php echo $obj->id; ?>" />

    </div>  

</form>  

