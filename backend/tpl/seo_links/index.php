<h1>Мета-теги по прямой ссылке</h1>
<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th>Ссылка</th>
        <th>Опубликован?</th>
        <th colspan="2">Операции</th>
    </tr>
    <?php foreach ($result as $obj): ?>
        <tr>
            <td><a href="<?php echo $obj->link; ?>" target="_blank"><?php echo $obj->link; ?></a></td>
            <td><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></td>     
            <td><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?></td>
            <td><?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить'); ?></td>
        </tr>
    <?php endforeach; ?>
</table>


<script type="text/javascript">
    $(function() {
        $('.setStatus').click(function(){
             var id  = $(this).attr('data-id');
             var it  = $(this);
            jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'seo_links'}, function(data) {
                if (data=='1') 
                {
                    it.html("<span style='color:#39C;'>Да</span>");
                }
                if (data=='0') 
                {
                    it.html("<span style='color:red;'>Нет</span>");
                }
            }); 
            return false;
        });
    });
</script>