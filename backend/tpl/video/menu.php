<?php foreach ($result as $obj): ?>
		
        <li id="listItem_<?php echo $obj->id; ?>">
            <span class="p_wr">
                <span class="control_link">
					<span class="ps_coll"><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></span>
					<span class="ps_coll"><?php echo system::show_data($obj->created_at); ?></span>
                    <span class="ps_coll">
						<?php if(file_exists(HOST.IMG_VIDEO_PATH.'/'.$obj->id.'.jpg')): ?>
							<span class="trigger_thumb"><img src="<?php echo IMG_VIDEO_PATH.'/'.$obj->id.'.jpg'; ?>"></span>
						<?php else: ?>
							<span style="color:red;">Нет</span>
						<?php endif; ?>					
					</span>
					<span class="ps_coll"><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?></span>
					<span class="ps_coll">
					   <?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?>
					</span>
				</span>
                <span class="name"><?php echo $obj->name; ?></span>
            </span>
			
		</li>						
			
<?php endforeach; ?>