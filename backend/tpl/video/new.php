	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Редактирование видео &rArr; "<?php echo $obj->name; ?>"</h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Наименование видео:</label>
			<input type="text" name="FORM[name]" value='<?php echo $obj->name; ?>' class="validate[required] input_1" id="name">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Описание видео:</label>
			<textarea name="FORM[text]" id="text" rows="10" class="text_1 tiny"><?php echo $obj->text; ?></textarea>
	    </div>
        
        <div class="item_1">
    		<label class="block title_1">Код YouTube:</label>
			<textarea name="FORM[code]" id="code" rows="5" class="text_1"><?php echo $obj->code; ?></textarea>
	    </div>			

    	<div class="item_1">
    		<label class="block title_1">Файл (обложка):</label>
			<?php if(file_exists(HOST.IMG_VIDEO_PATH.'/'.$obj->id.'.jpg')): ?>
				<img src="<?php echo IMG_VIDEO_PATH.'/'.$obj->id.'.jpg'; ?>">
				<?php echo general::link_to($tpl_folder.'/deletephoto/id/'.$obj->id,'Удалить фото?','onclick="return confirm(\'Вы уверены?\')"'); ?>
			<?php else: ?>				
				<input type="file" name="file1" class="input_1">    				
			<?php endif; ?>
	    </div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>