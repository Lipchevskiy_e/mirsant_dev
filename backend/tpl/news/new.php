	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Редактирование новости &rArr; <?php echo $obj->zag; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
		<?php if (TYPE_NEWS==1): ?>
    	<div class="item_1">
    		<label class="block title_1">Тип новостей:</label>
			<select name='FORM[type]' class="validate[required]" id="type">
				<?php foreach ($select as $key => $value): ?>
					<option value="<?php echo $key; ?>"<?php echo $key==$obj->type ? ' selected': '';?>><?php echo $value; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>	
		<?php endif; ?>		
		
    	<div class="item_1">
    		<label class="block title_1">Заголовок:</label>
			<input type="text" name="FORM[zag]" value='<?php echo $obj->zag; ?>' class="validate[required] input_1" id="zag">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">URL страницы:</label>
			<input type="text" name="FORM[url]" value="<?php echo $obj->url; ?>" class="validate[required] input_1" id="url">    
	    </div>		

    	<div class="item_1">
    		<label class="block title_1">Новость (кратко, для списка новостей):</label>
			<textarea name="FORM[text_short]" id="text_short" rows="10" class="text_1 tiny"><?php echo $obj->text_short; ?></textarea>
	    </div>		
		
    	<div class="item_1">
    		<label class="block title_1">Текстовое содержание раздела:</label>
			<textarea name="FORM[text]" id="content" rows="30" class="text_1 tiny"><?php echo $obj->text; ?></textarea>
	    </div>			
		
    	<div class="item_1">
    		<label class="block title_1">Дата новости:</label>
			<input type="text" value="<?php echo general::date_from_database($obj->date_news); ?>" class="validate[required] text_1" name="FORM[date_news]" id="datepicker" style="width: 20%;">			
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Файл (обложка):</label>
			<?php if(file_exists(HOST.IMG_NEWS_PATH.'/'.$obj->id.'.jpg')): ?>
				<a href="<?php echo IMG_NEWS_PATH.'/'.$obj->id.'.jpg'; ?>" rel="example1"><img src="<?php echo IMG_NEWS_PATH.'/_'.$obj->id.'.jpg'; ?>"></a>
				<?php echo general::link_to($tpl_folder.'/deletephoto/id/'.$obj->id,'Удалить фото?','onclick="return confirm(\'Вы уверены?\')"'); ?>
			<?php else: ?>				
				<input type="file" name="file1" class="input_1">    			
			<?php endif; ?>
	    </div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>    
	
	<script type="text/javascript">
	$(document).ready(function(){
		<!-- переводим в транслит -->
	  $('#zag').syncTranslit({destination: 'url', /*caseStyle: 'upper',*/ urlSeparator: '_'});
		<!-- colorbox -->
	  $("a[rel='example1']").colorbox();
	  	<!-- календарь -->
	  $.datepicker.setDefaults($.extend($.datepicker.regional["ru"]));
	  $("#datepicker").datepicker();	  
	});
	</script>