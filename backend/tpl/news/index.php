<h1>Управление новостями</h1>

<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
	
	<div class="search_sort">	
		<span class="search_sort_item">
			<label>Статус</label>	
			<select name='status' class="middle" id="status" onChange="commentForm.submit();">
				<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
				<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
				<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
			</select>
		</span>
	</div>
		
	</form>
</div>

<?php echo $msg ? $msg : '' ?>	

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<?php if ($_SESSION['manager'] != true): ?>
			<th>Удаление</th>
		<?php endif ?>
		<?php if (TYPE_NEWS==1): ?>
			<th>Тип новостей</th>
		<?php endif; ?>
		<th>Заголовок новости</th>
		<th>Дата публикации</th>
        <th>Обложка</th>
		<th>Опубликован?</th>
		<th>Операции</th>
	</tr>

	
<?php foreach ($result as $obj): ?>
	
	<tr>
		<?php if ($_SESSION['manager'] != true): ?>
			<td><?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>
		<?php endif ?>
		<?php if (TYPE_NEWS==1): ?>
			<td><?php echo $type_news[$obj->type]; ?></td>
		<?php endif; ?>			
		<td><?php echo $obj->zag; ?></td>
		<td><?php echo system::show_data($obj->date_news); ?></td>
        <td>
            <?php if(file_exists(HOST.IMG_NEWS_PATH.'/'.$obj->id.'.jpg')): ?>
    		      <span class="trigger_thumb"><img src="<?php echo IMG_NEWS_PATH.'/_'.$obj->id.'.jpg'; ?>"></span>
    		<?php else: ?>
    		      <span style="color:red;">Нет</span>
    		<?php endif; ?>	
        </td>
		<td><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></td>		
		<td><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?></td>
	</tr>


<?php endforeach; ?>

</table>

	<?php 
	// pager
	if (!isset($_POST['status'])) {
		echo $_total_news>ADMIN_NEWS_AT_PAGE ? pager::pager_J(_APPLICATION."/".$tpl_folder."/index",ADMIN_NEWS_AT_PAGE,$_total_news,intval($_GET['page'])) : '';
	}
	?>


<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
		/*Код изменения статуса*/	
		$('.setStatus').click(function(){
			 var id  = $(this).attr('data-id');
			 var it  = $(this);
			jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'news'}, function(data) {
				//alert($(this).text());
				if (data=='1') 
				{
					it.html("<span style='color:#39C;'>Да</span>");
				}
				if (data=='0') 
				{
					it.html("<span style='color:red;'>Нет</span>");
				}
			}); 
			return false;
		})
		/*конец кода изменения статуса*/
		});
</script>	