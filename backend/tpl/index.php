<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- (c) Wezom web-студия | http://www.wezom.com.ua/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<!--<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> -->
<title>Web-студия «Wezom»</title>
<script type="text/javascript" src="/backend/js/jquery-1.5.min.js"></script>
<script type="text/javascript" src="/backend/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/backend/js/main.js"></script>
<script type="text/javascript" src="/backend/js/jquery.litree_cms.js"></script>
<!--<script type="text/javascript" src="/backend/js/general.js"></script>-->
<script type="text/javascript" src="/backend/js/jquery.synctranslit.min.js"></script>
<link type="text/css" rel="stylesheet" href="/backend/css/style.css" />
<link type="text/css" rel="stylesheet" href="/backend/css/litree_cms.css" />

<script type="text/javascript" src="/backend/js/jquery.validationEngine-ru.js"></script>
<script type="text/javascript" src="/backend/js/jquery.validationEngine.js"></script>
<link type="text/css" rel="stylesheet" href="/backend/css/validationEngine.jquery.css" />

<link type="text/css" rel="stylesheet" href="/backend/css/colorbox.css" />
<script type="text/javascript" src="/backend/js/jquery.colorbox-min.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js"></script>

<script src="/backend/js/i18n/jquery-ui-i18n.js" type="text/javascript"></script>
<link type="text/css" href="/backend/css/smoothness/jquery-ui-1.8.2.custom.css" rel="stylesheet" />

<script type="text/javascript" src="/js/swfobject.js"></script>


<!--[if IE 6]>
		<script src="/backend/js/DD_belatedPNG_0.0.8a-min.js"></script>
		<script>
			DD_belatedPNG.fix('.png, img');
		</script>  
<![endif]-->
</head>
<body>

<div class="conteiner">

	<!-- выводим меню -->
	<?php echo $_GET['action']!='print' ? $menu : ''; ?>
	<!-- выводим меню -->

	
	<!-- выводим весь контент -->
	<?php echo $center_block; ?>
	<!-- выводим весь контент -->

	<?php if (SUPER_ADMIN==1) { ?>
	<?php if ($_SESSION['user_backend']=='superadmin'): ?>
		<div align="center"><?php echo general::link_to('index/access/user_backend/admin','Войти под админом?'); ?></div>
	<?php else: ?>
		<div align="center"><?php echo general::link_to('index/access/user_backend/superadmin','Войти под суперадмином?'); ?></div>
	<?php endif; ?>
    <?php } ?>
	<div align="center" style="marging: 0 auto;">
	<?php DEBUG_ADMIN==1 ? debug() : ''; ?>
	</div>

</div>

	<script type="text/javascript">
	$(document).ready(function(){
		<!-- валидация -->
	  $('#commentForm').validationEngine();
	});
	</script>	

</body>
</html>