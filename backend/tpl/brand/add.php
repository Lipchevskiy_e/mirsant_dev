	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Добавление нового бренда</h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/add') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Наименование брнеда:</label>
			<input type="text" name="FORM[pole]" class="validate[required] input_1" id="pole">    
	    </div>
		
		<div class="item_1">
    		<label class="block title_1">Скидка (%):</label>
			<input type="text" name="FORM[skidka]" class=" input_1" id="skidka">    
	    </div>

    	<div class="item_1">
    		<label class="block title_1">Описание бренда:</label>
			<textarea name="FORM[text]" id="content" rows="10" class="text_1 tiny"></textarea>
	    </div>	
        
        <div class="item_1">
    		<label class="block title_1">Логотип:</label>
			<input type="file" name="file1" class="input_1">    
	    </div>		
		
		<div class="item_1">
			<label class="block title_1">Серии:</label>
			<select name="SERIES[]" id="series" multiple="multiple" size="10" style="min-width:150px;">
				<option style="color:#c00;">нет серий</option>
				<?foreach($series AS $s):?>
					<option value="<?=$s->id?>"><?=$s->name?></option>
				<?endforeach?>
			</select>
		</div>
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" id="status" checked>
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>  