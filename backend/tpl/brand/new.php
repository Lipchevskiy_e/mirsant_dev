	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Редактирование бренда &rArr; <?php echo $obj->pole; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Наименование бренда:</label>
			<input type="text" name="FORM[pole]" value='<?php echo $obj->pole; ?>' class="validate[required] input_1" id="pole">    
	    </div>
		
		<div class="item_1">
    		<label class="block title_1">Скидка (%):</label>
			<input type="text" name="FORM[skidka]" value='<?php echo $obj->skidka; ?>' class="input_1" id="skidka">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Описание бренда:</label>
			<textarea name="FORM[text]" id="content" rows="10" class="text_1 tiny"><?php echo $obj->text; ?></textarea>
	    </div>	
        
        <div class="item_1">
    		<label class="block title_1">Логотип:</label>
			<?php if(file_exists(HOST.IMG_BRAND_PATH.'/'.$obj->id.'.jpg')): ?>
				<img src="<?php echo IMG_BRAND_PATH.'/'.$obj->id.'.jpg'; ?>">
				<?php echo general::link_to($tpl_folder.'/deletephoto/id/'.$obj->id,'Удалить фото?','onclick="return confirm(\'Вы уверены?\')"'); ?>
			<?php else: ?>				
				<input type="file" name="file1" class="input_1">    				
			<?php endif; ?>
	    </div>			
		
		<div class="item_1">
			<label class="block title_1">Серии:</label>
			<select name="SERIES[]" id="series" multiple="multiple" size="10" style="min-width:150px;">
				<option style="color:#c00;">нет серий</option>
				<?foreach($series AS $s):?>
					<?$k = 0;?>
					<?foreach($this_series AS $ts):?>
						<?if($s->id == $ts->id_serie):?>
							<?$k = 1?>
						<?endif?>
					<?endforeach?>
					<option value="<?=$s->id?>" <?= $k == 1 ? 'selected' : '' ?>><?=$s->name?></option>
				<?endforeach?>
			</select>
		</div>
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>