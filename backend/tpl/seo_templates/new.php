<h1>Редактировать "<?php echo $obj->name ?>"</h1>

<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new/id/'.$obj->id) ; ?>"  enctype='multipart/form-data'>

    <div class="items_wrap">

        <div class="item_1 textcenter">
            <?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
            <input type="submit" name="submit" value="Сохранить">           
        </div>

        <div class="item_1">
            <ul>
                <li><b>{group}</b> - Наименование группы товаров</li>
                <li><b>{group_vin}</b> - Наименование группы товаров в винительном падеже и единственном числе (при незаполненом поле в каталоге, будет браться обычное название группы)</li>
                <?php if ($obj->id == 1 || $obj->id == 3): ?>
                    <li><b>{brand}</b> - Производитель</li>
                <?php endif ?>
                <?php if ($obj->id == 3): ?>
                    <li><b>{serie}</b> - Серия</li>
                <?php endif ?>
            </ul>
        </div>

        <div class="item_1">
            <label class="block title_1">Title:</label>
            <input type="text" name="FORM[title]" value="<?php echo $obj->title; ?>" class="validate[required] input_1" id="title">    
        </div>
        <div class="item_1">
            <label class="block title_1">H1:</label>
            <input type="text" name="FORM[h1]" value="<?php echo $obj->h1; ?>" class="validate[required] input_1" id="h1">    
        </div>
        <div class="item_1">
            <label class="block title_1">Keywords:</label>
            <textarea name="FORM[keywords]" id="keywords" rows="10" class="text_1"><?php echo $obj->keywords; ?></textarea>
        </div>
        <div class="item_1">
            <label class="block title_1">Description:</label>
            <textarea name="FORM[description]" id="description" rows="10" class="text_1"><?php echo $obj->description; ?></textarea>
        </div>

        <div class="item_1 textcenter">
            <?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
            <input type="submit" name="submit" value="Сохранить">           
        </div>

        <input name="id" type="hidden" value="<?php echo $obj->id; ?>" />

    </div>  

</form>  

