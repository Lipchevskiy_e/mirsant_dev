	<h1>Настройки СЕО</h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/edit') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
    
    	<div class="item_1">
    		<label class="block title_1">Наименование раздела (где используются текущие МетаТеги):</label>
        	<input type="text" name="FORM[name_rasdel]" id="name_rasdel" value="<?php echo $obj->name_rasdel; ?>" class="input_1" disabled>
    	</div>
        <div class="item_1">
    		<label class="block title_1">H1:</label>
			<input type=text name="FORM[h1]" id="h1" value='<?php echo $obj->h1; ?>' class="validate[required] input_1">        	
    	</div>
        <div class="item_1">
    		<label class="block title_1">Title:</label>
			<input type=text name="FORM[title]" id="title" value='<?php echo $obj->title; ?>' class="validate[required] input_1">        	
    	</div>
	    <div class="item_1">
    		<label class="block title_1">Keywords:</label>
    		<textarea name="FORM[keywords]" id="keywords" rows="7" class="validate[required] text_1"><?php echo $obj->keywords; ?></textarea>
    	</div>
	    <div class="item_1">
    		<label class="block title_1">Description:</label>
    		<textarea name="FORM[description]" id="description" rows="7" class="validate[required] text_1"><?php echo $obj->description; ?></textarea>
    	</div>
    	
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>    