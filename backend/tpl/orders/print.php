				
				<div style="margin: 50px;">
                    
                    <h3>Информация о заказе №<?php echo $obj->id; ?></h3>
                    <img src="/pic/logo.png" alt="" width="200" class="logo_2" />
                    <p><?php echo NAME_FIRMS; ?></p>
                    
                    <strong>Информация о заказе</strong>
                    <table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="50%">Номер заказа:</td>
                        <td><?php echo $obj->id; ?></td>
                      </tr>
                      <tr>
                        <td>Дата заказа:</td>
                        <td><?php echo system::show_data($obj->created_at); ?></td>
                      </tr>
                      <tr>
                        <td>Статус заказа:</td>
                        <td><?php echo $select[$obj->status]; ?></td>
                      </tr>
                    </table>
                    <br />
                    <strong>Информация о клиенте</strong>
					
                    <table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="50%">Имя</td>
                        <td><?php echo $obj->name; ?></td>
                      </tr>
                      <tr>
                        <td>Адрес</td>
                        <td><?php echo $obj->address; ?></td>
                      </tr>
                      <tr>
                        <td>Телефон</td>
                        <td><?php echo $obj->phone; ?></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><?php echo $obj->email; ?></td>
                      </tr>
                      <tr>
                        <td>Примечание</td>
                        <td><?php echo $obj->note; ?></td>
                      </tr>
                    </table>
                    <br />
					<strong>Содержание заказа</strong>
					
                    <table class="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <th>Артикул</th>
                        <th>Название</th>
                        <th>Цена за штуку</th>
                        <th>Кол-во</th>
                        <th>Промежуточный итог</th>
                      </tr>
					  
					<?php $_total=0; ?>

					<?php foreach ($result0 as $d_ord): ?>
						
						<tr>
                            <td><?php echo $d_ord->artikul; ?></td>
							<td><?php echo $d_ord->name; ?></td>
							<td><?php echo mirsant::format_Cost($d_ord->cost); ?> грн.</td>
                            <td><?php echo $d_ord->kolvo; ?></td>
							<td><?php echo mirsant::format_Cost($d_ord->kolvo*$d_ord->cost); ?> грн.</td>
						</tr>

						<?php $_total+=$d_ord->kolvo*$d_ord->cost; ?>					  
						
					<?php endforeach; ?>
					  
                    </table>
                    
					<br />
					
                    <div align="right">
                        <strong>Итого:</strong> <?php echo mirsant::format_Cost($_total) ?> грн.
                    </div>
					
				</div>				
					
					
