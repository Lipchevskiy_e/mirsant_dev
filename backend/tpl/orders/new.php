	<h1>Редактирование заказа &rArr; №<?php echo $obj->id; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link('orders/new') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to('orders/index', 'Вернуться к общему списку'); ?>
			<?php if ($_SESSION['manager'] != true): ?>
				<input type="submit" name="submit" value="Сохранить">  
			<?php endif ?>
			<?php echo general::link_to('orders/print/id/'.$obj->id, 'Распечатать заказ', 'target="_blank"'); ?>
        </div>
		
    	<div class="item_1">		
			<label class="block title_1">Данные пользователя:</label>
			
			<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th>Имя</th>
					<th>Адрес</th>
					<th>Телефон</th>
					<th>Email</th>
					<th>Примечание</th>
				</tr>
				
				<tr>
					<td><?php echo $obj->name; ?></td>
					<td><?php echo $obj->address; ?></td>
					<td><?php echo $obj->phone; ?></td>
					<td><?php echo $obj->email; ?></td>
					<td><?php echo $obj->note; ?></td>
				</tr>				
				
			</table>				

	    </div>			
		
    	<div class="item_1">

			<label class="block title_1">Данные о товаре:</label>
			<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<?php if ($_SESSION['manager'] != true): ?>
						<th>Удаление</th>
					<?php endif; ?>
					<th>Артикул товара</th>
					<th>Наименование товара</th>
					<th>Цена товара</th>
					<th>Кол-во</th>
					<th>Итого</th>
				</tR>
				
			<?php $_total=0; ?>

			<?php foreach ($result0 as $d_ord): ?>
				
				<tr>
					<?php if ($_SESSION['manager'] != true): ?>
						<td><?php echo general::link_to('orders/deleteOrders/id_order/'.$d_ord->id.'/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>
					<?php endif ?>
					<td><?php echo $d_ord->artikul; ?></td>
					<td><?php echo $d_ord->name; ?></td>
					<td><?php echo mirsant::format_cost($d_ord->cost); ?></td>
					<td><?php echo $d_ord->kolvo; ?></td>
					<td><?php echo mirsant::format_cost($d_ord->kolvo*$d_ord->cost); ?></td>
				</tr>

				<?php $_total+=$d_ord->kolvo*$d_ord->cost; ?>
				
			<?php endforeach; ?>

				<tr>
					<th colspan="<?php echo $_SESSION['manager'] == true ? 4 : 5 ?>">ВСЕГО:</th>
					<th><?php echo mirsant::format_cost($_total); ?></th>
				</tR>			
				
			</table>

	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Статус товара:</label>
			<select name='FORM[status]' id="status" <?php echo $_SESSION['manager'] == true ? 'disabled' : '' ?>>
				<?php foreach ($select as $key => $value): ?>
					<option value="<?php echo $key; ?>"<?php echo $key==$obj->status ? ' selected': '';?>><?php echo $value; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>	
		
        <div class="item_1 textcenter">
			<?php echo general::link_to('orders/index', 'Вернуться к общему списку'); ?>
			<?php if ($_SESSION['manager'] != true): ?>
				<input type="submit" name="submit" value="Сохранить">    
			<?php endif ?>
			<?php echo general::link_to('orders/print/id/'.$obj->id, 'Распечатать заказ', 'target="_blank"'); ?>
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>	