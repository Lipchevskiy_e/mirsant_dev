<h1>Статистика по покупателям</h1>

<?php echo $msg ? $msg : '' ?>	

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th>Покупатель</th>
		<th>Кол-во покупок</th>
		<th>Сумма покупок</th>		
	</tR>

<?php foreach ($result as $obj): ?>
	
	<tr>
		<td><?php echo general::link_to('users/new/id/'.$obj->id_user,registration::get_User_By_Id($obj->id_user)->name.' ['.registration::get_User_By_Id($obj->id_user)->id.']','target="_blank"'); ?></td>
		<td><?php echo $obj->count; ?>
		&nbsp;
		<div style="float:right;" id="info<?= $obj->id_user ?>"><a href="#" class="detail" id="<?= $obj->id_user ?>">Список покупок</a></div>
		</td>
		<td><?php echo mirsant::format_cost($obj->summa); ?></td>		
	</tr>


<?php endforeach; ?>

	
</table>


	<script type="text/javascript">
	$(document).ready(function(){
		$('.detail').click(function(){
			//alert(this.id)
			var id=this.id;
			jQuery.post('<?php echo APPLICATION; ?>/ajax/stat_users.php', {'id': this.id}, function(data) {
							//alert('Сервер ответил: ' + data);
							$('#info' + id).empty().html(data.list_pokupok);
						},'json'); // POST-запрос к /ajax/script.php
			
		})
	});
	</script>	