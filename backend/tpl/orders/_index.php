<h1>Управление заказами</h1>

<!-- фильтр НАЧАЛО -->
<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link('orders/index') ; ?>">

		<div class="search_sort">	
			<span class="search_sort_item">
				<label>Дата заказа C </label><input type="text" name="date_s" id="datepicker">
			</span>
			<span class="search_sort_item">
				<label>Дата заказа ПО </label><input type="text" name="date_po" id="datepicker1">
			</span>
			<span class="item_last"><input type="submit" value="Найти"></span>

			<span class="search_sort_item">				
				<label>Статус</label>
				<select name='status_orders' class="middle" id="status" onChange="commentForm.submit();">
					<option value="3" <?php echo $status_orders==3 ? 'selected' : ''; ?>>Все</option>
					<option value="2" <?php echo $status_orders==2 ? 'selected' : ''; ?>>Проведенный</option>
					<option value="1" <?php echo $status_orders==1 ? 'selected' : ''; ?>>В работе</option>
					<option value="0" <?php echo $status_orders==0 ? 'selected' : ''; ?>>Новый</option>
				</select>
			</span>
		</div><!-- .search_sort--> 
		
	</form>
</div>
<!-- фильтр КОНЕЦ -->


<?php echo $msg ? $msg : '' ?>	

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th>Удаление</th>
		<th>№ заказа</th>
		<th>Покупатель</th>		
		<th>Сумма заказа</th>
		<th>Дата заказа</th>
		<th>Дата проведения</th>
		<th>Статус заказа</th>
		<th>Операции</th>
	</tR>

<?php foreach ($result as $obj): ?>
	
	<tr>
		<td><?php echo general::link_to('orders/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>
		<td><?php echo $obj->id; ?></td>
		<td><?php echo general::link_to('users/new/id/'.$obj->id_user,registration::get_User_By_Id($obj->id_user)->name.' ['.registration::get_User_By_Id($obj->id_user)->id.']','target="_blank"'); ?></td>
		<td><?php echo mirsant::format_cost($obj->cost); ?></td>
		<td><?php echo $obj->created_at ? system::show_data($obj->created_at) : '<span style="color:red;">Нет</span>'; ?></td>
		<td><?php echo $obj->updated_at ? system::show_data($obj->updated_at) : '<span style="color:red;">Нет</span>'; ?></td>
		<td><?php echo $status_order[$obj->status]; ?></td>		
		<td><?php echo general::link_to('orders/new/id/'.$obj->id,'Редактировать'); ?></td>
	</tr>


<?php endforeach; ?>

	
</table>


	<script type="text/javascript">
	$(document).ready(function(){
	  	<!-- календарь -->
	  $.datepicker.setDefaults($.extend($.datepicker.regional["ru"]));
	  $("#datepicker").datepicker();
	  $("#datepicker1").datepicker();
	});
	</script>