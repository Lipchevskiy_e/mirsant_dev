	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Редактирование обложки &rArr; <?php echo $obj->name; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Наименование:</label>
			<input type="text" name="FORM[name]" value='<?php echo $obj->name; ?>' class="input_1" id="pole">    
	    </div>  
		
    	<div class="item_1">
    		<label class="block title_1">URL:</label>
			<input type="text" name="FORM[url]" value='<?php echo $obj->url; ?>' class="input_1" id="url">    
	    </div>  
		
    	<div class="item_1">
    		<label class="block title_1">Обложка (<strong>размер не менее 532px*381px</strong>):</label>
			<?php if(file_exists(HOST.IMG_SPLASH_PATH.'/_'.$obj->id.'.jpg')): ?>
				<a href="<?php echo IMG_SPLASH_PATH.'/'.$obj->id.'.jpg'; ?>" rel="example1"><img src="<?php echo IMG_SPLASH_PATH.'/_'.$obj->id.'.jpg'; ?>"></a>
				<?php echo general::link_to($tpl_folder.'/deletephoto/id/'.$obj->id,'Удалить обложку?','onclick="return confirm(\'Вы уверены?\')"'); ?>
			<?php else: ?>				
				<input type="file" name="file1" class="input_1">    				
			<?php endif; ?>
	    </div>		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>
    
    <script>
    $(document).ready(function(){
        $("a[rel='example1']").colorbox();
    });
    </script>