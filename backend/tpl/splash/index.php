<h1>Управление обложками</h1>

<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
	
	<div class="search_sort">	
		<span class="search_sort_item">
			<label>Статус</label>	
			<select name='status' class="middle" id="status" onChange="commentForm.submit();">
				<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
				<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
				<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
			</select>
        </span>	
	</div>
		
	</form>
</div>

<?php echo $msg ? $msg : '' ?>	

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
  <thead>
	<tr>
		<th>Удаление</th>
		<th>Наименование</th>
		<th>Дата публикации</th>
        <th>Фото</th>
		<th>Опубликован?</th>
		<th>Операции</th>
	</tr>
  </thead>

	<tbody id="sortable">	
<?php foreach ($result as $obj): ?>
	
	<tr id="listItem_<?php echo $obj->id; ?>">
		<td><?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>		
		<td><?php echo $obj->name; ?></td>
		<td><?php echo system::show_data($obj->created_at); ?></td>
        <td>
            <?php if(file_exists(HOST.IMG_SPLASH_PATH.'/'.$obj->id.'.jpg')): ?>
				<span class="trigger_thumb"><img src="<?php echo IMG_SPLASH_PATH.'/_'.$obj->id.'.jpg'; ?>"></span>
			<?php else: ?>
				<span style="color:red;">Нет</span>
			<?php endif; ?>	
        </td>
		<td><?php echo $obj->status==1 ? 'Да' : '<span style="color:red;">Нет</span>'; ?></td>			
		<td><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?></td>
	</tr>


<?php endforeach; ?>
  </tbody>
</table>



	<div id="info" align="center" style="color: red;">Манипулируйте строками для сортировки данных!</div>

	<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
			$("#sortable, .s").sortable({
			  update : function () {
				var order = $('#sortable').sortable('serialize');
				$.ajax({
					type: "POST",
					url: "<?php echo APPLICATION ?>/ajax/writesort.php",
					data: order+'&tablename=splash'
				});				
			  }
			});
			$('#sortable td').each(function(){
				$(this).width($(this).width())
			})
		});
	</script>	
