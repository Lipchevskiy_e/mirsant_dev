	<h1>Редактирование пользователя &rArr; <?php echo $obj->name; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new') ; ?>">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Имя:</label>
			<input type="text" name="FORM[name]" value='<?php echo $obj->name; ?>' class="validate[required] input_1" id="name">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Адрес:</label>
			<input type="text" name="FORM[address]" value='<?php echo $obj->address; ?>' class="input_1" id="address">    
	    </div>

		<div class="item_1">
    		<label class="block title_1">Телефон:</label>
			<input type="text" name="FORM[phone]" value='<?php echo $obj->phone; ?>' class="validate[required] input_1" id="phone">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Email:</label>
			<input type="text" name="FORM[email]" value='<?php echo $obj->email; ?>' class="validate[required,custom[email]] input_1" id="email">    
	    </div>

		<div class="item_1">
    		<label class="block title_1">Логин:</label>
			<input type="text" name="FORM[login]" value='<?php echo $obj->login; ?>' class="validate[required] input_1" id="login">    
	    </div>
		
		<div class="item_1">
    		<label class="block title_1">Пароль:</label>
			<input type="text" name="FORM[pasword]" value='<?php echo $obj->pasword; ?>' class="validate[required] input_1" id="pasword">    
	    </div>	

        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>