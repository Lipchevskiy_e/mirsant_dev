<?php foreach ($select as $obj): ?>
			<?php $children=dbh::catalog_tree_get_child($obj->id); ?>
			
			<?php if(count($children)): ?>
			    <option style="font-weight: bold;" value="<?php echo $obj->id; ?>"><?php echo $obj->pole; ?></option>
                <?php echo system::show_tpl(array('select'=>$children,'level'=>$level.'&nbsp;&nbsp;','tpl_folder'=>$tpl_folder),$tpl_folder.'/select.php'); ?>
            <?php else: ?>
				<option value="<?php echo $obj->id; ?>" style="cursor: pointer;"><?php echo $level.$obj->pole; ?></option>
			<?php endif; ?>
			
<?php endforeach; ?>