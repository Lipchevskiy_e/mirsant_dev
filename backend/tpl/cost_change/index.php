	<h1>Изменение цен в каталоге</h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>"  enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
        <div class="item_1" id="where_2">
        		<label class="block title_1">Где изменяем цену?:</label>
    			<select name='where' id="where">
    				<option></option>
                    <option value="1">В товарах определенной группы</option>
                    <option value="2">В товарах определенного производителя</option>
                    <option value="3">В товарах производителя определенной группы</option>
                    <option value="4">В определенных товарах</option>
    			</select>
  	    </div>
        
        <div class="item_1" id="cost_plus">
        		<label class="block title_1">Увеличить на (%):</label>
    			<input type="text" value="" name="FORM[cost_plus]" />
  	    </div>
            
        <div class="item_1" id="cost_minus">
        		<label class="block title_1">Уменьшить на (%):</label>
    			<input type="text" value="" name="FORM[cost_minus]" />
  	    </div>
            
        <div class="item_1 textcenter">
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>  
    
<script>
    $(document).ready(function(){
        $('#cost_plus').hide();
        $('#cost_minus').hide();
        
        $('#where').live('change', function(){
            var val = $(this).val();
            jQuery.post('/backend/ajax/summa.php', {'val':val}, function(data) {

							if (data) {
							     if(data.choose == 1)
							     {
							        if($('#brands').is(':visible')) 
							             $('#brands').remove();
                                    
                                    if($('#grups2').is(':visible'))
                                    {
                                        $('#grups2').remove();
                                        $('#brand2').remove();
                                    }
                                    if($('#catalog').is(':visible')) 
                                        $('#catalog').remove();
                                    
                                    $('#where_2').after(data.str);
                                 }
                                 if(data.choose == 2)
							     {
							        if($('#grups').is(':visible')) 
                                        $('#grups').remove();
                                    if($('#grups2').is(':visible'))
                                    {
                                        $('#grups2').remove();
                                        $('#brand2').remove();
                                    }
                                    if($('#catalog').is(':visible')) 
                                        $('#catalog').remove();
								    $('#where_2').after(data.str);
                                 }
                                 if(data.choose == 3)
							     {
							        if($('#brands').is(':visible')) 
							             $('#brands').remove();
                                    
                                    if($('#grups').is(':visible')) 
                                        $('#grups').remove();
                                        
                                    if($('#catalog').is(':visible')) 
                                        $('#catalog').remove();
                                    
                                    $('#where_2').after(data.str);
                                 }
                                 if(data.choose == 4)
							     {
							        if($('#brands').is(':visible')) 
							             $('#brands').remove();
                                    
                                    if($('#grups').is(':visible')) 
                                        $('#grups').remove();
                                    
                                    if($('#grups2').is(':visible'))
                                    {
                                        $('#grups2').remove();
                                        $('#brand2').remove();
                                    }
                                    
                                    $('#where_2').after(data.str);
                                 }
                                 
                                $('#cost_plus').show();
								$('#cost_minus').show(); 
							} 
							
			},'json');
        });
        
        $('#id_parent2').live('change', function(){
            var val = $(this).val();
            jQuery.post('/backend/ajax/brands.php', {'id':val}, function(data) {
                            if($('#brand2').is(':visible')) 
                                 $('#brand2').remove();
							
                            if (data) {
                                $('#grups2').after(data.str);
							} 
							
			},'json');
        });
    });
</script>