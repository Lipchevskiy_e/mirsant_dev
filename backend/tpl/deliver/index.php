<h1>Управление стоимостью доставки</h1>

<!-- фильтр НАЧАЛО -->
<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
	
       <div class="filter_sort">	
		<span class="search_sort_item">
			<label>Город</label>
			<select name='city' class="" id="city" onChange="commentForm.submit();" style="width:100px;">
				<option value="0">Все</option>
				<?php foreach ($city as $city): ?>
					<option value="<?php echo $city->id; ?>"<?php echo $city->id==$_city ? ' selected': '';?>><?php echo $city->pole; ?></option>
				<?php endforeach; ?>			
			</select>
		</span>
		
		<span class="search_sort_item">
			<label>Статус</label>
			<select name='status' class="" id="status" onChange="commentForm.submit();"   style="width:100px;">
				<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
				<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
				<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
			</select>
		</span>
		
	   </div><!-- .filter_sort-->
		
	</form>
</div>
<!-- фильтр КОНЕЦ -->

<div class="clear"></div>

<?php echo $msg ? $msg : '' ?>	

<form id="commentForm1" name="commentForm1" method="post" action="<?php echo general::link($tpl_folder.'/authorize') ; ?>">

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0" id="kt_where">
  <thead>
	<tr>
		<th><input type="checkbox" class="color_reset" /></th>		
		<th>Удаление</th>
		<th>Наименование города</th>
		<th>Стоимость заказа</th>		
		<th>Стоимость доставки</th>
		<th>Опубликован?</th>
		<th>Операции</th>
	</tR>
  </thead>	

  <tbody id="sortable">
<?php foreach ($result as $obj): ?>
	
	<tr id="listItem_<?php echo $obj->id; ?>">
		<td><input type="checkbox" name="kt_where[]" value="<?php echo $obj->id; ?>" /></td>
		<td><?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>
		<td><?php echo $obj->pole; ?></td>
		<td>от <?php echo $obj->cost_order; ?> до <?php echo $obj->cost_order_to; ?></td>
		<td><?php echo $obj->cost_deliver; ?></td>		
		<td><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></td>		
		<td><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?></td>
	</tr>


<?php endforeach; ?>

	<tr>
		<th colspan="9">
		Выделенные: 
		<select name='st' class="" id="st">
			<option value="0">Что делаем?</option>
			<option value="1">Публиковать</option>
			<option value="2">Удалить</option>
		</select>				
		<input type="submit" name="submit" id="go" value="Выполнить" onclick="return confirm('Вы уверены?')" disabled>
		</th>
	</tR>

  </tbody>	
</table>


	
	<div id="info" align="center" style="color: red;">Манипулируйте строками для сортировки данных!</div>

	<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
			$("#sortable, .s").sortable({
				update : function () {
					var order = $('#sortable').sortable('serialize');
					//$("#info").load("<?php echo APPLICATION ?>/ajax/writesort.php?"+order+'&tablename=catalog');
					$.ajax({
						type: "POST",
						url: "<?php echo APPLICATION ?>/ajax/writesort.php",
						data: order+'&tablename=deliver'
					});				
				}
			});
			$('#sortable td').each(function(){
				$(this).width($(this).width())
			})
			
			<!-- multyselect -->
			$('.color_reset').click(function(){
			if ($(this).attr('checked')==true) {
			  $('#kt_where input').attr('checked',true);
			} else {
			  $('#kt_where input').attr('checked',false);
			}
			})
			<!-- multyselect GO -->
			$('#st').change(function(){
				if ($(this).val()==1 || $(this).val()==2) {
					$('#go').attr('disabled','');
				} else {
					$('#go').attr('disabled','disabled');
				}
			})	


			/*Код изменения статуса*/	
			$('.setStatus').click(function(){
				 var id  = $(this).attr('data-id');
				 var it  = $(this);
				jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'deliver'}, function(data) {
					//alert($(this).text());
					if (data=='1') 
					{
						it.html("<span style='color:#39C;'>Да</span>");
					}
					if (data=='0') 
					{
						it.html("<span style='color:red;'>Нет</span>");
					}
				}); 
				return false;
			})
			/*конец кода изменения статуса*/			
		});
	</script>	