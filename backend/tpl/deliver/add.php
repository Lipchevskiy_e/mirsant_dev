	<h1>Добавление материала</h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/add') ; ?>">
	
    <div class="items_wrap">

        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
	
    	<div class="item_1">
    		<label class="block title_1">Город:</label>
			<select name='FORM[id_city]' class="validate[required]" id="id_city">
				<option></option>			
				<?php foreach ($city as $city): ?>
					<option value="<?php echo $city->id; ?>"><?php echo $city->pole; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>	
    
    	<div class="item_1">
    		<label class="block title_1">Cтоимость заказа от:</label>
			<input type="text" name="FORM[cost_order]" class="validate[required] input_1" id="cost_order">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Cтоимость заказа до:</label>
			<input type="text" name="FORM[cost_order_to]" class="validate[required] input_1" id="cost_order_to">    
	    </div>		
		
    	<div class="item_1">
    		<label class="block title_1">Стоимость доставки:</label>
			<input type="text" name="FORM[cost_deliver]" class="validate[required] input_1" id="cost_deliver">    
	    </div>		

        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" id="status" checked>
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>	