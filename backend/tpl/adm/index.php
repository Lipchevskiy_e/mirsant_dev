<h1>Управление разделами сайта</h1>
<h2 align="center" style="color:red">Только для СуперАдмина!</h2>
<style>
.adm_table tr:hover td {background-color:#777;color:#FFF; cursor:pointer}
</style>
<script>
$(function () {
	$('.adm_table td').click(function() {	
		var c = $(this).parent().find('td input');
		c.is(':checked') ? c.removeAttr('checked') : c.attr('checked','checked');
	});
});
</script>
<?php echo $msg ? $msg : '' ?>	

<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link('adm/index') ; ?>">

    <div class="items_wrap">

        <div class="item_1 textcenter">
			<input type="submit" name="submit" value="Сохранить">        	
        </div>


		<table class="table1 adm_table" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<th>Наименование раздела</th>
				<th>URL раздела</th>
				<th>Опубликован?</th>
				<th>Публиковать?</th>
			</tR>

			
		<?php foreach ($result as $obj): ?>
			
			<tr>
				<td><b style="color:red"><?php echo $obj->name_menu; ?><b></td>
				<td><?php echo $obj->link_menu; ?></td>
				<td><?php echo $obj->status==1 ? 'Да' : '<span style="color:red;">Нет</span>'; ?></td>
				<td><input name="FORM[<?php echo $obj->id; ?>]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="<?php echo $obj->id; ?>"></td>
			</tr>
			
			<?php foreach (dbh::menu_get_child($obj->id) as $child): ?>
			
				<tr>
					<td><div style="padding: 0 0 0 25px"><?php echo $child->name_menu; ?></div></td>
					<td><div style="padding: 0 0 0 25px"><?php echo $child->link_menu; ?></div></td>
					<td><?php echo $child->status==1 ? 'Да' : '<span style="color:red;">Нет</span>'; ?></td>		
					<td><input name="FORM[<?php echo $child->id; ?>]" type="checkbox" <?php echo $child->status==1 ? checked : ''; ?> id="<?php echo $child->id; ?>"></td>
				</tr>
			
			<?php endforeach; ?>

		<?php endforeach; ?>

		</table>

        <div class="item_1 textcenter">
			<input type="submit" name="submit" value="Сохранить">        	
        </div>

	</div>
	
</form>