	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Добавление нового товара</h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/add') ; ?>" enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Артикул товара:</label>
			<input type="text" name="FORM[artikul]" class="validate[required] input_1" id="artikul">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Наименование товара:</label>
			<input type="text" name="FORM[name]" class="validate[required] input_1" id="name">    
	    </div>		
		<div class="item_1">
    		<label class="block title_1">URL товара:</label>
			<input type=text name="FORM[url]" class="validate[required] input_1" id="url">    
	    </div>	
        <script type="text/javascript">
    	$(document).ready(function(){
    		<!-- переводим в транслит -->
    	  $('#name').syncTranslit({destination: 'url', /*caseStyle: 'upper',*/ urlSeparator: '_'});
    	});
    	</script>
	
    	<div class="item_1">
    		<label class="block title_1">Полное описание товара:</label>
			<textarea name="FORM[text_full]" id="content1" rows="30" class="text_1 tiny"></textarea>
	    </div>
		
		<div class="item_1" id="id_par">
    		<label class="block title_1">Валюта:</label>
			<select name='FORM[valuta]' class="validate[required]" id="valuta">
				<option></option>
				<option value="1">грн</option>
				<option value="2">евро</option>
				<option value="3">доллар</option>
			</select>
	    </div>

		
    	<div class="item_1">
    		<label class="block title_1">Цена товара:</label>
			<input type="text" name="FORM[cost]" class="validate[required] input_1" id="cost">    
	    </div>
		<div class="item_1">
    		<label class="block title_1">Ночная цена товара:</label>
			<input type="text" name="FORM[cost_night]" class="input_1" id="cost_night">    
	    </div>
		

    	<div class="item_1" id="id_par">
    		<label class="block title_1">Группа товаров:</label>
			<select name='FORM[id_parent]' class="validate[required]" id="id_parent">
				<option></option>
				<?php echo system::show_tpl(array('select'=>$select,'level'=>'&nbsp;','curent_id_parent'=>$obj->id_parent,'tpl_folder'=>$tpl_folder),$tpl_folder.'/select.php'); ?>
			</select>
	    </div>	

		
    	<div class="item_1" id="this_brand">
    		<label class="block title_1">Бренд:</label>
			<select name='FORM[brand]' class="validate[required]" id="brand">
				<option></option>			
				<?php foreach ($brand as $brand): ?>
					<option value="<?php echo $brand->id; ?>"><?php echo $brand->pole; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>	

    	<div class="item_1">
    		<label class="block title_1">Фото товара:</label>
			<?php for($i=1; $i<=COUNT_PHOTO_CATALOG; $i++ ): ?>
				<input type="file" name="file<?php echo $i; ?>" class="input_1">    				
			<?php endfor; ?>
	    </div>	

		<div style="clear: both;"></div>

		<div class="item_1">
			<label class="block title_1">Видео:</label>
			<textarea name="FORM[video]" id="video"  class="text_1"></textarea>
		</div>
		
        <div class="item_1">
    		<label for="block_new" class="block title_1">Новинка?:</label>
			<input name="FORM[block_new]" type="checkbox" id="block_new">
    	</div>		

        <div class="item_1">
    		<label for="block_lider" class="block title_1">Один из лучших товаров?:</label>
			<input name="FORM[block_lider]" type="checkbox" id="block_lider">
    	</div>		

		
		<div class="item_1">
    		<label class="block title_1">H1:</label>
			<input type="text" name="FORM[h1]" class="input_1" id="h1">    
	    </div>
		<div class="item_1">
    		<label class="block title_1">Title:</label>
			<input type="text" name="FORM[title]" class="input_1" id="title">    
	    </div>
        <div class="item_1">
    		<label class="block title_1">Keywords:</label>
			<textarea name="FORM[keywords]" id="keywords" rows="10" class="text_1"></textarea>
	    </div>	
		<div class="item_1">
    		<label class="block title_1">Description:</label>
			<textarea name="FORM[description]" id="description" rows="10" class="text_1"></textarea>
	    </div>
		
		
        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" id="status" checked>
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	</form>
	
	<script type="text/javascript">
	$(document).ready(function(){
		<!-- colorbox -->
	  	$("a[rel='example1']").colorbox();

        $('#id_parent').live('change', function(){
            var val = $('#id_parent').val();
            jQuery.post('/backend/ajax/check_tovar.php', {'id':val}, function(data) {
                    if(data){
                        if($('#data').is(':visible'))
                            $('#data').remove();
                            
                        $('#id_par').after(data.str);
                    }
	        },'json');
        });
		
		$('#brand').live('change', function(){
			var val = $(this).val();
             jQuery.post('/backend/ajax/serie.php', {'id_brand':val}, function(data) {
                    if(data){
                        if($('#this_serie').is(':visible'))
                            $('#this_serie').remove();
                            
                        $('#this_brand').after(data.str);
                    }
	        },'json');
		});
	});
	</script>	