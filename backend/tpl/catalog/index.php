<h1>Управление товарами</h1>

<!-- фильтр НАЧАЛО -->
<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
	
	  <div class="search_sort">
		<span class="search_sort_item">
			<label for="name">Название</label>
			<input type="text" name="name" value="<?php echo isset($_SESSION['name']) ? $_SESSION['name'] : ''; ?>">
		</span>
		<span class="search_sort_item">
			<label for="artikul">Артикул</label>
			<input type="text" name="artikul" value="<?php echo isset($_SESSION['artikul']) ? $_SESSION['artikul'] : ''; ?>">
		</span>
		
		<span class="search_sort_item">
			<input name="block_new" type="checkbox" value="1" <?php echo isset($_SESSION['block_new']) ? 'checked' : ''; ?> id="block_new">
			<label for="block_new">Новинки</label>
		</span>
		
		<span class="search_sort_item">
			<input name="block_lider" type="checkbox" value="1" <?php echo isset($_SESSION['block_lider']) ? 'checked' : ''; ?> id="block_lider">
			<label for="block_lider">Лучшие товары</label>
		</span>
		
		<span class="item_last"><input type="submit" value="Найти"></span>
	   </div><!-- .search_sort--> 
	   
       <div class="filter_sort">	
		<span class="search_sort_item">
			<label>Группа</label>
			<select name='id_parent' class="" id="id_parent" onChange="commentForm.submit();" style="width:200px;">
				<option value="0">Все</option>
				<?php echo system::show_tpl(array('select'=>$select,'level'=>'&nbsp;','curent_id_parent'=>$curent_id_parent,'tpl_folder'=>$tpl_folder),$tpl_folder.'/select.php'); ?>
			</select>
		</span>
		
		<span class="search_sort_item">
			<label>Бренд</label>
			<select name='brand' class="" id="brand" onChange="commentForm.submit();" style="width:100px;">
				<option value="0">Все</option>
				<?php foreach ($brand as $__brand): ?>
					<option value="<?php echo $__brand->id; ?>"<?php echo $__brand->id==$_brand ? ' selected': '';?>><?php echo $__brand->pole; ?></option>
				<?php endforeach; ?>			
			</select>
		</span>
		
		<span class="search_sort_item">
			<label>Фото</label>
			<select name='photo' class="" id="photo" onChange="commentForm.submit();">
				<option value="2" <?php echo $_photo==2 ? 'selected' : ''; ?>>Все</option>
				<option value="1" <?php echo $_photo==1 ? 'selected' : ''; ?>>Есть фото</option>
				<option value="0" <?php echo $_photo==0 ? 'selected' : ''; ?>>Нет фото</option>
			</select>
		</span>

		<span class="search_sort_item">
			<label>Статус</label>
			<select name='status' class="" id="status" onChange="commentForm.submit();"   style="width:100px;">
				<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
				<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
				<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
			</select>
		</span>
		
	   </div><!-- .filter_sort-->
		
	</form>
</div>
<!-- фильтр КОНЕЦ -->

<div class="clear"></div>

<?php echo $msg ? $msg : '' ?>	
<div id="s_cost"></div>
<form id="commentForm1" name="commentForm1" method="post" action="<?php echo general::link($tpl_folder.'/authorize') ; ?>">

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0" id="kt_where">
  <thead>
	<tr>
		<th><input type="checkbox" class="color_reset" /></th>		
		<?php if ($_SESSION['manager'] != true): ?>
			<th>Удаление</th>
		<?php endif ?>
		<th>Фото</th>
        <th>Артикул</th>
		<th>Наименование товара</th>
		<th>URL</th>
		<th>Группа</th>		
		<th>Цена</th>
		<th>Просмотрен</th>
		<th>Дата публикации</th>
		<th>Опубликован?</th>
		<th>Операции</th>
	</tr>
  </thead>	

  <tbody id="sortable">
<?php foreach ($result as $obj): ?>
	
	<tr id="listItem_<?php echo $obj->id; ?>">
		<td><input type="checkbox" name="kt_where[]" value="<?php echo $obj->id; ?>" /></td>
		<?php if ($_SESSION['manager'] != true): ?>
			<td><?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>
		<?php endif; ?>
		<td>
			<?php for($i=1; $i<=COUNT_PHOTO_CATALOG; $i++ ): ?>
				<?php if(file_exists(HOST.IMG_CATALOG_PATH.'/02/'.$obj->id.'_'.$i.'.jpg')): ?>
					<?php if($i < 2):?>
					<a href="<?php echo IMG_CATALOG_PATH.'/original/'.$obj->id.'_'.$i.'.jpg'; ?>" rel="example<?php echo $obj->id; ?>"><img src="<?php echo IMG_CATALOG_PATH.'/02/'.$obj->id.'_'.$i.'.jpg'; ?>"></a>
					<script type="text/javascript">
					$(document).ready(function(){
						<!-- colorbox -->
						 $("a[rel='example<?php echo $obj->id; ?>']").colorbox();
					});
					</script>
					<?php endif; ?>
				<?php endif; ?>
			<?php endfor; ?>
		</td>
		<td><?=$obj->artikul?></td>
		<td><?php echo strip_tags($obj->name); ?></td>
		<td style="color:red;"><?=$obj->url?></td>
		<td><?php echo dbh::catalog_tree_name($obj->id_parent); ?></td>
		<td><input type="text" name="cost" data-id="<?php echo $obj->id; ?>" class="real_cost" value="<?php echo mirsant::format_cost($obj->cost); ?>" style="width:70px;">
			<select name='valuta' class="validate[required]" id="valuta">
				<option value="1" <?php if ($obj->valuta==1) echo 'selected'; ?>>грн</option>
				<option value="2" <?php if ($obj->valuta==2) echo 'selected'; ?>>евро</option>
				<option value="3" <?php if ($obj->valuta==3) echo 'selected'; ?>>доллар</option>
			</select>
			<br/>
			Ночная цена <input type="text" name="cost_night" data-id="<?php echo $obj->id; ?>" value="<?php echo mirsant::format_cost($obj->cost_night); ?>" style="width:70px;">
			<input type="button" class="save_cost" value="Сохранить"> 
		</td>
		<td><?php echo $obj->kolvoview; ?></td>		
		<td><?php echo system::show_data($obj->created_at); ?></td>
		<td><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></td>		
		<td><?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?></td>
	</tr>


<?php endforeach; ?>

	<tr>
		<th colspan="11">
		Выделенные: 
		<select name='st' class="" id="st">
			<option value="0">Что делаем?</option>
			<option value="1">Авторизовать</option>
			<?php if ($_SESSION['manager'] != true): ?>
				<option value="3">Изменить валюту</option>
				<option value="2">Удалить</option>
                <option value="4">Изменить характеристики</option>
			<?php endif; ?>
		</select>

		<div class="filter_params inline">
			
		</div>


		<?php if ($_SESSION['manager'] != true): ?>
			<span class="hidden currencyBlock">
                <div class="inline">
                    Валюта:
                    <select name='sel_valuta' class="" id="sel_valuta">
                        <option value="0"></option>
                        <option value="1">грн</option>
                        <option value="2">евро</option>
                        <option value="3">доллар</option>
                    </select>
                </div>
            </span>
            <span class="hidden generalBlock">
                <div class="inline">
                    Предназначение:
                    <select name="ST[type]">
                        <option value="0">Не изменять</option>
                        <?php foreach ($types as $type): ?>
                            <option value="<?php echo $type->id; ?>"><?php echo $type->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="inline">
                    Бренд:
                    <select name="ST[brand]">
                        <option value="0">Не изменять</option>
                        <?php foreach ($brand as $brand): ?>
                            <option value="<?php echo $brand->id; ?>"><?php echo $brand->pole; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="inline">
                    Серия:
                    <select name="ST[serie]">
                        <option value="0">Не изменять</option>
                    </select>
                </div>
                <div class="inline">
                    Группа:
                    <select name="ST[group]">
                        <option value="0">Не менять</option>
                        <?php echo system::show_tpl(array('select' => $select, 'level' => '&nbsp;&nbsp;&nbsp;', 'curent_id_parent' => 0, 'tpl_folder' => $tpl_folder), $tpl_folder.'/select.php'); ?>
                    </select>
                </div>
		    </span>
            <span class="hidden restBlock"></span>
		<?php endif; ?>
		<input type="submit" name="submit" id="go" value="Выполнить" onclick="return confirm('Вы уверены?')" disabled>
		</th>
	</tr>

  </tbody>	
</table>


	<?php 
	// pager
	echo $_total_catalog>ADMIN_CATALOG_AT_PAGE ? pager::pager_J(_APPLICATION."/".$tpl_folder."/index",ADMIN_CATALOG_AT_PAGE,$_total_catalog,intval($_GET['page'])) : '';
	?>
	
	
	<div id="info" align="center" style="color: red;">Манипулируйте строками для сортировки данных!</div>

    <style>
        .hidden {
            display: none;
        }
        .inline {
            display: inline-block;
            margin: 5px;
        }
    </style>


	<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
            $('#st').val(0);
            $('#go').attr('disabled', true);
			$("#sortable, .s").sortable({
				update : function () {
					var order = $('#sortable').sortable('serialize');
					//$("#info").load("<?php echo APPLICATION ?>/ajax/writesort.php?"+order+'&tablename=catalog');
					$.ajax({
						type: "POST",
						url: "<?php echo APPLICATION ?>/ajax/writesort.php",
						data: order+'&tablename=catalog'
					});				
				}
			});
			$('#sortable td').each(function(){
				$(this).width($(this).width())
			});

			$('.color_reset').click(function(){
                if ($(this).attr('checked')==true) {
                  $('#kt_where input').attr('checked',true);
                } else {
                  $('#kt_where input').attr('checked',false);
                }
			});

			$('#st').change(function(){
				if ($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 4 || ($(this).val()==3 && $('#sel_valuta').val())) {
					$('#go').attr('disabled',false);
				} else {
                    $('#go').attr('disabled', true);
				}
                if($(this).val() == 3) {
                    $('.currencyBlock').show();
                } else {
                    $('.currencyBlock').hide();
                }
                if($(this).val() == 4) {
                    $('.generalBlock').show();
                } else {
                    $('.generalBlock').hide();
                }
			});
			
			$('#sel_valuta').change(function(){
                if ($(this).val() && $('#st').val() == 3) {
                    $('#go').attr('disabled', false);
                } else {
                    $('#go').attr('disabled', true);
                }
			});

            $('select[name="ST[brand]"]').live('change', function(){
                var val = $(this).val();
                $.ajax({
                    url: '/backend/ajax/getSeries.php',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        brand_id: val
                    },
                    success: function(data){
                        $('select[name="ST[serie]"]').html(data.series);
                    },
                    error: function(){
                        $('select[name="ST[serie]"]').html('<option value="0">Не изменять</option>');
                    }
                });
            });

            $('select[name="ST[group]"]').live('change', function(){
                var val = $(this).val();
                $.ajax({
                    url: '/backend/ajax/check_tovar_index.php',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id: val
                    },
                    success: function(data){
                        $('.restBlock').html(data.str);
                        $('.restBlock').show();
                    },
                    error: function(){
                        $('.restBlock').html('');
                        $('.restBlock').hide();
                    }
                });
            });




            /*Код изменения статуса*/
            $('.setStatus').click(function(){
                 var id  = $(this).attr('data-id');
                 var it  = $(this);
                jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'catalog'}, function(data) {
                    //alert($(this).text());
                    if (data=='1')
                    {
                        it.html("<span style='color:#39C;'>Да</span>");
                    }
                    if (data=='0')
                    {
                        it.html("<span style='color:red;'>Нет</span>");
                    }
                });
                return false;
            });

            $('.save_cost').click(function(){
                cost=$(this).parent().find('.real_cost');
                valuta=$(this).parent().find('select');
				var cost_night = $(this).parent().find('input[name="cost_night"]').val();
                 var id  = cost.attr('data-id');
                 var val  = cost.val();
                 var valuta_zna  = valuta.val();
                jQuery.post('/backend/ajax/save_cost.php', {'id': id, 'cost':val,'valuta':valuta_zna, 'cost_night' : cost_night}, function(data) {
                    if (data=='1') {
                        if (valuta_zna==1) v='грн';
                        if (valuta_zna==3) v='доллар';
                        if (valuta_zna==2) v='евро';
                        $('#s_cost').html('Цена изменена на '+val+' Ночная цена '+cost_night +' Валюта '+v );
                    }
                });
                return false;
            });

			$('#st').change(function () {
				$('.filter_params').html('');
				var val = $(this).val();
				var parent_id = $('#id_parent').val();
				if(val == 4 && parent_id){

					$.ajax({
						url: '/backend/ajax/get_filter_params.php',
						type: 'POST',
						dataType: 'JSON',
						data: {
							parent_id: parent_id
						},
						success: function(data){
							console.log(data);
							$('.filter_params').html(data.str);
						}
					});
				}
			});
            /*конец кода изменения статуса*/
		});
	</script>	