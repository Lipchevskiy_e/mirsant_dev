	<!-- Подключаем Tyny -->
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/tiny_mce/tiny_mce_src.js"></script>
	<script type='text/javascript' src='<?php echo APPLICATION; ?>/tinymce.php'></script>
	<!-- Конец подключения Tyny -->

	<h1>Редактирование товара &rArr; <?php echo $obj->name; ?></h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/new/id/'.$_GET['id']) ; ?>" enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Артикул товара:</label>
			<input type="text" name="FORM[artikul]" value='<?php echo $obj->artikul; ?>' class="validate[required] input_1" id="artikul">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Наименование товара:</label>
			<input type="text" name="FORM[name]" value='<?php echo $obj->name; ?>' class="validate[required] input_1" id="name">    
	    </div>		
		<div class="item_1">
    		<label class="block title_1">URL товара:</label>
			<input type=text name="FORM[url]" value='<?php echo $obj->url; ?>' class="validate[required] input_1" id="url">    
	    </div>	
        <script type="text/javascript">
    	$(document).ready(function(){
    		<!-- переводим в транслит -->
    	  $('#name').syncTranslit({destination: 'url', /*caseStyle: 'upper',*/ urlSeparator: '_'});
    	});
    	</script>
   <!-- 	<div class="item_1">
    		<label class="block title_1">Краткое описание товара:</label>
			<textarea name="FORM[text_short]" id="content" rows="10" class="text_1 tiny"><?php echo $obj->text_short; ?></textarea>
	    </div>	
        
        <div class="item_1">
    		<label class="block title_1">Характеристики:</label>
			<textarea name="FORM[charstik]" id="charstik" rows="10" class="text_1 tiny"><?php echo $obj->charstik; ?></textarea>
	    </div>-->
		
    	<div class="item_1">
    		<label class="block title_1">Описание товара:</label>
			<textarea name="FORM[text_full]" id="content1" rows="30" class="text_1 tiny"><?php echo $obj->text_full; ?></textarea>
	    </div>
		
		<div class="item_1" id="id_par">
    		<label class="block title_1">Валюта:</label>
			<select name='FORM[valuta]' class="validate[required]" id="valuta">
				<option></option>
				<option value="1" <?php if ($obj->valuta==1) echo 'selected'; ?>>грн</option>
				<option value="2" <?php if ($obj->valuta==2) echo 'selected'; ?>>евро</option>
				<option value="3" <?php if ($obj->valuta==3) echo 'selected'; ?>>доллар</option>
			</select>
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Цена товара:</label>
			<input type="text" name="FORM[cost]" value='<?php echo mirsant::format_cost($obj->cost); ?>' class="validate[required] input_1" id="cost">    
	    </div>		
		<div class="item_1">
    		<label class="block title_1">Ночная цена товара:</label>
			<input type="text" name="FORM[cost_night]" value='<?php echo mirsant::format_cost($obj->cost_night); ?>' class="input_1" id="cost_night">    
	    </div>	
        
    	<div class="item_1" id="id_par">
    		<label class="block title_1">Группа товаров:</label>
			<select name='FORM[id_parent]' class="validate[required]" id="id_parent">
				<option></option>
				<?php echo system::show_tpl(array('select'=>$select,'level'=>'&nbsp;','curent_id_parent'=>$obj->id_parent,'tpl_folder'=>$tpl_folder),$tpl_folder.'/select.php'); ?>
			</select>
	    </div>	
        
		<div id="data">
            <?if($grupa->has_weight == 1):?>
                <div class="item_1">
                    <label class="block title_1">Ширина (см):</label>
                    <input type="text" name="FORM[width]" class="input_1" id="width" value="<?=$obj->width?>">
                </div>
                <div class="item_1">
                    <label class="block title_1">Высота (см):</label>
                    <input type="text" name="FORM[height]" class="input_1" id="height" value="<?=$obj->height?>">
                </div>
                <div class="item_1">
                    <label class="block title_1">Длина (см):</label>
                    <input type="text" name="FORM[length]" class="input_1" id="length" value="<?=$obj->length?>">
                </div>
            <?endif?>

			<?if($grupa->has_type == 1):?>
				<div class="item_1" id="type_tovar">
					<label class="block title_1">Предназначение:</label>
					<select name="FORM[has_type]" id="has_type">
						<option></option>
						<?foreach($type_tovar as $type_tovar):?>
							<option value="<?=$type_tovar->id?>" <?=$obj->has_type == $type_tovar->id ? selected : '' ?>><?=$type_tovar->name?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_toilet == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_toilet_mount ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Тип монтажа унитаза:</label>
					<select name="FORM[toilet_mount]" id="toilet_mount">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->toilet_mount ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_gidr_van == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_gidr_van ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Форма ванны:</label>
					<select name="FORM[gidr_van]" id="gidr_van">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->gidr_van ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_tumb_van == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_tumb_van ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Тип монтажа:</label>
					<select name="FORM[tumb_van]" id="tumb_van">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->tumb_van ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_penal_van == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_penal_van ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Тип монтажа:</label>
					<select name="FORM[penal_van]" id="penal_van">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->penal_van ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_bide_mont == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_bide_mont ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Тип монтажа:</label>
					<select name="FORM[bide_mont]" id="bide_mont">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->bide_mont ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_kuh_mont == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_kuh_mont ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Тип монтажа:</label>
					<select name="FORM[kuh_mont]" id="kuh_mont">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->kuh_mont ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_kuh_count == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_kuh_count ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Кол-во секций:</label>
					<select name="FORM[kuh_count]" id="kuh_count">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->kuh_count ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_bathtub == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_bathtub_form ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Форма ванны:</label>
					<select name="FORM[bathtub_form]" id="bathtub_form">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->bathtub_form ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_shower == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_tray_depth ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Глубина поддона:</label>
					<select name="FORM[tray_depth]" id="tray_depth">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->tray_depth ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
				<?php $result = mysql::query('SELECT id, name FROM dict_cabin_form ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Форма кабины:</label>
					<select name="FORM[cabin_form]" id="cabin_form">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->cabin_form ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>

			<?if($grupa->has_mixer == 1):?>
				<?php $result = mysql::query('SELECT id, name FROM dict_colors ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Цвет:</label>
					<select name="FORM[color]" id="color">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->color ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
				<?php $result = mysql::query('SELECT id, name FROM dict_mounting_type ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Тип монтажа:</label>
					<select name="FORM[mounting_type]" id="mounting_type">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->mounting_type ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
				<?php $result = mysql::query('SELECT id, name FROM dict_mixer_style ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Стиль смесителя:</label>
					<select name="FORM[mixer_style]" id="mixer_style">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->mixer_style ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
				<?php $result = mysql::query('SELECT id, name FROM dict_mixer_type ORDER BY sort ASC, id DESC'); ?>
				<div class="item_1">
					<label class="block title_1">Тип смесителя:</label>
					<select name="FORM[mixer_type]" id="mixer_type">
						<option></option>
						<?foreach($result as $_obj):?>
							<option value="<?php echo $_obj->id; ?>" <?php echo $_obj->id == $obj->mixer_type ? 'selected' : NULL; ?>><?php echo $_obj->name; ?></option>;
						<?endforeach?>
					</select>
				</div>
			<?endif?>
        </div>

        <div class="item_1" id="this_brand">
    		<label class="block title_1">Бренд:</label>
			<select name='FORM[brand]' class="validate[required]" id="brand">
				<option></option>			
				<?php foreach ($brand as $brand): ?>
					<option value="<?php echo $brand->id; ?>" <?=$brand->id == $obj->brand ? 'selected' : '' ?>><?php echo $brand->pole; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>	
		
		<div class="item_1" id="this_serie">
    		<label class="block title_1">Серия:</label>
			<select name='FORM[serie]' id="serie" style="min-width:150px;">
				<option></option>
				<?php foreach ($series as $s): ?>
					<option value="<?php echo $s->id; ?>" <?=$s->id == $obj->serie ? 'selected' : '' ?>><?php echo $s->name; ?></option>
				<?php endforeach; ?>			
			</select>
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Фото товара:</label>
			<?php for($i=1; $i<=COUNT_PHOTO_CATALOG; $i++ ): ?>
				<?php if(file_exists(HOST.IMG_CATALOG_PATH.'/02/'.$obj->id.'_'.$i.'.jpg')): ?>
					<a href="<?php echo IMG_CATALOG_PATH.'/original/'.$obj->id.'_'.$i.'.jpg'; ?>" rel="example1"><img src="<?php echo IMG_CATALOG_PATH.'/02/'.$obj->id.'_'.$i.'.jpg'; ?>"></a>
					<?php echo general::link_to($tpl_folder.'/deletephoto/id/'.$obj->id.'/i/'.$i,'Удалить фото?','onclick="return confirm(\'Вы уверены?\')"'); ?>
				<?php else: ?>				
					<input type="file" name="file<?php echo $i; ?>" class="input_1">    				
				<?php endif; ?>
			<?php endfor; ?>
	    </div>	

		<div style="clear: both;"></div>

		<div class="item_1">
			<label class="block title_1">Видео:</label>
			<textarea name="FORM[video]" id="video"  class="text_1"><?php echo $obj->video; ?></textarea>
		</div>
		
        <div class="item_1">
    		<label for="block_new" class="block title_1">Новинка?:</label>
			<input name="FORM[block_new]" type="checkbox" <?php echo $obj->block_new==1 ? checked : ''; ?> id="block_new">
    	</div>	

        <div class="item_1">
    		<label for="block_lider" class="block title_1">Один из лучших товаров?:</label>
			<input name="FORM[block_lider]" type="checkbox" <?php echo $obj->block_lider==1 ? checked : ''; ?> id="block_lider">
    	</div>			

		<div class="item_1">
    		<label class="block title_1">H1:</label>
			<input type="text" name="FORM[h1]" value='<?php echo $obj->h1; ?>' class="input_1" id="h1">    
	    </div>
		<div class="item_1">
    		<label class="block title_1">Title:</label>
			<input type="text" name="FORM[title]" value='<?php echo $obj->title; ?>' class="input_1" id="title">    
	    </div>
        <div class="item_1">
    		<label class="block title_1">Keywords:</label>
			<textarea name="FORM[keywords]" id="keywords" rows="10" class="text_1"><?php echo $obj->keywords; ?></textarea>
	    </div>	
		<div class="item_1">
    		<label class="block title_1">Description:</label>
			<textarea name="FORM[description]" id="description" rows="10" class="text_1"><?php echo $obj->description; ?></textarea>
	    </div>	

        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
		<?php if(IS_CATALOG_ASS): ?>

			<div class="conteiner">

				<h1>Сопутствующие товары</h1>
				
				<?php
				/*
				<div class="s_tovar_poss">
						<label>Поиск сопутствующего товара</label>
						<input type="text" />
						<input type="submit" value="Найти" />
					<!--
					<div class="s_result">
						<div id="0" class="t_item">
							<img src="images/tovar_1.jpg" align="" class="t_thumb" />
							<div class="t_descr">
								<div class="t_name">Гантеля, Дыбенко</div>
								<div class="t_cost">100,00 грн.</div>
							</div>
						</div>
					</div>
					-->
				</div>
				*/ ?>
				
				<div class="tovars">
					<div align="center" class="pad_5">
						Выберите группу товаров
							<select class="grupa_ass">
								<option></option>
								<?php echo system::show_tpl(array('select'=>$select,'level'=>'&nbsp;','curent_id_parent'=>0,'tpl_folder'=>$tpl_folder),$tpl_folder.'/select.php'); ?>
							</select>
					</div>
					
					<div class="b_title">Список товаров</div>
					<div class="t_wrap">
						<div class="t_list"></div>
					</div>
					<div class="textcenter pad_5"><input class="t_add" type="button" value="Добавить в «Сопутствующие товары»" /></div>
					
					<div class="b_title">Сопутствующие товары</div>
					<div class="t_wrap">
						<div class="t_sop">
						
							<?php foreach ($catalog_ass as $catalog_ass): ?>
								<div id="<?= $catalog_ass->id ?>" class="t_item">
									<?php if(file_exists(HOST.IMG_CATALOG_PATH.'/01/'.$catalog_ass->id.'_1.jpg')): ?>
										<img src="<?= IMG_CATALOG_PATH.'/01/'.$catalog_ass->id.'_1.jpg'; ?>" align="" class="t_thumb" />
									<?php endif; ?>
									<div class="t_descr">
										<div class="t_name"><?= $catalog_ass->name ?></div>
										<div class="t_cost"><?= $catalog_ass->cost ?> грн.</div>
									</div>
								</div>
							<?php endforeach; ?>	
							
						</div>
					</div>
					<div class="textcenter pad_5"><input class="t_del" type="button" value="Удалить товар из списка" /></div>
				</div>  
			</div>		
		
		
		<?php endif ?>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input id="id_good" name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
	</form>
	
	<script type="text/javascript">
	$(document).ready(function(){
		<!-- colorbox -->
	 	 $("a[rel='example1']").colorbox();
		// выводим товар
		$('.grupa_ass').change(function(){
			//alert($(this).attr('value'));
			var id_group=$(this).attr('value');
			$(".t_list").html('');
			$.ajax({
				type: 'POST',
				url: '/backend/ajax/catalog_ass_get.php',
				data: ({id_group:id_group}),
				success: function(data){
					if(data){
						$(".t_list").html(data);
						// проверяем на уже добавленные
						var t_list = $('.t_list');
						var t_sop = $('.t_sop');			
						$('.t_item',t_sop).each(function(){
							t_list.find('#'+$(this).attr('id')).addClass('added');  
						})
					};
				}
			});				
		});
		
		
		$('#id_parent').live('change', function(){
            var val = $('#id_parent').val();
            jQuery.post('/backend/ajax/check_tovar.php', {'id':val}, function(data) {
				if(data){
					if($('#data').is(':visible'))
						$('#data').remove();

					$('#id_par').after(data.str);
				}
	        },'json');
        });
		
		$('#brand').live('change', function(){
			var val = $(this).val();
             jQuery.post('/backend/ajax/serie.php', {'id_brand':val}, function(data) {
				if(data){
					if($('#this_serie').is(':visible'))
						$('#this_serie').remove();

					$('#this_brand').after(data.str);
				}
	        },'json');
		});
	});
	</script>		