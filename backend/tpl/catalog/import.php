	<h1>Импорт товаров</h1>
	
	<?php echo $msg ? $msg : '' ?>

	<form id="commentForm" name="form_edit" method="post" action="<?php echo general::link($tpl_folder.'/import') ; ?>" enctype='multipart/form-data'>
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
    	<div class="item_1">
    		<label class="block title_1">Структура файла импорта:</label>
			<img src='<?php echo APPLICATION; ?>/images/import.jpg'>  
	    </div>		
		
    	<div class="item_1">
    		<label class="block title_1">Файл импорта (формат файла - excel):</label>
			<input type="file" name="file1" class="input_1">  
	    </div>
		
        <div class="item_1">
    		<label for="supplier" class="block title_1">Поставщик:</label>
			<select name='supplier'  id="supplier" class="validate[required]">
				<option></option>
				<?php foreach ($select as $select): ?>
					<option value="<?php echo $select->id; ?>"><?php echo $select->pole; ?></option>
				<?php endforeach; ?>			
			</select>			
    	</div>
		
        <div class="item_1">
    		<label for="type_import" class="block title_1">Тип загрузки:</label>
			<input name="type_import" type="radio" value="0" checked="checked" /> С отключением товара <span class="red">*</span>
			<input name="type_import" type="radio" value="1" /> Без отключения товара
			<div class="size10 red" style="padding: 0 0 15px 0;">* Если в файле импорта товара нет - автоматически будет установлен статус "Нет на складе"</div>
			
			<input name="type_import1" type="radio" value="0" checked="checked" /> C обновленимем цены 
			<input name="type_import1" type="radio" value="1" /> Без обновления цены <span class="red">*</span>
			<div class="size10 red">* Цена товара не изменяется</div>
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить" onclick="return confirm('Вы уверены?')">        	
        </div>
        
    </div>	
    
	</form>