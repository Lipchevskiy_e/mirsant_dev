		<?php foreach ($select as $obj): ?>
			<?php $children=dbh::catalog_tree_get_child($obj->id); ?>
			
			<?php if(count($children)): ?>
				<optgroup label="<?php echo $level.$obj->pole; ?>">
					<?php echo system::show_tpl(array('select'=>$children,'level'=>$level.'&nbsp;', 'curent_id_parent'=>$curent_id_parent, 'tpl_folder'=>$tpl_folder),$tpl_folder.'/select.php'); ?>
				</optgroup>
			<?php else: ?>
				<option value="<?php echo $obj->id; ?>"<?php echo $obj->id==$curent_id_parent ? ' selected' : '';?>><?php echo $obj->pole; ?></option>
			<?php endif; ?>
			
		<?php endforeach; ?>
