    <div style="background:#fff; overflow:hidden; //zoom:1; -moz-border-radius: 20px; -webkit-border-radius: 20px; -khtml-border-radius: 20px; border-radius: 20px;">
    	<div style="width:300px; margin:0 -300px 0 0; float:left; position:relative">
        	<script type="text/javascript">
				$(function(){
					var h1 = $('.h1').outerHeight()
					var h2 = $('.h2').outerHeight()
					$('title').html(h1+','+h2)
					if(h1 > h2){
						$('.h2').height(h1-40)	
					}
				})
			</script>
			
        	<div class="h1" style="padding:20px">
                <h2>Панель быстрой навигации</h2>
                <div class="menu_left">
				
					<?php if($catalog_status==1): ?>
						<h3>Товары</h3>
						<a href="<?php echo general::link('catalog/index/status/0'); ?>">Неактивные&nbsp;[<?php echo $catalog_inctive; ?>]</a>
						<a href="<?php echo general::link('orders/index/status/0'); ?>">Заказы новые (неактивные)&nbsp;[<?php echo $order_inctive; ?>]</a>
					<?php endif; ?>
					
					<?php if($gb_status==1): ?>
						<h3>Отзывы</h3>
						<a href="<?php echo general::link('gb/index/status/0'); ?>">Новые (неактивные)&nbsp;[<?php echo $gb_inctive; ?>]</a>
					<?php endif; ?>
					
					<?php if($news_status==1): ?>
						<h3>Новости</h3>
						<a href="<?php echo general::link('news/index/status/0'); ?>">Неактивные&nbsp;[<?php echo $news_inctive; ?>]</a>
						<?php if($news_gb_status==1): ?>
						<a href="<?php echo general::link('news_gb/index/status/0'); ?>">Комментарии к новостям (неактивные)&nbsp;[<?php echo $news_gb_inctive; ?>]</a>
						<?php endif; ?>
					<?php endif; ?>

					<?php if($users_status==1): ?>
						<h3>Пользователи</h3>
						<a href="<?php echo general::link('users/index/status/0'); ?>">Новые (неактивные)&nbsp;[<?php echo $users_inctive; ?>]</a>
					<?php endif; ?>
					
					
                </div>
            </div>
			
        </div>
		
        <div style="float:left; width:100%;">
        	<div style="padding:0 0 0 300px;">
            	<div class="h2" style="-moz-border-radius: 20px; -webkit-border-radius: 20px; -khtml-border-radius: 20px; border-radius: 20px; background:#d9d9d9; padding:20px; font-size:0; text-align:center; border:1px solid #fff; ">
				
                	<h2>Панель управления сайтом</h2>
					
					<?php if($content_status==1): ?>
						<a class="menu_block" href="<?php echo general::link('content/index/menu/90'); ?>">
							<span class="control_razdel_ic"></span>
							Управление подразделами
						</a>
					<?php endif; ?>
                    
					<?php if($catalog_status==1): ?>
						<a class="menu_block" href="<?php echo general::link('catalog/index/menu/164'); ?>">
							<span class="catalog_ic"></span>
							Каталог
						</a>
					<?php endif; ?>
                    
					<?php if($gallery_status==1): ?>
						<a class="menu_block" href="<?php echo general::link('gallery/index/menu/4'); ?>">
							<span class="photo_ic"></span>
							Фотогалерея
						</a>
					<?php endif; ?>
                    
					<?php if($gb_status==1): ?>
						<a class="menu_block" href="<?php echo general::link('gb/index/menu/8'); ?>">
							<span class="guest_ic"></span>
							Отзывы
						</a>
					<?php endif; ?>
                    
					<?php if($news_status==1): ?>
						<a class="menu_block" href="<?php echo general::link('news/index/menu/2'); ?>">
							<span class="news_ic"></span>
							Новости
						</a>
					<?php endif; ?>
                    
					<?php if($voting_status==1): ?>
						<a class="menu_block" href="<?php echo general::link('voting/index/menu/103'); ?>">
							<span class="vote_ic"></span>
							Голосование
						</a>
					<?php endif; ?>
                    
					<?php if($users_status==1): ?>
						<a class="menu_block" href="<?php echo general::link('users/index'); ?>">
							<span class="users_ic"></span>
							Пользователи
						</a>
					<?php endif; ?>
                    
					<?php if($config_status==1): ?>
						<a class="menu_block" href="<?php echo general::link('config/edit'); ?>">
							<span class="options_ic"></span>
							Настройки сайта
						</a>
					<?php endif; ?>
                    
					<?php if($ceo_status==1): ?>
						<a class="menu_block" href="<?php echo general::link('seo/index'); ?>">
							<span class="seo_ic"></span>
							Настройки SEO
						</a>
					<?php endif; ?>

                </div>
            </div>
        </div>
        
    </div>