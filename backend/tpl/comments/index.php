<h1>Управление комментариями к товарам</h1>

<div style="text-align:center; padding:0 0 20px;">
	<form id="commentForm" name="commentForm" method="post" action="<?php echo general::link($tpl_folder.'/index') ; ?>">
		<div class="search_sort">	
			<span class="search_sort_item">
				<label>Статус</label>	
				<select name='status' class="middle" id="status" onChange="commentForm.submit();">
					<option value="2" <?php echo $_status==2 ? 'selected' : ''; ?>>Все</option>
					<option value="1" <?php echo $_status==1 ? 'selected' : ''; ?>>Опубликованные</option>
					<option value="0" <?php echo $_status==0 ? 'selected' : ''; ?>>Неопубликованные</option>
				</select>
			</span>
		</div>
	</form>
</div>

<div class="clear"></div>

<?php echo $msg ? $msg : '' ?>	

<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0" id="kt_where">
	<tr>	
		<th width="150">ФИО</th>
		<th width="150">Email</th>
		<th>Содержание</th>
		<th width="250">Товар</th>
		<th width="125">Дата публикации</th>
		<th width="100">Опубликован?</th>
		<th width="100">Операции</th>
	</tR>

	
	<?php foreach ($result as $obj): ?>
		
		<tr>
			<td><?php echo $obj->name; ?></td>
			<td><?php echo $obj->email; ?></td>
			<td><?php echo nl2br($obj->text); ?></td>
			<td><a href="/backend/catalog/new/id/<?php echo $obj->id_goods; ?>" target="_blank"><?php echo $obj->goods_name; ?></a></td>
			<td><?php echo date('d.m.Y H:i',$obj->created_at); ?></td>
			<td><a href="#" data-id="<?php echo $obj->id; ?>" class="setStatus"><?php echo $obj->status==1 ? '<span style="color:#39C;">Да</span>' : '<span style="color:red;">Нет</span>'; ?></a></td>		
			<td>
				<?php echo general::link_to($tpl_folder.'/new/id/'.$obj->id,'Редактировать'); ?>
				<br />
				<?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?>
			</td>
		</tr>

	<?php endforeach; ?>


</table>

<script type="text/javascript">
	$(document).ready(function(){		
		/*Код изменения статуса*/	
		$('.setStatus').click(function(){
			 var id  = $(this).attr('data-id');
			 var it  = $(this);
			jQuery.post('/backend/ajax/set_status.php', {'id': id, tablename:'catalog_comments'}, function(data) {
				//alert($(this).text());
				if (data=='1') 
				{
					it.html("<span style='color:#39C;'>Да</span>");
				}
				if (data=='0') 
				{
					it.html("<span style='color:red;'>Нет</span>");
				}
			}); 
			return false;
		})
		/*конец кода изменения статуса*/
	});
</script>	