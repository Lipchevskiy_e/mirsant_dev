<h1>Редактирование отзыва о товаре "<?php echo $good->name; ?>"</h1>

<?php echo $msg ? $msg : '' ?>

<form id="commentForm" name="form_edit" method="post" action="">
	
    <div class="items_wrap">
    
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
		
		<div class="item_1">
    		<label class="block title_1">Товар:</label>
			<p><a href="/backend/catalog/new/id/<?php echo $good->id; ?>" target="_blank"><?php echo $good->name; ?></a></p>  
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Кто оставил:</label>
			<input type="text" name="FORM[name]" value='<?php echo $obj->name; ?>' class="validate[required] input_1" id="name">    
	    </div>
		
    	<div class="item_1">
    		<label class="block title_1">Email:</label>
			<input type="text" name="FORM[email]" value='<?php echo $obj->email; ?>' class="validate[required,custom[email]] input_1" id="email">    
	    </div>

    	<div class="item_1">
    		<label class="block title_1">Содержание отзыва:</label>
			<textarea name="FORM[text]" id="text" rows="15" class="text_1"><?php echo $obj->text; ?></textarea>
	    </div>	

        <div class="item_1">
    		<label for="status" class="block title_1">Публиковать?:</label>
			<input name="FORM[status]" type="checkbox" <?php echo $obj->status==1 ? checked : ''; ?> id="status">
    	</div>
		
        <div class="item_1 textcenter">
			<?php echo general::link_to($tpl_folder.'/index', 'Вернуться к общему списку'); ?>
			<input type="submit" name="submit" value="Сохранить">        	
        </div>
        
    </div>	
    
	<input name="id" type="hidden" value="<?php echo $obj->id; ?>">        
    
</form>