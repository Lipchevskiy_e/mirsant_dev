<h1>Управление прайс-листами</h1>

<?php echo $msg ? $msg : '' ?>	

<form action="">
<table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
  <thead>
	<tr>
		<th>Наименование прайс-листа</th>
		<th>Дата публикации</th>
		<th>Файл</th>
		<th>Операции</th>
	</tR>
  </thead>

	<tbody id="sortable">	
<?php foreach ($result as $obj): ?>
	
	<tr id="listItem_<?php echo $obj->id; ?>">
		<td><?php echo $obj->name; ?></td>
		<td><?php echo system::show_data($obj->created_at); ?></td>
		<td>
			<?php if(file_exists(HOST.PRICE_LIST_PATH.'/'.$obj->pole)): ?>
				<?php echo '<a href="'.PRICE_LIST_PATH.'/'.$obj->pole.'">'.$obj->pole.'</a>'; ?>
			<?php else: ?>
				<span style="color:red;">Нет</span>
			<?php endif; ?>
		</td>		
		<td><?php echo general::link_to($tpl_folder.'/delete/id/'.$obj->id,'Удалить?','onclick="return confirm(\'Вы уверены?\')"'); ?></td>
	</tr>


<?php endforeach; ?>
  </tbody>
</table>
</form>



	<div id="info" align="center" style="color: red;">Меняйте порядок фотоальбомов перетаскивая картинки!</div>

	<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
			$("#sortable, .s").sortable({
				update : function () {
					var order = $('#sortable').sortable('serialize');
					//$("#info").load("<?php echo APPLICATION ?>/ajax/writesort.php?"+order+'&tablename=catalog');
					$.ajax({
						type: "POST",
						url: "<?php echo APPLICATION ?>/ajax/writesort.php",
						data: order+'&tablename=price'
					});				
				}
			});
			$('#sortable td').each(function(){
				$(this).width($(this).width())
			})
		});
	</script>
	
	
	<script type="text/javascript">
	  	// When the document is ready set up our sortable with it's inherant function(s)
	  	$(function() {
			$("#sortable, .s").sortable({
			  update : function () {
			  var sort_wrap = $(this);
			  
			  if(sort_wrap.is('#sortable')){
				order = $('#sortable').sortable('serialize');
			  }
			  if(sort_wrap.parent().parent().is('#sortable')){
				order = $('#sortable ul').sortable('serialize');
			  }
			  if(sort_wrap.parent().parent().parent().parent().is('#sortable')){
				order = $('#sortable ul ul').sortable('serialize');
			  }
				$.ajax({
					type: "POST",
					url: "<?php echo APPLICATION ?>/ajax/writesort.php",
					data: order+'&tablename=price'
				});
			  }
			});
		});
	</script>		