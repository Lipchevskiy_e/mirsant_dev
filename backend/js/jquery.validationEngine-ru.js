

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Add your regex rules here, you can take telephone as an example
						"regex":"нет",
						"alertText":"* Обязательное поле",
						"alertTextCheckboxMultiple":"* Необходимо выбрать",
						"alertTextCheckboxe":"* Обязательное поле"},
					"length":{
						"regex":"нет",
						"alertText":"*Между ",
						"alertText2":" и ",
						"alertText3": " только символы"},
					"maxCheckbox":{
						"regex":"нет",
						"alertText":"* лимит превышен"},	
					"minCheckbox":{
						"regex":"нет",
						"alertText":"* Необходимо выбрать ",
						"alertText2":" опции"},	
					"confirm":{
						"regex":"нет",
						"alertText":"* Поля не совпадают"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"* Неверный формат телефона"},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						"alertText":"* E-mail введен неправильно "},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* Неправишьная дата, формат даты YYYY-MM-DD"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* Только цифры"},	
					"noSpecialCaracters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* Спец символы запрещены"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"name=eric",
						"alertTextOk":"* This user is available",	
						"alertTextLoad":"* Loading, please wait",
						"alertText":"* This user is already taken"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* This name is already taken",
						"alertTextOk":"* This name is available",	
						"alertTextLoad":"* Loading, please wait"},		
					"onlyLetter":{
						"regex":"/^[а-яА-Я\ \']+$/",
						"alertText":"* Только буквы"},
					"validate2fields":{
    					"nname":"validate2fields",
    					"alertText":"* You must have a firstname and a lastname"}	
					}	
					
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});