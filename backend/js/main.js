$(document).ready(function(){


	//start tovars
	var t_list = $('.t_list');
	var t_sop = $('.t_sop');
	
	$('.t_item:not(".added")',t_list).live('click',function(){
		$(this).toggleClass('t_selected');
		but_show();
	})
	
	$('.t_item',t_sop).live('click',function(){
		$(this).toggleClass('t_selected');
		but_show();
	})
	
	$('.added').live('click',function(){
		alert('Этот товар уже добавлен');
	})

	$('.t_add').live('click',function(){
		var arr = [];
		$('.t_selected',t_list).each(function(){
			var t_id = $(this).attr('id')
			var pushed = arr.push(t_id);	
			/*	
			$(this).removeClass('t_selected').clone().addClass('t_clone').appendTo(t_sop);
			$(this).addClass('added');
			*/
		});
		
		var id_t = $('#id_good').val()
		$.ajax({
			type: 'POST',
			url: '/backend/ajax/catalog_ass.php',
			data: ({id:arr,id_good:id_t,add:1}),
			success: function(data){
				if(data == true){
					$('.t_selected',t_list).each(function(){
						$(this).removeClass('t_selected').clone().addClass('t_clone').appendTo(t_sop);
						$(this).addClass('added');
					});
					but_show();
				};
			}
		});	
	})
	$('.t_del').live('click',function(){
		var arr = [];
		$('.t_selected',t_sop).each(function(){
			var t_id = $(this).attr('id');
			var pushed = arr.push(t_id);		
		});

		var id_t = $('#id_good').val()
		
		$.ajax({
			type: 'POST',
			url: '/backend/ajax/catalog_ass.php',
			data: ({id:arr,id_good:id_t,add:0}),
			success: function(data){
				if(data == true){
					$('.t_selected',t_sop).each(function(){
						$(this).remove();
						var t_id = $(this).attr('id');
						$('.added',t_list).filter('#'+t_id).removeClass('added')
					});
					but_show();
				};
			}
		});
	});
	
	function but_show(){
		if($('.t_selected',t_list).length == '0'){
			$('.t_add').hide();	
		}else{$('.t_add').show();}
		if($('.t_selected',t_sop).length == '0'){
			$('.t_del').hide();	
		}else{$('.t_del').show();}
	}
	
	function id_test(){
		$('.t_item',t_sop).each(function(){
			t_list.find('#'+$(this).attr('id')).addClass('added');  
		})
	}
	
	but_show()
	id_test()
	//end tovars
	
	
	$(".tree").litree({
		speed: 200, // Скорость анимации
		classActive: '.cur' // Класс активного элемента					   
	});
	
	//all check
	var but_all = $('.check_all')
	var check = $('.check')
	but_all.click(function(){
		if(but_all.is(':checked')){
			check.attr('checked','checked')
		}
		if(!but_all.is(':checked')){
			check.attr('checked','')
		}
	})
	//*all check


	//menu top
	var menu = $('.menu')
	var mh = menu.outerHeight()
	$('ul',menu).css({top:mh})
	var link_1 = $('.menu>li>a')
	var link_2 = $('.menu ul a')
	if(link_2.is('.cur')){
		var podmenu = $('.menu ul a.cur').parent().parent().show()
		podmenu.parent('li').children('a').addClass('cur')
		menu.height(mh + podmenu.outerHeight())
	}
	$(link_1).click(function(){
		link_1.removeClass('cur')
		$(this).addClass('cur')
		var podmenu = $(this).next('ul').show()
		$('ul',menu).not(podmenu).hide()
		menu.height(mh + podmenu.outerHeight())
	})
	$(link_2).click(function(){
		link_2.removeClass('cur')
		$(this).addClass('cur')
	})
	//*menu top
	
	//tables style
	$(".table1 tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
	$(".table1 tr:even").css({border:'1px solid #fff'}).addClass("alt");
	
	
			$(function(){
				sp = 300
				$('.trigger_thumb').each(function(){
					$('<a>').attr('href','#').css({right:'-20px'}).addClass('ico_min').prependTo(this)
				})
				$('.ico_min').each(function(){
					var el = $(this)
					var img = el.next('img')
					$(this).toggle(function(){
						img.animate({height:'120'},sp)
						el.animate({right:'0'},sp)
					},function(){
						img.animate({height:'20'},sp)
						el.animate({right:'-20'},sp)
					})
				})
			})	
			
	//tables style
	$(".table1 tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
	$(".table1 tr:even").css({border:'1px solid #fff'}).addClass("alt");
	
	
	/*выделение текта про клике*/
	$('input.red').click(function(){
		 $(this).select();
	})
	/*запрет на изменение урла в инпуте*/
	$("input.red").keypress(function() { 
		return false;
	  
	});
});
