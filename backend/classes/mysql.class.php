<?php

class mysql {

		/**
		* 	connect to database
		*	@param  none
		*/	
		
		static  function connect(){
			
			global $dbname;
			global $hostname;
			global $username;
			global $password;
			
			//mysql_connect(v::getConfig('db/host'), v::getConfig('db/user'), v::getConfig('db/password')) or
				//die(v::getI18n('mysql_connect_faild'));			
			mysql_connect($hostname, $username, $password) or die(v::getI18n('mysql_connect_faild'));			
			$result=mysql_db_query($dbname,"SET CHARACTER SET utf8");
			
		}
		
		/**
		* 	do query 
		*	@param 	varchar  		$sql 		- query
		* 			int				$default   	- show query (0 - no, 1 - yes)
		*/	
		
		static  function query($sql,$default=0){

			if ($default==1) { echo '<div style="border: solid 1px red">'.$sql.'</div>'; }
			
			$result=mysql_query($sql);
			
			$a=array();
			while ($obj = mysql_fetch_object($result)) {
				$a[]=$obj;
			}
			
			return $a;
			
		}
		
		
		/**
		 * show one record of query
		 *
		 * @param varchar		 $sql	-	query
		 */
		
		static  function query_one($sql,$default=0){

			$a=self::query($sql,$default);
			
			return count($a)? $a[0] : false;
			
		}		
		

		

		/**
		* 	show result of query
		*	@param 	varchar 	 		$sql 				- query
		* 			varchar				$pole_for_return  	- field's name for return
		* 			int					$default   			- show query (0 - no, 1 - yes)
		*/	
		
		static  function query_findpole($sql,$pole_for_return,$default=0){

			$a=self::query($sql,$default);
			
			return count($a)? $a[0]->$pole_for_return : false;			
			
		}
		

		
		/**
		* 	do query 
		*	@param 	varchar  		$sql 		- query
		* 			int				$default   	- show query (0 - no, 1 - yes)
		*/	
		
		static  function query_only($sql,$default=0){

			if ($default==1) { echo '<div style="border: solid 1px red">'.$sql.'</div>'; }
			
			return mysql_query($sql);
			
			
		}		
		
		/**
		* 	get data parent 
		*	@param 	int				id_parent
		*/	
		
		static  function get_parent_name($table_name,$id,$pole_for_return){

			// запрос
			$sql='select '.$pole_for_return.' from '.$table_name.' where id='.$id;
		
			return self::query_findpole($sql,$pole_for_return,$default=0);
			
			
		}		

		
		/**
		* 	do query 
		*	@param 	varchar  		$sql 		- query
		* 			int				$default   	- show query (0 - no, 1 - yes)
		*/	
		
		static  function just_query($sql,$default=0){

			if ($default==1) { echo '<div style="border: solid 1px red">'.$sql.'</div>'; }
			
			$result=mysql_query($sql);
			
		}
	
		
}


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------




?>