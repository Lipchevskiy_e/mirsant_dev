<?php

class forms {

		/**
		 * update array FORM[]
		 *
		 * @param varchar $table_name
		 * @param int $id
		 * @param 0/1 $default
		 */
		static function multy_update_form($table_name, $id, $default=0) {	

            if(!$_POST['FORM']['updated_at']) {
                $_POST['FORM']['updated_at']=date('Y-m-d');
            }
		
			if ($default==1) { echo "<pre>".print_r($_POST['FORM'] ,1)."</pre>"; }
						
			if (isset($_POST['FORM'])) {
				$_sql_01=$_sql_02="";
				foreach($_POST['FORM'] as $key => $value) {
				    $value = str_replace("'",'&#39;',$value);
					$value = str_replace("\\",'&#92;',$value);
                    if ($default==1) { echo '<div style="width:200px; border: solid 0px red; color:#000000; background: yellow; padding: 2px; margin-left: 15px;"><b>'.$key.'</b> - '.$value.'</div>'; }
					// поле
					$_sql_01.="`".$key."` = ";
					// значение
					$_sql_01.="'".$value."', ";
				}
						
				$_sql_01=substr($_sql_01,0,-2);
				
				
				// формируем запрос
				$_sql="UPDATE `".$table_name."` SET ".$_sql_01." WHERE (`id`='".$id."')";			  		
				// выполняем запрос + при необходимости выводим сам запрос
				$result=mysql::query_only($_sql,$default);
				
			}

		}

		/**
		 * update all records in table_name where id=FORM[id]
		 *
		 * @param varchar $table_name
		 * @param 0/1 $default
		 * @param varchar - name fiels to update $field_to_update
		 * 
		 */
		static function multy_update_form_all_records($table_name, $field_to_update, $default=0) {	
		
			$_POST['FORM']['updated_at']=date('Y-m-d');			
			
			if ($default==1) { echo "<pre>".print_r($_POST['FORM'] ,1)."</pre>"; }
						
			if (isset($_POST['FORM'])) {
				$_sql_01=$_sql_02="";
				foreach($_POST['FORM'] as $key => $value) {
					if ($default==1) { echo '<div style="width:200px; border: solid 0px red; color:#000000; background: yellow; padding: 2px; margin-left: 15px;"><b>'.$key.'</b> - '.$value.'</div>'; }
					$value = str_replace("'",'&#39;',$value);
					$value = str_replace("\\",'&#92;',$value);
					$_sql_01='';
					
					// поле
					$_sql_01.="`".$field_to_update."` = ";
					// значение
					$_sql_01.="'".$value."', ";
					
					$_sql_01=substr($_sql_01,0,-2);
					
					
					// формируем запрос
					$_sql="UPDATE `".$table_name."` SET ".$_sql_01." WHERE (`id`='".$key."')";			  		
					// выполняем запрос + при необходимости выводим сам запрос
					$result=mysql::query_only($_sql,0);
					
				}
			
			}

		}		
		
		

	/**
	 * проверяем на checkbox, если выделили - регистрируем $_POST['FORM'] с таким именем
	 *
	 * @param array $arr
	 */
	function check_box ($arr) {
		
		for ($i=0; $i<count($arr); $i++) {

			if (isset($_POST['FORM'][$arr[$i]])) {
				$_POST['FORM'][$arr[$i]]=1;

			} else {
				$_POST['FORM'][$arr[$i]]=0;

			}
			
		}
		
	}
		
	
	/*
	* insert форму в базу НАЧАЛО
	*/ 
	function multy_insert_form($table_name, $default=0) {	

		if(!$_POST['FORM']['created_at']) {
			$_POST['FORM']['created_at']=date('Y-m-d');
		}
		
		if ($default==1) { echo "<pre>".print_r($_POST['FORM'] ,1)."</pre>"; }
					
	  	if (isset($_POST['FORM'])) {
	  		$_sql_01=$_sql_02="";
	  		foreach($_POST['FORM'] as $key => $value) {
				//echo '<div style="width:200px; border: solid 0px red; color:#000000; background: yellow; padding: 2px; margin-left: 15px;"><b>'.$key.'</b> - '.$value.'</div>';  
				// поле
                $value = str_replace("'",'&#39;',$value);
				$value = str_replace("\\",'&#92;',$value);
				$_sql_01.="`".$key."`, ";
				// значение
				$_sql_02.="'".$value."', ";
	  		}
			  		
	  		$_sql_01=substr($_sql_01,0,-2);
	  		$_sql_02=substr($_sql_02,0,-2);

	  		// формируем запрос
			$_sql="INSERT INTO `".$table_name."` (".$_sql_01.") VALUES (".$_sql_02.")";			  		
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_only($_sql,$default);
			
	  	}
	  	
	}
	/*
	* insert форму в базу КОНЕЦ
	*/ 	
	
	
	/*
	* загрузка фото 
	*/
	
	function multy_update_photo ($_dir_name, $_count_photo, $id, $_w, $_h, $_w_b, $_h_b, $_watermark=false) {
		
			//echo "<pre>".print_r($_FILES,1)."</pre>";
	
			// проверяем на картинку
		   for ($i=1; $i<=$_count_photo; $i++) {
	    		// имя картинки
			    $file_name="file".$i;

			    if ($_FILES[$file_name]['name']!=""){
			    	
		    		$name_file_destination=$id.".jpg";
		
					// -------------------------------------------------------------------
					$fileuplo = $_dir_name.'/'.md5($_FILES[$file_name]['name']);
		     		move_uploaded_file($_FILES[$file_name]['tmp_name'], $fileuplo);
					// -------------------------------------------------------------------
		
		    		$filename=$_dir_name.'/'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename, $_w_b, $_h_b, true, 0);
		
		    		$filename1=$_dir_name.'/_'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename1, $_w, $_h, true, 0);
		    		
		    		
					// + watermark
    				if ($_watermark) images::img_watermark($filename);
		    		
					// удаляем загруженное фото
					if (file_exists($fileuplo)) {
						unlink($fileuplo);
					}
					
			    }
			}
			
	}
	
	function multy_update_photo_catalog_grupa ($_dir_name, $_count_photo, $id, $_w, $_h, $_w_b, $_h_b, $_watermark=false) {
		
			//echo "<pre>".print_r($_FILES,1)."</pre>";
	
			// проверяем на картинку
		   for ($i=1; $i<=$_count_photo; $i++) {
	    		// имя картинки
			    $file_name="file".$i;

			    if ($_FILES[$file_name]['name']!=""){
			    	
		    		$name_file_destination=$id.".jpg";
		
					// -------------------------------------------------------------------
					$fileuplo = $_dir_name.'/'.md5($_FILES[$file_name]['name']);
		     		move_uploaded_file($_FILES[$file_name]['tmp_name'], $fileuplo);
					// -------------------------------------------------------------------
		
		    		$filename=$_dir_name.'/'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename, $_w_b, $_h_b, false, 0);
		
		    		$filename1=$_dir_name.'/_'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename1, $_w, $_h, false, 0);
		    		
		    		
					// + watermark
    				//if ($_watermark) images::img_watermark($filename);
		    		
					// удаляем загруженное фото
					if (file_exists($fileuplo)) {
						unlink($fileuplo);
					}
					
			    }
			}
			
	}
    
    function multy_update_photo_brand ($_dir_name, $id) {

			    $file_name="file1";

			    if ($_FILES[$file_name]['name']!=""){
			    	
		    		$name_file_destination=$id.".jpg";
		
					// -------------------------------------------------------------------
					$fileuplo = $_dir_name.'/'.md5($_FILES[$file_name]['name']);
		     		move_uploaded_file($_FILES[$file_name]['tmp_name'], $fileuplo);
					// -------------------------------------------------------------------
		
                    $size = getimagesize($fileuplo);
        
		    		$filename=$_dir_name.'/'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename, $size[0], $size[1], false, 0);
		    		
					// удаляем загруженное фото
					if (file_exists($fileuplo)) {
						unlink($fileuplo);
					}
					
			    }
			
	}
    
    function multy_update_photo_news ($_dir_name, $_count_photo, $id, $_w, $_h, $_w_b, $_h_b, $_watermark=false) {
		
			//echo "<pre>".print_r($_FILES,1)."</pre>";
	
			// проверяем на картинку
		   for ($i=1; $i<=$_count_photo; $i++) {
	    		// имя картинки
			    $file_name="file".$i;

			    if ($_FILES[$file_name]['name']!=""){
			    	
		    		$name_file_destination=$id.".jpg";
		
					// -------------------------------------------------------------------
					$fileuplo = $_dir_name.'/'.md5($_FILES[$file_name]['name']);
		     		move_uploaded_file($_FILES[$file_name]['tmp_name'], $fileuplo);
					// -------------------------------------------------------------------
		
		    		$filename=$_dir_name.'/'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename, $_w_b, $_h_b, false, 0);
		
		    		$filename1=$_dir_name.'/_'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename1, $_w, $_h, true, 0);
		    		
		    		
					// + watermark
    				if ($_watermark) images::img_watermark($filename);
		    		
					// удаляем загруженное фото
					if (file_exists($fileuplo)) {
						unlink($fileuplo);
					}
					
			    }
			}
			
	}
    
    
   	function multy_update_photo_video ($_dir_name, $id, $_w_b, $_h_b) {
	    
			    $file_name="file1";

			    if ($_FILES[$file_name]['name']!=""){
			    	
		    		$name_file_destination=$id.".jpg";
		
					// -------------------------------------------------------------------
					$fileuplo = $_dir_name.'/'.md5($_FILES[$file_name]['name']);
		     		move_uploaded_file($_FILES[$file_name]['tmp_name'], $fileuplo);
					// -------------------------------------------------------------------
		
		    		$filename=$_dir_name.'/'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename, $_w_b, $_h_b, true, 0);
		    		
					// удаляем загруженное фото
					if (file_exists($fileuplo)) {
						unlink($fileuplo);
					}
					
			    }

	}
	
	/*
	* удаляем фото
	*/
	function delete_photo($_dir_name, $_files, $default=0) {
	
		for ($i=0; $i<count($_files); $i++) {
			if (file_exists($_dir_name.'/'.$_files[$i])) {
				unlink($_dir_name."/".$_files[$i]);
				echo $default==1 ? '<div style="border: solid 1px red">'.$_dir_name."/".$_files[$i]."</div>" : '';
			}
		}
		
	}
	

	/*
	* загрузка фото для каталога
	*/
	
	function multy_update_photo_catalog ($id) {
		
			// echo "<pre>".print_r($_FILES,1)."</pre>";
	
			// проверяем на картинку
		   for ($i=1; $i<=COUNT_PHOTO_CATALOG; $i++) {
	    		// имя картинки
			    $file_name="file".$i;

			    if ($_FILES[$file_name]['name']!=""){
			    	
		    		$name_file_destination=$id.'_'.$i.'.jpg';
		    		
					// -------------------------------------------------------------------
					$fileuplo = HOST.IMG_CATALOG_PATH.'/'.md5($_FILES[$file_name]['name']);
		     		move_uploaded_file($_FILES[$file_name]['tmp_name'], $fileuplo);
					// -------------------------------------------------------------------
		
					// большое
		    		$filename=HOST.IMG_CATALOG_PATH.'/03/'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename, 330, 264, false, 0);
		
		    		// среднее фото
		    		$filename1=HOST.IMG_CATALOG_PATH.'/02/'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename1, 155, 155, false, 0);

		    		// маленькое фото
		    		$filename2=HOST.IMG_CATALOG_PATH.'/01/'.$name_file_destination;
		    		images::img_resize($fileuplo, $filename2, 64, 64, false, 0);		    		

                    $filename3=HOST.IMG_CATALOG_PATH.'/original/'.$name_file_destination;
					images::img_resize($fileuplo, $filename3, 1024, 768, false, 0);    					
		    		
					// + watermark
    				images::img_watermark($filename3);
    				
					// удаляем загруженное фото
					if (file_exists($fileuplo)) {
						unlink($fileuplo);
					}
					
			    }
			}
			
	}	
	
	
	/*
	* update checkbox в базу НАЧАЛО
	*/ 
	function multy_update_checkbox($table_name, $_arr, $_id, $_name_field1, $_name_field2) {	
		//echo "<pre>".print_r($_arr ,1)."</pre>";
		
		// очищаем связи
		$_sql='DELETE FROM `'.$table_name.'` WHERE (`'.$_name_field1.'`="'.$_id.'")';
		$result=mysql::query_only($_sql,0);
		
	  if ($_arr) {		
		
	  	foreach($_arr as $key => $value) {
				//echo '<div style="width:200px; border: solid 0px red; color:#000000; background: yellow; padding: 2px; margin-left: 15px;"><b>'.$key.'</b> - '.$value.'</div>';  
				
				// добавляем новые связи
				$_sql='INSERT INTO `'.$table_name.'` 
						(`'.$_name_field1.'`, `'.$_name_field2.'`) 
						VALUES ("'.$_id.'", "'.$value.'")';
				$result=mysql::query_only($_sql,0);
				//echo $_sql;
	  		}
		}
		/*
		* update  checkbox в базу КОНЕЦ
		*/ 	
	}
	  
	  
}
	
	

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------




?>