<?php

class ImageManager {
   
   private $image;
   private $image_type;
   private static $instance;
   private $imgpath;
   
   public static function Instance()
   {
   		if (self::$instance == null)
			self::$instance = new ImageManager();
			
		return self::$instance;
   }
   
private function preview_resize($img_name,$maxx,$maxy)
{
	$size = getimagesize($img_name);
	$var_x=$size[0];$var_y=$size[1];
	if (($var_y>$maxy))
	{
	 $tvar_y=$maxy;$reratio=$var_y/$maxy;
	 $tvar_x=$var_x/$reratio;	 
	 $var_x=$tvar_x;$var_y=$tvar_y;
	}
	if (($var_x>$maxx))
	{
	 $tvar_x=$maxx;$reratio=$var_x/$maxx;
	 $tvar_y=$var_y/$reratio;
	 $var_x=$tvar_x;$var_y=$tvar_y;
	}
	$newsize=array('width'=>$var_x,'height'=>$var_y);
	return $newsize;
}
   //�������� ��������
  	public function load($filename) {
	  $this->imgpath=$filename;
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      }
   }
   
   //���������� ��������
  	public function save($filename, $image_type=IMAGETYPE_JPEG, $compression=80, $permissions=null) {

      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
      }   
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   
   
 	public  function output($image_type=IMAGETYPE_JPEG) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image);
      }   
   }
   
   //��������� ������ �����������
  	private function getWidth() {
      return imagesx($this->image);
   }
   
   //��������� ������ �����������
   private function getHeight() {
      return imagesy($this->image);
   }
   
   //������ �� ������
   public function resizeToHeight($height) {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
   
   //������ �� ������
   public function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
   
   //�������� ���������
   public function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100; 
      $this->resize($width,$height);
   }
   
   //����������� ������
   public function resize($widtha,$heighta) {
      $dimensions=$this->preview_resize($this->imgpath,$widtha,$heighta);
	  $width=$dimensions['width'];$height=$dimensions['height'];
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;   
   }
  
  //���� ��������
	public function crop($nw, $nh)
	{

		$w = $this->getWidth();
		$h = $this->getHeight();
		
		$dimg = imagecreatetruecolor($nw, $nh);
		
		$wm = $w/$nw;
		$hm = $h/$nh;

		$h_height = $nh/2;
		$w_height = $nw/2;

		if($w> $h) {

			$adjusted_width = $w / $hm;
			$half_width = $adjusted_width / 2;
			$int_width = $half_width - $w_height;
			
			$new_image = imagecopyresampled($dimg,$this->image,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);

		} elseif(($w <$h) || ($w == $h)) {

			$adjusted_height = $h / $wm;
			$half_height = $adjusted_height / 2;
			$int_height = $half_height - $h_height;

			$new_image = imagecopyresampled($dimg,$this->image,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);

		} else {
			$new_image = imagecopyresampled($dimg,$this->image,0,0,0,0,$nw,$nh,$w,$h);
		}
		$this->image = $dimg;
	
	}
   
}
?>

