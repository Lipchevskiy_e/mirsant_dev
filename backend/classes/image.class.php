<?php

class images {


	/**
	* 	выодим картинку, если она естьы
	*	@param  		$file 		- картинка с путем
	* 					$alt		- alt для картинки
	* 					$class  	- имя класса (при необходимости)
	*/

	static function isset_image($file,$alt,$class) {

		if (file_exists(HOST.$file)) {
			return '<img src="'.$file.'" alt="'.$alt.'" title="'.$alt.'" class="'.$class.'" />';
		} else {
			return '';
		}

	}


	/**
	 * накладываем водяной знак
	 *
	 * @param имя фото $fileuplo
	 */
	static function img_watermark ($filename) {

		/*
		* watermark
		*/
		// картинки
	 if (_IMAGE_MAGICK==2)
	 {
		$im = new Imagick($filename);
		if ( ! $im->getImageAlphaChannel())
        {
            // Force the image to have an alpha channel
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_SET);
        }

		$im1 = new Imagick( HOST."/images/watermark/pic.png" );
        if ( ! $im1->getImageAlphaChannel())
        {
            // Force the image to have an alpha channel
            $im1->setImageAlphaChannel(Imagick::ALPHACHANNEL_SET);
        }

		// размеры
		$_w=$im->getImageWidth();
		$_h=$im->getImageHeight();

		$_w1=$im1->getImageWidth();
		$_h1=$im1->getImageHeight();

		// накладываем
		$im->compositeImage ( $im1, $im1->getImageCompose(),$_w-$_w1,$_h-$_h1);

		// выводим
		//header( 'Content-Type: image/jpg' );
		//die($im);

		$im->writeImage($filename);
	 } else
	 {
		passthru(CONVERT."convert ".escapeshellarg($filename)." -gravity SouthEast -geometry +10+8 ".escapeshellarg(HOST."/images/watermark/pic.png")." -composite ".escapeshellarg($filename));   
	 }
	}


	/**
	 * обжимаем картинку по заданным размерам
	 *
	 * @param исходник $fileuplo
	 * @param что будет $filename
	 * @param ширина $_width
	 * @param высота $_height
	 */
	static function img_resize ($fileuplo, $filename, $_width, $_height, $_crop, $default=0) {
      
     if (_IMAGE_MAGICK==2)
	 {
		/* Create new object */
		$im = new Imagick( $fileuplo );

		if ( ! $im->getImageAlphaChannel())
        {
            // Force the image to have an alpha channel
            $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_SET);
        }

		// Размеры
		$_w = $im->getImageWidth();  // Ширина исходного изображения
		$_h = $im->getImageHeight(); // Высота исходного изображения

		if ($_crop)
		{
			// Корректировка на вертикальное фото
			// Если фотка вертикальная
			if ($_h > $_w)
			{
				// Извлекаем из исходного изображения область
				// начиная с левого верхнего угла (0, 0)
				// шириной = $_w, высотй = $_w
				// Выходит квадрат - нижняя часть отсекается
				$im->cropImage($_w, $_w, 0, 0);
			}

			// Кропаем (теперь уже квадратное изображение)
			$im->cropThumbnailImage($_width, $_height);
		}

		else
		{
			// Если ширина или высота исходной картинки больше заданной
			if (($_w > $_width) OR ($_h > $_height))
			{
				// Ставим пропорциональный размер
				$im->thumbnailImage($_width, 0);
			}
		}

		/* Display */
		//header( 'Content-Type: image/jpg' );
		//die($im);

		$im->writeImage($filename);

		//echo $default==1 ? $_todo : '';
	 }
	 if (_IMAGE_MAGICK==1)
	 {
			// определяем размер картинки
			$_img = getimagesize($fileuplo);
			$_real_width=$_img[0];
			$_real_height=$_img[1];
				
			if ($_real_width<$_width) {
				$_width=$_real_width;
			}
			if ($_real_height<$_height) {
				$_height=$_real_height;
			}

		
			if ($_crop) {
				//$_todo=CONVERT."convert ".$fileuplo." -resize \"".$_width."x".$_height."^\" -gravity center -crop ".$_width."x".$_height."+0+0 +repage ".$filename;
				$_todo=CONVERT."convert ".$fileuplo." -resize \"".$_width."x".$_height."^\" -gravity center -crop ".$_width."x".$_height."+0+0 +repage ".$filename;
				
			} else {
		  		$_todo=CONVERT."convert -quality 80 -thumbnail ".$_width."x".$_height." ".$fileuplo." ".$filename;
			}
			passthru($_todo);			
		  	echo $default==1 ? $_todo : '';	 
	 }
	 if (_IMAGE_MAGICK==0)
	 {
	  $iman = new ImageManager;
	  $iman->load($fileuplo);
	  if ($_crop) {
	   $iman->crop($_width,$_height);
	  } else {
	   $iman->resize($_width,$_height);
	  }
	  $iman->save($filename);
	 }
	}


}

?>