<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

    require "../../main.php";

    $id = intval($_POST['id']);
    if($id != 0)
    {
        $str = '';
        $sql = "SELECT has_weight, has_type, has_mixer, has_shower, has_toilet, has_bathtub FROM catalog_tree WHERE id=".$id;
        $res = mysql::query_one($sql);

        /*if($res->has_weight == 1)
            $str .= '<div class="inline">Ширина (см):<input type="text" name="ST[width]"></div><div class="inline">Высота (см):<input type="text" name="ST[height]"></div><div class="inline">Длина (см):<input type="text" name="ST[length]"></div>';

        if($res->has_type == 1) {
            $type_tovar = mysql::query("SELECT * FROM type_tovar WHERE status=1 ORDER BY sort, created_at DESC, id DESC");
            $str .= '<div class="inline">Тип:<select name="ST[has_type]"><option value="0">Не менять</option>';
            foreach ($type_tovar as $type_tovar) {
                $str .= '<option value="'.$type_tovar->id.'">'.$type_tovar->name.'</option>';
            }
            $str .= '</select></div>';
        }*/

        if($res->has_toilet){
            $result = mysql::query('SELECT id, name FROM dict_toilet_mount ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип монтажа унитаза:<select name="ST[toilet_mount]"><option value="0">Не менять</option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_bathtub){
            $result = mysql::query('SELECT id, name FROM dict_bathtub_form ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Форма ванны:<select name="ST[bathtub_form]"><option value="0">Не менять</option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_shower) {
            $result = mysql::query('SELECT id, name FROM dict_tray_depth ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Глубина поддона:<select name="ST[tray_depth]"><option value="0">Не менять</option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
            $result = mysql::query('SELECT id, name FROM dict_cabin_form ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Форма кабины:<select name="ST[cabin_form]"><option value="0">Не менять</option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_mixer) {
            $result = mysql::query('SELECT id, name FROM dict_colors ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Цвет:<select name="ST[color]"><option value="0">Не менять</option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_mounting_type ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип монтажа:<select name="ST[mounting_type]"><option value="0">Не менять</option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_mixer_style ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Стиль смесителя:<select name="ST[mixer_style]"><option value="0">Не менять</option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_mixer_type ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип смесителя:<select name="ST[mixer_type]"><option value="0">Не менять</option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }
    }
    else
        $str = '';

    die(json_encode(array(
        'str' => $str,
    )));