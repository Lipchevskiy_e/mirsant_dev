<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    $val = intval($_POST['val']);
    
    require "../../main.php";
    define ('APPLICATION','/backend');
    
    switch($val)
    {
        case 1:

            $_res = dbh::get_catolog_tree();
        
            $str = '<div class="item_1" id="grups"><label class="block title_1">Группа товаров:</label><select name="FORM[id_parent]" id="id_parent"><option></option>';
            $str .= system::show_tpl(array('select'=>$_res,'level'=>'','tpl_folder'=>'cost_change'),'cost_change/select.php');
            $str .= '</select></div>';
            
            break;
        
        case 2:
            
            $_sql = "SELECT * FROM brand ORDER BY sort, pole";
            $_res = mysql::query($_sql);
            
            $str .= '<div class="item_1" id="brands"><label class="block title_1">Производитель:</label><select name="FORM[brand]" id="brand"><option></option>';
            foreach($_res as $b)
            {
                $str .= '<option value="'.$b->id.'">'.$b->pole.'</option>';
            }
    		$str .= '</select></div>';
            
            break;
            
        case 3:
        
            $_res = dbh::get_catolog_tree();
        
            $str = '<div class="item_1" id="grups2"><label class="block title_1">Группа товаров:</label><select name="FORM[id_parent]" id="id_parent2"><option></option>';
            $str .= system::show_tpl(array('select'=>$_res,'level'=>'','tpl_folder'=>'cost_change'),'cost_change/select.php');
            $str .= '</select></div>';
            
            break;
            
        case 4:
            
            $sql = "SELECT * FROM catalog WHERE status=1 ORDER BY sort, name";
            $res = mysql::query($sql);

            $str .= '<div class="item_1" id="catalog"><label class="block title_1">Выберите товары:</label><select name="GOODS[]" id="good" multiple="multiple" size="15" style="min-width:200px;">';
            foreach($res as $b)
            {
                $str .= '<option value="'.$b->id.'">'.$b->name.'</option>';
            }
    		$str .= '</select></div>';
            
            break;
    }
    
    
    die(json_encode(array(
		'str'=>$str,
        'choose'=>$val
	)));
?>