<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    
    require "../../main.php";
    
    $parent_id = intval($_POST['parent_id']);
    if($parent_id != 0)
    {
        $sql = "SELECT has_weight, has_type, has_mixer, has_shower, has_toilet, has_bathtub, has_gidr_van, has_tumb_van, has_penal_van, has_bide_mont, has_kuh_mont, has_kuh_count FROM catalog_tree WHERE id=".$parent_id;
        $res = mysql::query_one($sql);


        if($res->has_toilet){
            $result = mysql::query('SELECT id, name FROM dict_toilet_mount ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип монтажа унитаза:<select name="ST[toilet_mount]" id="toilet_mount"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_gidr_van){
            $result = mysql::query('SELECT id, name FROM dict_gidr_van ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Форма ванны:<select name="ST[gidr_van]" id="gidr_van"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_tumb_van){
            $result = mysql::query('SELECT id, name FROM dict_tumb_van ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип монтажа:<select name="ST[tumb_van]" id="tumb_van"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_penal_van){
            $result = mysql::query('SELECT id, name FROM dict_penal_van ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип монтажа:<select name="ST[penal_van]" id="penal_van"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_bide_mont){
            $result = mysql::query('SELECT id, name FROM dict_bide_mont ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип монтажа:<select name="ST[bide_mont]" id="bide_mont"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_kuh_mont){
            $result = mysql::query('SELECT id, name FROM dict_kuh_mont ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип монтажа:<select name="ST[kuh_mont]" id="kuh_mont"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_kuh_count){
            $result = mysql::query('SELECT id, name FROM dict_kuh_count ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Кол-во секций:<select name="ST[kuh_count]" id="kuh_count"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_bathtub){
            $result = mysql::query('SELECT id, name FROM dict_bathtub_form ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Форма ванны:<select name="ST[bathtub_form]" id="bathtub_form"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_shower) {
            $result = mysql::query('SELECT id, name FROM dict_tray_depth ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Глубина поддона:<select name="ST[tray_depth]" id="tray_depth"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_cabin_form ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Форма кабины:<select name="ST[cabin_form]" id="cabin_form"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_mixer) {
            $result = mysql::query('SELECT id, name FROM dict_colors ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Цвет:<select name="ST[color]" id="color"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_mounting_type ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип монтажа:<select name="ST[mounting_type]" id="mounting_type"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_mixer_style ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Стиль смесителя:<select name="ST[mixer_style]" id="mixer_style"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_mixer_type ORDER BY sort ASC, id DESC');
            $str .= '<div class="inline">Тип смесителя:<select name="ST[mixer_type]" id="mixer_type"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }
    }
    else
        $str = '';
    
    die(json_encode(array(
		'str' => $str,
	)));
?>