<?php

    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

    require "../../main.php";

    $id_brand = intval($_POST['brand_id']);

    $str = '';
    if($id_brand != 0)
    {
        $serie_brand = mysql::query("SELECT * FROM serie_brand WHERE id_brand=".$id_brand);
        $arr = array();
        foreach($serie_brand AS $s)
        {
            $arr[] = $s->id_serie;
        }
        $arr = implode(',', $arr);
        if($arr == '')
            $arr = '0';

        $series = mysql::query("SELECT id, name FROM serie WHERE id IN(".$arr.") ORDER BY sort, id DESC");

        $str = '<option value="0">Не изменять</option>';
        foreach($series as $s1) {
            $str .= '<option value="'.$s1->id.'">'.$s1->name.'</option>';
        }
    }

    die(json_encode(array(
        'series' => $str
    )));