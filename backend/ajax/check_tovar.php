<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    
    require "../../main.php";
    
    $id = intval($_POST['id']);
    if($id != 0)
    {
        $sql = "SELECT has_weight, has_type, has_mixer, has_shower, has_toilet, has_bathtub, has_gidr_van, has_tumb_van, has_penal_van, has_bide_mont, has_kuh_mont, has_kuh_count FROM catalog_tree WHERE id=".$id;
        $res = mysql::query_one($sql);
        
        $str = '<div id="data">';
        
        if($res->has_weight == 1)
            $str .= '<div class="item_1"><label class="block title_1">Ширина (см):</label><input type="text" name="FORM[width]" class="input_1" id="width"> </div><div class="item_1"><label class="block title_1">Высота (см):</label><input type="text" name="FORM[height]" class="input_1" id="height"></div><div class="item_1"><label class="block title_1">Длина (см):</label><input type="text" name="FORM[length]" class="input_1" id="length"></div>';
        
        if($res->has_type == 1)
        {
            $type_tovar = mysql::query("SELECT * FROM type_tovar WHERE status=1 ORDER BY sort, created_at DESC, id DESC");
        
            $str .= '<div class="item_1" id="type_tovar"><label class="block title_1">Тип:</label><select name="FORM[has_type]" id="has_type"><option></option>';
            				
            foreach ($type_tovar as $type_tovar) {
                $str .= '<option value="'.$type_tovar->id.'">'.$type_tovar->name.'</option>';
            }

            $str .= '</select></div>';
        }

        if($res->has_toilet){
            $result = mysql::query('SELECT id, name FROM dict_toilet_mount ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Тип монтажа унитаза:</label><select name="FORM[toilet_mount]" id="toilet_mount"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_gidr_van){
            $result = mysql::query('SELECT id, name FROM dict_gidr_van ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Форма ванны:</label><select name="FORM[gidr_van]" id="gidr_van"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_tumb_van){
            $result = mysql::query('SELECT id, name FROM dict_tumb_van ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Тип монтажа:</label><select name="FORM[tumb_van]" id="tumb_van"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_penal_van){
            $result = mysql::query('SELECT id, name FROM dict_penal_van ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Тип монтажа:</label><select name="FORM[penal_van]" id="penal_van"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_bide_mont){
            $result = mysql::query('SELECT id, name FROM dict_bide_mont ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Тип монтажа:</label><select name="FORM[bide_mont]" id="bide_mont"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_kuh_mont){
            $result = mysql::query('SELECT id, name FROM dict_kuh_mont ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Тип монтажа:</label><select name="FORM[kuh_mont]" id="kuh_mont"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_kuh_count){
            $result = mysql::query('SELECT id, name FROM dict_kuh_count ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Кол-во секций:</label><select name="FORM[kuh_count]" id="kuh_count"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_bathtub){
            $result = mysql::query('SELECT id, name FROM dict_bathtub_form ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Форма ванны:</label><select name="FORM[bathtub_form]" id="bathtub_form"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_shower) {
            $result = mysql::query('SELECT id, name FROM dict_tray_depth ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Глубина поддона:</label><select name="FORM[tray_depth]" id="tray_depth"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_cabin_form ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Форма кабины:</label><select name="FORM[cabin_form]" id="cabin_form"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }

        if($res->has_mixer) {
            $result = mysql::query('SELECT id, name FROM dict_colors ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Цвет:</label><select name="FORM[color]" id="color"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_mounting_type ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Тип монтажа:</label><select name="FORM[mounting_type]" id="mounting_type"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_mixer_style ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Стиль смесителя:</label><select name="FORM[mixer_style]" id="mixer_style"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';

            $result = mysql::query('SELECT id, name FROM dict_mixer_type ORDER BY sort ASC, id DESC');
            $str .= '<div class="item_1"><label class="block title_1">Тип смесителя:</label><select name="FORM[mixer_type]" id="mixer_type"><option></option>';
            foreach ($result as $obj) {
                $str .= '<option value="'.$obj->id.'">'.$obj->name.'</option>';
            }
            $str .= '</select></div>';
        }
        
        $str .= '</div>';
    }
    else
        $str = '';
    
    die(json_encode(array(
		'str' => $str,
	)));
?>