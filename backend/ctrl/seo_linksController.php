<?php

class seo_linksController extends Controller {

    public $tpl_folder = 'seo_links';
    public $tablename = 'seo_links';    

    function indexAction () {
        $result = mysql::query('select * from '.$this->tablename.' order by id desc');
        return system::show_tpl(
            array(
                'result'=>$result,
                'tpl_folder'=>$this->tpl_folder
            ),$this->tpl_folder.'/index.php');
    }

    function newAction () {
        if ($_POST) {
            forms::check_box (array('status'));
            forms::multy_update_form($this->tablename, $_POST['id']);
            $_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];
        } else {
            $_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];
        }
        $result=mysql::query_one($_sql);
        return system::show_tpl(
            array(
                'obj'=>$result,
                'tpl_folder'=>$this->tpl_folder
            ),$this->tpl_folder.'/new.php');     
    }

    function addAction () {
        
        if ($_POST) {                   
            forms::check_box (array('status'));
            forms::multy_insert_form($this->tablename,0);          
        }           
    
        return system::show_tpl(
            array(
                'tpl_folder'=>$this->tpl_folder
            ),$this->tpl_folder.'/add.php'); 
                        
    }

    function deleteAction () {
        mysql::just_query('DELETE FROM seo_links WHERE id = "'.$_GET['id'].'"');
        header('Location:/backend/seo_links/index');
        exit(0);
    }
    
}