<?php

class contentController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='content';
	public $tablename='content';	
	
	/**
	 * show all podrazdel with parents
	 */
	function indexAction () {
	
		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }

		// проверяем на суперадмина
		//if (general::sadmin()) {

		// строим запрос
		$_sql='SELECT * FROM '.$this->tablename.' where id_parent=0 '.general::get_status_for_filter($this->tablename).' order by sort, name';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * edit content page site
	 */
	function editAction () {
		
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];			
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result,
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/edit.php');
					
	}	
	
	
	/**
	 * edit podrazdel
	 */
	function newAction () {
	
		if ($_POST) {
			
			
			// проверяем на суперадмина
			if (general::sadmin()) {
				// проверяем на checkbox
				forms::check_box (array('status','has_podrasdel','is_map'));
			} else {
				// проверяем на checkbox
				forms::check_box (array('status'));
			}
			
			// проверяем на существование такого action, если есть - добавляем несколько символов
			$_sql='SELECT count(*) as count FROM '.$this->tablename.' where action="'.$_POST['FORM']['action'].'" and id!='.$_POST['id'];
			// выполняем запрос + при необходимости выводим сам запрос
			if (mysql::query_findpole($_sql,'count',0)>0) {
				$_POST['FORM']['action'].='_new';
			}
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			// записываем multychechbox галереи
			forms::multy_update_checkbox('content_gallerygrupa',$_POST['gallery_grupa'],$_POST['id'],'id_content','id_gallerygrupa');
			
			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			
			
			// выбираем все загруженные файлы к разделу
			$_sql_files='SELECT * FROM files where id_content='.$_POST['id'];

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'].' order by id desc';			

			// выбираем все загруженные файлы к разделу
			$_sql_files='SELECT * FROM files where id_content='.$_GET['id'].' order by id';
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);

		// выбираем все загруженные файлы к разделу
		$_files=mysql::query($_sql_files,0);
		
		// выбираем разделы у которых могут быть подразделы
		$_sql='SELECT * FROM '.$this->tablename.' where has_podrasdel=1';
		$select=mysql::query($_sql,0);
		
		// выбираем фотоальбомы
		$_sql='SELECT gallery_grupa.* FROM gallery_grupa order by gallery_grupa.sort';
		$gallery_grupa=mysql::query($_sql,0);		
		
		return system::show_tpl(
			array(
				'obj'=>$result,
				'select'=>$select,
				'msg'=>$this->msg, 
				'gallery_grupa'=>$gallery_grupa,
				'tpl_folder'=>$this->tpl_folder,
				'_files'=>$_files
			),$this->tpl_folder.'/new'.general::sadmin_tpl().'.php');
	
	}

	
	/**
	 * add new podrazdel
	 */
	function addAction () {
		if ($_POST['parid'])
		{
		 if (!$_SESSION['bparid'] || $_SESSION['bparid']!=$_POST['parid'])
		 { $_SESSION['bparid']=$_POST['parid']; }
		 unset ($_POST['parid']);
		}		
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status','has_podrasdel','is_map'));
			
			// проверяем на существование такого action, если есть - добавляем несколько символов
			$_sql='SELECT count(*) as count FROM '.$this->tablename.' where action="'.$_POST['FORM']['action'].'"';			
			// выполняем запрос + при необходимости выводим сам запрос
			if (mysql::query_findpole($_sql,'count',0)>0) {
				$_POST['FORM']['action'].='_new';
			}
			
			// записываем в базу
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
		}
		if ($_SESSION['bparid'])
		{
		 $_POST['parid']=$_SESSION['bparid'];
		 unset ($_SESSION['bparid']);
		}
		// выбираем разделы у которых могут быть подразделы
		$_sql='SELECT * FROM '.$this->tablename.' where has_podrasdel=1 order by name';
		$select=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'select'=>$select,
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');
	
	}
	
	
	/**
	 * delete podrasdel
	 */
	function deleteAction () {
		
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));

		// удаляем из таблицы Content
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		return self::indexAction();		
					
	}	
	
	
	
	/********************************************************************
	/***************************** SUPERADMIN   *************************
	/********************************************************************
	
	/**
	 * добавляем раздел для СУПЕРАДМИНА
	 */
	function add_sadminAction () {
		
		// только для СуперАдмина
		if (!general::sadmin()) { return general::messages(0,v::getI18n('backend_orror_access')); }
	
		$this->tpl=$this->tpl_folder.'/add'.general::sadmin_tpl().'.php';		

		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status','has_podrasdel','is_map'));
			
			// проверяем на существование такого action, если есть - добавляем несколько символов
			$_sql='SELECT count(*) as count FROM '.$this->tablename.' where action="'.$_POST['FORM']['action'].'"';			
			// выполняем запрос + при необходимости выводим сам запрос
			if (mysql::query_findpole($_sql,'count',0)>0) {
				$_POST['FORM']['action'].='_new';
			}
			
			// записываем в базу
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));


		}
		
		// выбираем разделы у которых могут быть подразделы
		$_sql='SELECT * FROM '.$this->tablename.' where has_podrasdel=1 order by name';
		$select=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'select'=>$select,
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl);		
	
	}	
	
	/**
	 * удаляем загруженный файл
	 */
	function deletefileAction () {

		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete_file'));

		// определяем имя удаляего файла
		$_sql='SELECT * FROM `files` where id='.$_GET['id_photo'];
		$result=mysql::query_one($_sql,0);
		
		// имя файла на удаление
		$_name=array (
			$_GET['id_photo'].'.'.end(explode(".", $result->name))
		);
		
		// удаляем файл
		forms::delete_photo(HOST.FILES_PATH, $_name, 0);
		
		// удаляем запись из базы
		$_sql='DELETE FROM `files` WHERE (`id`="'.$_GET['id_photo'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// выводим новость
		return self::newAction();
		
	}	
	
	
} 

?>