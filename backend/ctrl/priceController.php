<?php

class priceController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='price';
	public $tablename='price';	
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {
	
		// строим запрос
		$_sql='SELECT
					price.*,
					count(price.id) as count
				FROM
					price
				GROUP BY
					price.id desc';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {
		
		// удаляем из таблицы NEWS
		$_sql='SELECT * FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::query_one($_sql,0);

		// удаляем файл
		if (file_exists(HOST.PRICE_LIST_PATH.'/'.$result->pole)) {
			unlink(HOST.PRICE_LIST_PATH.'/'.$result->pole);
		}		
		
		// удаляем из таблицы NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * добавляем новый материал
	 */
	function addAction () {
		
		if ($_POST) {
			
			// загружаем файл
			if ($_FILES) {
			
				//$_arr=explode('.',$_FILES[file1][name]);
				//$_file_name=v::toSlug($_arr[0]).'.'.$_arr[1];

				preg_match('/(\S+)\.(\S+)$/',$_FILES[file1][name],$_arr);
				$_file_name=v::toSlug($_arr[1]).'.'.$_arr[2];				
			
				//echo "<pre>".print_r($_FILES,1)."</pre>";
		     	move_uploaded_file($_FILES[file1]['tmp_name'], HOST.PRICE_LIST_PATH.'/'.$_file_name);
				$_POST['FORM']['pole']=$_file_name;
		
			}
			
			// записываем в базу
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			header('Location: /backend/'.$this->tpl_folder.'/index');
		
		}	
		
	
		return system::show_tpl(
			array(
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');		
			
	}	
	
} 

?>