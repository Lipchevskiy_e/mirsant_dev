<?php

class votingController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='voting';
	public $tablename='voting';	
	
	/**
	 * show all podrazdel with parents
	 */
	function indexAction () {
	
		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }

		// проверяем на суперадмина
		//if (general::sadmin()) {

		// строим запрос
		$_sql='SELECT * FROM '.$this->tablename.' where id_parent=0 '.general::get_status_for_filter($this->tablename).' order by sort';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * edit podrazdel
	 */
	function newAction () {
	
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];				
			
		} else {
			
			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];				
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);

		return system::show_tpl(
			array(
				'msg'=>$this->msg, 
				'tpl_folder'=>$this->tpl_folder,
				'obj'=>$result
			),$this->tpl_folder.'/new.php');
	
	}

	
	/**
	 * add new podrazdel
	 */
	function addAction () {

		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
		}

		return system::show_tpl(
			array(
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');
				
	}
	
	
	/**
	 * add new podrazdel
	 */
	function add_answerAction () {
	
		if ($_POST) {
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			$_GET['id_parent']=$_POST['FORM']['id_parent'];
			
		}

		// выбираем разделы у которых могут быть подразделы
		$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id_parent'];
		$select=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'select'=>$select,			
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add_answer.php');		
		
			
	}
		
	
	
	/**
	 * delete podrasdel
	 */
	function deleteAction () {
		
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));

		// удаляем из таблицы Content
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		return self::indexAction();		
					
	}	
	
	
} 

?>