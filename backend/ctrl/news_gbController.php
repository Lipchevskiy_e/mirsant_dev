<?php

class news_gbController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='news_gb';
	public $tablename='news_gb';	
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {
		
		// если перешли по get ссылке из меню
		//if(isset($_GET['status'])) { $_POST['status']=$_GET['status']; }	
		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }		

		// строим запрос
		$_sql='SELECT news_gb.*, news.zag as zag 
				FROM news_gb, news 
				WHERE news.id=news_gb.id_news '.general::get_status_for_filter('news_gb').' 
				ORDER by id desc';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	 */
	function newAction () {
	
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];			
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {
		
		// удаляем из таблицы
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	
	/**
	 * мультиредактирование 
	 */
	function authorizeAction () {
		
		//echo "<pre>".print_r($_POST['kt_where'] ,1)."</pre>";

	  	foreach($_POST['kt_where'] as $key => $value) {
			//echo '<div style="width:200px; border: solid 0px red; color:#000000; background: yellow; padding: 2px; margin-left: 15px;"><b>'.$key.'</b> - '.$value.'</div>';  
			
			if ($_POST['st']==1) {	
				// авторизовуем
				$_sql="UPDATE `".$this->tablename."` SET `status`='1' WHERE (`id`='".$value."')";
				$result=mysql::just_query($_sql,0);
			} else if ($_POST['st']==2) {	
				// авторизовуем
				$_sql="DELETE FROM `".$this->tablename."` WHERE (`id`='".$value."')";
				$result=mysql::just_query($_sql,0);
			}

	  	}
	  				
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_save'));		
		
		return self::indexAction();
		
		// редирект
		//header('Location: /backend/gb/index'); ??????????????
					
	}	
	
	

} 

?>