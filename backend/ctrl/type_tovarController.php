<?php

class type_tovarController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// ������ / ������ ����
	public $_width=0;
	public $_height=0;
	public $_width_b=0;
	public $_height_b=0;	
	
	// ��� �������� ������� + ��� ����� � ���������
	public $tpl_folder='type_tovar';
	public $tablename='type_tovar';	
	
	/**
	 * ���������� ���� ������ ����������
	 */
	function indexAction () {

		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }
	
		// ������ ������
		$_sql='SELECT * FROM type_tovar WHERE id>0 '.general::get_status_for_filter('type_tovar').' ORDER BY sort, created_at DESC, id DESC';
		
		// ��������� ������ + ��� ������������� ������� ��� ������
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * ����������� ��������� ��������
	 */
	function newAction () {
	
		if ($_POST) {
			
			// ��������� �� checkbox
			forms::check_box (array('status'));
			
			// ���������� � ����
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			//  ������ ������
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {

			// ������ ������
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];			
		
		}
		
		// ��������� ������ + ��� ������������� ������� ��� ������
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	/**
	 * ������� ��������
	 */
	function deleteAction () {
		
		// ������� �� ������� NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// ���������
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * ��������� ����� ��������
	 */
	function addAction () {
		
		if ($_POST) {
			
			// ��������� �� checkbox
			forms::check_box (array('status'));
			
			// ���������� � ����
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
		
		}	
		
	
		return system::show_tpl(
			array(
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');		

	
	}	
	
} 

?>