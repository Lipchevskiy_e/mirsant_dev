<?php

class deliverController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='deliver';
	public $tablename='deliver';		
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {
		
		if(isset($_POST['city'])) { $_SESSION['city']=$_POST['city']; }
		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }
		
		// get limit 
		// всего в каталоге 
		
		// строим запрос
		$_sql='SELECT '.$this->tablename.'.*, city.pole FROM '.$this->tablename.', city
				where city.id='.$this->tablename.'.id_city '.general::get_status_for_filter($this->tablename).' 
				order by id_city, sort';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_SESSION['status']) ? $_SESSION['status'] : 2,
				'_city'=>isset($_SESSION['city']) ? $_SESSION['city'] : 0,
				'tpl_folder'=>$this->tpl_folder,
				'city'=>dbh::get_city()
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	 */
	function newAction () {
	
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			
			
		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];		

		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg, 
				'city'=>dbh::get_city(),
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {
		
		// удаляем из таблицы NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * добавляем новый материал
	 */
	function addAction () {
	
		global $_sklad;
		
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			//for($i=0; $i<50; $i++) {
			
			// записываем в базу
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			//}
			
		}	
		
		// строим запрос
		$_sql='SELECT * FROM '.$this->tablename.' where id>0 order by sort desc';
		// выполняем запрос + при необходимости выводим сам запрос
		$select=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'city'=>dbh::get_city(),
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');		

	
	}	
	
	
	/**
	 * мультиредактирование 
	 */
	function authorizeAction () {
		
		//echo "<pre>".print_r($_POST['kt_where'] ,1)."</pre>";

	  	foreach($_POST['kt_where'] as $key => $value) {
			//echo '<div style="width:200px; border: solid 0px red; color:#000000; background: yellow; padding: 2px; margin-left: 15px;"><b>'.$key.'</b> - '.$value.'</div>';  
			
			if ($_POST['st']==1) {	
				// авторизовуем
				$_sql="UPDATE `".$this->tablename."` SET `status`='1' WHERE (`id`='".$value."')";
				$result=mysql::just_query($_sql,0);
			} else if ($_POST['st']==2) {	
				// авторизовуем
				$_sql="DELETE FROM `".$this->tablename."` WHERE (`id`='".$value."')";
				$result=mysql::just_query($_sql,0);
			}

	  	}
	  				
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_save'));		
		
		return self::indexAction();
		
		// редирект
		//header('Location: /backend/gb/index'); ??????????????
					
	}	
	
	
} 

?>