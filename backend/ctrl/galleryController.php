<?php

class galleryController extends Controller {

	public $msg=false;
	public $tpl=false;

	// ширина / высота фото
	public $_width=0;
	public $_height=0;
	public $_width_b=0;
	public $_height_b=0;

	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='gallery';
	public $tablename='gallery';

	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {

		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }

		if (isset($_POST['id_parent']) and $_POST['id_parent']>0) {
			$limit=1000;
		} else {
			// get limit 
			// всего в каталоге 
			$_total_gallery=dbh::gallery_get_total_count();
			$limit=pager::pager_limit($_total_gallery,ADMIN_GALLERY_AT_PAGE);			
		}
		
		// строим запрос
		$_sql='SELECT * FROM '.$this->tablename.' where id>0 '.general::get_status_for_filter($this->tablename).' '.general::get_for_filter ($this->tablename, 'id_parent').' order by sort  limit '.$limit;

		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		// выбираем фотоальбомы
		$_sql='SELECT * FROM gallery_grupa where status=1 and id_parent=0 order by pole';
		$gallery_grupa=mysql::query($_sql,0);

		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'gallery_grupa'=>$gallery_grupa,
				'tpl_folder'=>$this->tpl_folder,
				'_total_gallery'=>$_total_gallery
			),$this->tpl_folder.'/index.php');

	}


	/**
	 * редактируем выбранный материал
	 */
	function newAction () {

		if ($_POST) {

			$this->_width=mysql::query_findpole('select zna from config where id=23','zna');
			$this->_height=mysql::query_findpole('select zna from config where id=24','zna');
			$this->_width_b=mysql::query_findpole('select zna from config where id=25','zna');
			$this->_height_b=mysql::query_findpole('select zna from config where id=26','zna');

			// проверяем на checkbox
			forms::check_box (array('status','firstpage','alt'));

			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));

			// загружаем фото
			forms::multy_update_photo(
				HOST.IMG_GALLERY_PATH,
				1,
				$_POST['id'],
				$this->_width,
				$this->_height,
				$this->_width_b,
				$this->_height_b,
				true
			);

			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];

		}

		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);

		// выбираем фотоальбомы
		$_sql='SELECT * FROM gallery_grupa WHERE id_parent=0 order by sort';
		$select=mysql::query($_sql,0);

		return system::show_tpl(
			array(
				'obj'=>$result,
				'msg'=>$this->msg,
				'select'=>$select,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/new.php');

	}


	/**
	 * удаляем фото новости
	 */
	function deletephotoAction () {

		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete_photo'));

		// имя файла на удаление
		$_name= array (
			"_".$_GET['id'].'.jpg',
			$_GET['id'].'.jpg'
		);

		// удаляем
		forms::delete_photo(HOST.IMG_GALLERY_PATH, $_name, 0);

		// выводим новость
		return self::newAction();

	}


	/**
	 * удаляем материал
	 */
	function deleteAction () {

		// удаляем из таблицы NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';
		$result=mysql::just_query($_sql,0);

		// удаляем фото новости
		self::deletephotoAction();

		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));

		return self::indexAction();

	}


	/**
	 * добавляем новый материал
	 */
	function addAction () {

		if ($_POST) {

			/*
			$this->_width=mysql::query_findpole('select zna from config where id=23','zna');
			$this->_height=mysql::query_findpole('select zna from config where id=24','zna');
			$this->_width_b=mysql::query_findpole('select zna from config where id=25','zna');
			$this->_height_b=mysql::query_findpole('select zna from config where id=26','zna');
			*/

			$_sql_files='SELECT * FROM gallery where id_parent=0 order by id';
			$_files=mysql::query($_sql_files,0);
			foreach ($_files as $_files) {

				// поля
				if (isset($_POST['FORM1']['status_'.$_files->id])) {
					$_POST['FORM']['status']=1;
				} else {
					$_POST['FORM']['status']=0;
				}
				if (isset($_POST['FORM1']['firstpage_'.$_files->id])) {
					$_POST['FORM']['firstpage']=1;
				} else {
					$_POST['FORM']['firstpage']=0;
				}
				if (isset($_POST['FORM1']['alt_'.$_files->id])) {
					$_POST['FORM']['alt']=1;
				} else {
					$_POST['FORM']['alt']=0;
				}

				$_POST['FORM']['pole']=$_POST['FORM1']['pole_'.$_files->id];

				// записываем в базу
				forms::multy_update_form($this->tablename,$_files->id,0);

				/*
				// загружаем фото
				forms::multy_update_photo(
					HOST.IMG_GALLERY_PATH,
					10,
					mysql_insert_id(),
					$this->_width,
					$this->_height,
					$this->_width_b,
					$this->_height_b,
					true
				);
				*/

			}

			$this->msg=general::messages(1,v::getI18n('backend_after_save'));

		}

		// выбираем фотоальбомы
		$_sql='SELECT * FROM gallery_grupa where id_parent=0 order by sort';
		$_sql_files='SELECT * FROM gallery where id_parent=0 order by sort';
		$select=mysql::query($_sql,0);
        $_files=mysql::query($_sql_files,0);

		return system::show_tpl(
			array(
				'msg'=>$this->msg,
				'select'=>$select,
				'tpl_folder'=>$this->tpl_folder,
				'_files'=>$_files
			),$this->tpl_folder.'/add.php');


	}



	/**
	 * мультиредактирование
	 */
	function authorizeAction () {

		//echo "<pre>".print_r($_POST['kt_where'] ,1)."</pre>";

	  	foreach($_POST['kt_where'] as $key => $value) {
			//echo '<div style="width:200px; border: solid 0px red; color:#000000; background: yellow; padding: 2px; margin-left: 15px;"><b>'.$key.'</b> - '.$value.'</div>';

			if ($_POST['st']==1) {
				// авторизовуем
				$_sql="UPDATE `".$this->tablename."` SET `status`='1' WHERE (`id`='".$value."')";
				$result=mysql::just_query($_sql,0);
			} else if ($_POST['st']==2) {
				// авторизовуем
				$_sql="DELETE FROM `".$this->tablename."` WHERE (`id`='".$value."')";
				$result=mysql::just_query($_sql,0);

				// Удаляем фото (добавлено)
				// Amber (25-05-2011)
				$_name= array (
					'_'.$value.'.jpg',
					$value.'.jpg'
				);
				forms::delete_photo(HOST.IMG_GALLERY_PATH, $_name, 0);
			}

	  	}

		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_save'));

		return self::indexAction();

		// редирект
		//header('Location: /backend/gb/index'); ??????????????

	}


}

?>