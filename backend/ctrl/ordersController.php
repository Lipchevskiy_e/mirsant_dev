<?php

class ordersController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {
	
		if ($_POST) {
			// даты
			if ($_POST['date_s']!='') $_POST['date_s']=general::date_to_database($_POST['date_s']);
			if ($_POST['date_po']!='') $_POST['date_po']=general::date_to_database($_POST['date_po']);
		}
		
		// строим запрос
		$_sql="SELECT order_number.*, sum(orders.cost*orders.kolvo) as cost
				FROM order_number
				Left Outer Join orders ON order_number.id =orders.number_order
				WHERE order_number.id>0 ".general::get_status_for_filter('order_number')."				
				GROUP BY order_number.id
				ORDER BY order_number.id DESC";
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		// массив типов
		global $_status_order;		
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg, 
				'status_order'=>$_status_order,
				'status_orders'=>isset($_POST['status_orders']) ? $_POST['status_orders'] : 3,
			),'orders/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	 */
	function newAction () {
	
		if ($_POST) {
			
			// записываем в базу
			forms::multy_update_form('order_number',$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			//  строим запрос
			$_sql="SELECT order_number.*, users.name, users.address, users.phone, users.email, users.city
					FROM order_number, users 
					where order_number.id_user=users.id and order_number.id=".$_POST['id'];		
			
			// выбираем данные о заказе
			$_sql0='SELECT orders.*, catalog.name, catalog.artikul
					FROM orders 
					Left Outer Join catalog ON orders.id_good=catalog.id
					where number_order='.$_POST['id'].'
					order by catalog.name';

		} else {

			// строим запрос
			$_sql="SELECT order_number.*, users.name, users.address, users.phone, users.email, users.city 
					FROM order_number, users 
					where order_number.id_user=users.id and order_number.id=".$_GET['id'];	
			
			// выбираем данные о заказе
			$_sql0='SELECT orders.*, catalog.name, catalog.artikul  
					FROM orders 
					Left Outer Join catalog ON orders.id_good=catalog.id
					where number_order='.$_GET['id'].'
					order by catalog.name';
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result0=mysql::query($_sql0,0);
			
		
		// массив типов
		global $_status_order;			
		
		return system::show_tpl(array('obj'=>$result, 'msg'=>$this->msg, 'select'=>$_status_order, 'result0'=>$result0),'orders/new.php');
	
	}
	
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {
		
		// удаляем из таблицы
		$_sql='DELETE FROM `order_number` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);

		// удаляем из таблицы
		$_sql='DELETE FROM `order` WHERE (`number_order`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * удаляем материал
	 */
	function deleteOrdersAction () {
		
		// удаляем из таблицы
		$_sql='DELETE FROM `orders` WHERE (`id`="'.$_GET['id_order'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::newAction();		
					
	}	
	
	
	
	/**
	 * отображаем статистику по покупателям
	 */
	function stat_usersAction () {

		// строим запрос
		$_sql="SELECT order_number.*, sum(orders.cost*orders.kolvo) as summa, count(*) as count
				FROM `orders`, `order_number`
				where orders.number_order=order_number.id and order_number.status=2
				group by order_number.id_user
				order by summa desc";
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		

		return system::show_tpl(array('result'=>$result,'msg'=>$this->msg),'orders/stat_users.php');
		
	}	
	

	
	/**
	 * отображаем статистику по товарам
	 */
	function stat_goodsAction () {

		// строим запрос
		$_sql="SELECT orders.id_good, count(*) as count, SUM(orders.kolvo*orders.cost) as summa, catalog.name as name_goods
				FROM `orders`, `order_number`, `catalog`
				where orders.number_order=order_number.id and order_number.status=2 and catalog.id=orders.id_good
				group by orders.id_good
				order by count desc";
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		

		return system::show_tpl(array('result'=>$result,'msg'=>$this->msg),'orders/stat_goods.php');
		
	}	
	
	/**
	 * распечатываем
	 */
	function printAction () {
	

		// строим запрос
		$_sql="SELECT order_number.*, users.name, users.address, users.phone, users.email, users.city 
				FROM order_number, users 
				where order_number.id_user=users.id and order_number.id=".$_GET['id'];	
			
		// выбираем данные о заказе
		$_sql0='SELECT orders.*, catalog.name, catalog.artikul  
				FROM orders 
				Left Outer Join catalog ON orders.id_good=catalog.id
				where number_order='.$_GET['id'].'
				order by catalog.name';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result0=mysql::query($_sql0,0);
			
		
		// массив типов
		global $_status_order;			
		
		return system::show_tpl(array('obj'=>$result, 'msg'=>$this->msg, 'select'=>$_status_order, 'result0'=>$result0),'orders/print.php');
	
	}	
	
	

} 

?>