<?php

class commentsController extends Controller {

	public $msg=false;
	public $tpl_folder='comments';
	public $tablename='catalog_comments';	

	function indexAction () {

		if(isset($_POST['status'])) { 
			$_SESSION['status'] = $_POST['status'];
			$_status = $_POST['status'];
		} else {
			$_status = 2;
		}

		$result=mysql::query('SELECT 
									catalog_comments.*,
									catalog.name AS goods_name
							  FROM '.$this->tablename.'
							  LEFT JOIN catalog ON catalog.id = catalog_comments.id_goods
							  WHERE catalog_comments.id > 0 '.general::get_status_for_filter($this->tablename).'
							  ORDER BY catalog_comments.created_at DESC');

		return system::show_tpl(
			array(
				'result'		=>	$result,
				'msg'			=>	$this->msg,
				'_status'		=>	$_status,
				'tpl_folder'	=>	$this->tpl_folder
			),$this->tpl_folder.'/index.php');
	}

	function addAction () {

		if ($_POST) {

			forms::check_box (array('status'));
			$post = $_POST['FORM'];
			mysql::query_only('INSERT INTO catalog_comments (status,created_at,name,email,text,id_goods) VALUES ("'.$post['status'].'","'.time().'","'.$post['name'].'","'.$post['email'].'","'.$post['text'].'","'.$post['id_goods'].'")');
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));

		}

		return system::show_tpl(
			array(
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');
	}	

	function newAction () {

		if ($_POST) {

			forms::check_box (array('status'));
			$post = $_POST['FORM'];
			mysql::query_only('UPDATE catalog_comments SET status = "'.$post['status'].'",updated_at = "'.time().'",name = "'.$post['name'].'",email = "'.$post['email'].'",text = "'.$post['text'].'" WHERE id = '.$_POST['id']);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {

			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];

		}

		$result = mysql::query_one($_sql,0);
		
		$good   = mysql::query_one('SELECT id,name FROM catalog WHERE id = '.$result->id_goods);

		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder,
				'good' => $good
			),$this->tpl_folder.'/new.php');
	}

	function deleteAction () {

		$result=mysql::just_query('DELETE FROM catalog_comments WHERE id = '.$_GET['id']);
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		return self::indexAction();		

	}	
	
} 

?>