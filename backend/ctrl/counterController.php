<?php

class counterController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='counter';
	public $tablename='counter';	
	
	/**
	 * edit content page site
	 */
	function editAction () {
		
		if ($_POST) {
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id=1';			
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result,
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/edit.php');
					
	}	
	
} 

?>