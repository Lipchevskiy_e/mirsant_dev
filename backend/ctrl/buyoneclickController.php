<?php

class buyoneclickController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {
	
		if ($_POST) {
			// даты
			if ($_POST['date_s']!='') $_POST['date_s']=general::date_to_database($_POST['date_s']);
			if ($_POST['date_po']!='') $_POST['date_po']=general::date_to_database($_POST['date_po']);
		}
		
		// строим запрос
		$_sql="SELECT buyoneclick.*, catalog.name, catalog.url
				FROM buyoneclick
				Left Outer Join catalog ON buyoneclick.id_product=catalog.id
				GROUP BY buyoneclick.id				
				ORDER BY buyoneclick.id DESC";
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);	
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg, 
			),'buyoneclick/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	 */
	function newAction () {
	
		global $_status_order;		
	
		if ($_POST) {
			
			// записываем в базу
			forms::multy_update_form('buyoneclick',$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			//  строим запрос
			$_sql="SELECT buyoneclick.*
					FROM buyoneclick
					where buyoneclick.id=".$_POST['id'];		
			
			// выбираем данные о заказе
			$_sql0='SELECT buyoneclick.*, catalog.name, catalog.url
					FROM buyoneclick 
					Left Outer Join catalog ON buyoneclick.id_product=catalog.id
					where buyoneclick.id='.$_POST['id'];
					
		} else {

			// строим запрос
			$_sql="SELECT order_number.*
					FROM order_number
					where order_number.id=".$_GET['id'];	
			
			// выбираем данные о заказе
			$_sql0='SELECT orders.*, catalog.name, catalog.artikul  
					FROM orders 
					Left Outer Join catalog ON orders.id_good=catalog.id
					where number_order='.$_GET['id'].'
					order by catalog.name';
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result0=mysql::query($_sql0,0);
		// массив типов
			
		
		return system::show_tpl(array('obj'=>$result, 'msg'=>$this->msg, 'select'=>$_status_order, 'result0'=>$result0,'user'=>$user),'buyoneclick/new.php');
	}
	
	function deleteAction () {

		// удаляем из таблицы
		$_sql='DELETE FROM `buyoneclick` WHERE (`id`="'.$_GET['id'].'")';
		$result=mysql::just_query($_sql,0);
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));
		
		return self::indexAction();

	}	
			
		
		
	
}
?>