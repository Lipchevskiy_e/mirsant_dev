<?php

class catalogController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='catalog';
	public $tablename='catalog';		
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {
		
		// если перешли по get ссылке из меню
		if(isset($_POST['name'])) { $_SESSION['name']=$_POST['name']; }
		if(isset($_POST['artikul'])) { $_SESSION['artikul']=$_POST['artikul']; }
		
		if ($_POST) {
			if(isset($_POST['block_new'])) { 
				$_SESSION['block_new']=$_POST['block_new']; 
			} else {
				unset($_SESSION['block_new']);
			}
			
			if(isset($_POST['block_lider'])) { 
				$_SESSION['block_lider']=$_POST['block_lider']; 
			} else {
				unset($_SESSION['block_lider']);
			}
		}
		
		if(isset($_POST['id_parent'])) { $_SESSION['id_parent']=$_POST['id_parent']; }
		if(isset($_POST['brand'])) { $_SESSION['brand']=$_POST['brand']; }
		if(isset($_POST['photo'])) { $_SESSION['photo']=$_POST['photo']; }
		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }
		
		// get limit 
		// всего в каталоге 
		$_total_catalog=dbh::catalog_get_total_count();
		$limit=pager::pager_limit($_total_catalog,ADMIN_CATALOG_AT_PAGE);				
		
		// строим запрос
		$_sql='SELECT * FROM '.$this->tablename.' where id>0 '.general::get_status_for_filter($this->tablename).' order by sort limit '.$limit;
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);

		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_total_catalog'=>$_total_catalog,
				'_status'=>isset($_SESSION['status']) ? $_SESSION['status'] : 2,
				'_brand'=>isset($_SESSION['brand']) ? $_SESSION['brand'] : 0,
				'_photo'=>isset($_SESSION['photo']) ? $_SESSION['photo'] : 2,
				'tpl_folder'=>$this->tpl_folder,
				'select'=>dbh::get_catolog_tree(), 
				'brand'=>dbh::get_brand(),
				'curent_id_parent'=>isset($_SESSION['id_parent']) ? $_SESSION['id_parent'] : 0,
				'types' => mysql::query('SELECT id, name FROM type_tovar ORDER BY sort ASC, id DESC'),
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	 */
	function newAction () {

		if ($_POST) {
			// проверяем на checkbox
			forms::check_box (array('status','block_new','block_lider'));
			$_sql='SELECT count(*) as count FROM '.$this->tablename.' where url="'.$_POST['FORM']['url'].'" AND id!='.$_POST['id'];		
			// выполняем запрос + при необходимости выводим сам запрос
			if (mysql::query_findpole($_sql,'count',0)>0) {
				$_POST['FORM']['url'].='_new';
			}
			$_POST['FORM']['serie'] = (int) $_POST['FORM']['serie'];
			if(!isset($_POST['FORM']['height']))
			{
				$_POST['FORM']['height'] = 0;
				$_POST['FORM']['width'] = 0;
				$_POST['FORM']['length'] = 0;
			} else {
				$_POST['FORM']['height'] = (int) $_POST['FORM']['height'];
				$_POST['FORM']['width'] = (int) $_POST['FORM']['width'];
				$_POST['FORM']['length'] = (int) $_POST['FORM']['length'];
			}
			$item = mysql::query_one('SELECT * FROM catalog WHERE id = '.$_POST['id']);

			if(!isset($_POST['FORM']['has_type']))
				$_POST['FORM']['has_type'] = 0;

			if(isset($_POST['FORM']['color']) || isset($_POST['FORM']['mounting_type']) || isset($_POST['FORM']['mixer_style']) || isset($_POST['FORM']['mixer_type'])) {
				$_POST['FORM']['color'] = (int) $_POST['FORM']['color'];
				$_POST['FORM']['mounting_type'] = (int) $_POST['FORM']['mounting_type'];
				$_POST['FORM']['mixer_style'] = (int) $_POST['FORM']['mixer_style'];
				$_POST['FORM']['mixer_type'] = (int) $_POST['FORM']['mixer_type'];
			}

			if(isset($_POST['FORM']['tray_depth']) || isset($_POST['FORM']['cabin_form'])) {
				$_POST['FORM']['tray_depth'] = (int) $_POST['FORM']['tray_depth'];
				$_POST['FORM']['cabin_form'] = (int) $_POST['FORM']['cabin_form'];
			}

			//die(print_r($_POST['FORM']));
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id']);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));

			if (isset($_SESSION['manager']) && $_SESSION['manager'] === true) {
	
				$newItem = mysql::query_one('SELECT * FROM catalog WHERE id = '.$_POST['id']);
				// парсим тело письма
				catalog::notice_change_price($item, $newItem);
			}

			
			// загружаем фото
			forms::multy_update_photo_catalog($_POST['id']);
			
			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			
			
			// проверяем есть ли фото у данного товара
			dbh::is_good_has_photo($_POST['id']);
			
			// ищем связанные товары
			$_sql_ass='SELECT catalog.id, catalog.name, catalog.cost 
					FROM `catalog_ass` 
					LEFT JOIN catalog ON catalog.id=catalog_ass.id_catalog_with 
					WHERE id_catalog_who='.$_POST['id'];
			$catalog_ass=mysql::query($_sql_ass,0);		

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];		

			// проверяем есть ли фото у данного товара
			dbh::is_good_has_photo($_GET['id']);

			// ищем связанные товары
			$_sql_ass='SELECT catalog.id, catalog.name, catalog.cost 
					FROM `catalog_ass` 
					LEFT JOIN catalog ON catalog.id=catalog_ass.id_catalog_with 
					WHERE id_catalog_who='.$_GET['id'];
			$catalog_ass=mysql::query($_sql_ass,0);		
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		$series = mysql::query("SELECT * FROM serie_brand WHERE id_brand=".$result->brand);
		$arr = array();
		foreach($series AS $s)
		{
			$arr[] = $s->id_serie;
		}
		$arr = implode(',', $arr);
		if($arr == '')
			$arr = '0';
		
		$series = mysql::query("SELECT * FROM serie WHERE id IN(".$arr.") ORDER BY sort, id DESC");
		
		$grupa = mysql::query_one("SELECT * FROM catalog_tree WHERE id=".$result->id_parent);
		// выбираем поставщиков
		$_sql='SELECT * FROM `supplier` where status=1 order by sort';
		$supplier=mysql::query($_sql,0);		
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg, 
				'select'=>dbh::get_catolog_tree(), 
				'brand'=>dbh::get_brand(),
				'tpl_folder'=>$this->tpl_folder,
				'sklad'=>$_sklad,
				'supplier'=>$supplier,
				'catalog_ass'=>$catalog_ass,
				'type_tovar'=>mysql::query("SELEct * FROM type_tovar WHERE status=1 ORDER BY sort, created_at DESC, id DESC"),
				'grupa'=>$grupa,
				'series'=>$series
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	/**
	 * удаляем фото новости
	 */
	function deletephotoAction () {

		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete_photo'));

		// имя файла на удаление
		$_name= array (
			$_GET['id'].'_'.$_GET['i'].'.jpg'
		);
		
		// удаляем все фото
		forms::delete_photo(HOST.IMG_CATALOG_PATH.'/03', $_name, 0);
		forms::delete_photo(HOST.IMG_CATALOG_PATH.'/02', $_name, 0);
		forms::delete_photo(HOST.IMG_CATALOG_PATH.'/01', $_name, 0);
		forms::delete_photo(HOST.IMG_CATALOG_PATH.'/original', $_name, 0);
		
		// выводим новость
		return self::newAction();
		
	}	
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {
		
		// удаляем из таблицы NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// удаляем все фото
		for($i=1; $i<=COUNT_PHOTO_CATALOG; $i++ ) {
			$_name= array (
				$_GET['id'].'_'.$i.'.jpg'
			);
		
			// удаляем все фото
			forms::delete_photo(HOST.IMG_CATALOG_PATH.'/03', $_name, 0);
			forms::delete_photo(HOST.IMG_CATALOG_PATH.'/02', $_name, 0);
			forms::delete_photo(HOST.IMG_CATALOG_PATH.'/01', $_name, 0);
			forms::delete_photo(HOST.IMG_CATALOG_PATH.'/original', $_name, 0);
		}
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * добавляем новый материал
	 */
	function addAction () {
	
		global $_sklad;
		
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status','block_new','block_lider'));
			$_sql='SELECT count(*) as count FROM '.$this->tablename.' where url="'.$_POST['FORM']['url'].'"';			
			// выполняем запрос + при необходимости выводим сам запрос
			if (mysql::query_findpole($_sql,'count',0)>0) {
				$_POST['FORM']['url'].='_new';
			}

			$_POST['FORM']['serie'] = (int) $_POST['FORM']['serie'];
			if(!isset($_POST['FORM']['height']))
			{
				$_POST['FORM']['height'] = 0;
				$_POST['FORM']['width'] = 0;
				$_POST['FORM']['length'] = 0;
			} else {
				$_POST['FORM']['height'] = (int) $_POST['FORM']['height'];
				$_POST['FORM']['width'] = (int) $_POST['FORM']['width'];
				$_POST['FORM']['length'] = (int) $_POST['FORM']['length'];
			}
            
            if(!isset($_POST['FORM']['has_type']))
                $_POST['FORM']['has_type'] = 0;
			
			// записываем в базу
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			// загружаем фото
			forms::multy_update_photo_catalog(mysql_insert_id());			
			
		}	
		
		// строим запрос
		$_sql='SELECT * FROM '.$this->tablename.' where id_parent=0 order by sort desc';
		// выполняем запрос + при необходимости выводим сам запрос
		$select=mysql::query($_sql,0);
		
		// выбираем поставщиков
		$_sql='SELECT * FROM `supplier` where status=1 order by sort';
		// выполняем запрос + при необходимости выводим сам запрос
		$supplier=mysql::query($_sql,0);			
	
		return system::show_tpl(
			array(
				'select'=>dbh::get_catolog_tree(), 
				'brand'=>dbh::get_brand(),
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder,
				'sklad'=>$_sklad,
				'supplier'=>$supplier
			),$this->tpl_folder.'/add.php');		

	
	}	
	
	
	/**
	 * мультиредактирование 
	 */
	function authorizeAction () {
		$data = array();
		if($_POST['st'] == '4') {
            $st = $_POST['ST'];
            if(isset($st['type']) && $st['type']) {
                $data['has_type'] = $st['type'];
            }
            if(isset($st['brand']) && $st['brand']) {
                $data['brand'] = $st['brand'];
            }
            if(isset($st['serie']) && $st['serie']) {
                $data['serie'] = $st['serie'];
            }
            if(isset($st['group']) && $st['group']) {
                $data['id_parent'] = $st['group'];
            }
			if(isset($st['tray_depth']) && $st['tray_depth']) {
				$data['tray_depth'] = $st['tray_depth'];
			}
			if(isset($st['cabin_form']) && $st['cabin_form']) {
				$data['cabin_form'] = $st['cabin_form'];
			}
			if(isset($st['color']) && $st['color']) {
				$data['color'] = $st['color'];
			}
			if(isset($st['mounting_type']) && $st['mounting_type']) {
				$data['mounting_type'] = $st['mounting_type'];
			}
			if(isset($st['mixer_style']) && $st['mixer_style']) {
				$data['mixer_style'] = $st['mixer_style'];
			}
			if(isset($st['mixer_type']) && $st['mixer_type']) {
				$data['mixer_type'] = $st['mixer_type'];
			}
			if(isset($st['toilet_mount']) && $st['toilet_mount']) {
				$data['toilet_mount'] = $st['toilet_mount'];
			}
			if(isset($st['bathtub_form']) && $st['bathtub_form']) {
				$data['bathtub_form'] = $st['bathtub_form'];
			}
			if(isset($st['gidr_van']) && $st['gidr_van']) {
				$data['gidr_van'] = $st['gidr_van'];
			}
			if(isset($st['tumb_van']) && $st['tumb_van']) {
				$data['tumb_van'] = $st['tumb_van'];
			}
			if(isset($st['penal_van']) && $st['penal_van']) {
				$data['penal_van'] = $st['penal_van'];
			}
			if(isset($st['bide_mont']) && $st['bide_mont']) {
				$data['bide_mont'] = $st['bide_mont'];
			}
			if(isset($st['kuh_mont']) && $st['kuh_mont']) {
				$data['kuh_mont'] = $st['kuh_mont'];
			}
			if(isset($st['kuh_count']) && $st['kuh_count']) {
				$data['kuh_count'] = $st['kuh_count'];
			}
		}
		//echo "<pre>".print_r($_POST['kt_where'] ,1)."</pre>";
        if(isset($_POST['kt_where']) AND is_array($_POST['kt_where']) AND count($_POST['kt_where'])) {
            foreach($_POST['kt_where'] as $key => $value) {
                //echo '<div style="width:200px; border: solid 0px red; color:#000000; background: yellow; padding: 2px; margin-left: 15px;"><b>'.$key.'</b> - '.$value.'</div>';

                if ($_POST['st']==1) {
                    // авторизовуем
                    $_sql="UPDATE `".$this->tablename."` SET `status`='1' WHERE (`id`='".$value."')";
                    $result=mysql::just_query($_sql,0);
                } elseif ($_POST['st']==2) {
                    // авторизовуем
                    $_sql="DELETE FROM `".$this->tablename."` WHERE (`id`='".$value."')";
                    $result=mysql::just_query($_sql,0);
                } elseif ($_POST['st']==3) {
                    // авторизовуем
                    $_sql="UPDATE `".$this->tablename."` SET `valuta`=".$_POST['sel_valuta']." WHERE (`id`='".$value."')";
                    $result=mysql::just_query($_sql,0);
                } elseif($_POST['st'] == 4) {
                    if($data) {
                        $sql = 'UPDATE `'.$this->tablename.'` SET ';
                        $d = array();
                        foreach ($data as $k => $v) {
                            $d[] = '`'.$k.'` = "'.$v.'"';
                        }
                        $sql .= implode(', ', $d).' WHERE `id` = "'.$value.'"';
                        mysql::just_query($sql);
                    }
                }

            }
        }
	  				
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_save'));		
		
		return self::indexAction();
		
		// редирект
		//header('Location: /backend/gb/index'); ??????????????
					
	}	
	
	
	/**
	 * импорт товаров
	 */
	function importAction () {
	
		if ($_POST) {
			
			// коннектим функцию
			require_once ADMIN_PATH.'/plagin/excel/reader.php';
			// ExcelFile($filename, $encoding);
			$data = new Spreadsheet_Excel_Reader();
			// Set output Encoding.
			$data->setOutputEncoding('UTF8');
			$data->setUTFEncoder('mb');
			$data->read($_FILES['file1']['tmp_name']);
			error_reporting(E_ALL ^ E_NOTICE);

			$_str=array();

			// С отключением товара			
			if ($_POST['type_import']==0) {
				// обнуляем склад
				$_sql="UPDATE `catalog` SET `sklad`='0' WHERE (`supplier`='".$_POST['supplier']."')";
				$result=mysql::just_query($_sql,0);
			}			
			
			for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
				for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
						$_str[]=$data->sheets[0]['cells'][$i][$j];
				}

				$_sql="SELECT * FROM `catalog` WHERE (`artikul`='".$_str[0]."')";
				$result_artikul=mysql::query_one($_sql,0);				
				
				if ($result_artikul) {
					// работаем с ценой
					if ($_str[2]=="") {
						$_sql="UPDATE `catalog` 
								SET `name`='".$_str[1]."', `sklad`='2'
								WHERE (`artikul`='".$_str[0]."')";
						$result=mysql::just_query($_sql,0);						
					} else {
						$_sql="UPDATE `catalog` 
								SET `name`='".$_str[1]."', `sklad`='1'
								WHERE (`artikul`='".$_str[0]."')";
						$result=mysql::just_query($_sql,0);					
					}
					
					// C обновленимем цены 
					if ($_POST['type_import1']==0) {
						$_sql="UPDATE `catalog` 
								SET `cost`='".$_str[3]."'
								WHERE (`artikul`='".$_str[0]."')";
						$result=mysql::just_query($_sql,0);	
					} 				
				} else {
					// товара нет на сайте
					if ($_str[2]=="") {
						$_sklad=2;
					} else {
						$_sklad=1;
					}
					$_sql="INSERT INTO `catalog` (`artikul`, `name`, `cost`, `id_parent`, `status`, `created_at`, `sklad`, `supplier`) 
							VALUES ('".$_str[0]."', '".$_str[1]."', '".$_str[3]."', '1', '1', '".date('Y-m-d')."', '".$_sklad."', '".$_POST['supplier']."')";
					$result=mysql::just_query($_sql,0);	
				}

				$_str="";

			}

		} 
		
		// строим запрос
		$_sql='SELECT * FROM `supplier` where status=1 order by sort';
		// выполняем запрос + при необходимости выводим сам запрос
		$select=mysql::query($_sql,0);		
		
		return system::show_tpl(
			array(
				'msg'=>$this->msg, 
				'tpl_folder'=>$this->tpl_folder,
				'select'=>$select
			),$this->tpl_folder.'/import.php');
	
	}
		
	
	
} 

?>