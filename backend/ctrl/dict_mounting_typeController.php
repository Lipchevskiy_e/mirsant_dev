<?php

    class dict_mounting_typeController extends Controller {

        public $msg=false;
        public $tpl=false;

        public $tpl_folder='dicts';
        public $tablename='dict_mounting_type';

        function indexAction () {
            if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }
            $_sql='SELECT * FROM '.$this->tablename.' WHERE id>0 '.general::get_status_for_filter($this->tablename).' ORDER BY sort, created_at DESC, id DESC';
            $result=mysql::query($_sql,0);
            return system::show_tpl(
                array(
                    'result'=>$result,
                    'msg'=>$this->msg,
                    '_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
                    'tpl_folder'=>$this->tablename,
                    'h1' => 'Управление типами монтажа',
                ),$this->tpl_folder.'/index.php');

        }

        function newAction () {
            if ($_POST) {
                forms::check_box (array('status'));
                $_POST['FORM']['updated_at'] = time();
                forms::multy_update_form($this->tablename,$_POST['id'],0);
                $this->msg=general::messages(1,v::getI18n('backend_after_save'));
                $_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];
            } else {
                $_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];
            }
            $result=mysql::query_one($_sql,0);
            return system::show_tpl(
                array(
                    'obj'=>$result,
                    'msg'=>$this->msg,
                    'tpl_folder'=>$this->tablename,
                    'h1' => 'Редактирование типа монтажа &nbsp; '.$result->name,
                ),$this->tpl_folder.'/new.php');
        }

        function deleteAction () {
            $_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';
            mysql::just_query($_sql,0);
            $this->msg=general::messages(1,v::getI18n('backend_after_delete'));
            return self::indexAction();
        }

        function addAction () {
            if ($_POST) {
                forms::check_box (array('status'));
                $_POST['FORM']['created_at'] = time();
                forms::multy_insert_form($this->tablename);
                $this->msg=general::messages(1,v::getI18n('backend_after_save'));
            }
            return system::show_tpl(
                array(
                    'msg'=>$this->msg,
                    'tpl_folder'=>$this->tablename,
                    'h1' => 'Добавление нового типа монтажа',
                ),$this->tpl_folder.'/add.php');
        }

    }