<?php

class price_exportController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='price_export';
	public $tablename='catalog';		
	
	private $priceUaFileName = 'price_ua.xml';

	private $hotlineFileName = 'hotline.xml';

	private $folder = '/files/price/';

	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {
		// print_r($_SERVER);
		
		$priceUa = array('file_name' => $this->priceUaFileName);
		$hotline = array('file_name' => $this->hotlineFileName);
		
		return system::show_tpl(
			array(
				'priceUa' => $priceUa,
				'hotline' => $hotline,
				'folder' => $this->folder,
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder,
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	function priceUaGenerateAction () {
		ini_set('display_errors', 0);
		ini_set('max_execution_time', 3600);

		// Load data from DB
		$groups = $this->getCategory();
		$products = $this->getProducts();

		 /* create a dom document with encoding utf8 */
	    $dom = new \DOMDocument('1.0', 'UTF-8');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		$priceTag = $dom->createElement('price');
		$priceAttr = $dom->createAttribute('date');
		$priceAttr->value = date('Y-m-d H:i:s');
		$priceTag->appendChild($priceAttr);

		$priceTag->appendChild($dom->createElement('name', 'Интернет-магазин'));

		// Currency ?
		
		// Catalog
		$catalog = $dom->createElement('catalog'); 

		foreach ($groups as $category) {
			$cat = $dom->createElement('category', $category->pole);
			$attrId = $dom->createAttribute('id');
			$attrId->value= $category->id;
			$cat->appendChild($attrId);

			if ($category->id_parent != 0) {
				$attrParId = $dom->createAttribute('parentID');
				$attrParId->value = $category->id_parent;
				$cat->appendChild($attrParId);
			}
			$catalog->appendChild($cat);
		}
		$priceTag->appendChild($catalog);


		// Items
		$items = $dom->appendChild($dom->createElement('items'));

		foreach ($products as $key => $product) {
			$item = $dom->createElement('item');

			// Id attribute
			$attrId = $dom->createAttribute('id');
			$attrId->value = $product->id;
			$item->appendChild($attrId);

			// Name
			$name = $dom->createElement('name', $product->name);
			$item->appendChild($name);

			// CategoryId
			$categoryId = $dom->createElement('categoryId', $product->id_parent);
			$item->appendChild($categoryId);

			// Price
			if ($product->valuta == 2) {
				$price = $product->cost * EVRO;
			} elseif ($product->valuta==3) {
				$price = $product->cost*DOLLAR;
			} else {
				$price = $product->cost;
			}
			$priceEl = $dom->createElement('price', $price);
			$item->appendChild($priceEl);

			// Url
			$url = $dom->createElement('url', 'http://'.$_SERVER['HTTP_HOST'].'/catalog/goods/'.$product->url);
			$item->appendChild($url);

			// Image
			if (is_file(HOST.'/images/_catalog/03/'.$product->id.'_1.jpg')) {
				$img = '/images/_catalog/03/'.$product->id.'_1.jpg';
			} else {
				$img = '/pic/no_photo_big2.jpg';
			}
			$image = $dom->createElement('image', 'http://'.$_SERVER['HTTP_HOST'].$img);
			$item->appendChild($image);

			// Vendor
			$vendor = $dom->createElement('vendor', $product->brand_pole);
			$item->appendChild($vendor);

			// Description
			// $description = $dom->createElement('description', $product->);
			// $item->appendChild($description);
			
			// Warranty ?

			$items->appendChild($item);
		}

		$priceTag->appendChild($items);

		$dom->appendChild($priceTag);

		// Save
		$dom->save(HOST.$this->folder.$this->priceUaFileName);
		header('Location: /backend/price_export/index');
		return false;
	}


	private function getCategory() {
		$result = mysql::query('SELECT * FROM catalog_tree WHERE status = 1');
		return $result;
	}

	private function getProducts() {
		$result = mysql::query('SELECT catalog.*, brand.pole as brand_pole 
			FROM catalog 
			LEFT JOIN brand 
			ON catalog.brand = brand.id
			WHERE catalog.status = 1');
		return $result;
	}
	
	function getFileAction() {
		$fileName = $_GET['regenerator'] == 'price_ua' ? $this->priceUaFileName : 'hotline.xml';
		$file 	  = $_GET['regenerator'] == 'price_ua' ? HOST.$this->folder.$fileName : HOST.'/xml/'.$fileName;
		if (file_exists($file)) {
			// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
			// если этого не сделать файл будет читаться в память полностью!
			if (ob_get_level()) {
				ob_end_clean();
			}
			// заставляем браузер показать окно сохранения файла
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . basename($file));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			// читаем файл и отправляем его пользователю
			readfile($file);
			exit;
		}
	}
	
	function hotlineGenerateAction() {
		global $bathtubForm,
				$colors,
				$cabinForms;
				
		/*ini_set('display_errors', 1);
		ini_set('max_execution_time', 3600);
		ini_set('memory_limit', '256M');*/

				// Format
		$bathtubObj = mysql::query('SELECT id, name FROM dict_bathtub_form');
		$bathtubForm = array();
		foreach($bathtubObj as $obj) {
			$bathtubForm[$obj->id] = $obj->name;
		}
		// Color
		$colorsObj = mysql::query('SELECT id, name FROM dict_colors');
		$colors = array();
		foreach($colorsObj as $obj) {
			$colors[$obj->id] = $obj->name;
		}
		// Cabin forms
		$cabinFormsObj = mysql::query('SELECT id, name FROM dict_cabin_form');
		$cabinForms = array();
		foreach($cabinFormsObj as $obj) {
			$cabinForms[$obj->id] = $obj->name;
		}

		function clearString($data) {
			$data = str_replace('&nbsp;',' ',strip_tags($data));
			$data = str_replace(array('&laquo;','&raquo;','&ldquo;','&rdquo;'),'&quot;',$data);
			$data = str_replace(array('&ndash;','&mdash;','&deg;','&hellip;','&not;'),'',$data);
			$data = str_replace(array('&'),'&amp;',$data);

			return $data;
		}

		function parsePattern($product, $pattern) {
			global $bathtubForm,
				$colors,
				$cabinForms;
			$assocRelation = array(
				'[item_name]' => clearString($product->name),
				'[item_size:XxY]' => ($product->length ? $product->length : $product->height).'x'.$product->width,
				'[item_size:XxYxZ]' => ($product->length ? $product->length : $product->height).'x'.$product->width.'x'.$product->height,
				'[execution]' => '',
				'[format]' => ($product->has_bathtub && array_key_exists($product->bathtub_form, $bathtubForm)
					? $bathtubForm[$product->bathtub_form]
					: ($product->cabin_form && array_key_exists($product->cabin_form, $cabinForms)? $cabinForms[$product->cabin_form] : '')),
				'[model_number]' => $product->artikul,
				'[item_color]' => (array_key_exists($product->color, $colors) ? $colors[$product->color] : ''),
				'[item_type]' => '',
				'[item_view]' => '',
				'[item_assignment]' => '',
				'[item_surface]' => '',
				'[description]' => clearString($product->text_full),
			);

			$result = str_replace(array_keys($assocRelation), array_values($assocRelation), $pattern);
			return $result;
		}

		// Items structure by category
		/*
		 * Fields:
		 * [item_name] - название товара
		 * [item_size:XxY] - размеры изделия ШИРИНАxДЛИНА
		 * [item_size:XxYxZ] - размеры изделия ШИРИНАxДЛИНАхВЫСОТА
		 * [execution] - исполнение, левое или правое (L, R)
		 * [format] - форма (душевой кабины или душевого поддона)
		 * [model_number] - Код модели (артикул модели)
		 * [item_color] - Цвет (в формате предусмотренном производителем)
		 * [item_type] - Тип товара
		 * [item_view] - Вид
		 * [item_assignment] - Назначение
		 * [item_surface] - Поверхность
		 * [description] - Описание товара
		 * */
		$catStucture = array(
		//    Ванны
			18 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
			20 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
			22 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
			31 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
		//    Гидромассажные ванны
			25 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
		//    Душевые боксы
			14 => array('name' => '[item_name] [item_size:XxY] [format]', 'code' => '[model_number]'),
		//    Душевые кабины
			30 => array('name' => '[item_name] [item_size:XxY] [format]', 'code' => '[model_number]'),
		//    Смесители
			17 => array('name' => '[item_name]', 'code' => '[model_number]'),
		//    Мебель для ванной
			13 => array('name' => '[item_name] [item_color] [item_type]', 'code' => '[model_number]'),
		//    Унитазы
			16 => array('name' => '[item_name]', 'code' => '[model_number]'),
		//    Умывальники
			15 => array('name' => '[item_name]', 'code' => '[model_number]'),
		//    Инсталляции
			26 => array('name' => '[item_name]', 'code' => '[model_number]'),
		//    Шторки на ванну
			27 => array('name' => '[item_name]', 'code' => '[model_number]'),
		//    Гидромассажные панели
			28 => array('name' => '[item_name] [item_size:XxY] [execution]', 'code' => '[model_number]'),
		//    Плитка
			29 => array('name' => '[item_name] [item_type] [item_size:XxY] [execution]', 'code' => '[model_number]',
				'params' => array(
					'Вид' => '[item_view]',
					'Назначение' => '[item_assignment]',
					'Поверхность' => '[item_surface]',
					'Размеры' => '[item_size:XxYxZ]',
				),
				'description' => '[description] [item_color]',
			),
		);

		// строим запрос
		$_sql="select * FROM config WHERE id = 1009";
		// выполняем запрос + при необходимости выводим сам запрос
		$firmId=mysql::query_one($_sql,0);

		$firmId = $firmId->zna;

		$products = mysql::query("SELECT catalog.*, brand.pole as brand_name FROM catalog LEFT JOIN brand ON brand.id=catalog.brand WHERE catalog.status = 1",0);
		$categories = mysql::query("SELECT * FROM catalog_tree WHERE status = 1",0);

		// Categories
		$str  = '<?xml version="1.0" encoding="UTF-8"?>';
		$str .= '<price>';
		$str .= '<date>'.date('Y-m-d H:i').'</date>';
		$str .= '<firmName>http://mirsant.com.ua/</firmName>';
		$str .= '<firmId>'.$firmId.'</firmId>';
		$str .= '<categories>';
		foreach($categories as $category) {
			$str .= '<category><id>'.$category->id.'</id><name>'.$category->pole.'</name></category>';
		}
		$str .= '</categories>';

		// Items
		$str .= '<items>';

		foreach($products as $product) {
			$str .= '<item>';
			$str .= '<id>'.$product->id.'</id>';
			$str .= '<categoryId>'.$product->id_parent.'</categoryId>';
			
			$str .= '<vendor>'.clearString($product->brand_name).'</vendor>';
			$product_name =  clearString($product->name);
			$description = clearString($product->text_full);

			$avi = 'В наличии';

			if ($product->valuta==2)
				$cost=$product->cost*EVRO;
			elseif ($product->valuta==3)
				$cost=$product->cost*DOLLAR;
			else $cost=$product->cost;

		//    Regenerate fields by category templates
			if (array_key_exists($product->id_parent, $catStucture)) {
				$tags = $catStucture[$product->id_parent];

				foreach($tags as $tag => $pattern) {
					switch ($tag) {
						case "name":
							$product_name = parsePattern($product, $pattern);
							break;
						case "description":
							$description = parsePattern($product, $pattern);
							break;
						case "params":
							foreach ((array)$pattern as $paramName => $paramPattern) {
								$paramValue = parsePattern($product, $paramPattern);
								if ($paramValue != '') {
									$str .= '<param name="'.$paramName.'">'.parsePattern($product, $paramPattern).'</param>';
								}
							}
							break;
						case "code":
							$str .= '<code>'.parsePattern($product, $pattern).'</code>';
							break;
					}
				}
			}
			$str .= '<name>'.htmlspecialchars($product_name).'</name>';
			$str .= '<description>'.htmlspecialchars($description).'</description>';
			$str .= '<url>'.'http://'.$_SERVER['HTTP_HOST'].'/catalog/goods/'.$product->url.'</url>';
			$str .= '<image>'.'http://'.$_SERVER['HTTP_HOST'].IMG_CATALOG_PATH.'/original/'.$product->id.'_1.jpg</image>';
			$str .= '<priceRUAH>'.ceil($cost).'</priceRUAH>';
			$str .= '<stock>'.$avi.'</stock>';
			$str .= '<guarantee>От производителя</guarantee>';
			$str .= '</item>';
		}
		
		$str .= '</items>';
		$str .= '</price>';
		
		$fp = fopen(HOST.'/xml/hotline.xml','w+');
		fwrite($fp, $str);
		fclose($fp);
		
		header('Location: /backend/price_export');
	}
} 

?>