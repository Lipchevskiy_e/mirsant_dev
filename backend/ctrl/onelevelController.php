<?php

class onelevelController extends Controller {

	public $msg=false;
	public $tpl=false;
	public $table_name=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='onelevel';
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {

		$this->table_name=$_GET['tablename'];
		
		// строим запрос
		$_sql='SELECT * FROM '.$this->table_name.' where id>0 '.general::get_status_for_filter($_GET['tablename']).' order by sort';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'tpl_folder'=>$this->tpl_folder				
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	 */
	function newAction () {
	
		$this->table_name=$_GET['tablename'];
		
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_update_form($this->table_name,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			//  строим запрос
			$_sql='SELECT * FROM '.$this->table_name.' where id='.$_POST['id'];			

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->table_name.' where id='.$_GET['id'];			
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {
		
		$this->table_name=$_GET['tablename'];
		
		// удаляем из таблицы
		$_sql='DELETE FROM '.$this->table_name.' WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * добавляем новый материал
	 */
	function addAction () {
		
		$this->table_name=$_GET['tablename'];
		
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_insert_form($this->table_name,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
		}	
		
		return system::show_tpl(
			array(
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');		

	
	}		
	

} 

?>