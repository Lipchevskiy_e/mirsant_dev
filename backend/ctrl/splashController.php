<?php

class splashController extends Controller {

	public $msg=false;
	public $tpl=false;

	public $_width_b=670;
	public $_height_b=536;	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='splash';
	public $tablename='splash';	
	
	/**
	 * отображаем весь список материалов
	*/
	function indexAction () {
	
		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }

		// строим запрос
		$_sql='SELECT * FROM `'.$this->tablename.'` where id>0 '.general::get_status_for_filter($this->tablename).' order by sort';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	*/
	function newAction () {
	
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			// загружаем фото
			forms::multy_update_photo(
				HOST.IMG_SPLASH_PATH,
				1,
				$_POST['id'],
				177,
				128,
				532,
				381,
                0
			);
			
			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];			
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	/**
	 * удаляем фото новости
	 */
	function deletephotoAction () {

		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete_photo'));

		// имя файла на удаление
		$_name= array (
			"_".$_GET['id'].'.jpg',
			$_GET['id'].'.jpg'
		);
		
		// удаляем
		forms::delete_photo(HOST.IMG_SPLASH_PATH, $_name, 0);
		
		// выводим новость
		return self::newAction();
		
	}	
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {

		// удаляем фото новости
		self::deletephotoAction();
	
		// удаляем из таблицы NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * добавляем новый материал
	 */
	function addAction () {
		
	
		
			if ($_POST) {
			 
			$_sql_files='SELECT * FROM splash WHERE status=0 order by id';
			$_files=mysql::query($_sql_files,0);
            
			foreach ($_files as $_files) {

				// поля
				if (isset($_POST['FORM1']['status_'.$_files->id])) {
					$_POST['FORM']['status']=1;
				} else {
					$_POST['FORM']['status']=0;
				}

                $_POST['FORM']['url']=$_POST['FORM1']['url_'.$_files->id];
				$_POST['FORM']['name']=$_POST['FORM1']['name_'.$_files->id];

				// записываем в базу
				forms::multy_update_form($this->tablename,$_files->id,0);

			}

			
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
		
		}	
		
		$_sql_files='SELECT * FROM splash where status=0 order by sort';
        $_files=mysql::query($_sql_files,0);

		return system::show_tpl(
			array(
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder,
				'_files'=>$_files
			),$this->tpl_folder.'/add.php');
	
	}
	
} 

?>