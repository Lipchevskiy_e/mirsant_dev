<?php

class indexController extends Controller {

	function indexAction () {
		$and = '';
		if ($_SERVER['PHP_AUTH_USER'] == 'manager') {
			$and  = ' and manager = 1';
		}
		// товары
		$this->catalog_status=mysql::query_findpole('select status from menu where id=59'.$and,'status');
		$this->catalog_inctive=dbh::get_count_status_off('catalog');
		
		// заказы
		$this->order_inctive=dbh::get_count_status_off('order_number');	
		
		// отзывы 
		$this->gb_status=mysql::query_findpole('select status from menu where id=8'.$and,'status');
		$this->gb_inctive=dbh::get_count_status_off('guestbook');
		
		// новости 
		$this->news_status=mysql::query_findpole('select status from menu where id=1'.$and,'status');
		$this->news_inctive=dbh::get_count_status_off('news');
		
		// комментарии к новостям 
		$this->news_gb_status=mysql::query_findpole('select status from menu where id=232'.$and,'status');		
		$this->news_gb_inctive=dbh::get_count_status_off('news_gb');

		// пользователи
		$this->users_status=mysql::query_findpole('select status from menu where id=26'.$and,'status');
		$this->users_inctive=dbh::get_count_status_off('users');
		
		
		// контентовые разделы
		$this->content_status=mysql::query_findpole('select status from menu where id=89'.$and,'status');
		
		// галерея
		$this->gallery_status=mysql::query_findpole('select status from menu where id=3'.$and,'status');
		
		// голосование
		$this->voting_status=mysql::query_findpole('select status from menu where id=102'.$and,'status');
		
		// настройки		
		$this->config_status=mysql::query_findpole('select status from menu where id=80'.$and,'status');
		
		// CEO 
		$this->ceo_status=mysql::query_findpole('select status from menu where id=11'.$and,'status');
		
		return system::show_tpl((array)$this,'mainmenu/mainpage.php');
		
	}
	
	function exitAction () {
		unset($_SERVER['PHP_AUTH_USER']);
	 	header('HTTP/1.1 401 Unauthorized');
		die ("<meta http-equiv='refresh' content='0;URL=/'>");
		
	}
	
	function accessAction () {
		
		//die ("<meta http-equiv='refresh' content='0;URL=/backend/index'>");
		
	}	
	

} 

?>