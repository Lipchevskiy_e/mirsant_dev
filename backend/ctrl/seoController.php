<?php

class seoController extends Controller {

	public $msg=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='seo';
	public $tablename='ceo';	
		
	/**
	 * выводим материал
	 */
	function indexAction () {
		
		$_sql='select * from '.$this->tablename.' where status=1 order by sort';
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}

	/**
	 * редактируем материал
	 */
	function editAction () {
		
		if ($_POST) {
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {
			
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];			
			
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result,
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/edit.php');
						
	}	
	

} 

?>