<?php

class videoController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// ������ / ������ ����
	public $_width=0;
	public $_height=0;
	public $_width_b=0;
	public $_height_b=0;	
	
	// ��� �������� ������� + ��� ����� � ���������
	public $tpl_folder='video';
	public $tablename='video';	
	
	/**
	 * ���������� ���� ������ ����������
	 */
	function indexAction () {

		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }
		
		// ������ ������
		$_sql='SELECT * FROM '.$this->tablename.' where id>0 '.general::get_status_for_filter($this->tablename).' order by sort, created_at desc';
		
		// ��������� ������ + ��� ������������� ������� ��� ������
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * ����������� ��������� ��������
	 */
	function newAction () {
	
		if ($_POST) {
		  
			$this->_width_b=155;
			$this->_height_b=155;
			
			// ��������� �� checkbox
			forms::check_box (array('status'));
			
			// ���������� � ����
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			// ��������� ����
			forms::multy_update_photo_video(
				HOST.IMG_VIDEO_PATH,
				$_POST['id'],
				$this->_width_b,
				$this->_height_b
			);
			
			//  ������ ������
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {

			// ������ ������
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];			
		
		}
		
		// ��������� ������ + ��� ������������� ������� ��� ������
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg, 
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	/**
	 * ������� ���� �������
	 */
	function deletephotoAction () {

		// ���������
		$this->msg=general::messages(1,v::getI18n('backend_after_delete_photo'));

		// ��� ����� �� ��������
		$_name= array (
			"_".$_GET['id'].'.jpg',
			$_GET['id'].'.jpg'
		);
		
		// �������
		forms::delete_photo(HOST.IMG_VIDEO_PATH, $_name, 0);
		
		// ������� �������
		return self::newAction();
		
	}	
	
	
	/**
	 * ������� ��������
	 */
	function deleteAction () {
		
		// ������� �� ������� NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// ������� ���� �������
		self::deletephotoAction();
		
		// ���������
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * ��������� ����� ��������
	 */
	function addAction () {
		
		if ($_POST) {

			$this->_width_b=155;
			$this->_height_b=155;
			
			// ��������� �� checkbox
			forms::check_box (array('status'));
			
			// ���������� � ����
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			// ��������� ����
			forms::multy_update_photo_video(
				HOST.IMG_VIDEO_PATH,
				mysql_insert_id(),
				$this->_width_b,
				$this->_height_b
			);
		
		}	
		
	
		return system::show_tpl(
			array(
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');		

	
	}	
	
} 

?>