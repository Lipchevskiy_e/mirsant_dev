<?php

class admController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='adm';
	public $tablename='menu';
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {

		// только для СуперАдмина
		if (!general::sadmin()) { return general::messages(0,v::getI18n('backend_orror_access')); }

		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box(dbh::menu_get_id());

			// записываем в базу
			forms::multy_update_form_all_records($this->tablename,'status',0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));

		} 			
		
		// строим запрос
		$_sql='SELECT * FROM '.$this->tablename.' where id_parent=0 order by zindex';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(array('result'=>$result,'msg'=>$this->msg),$this->tpl_folder.'/index.php');
		
	}
	
	
} 

?>