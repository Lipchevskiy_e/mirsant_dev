<?php

class brandController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// ширина / высота фото
	public $_width=0;
	public $_height=0;
	public $_width_b=0;
	public $_height_b=0;	
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='brand';
	public $tablename='brand';	
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {

		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }
	
		// строим запрос
		$_sql='SELECT
					brand.*,
					count(catalog.id) as count
				FROM
					brand
				Left Outer Join catalog ON brand.id=catalog.brand
				WHERE brand.id>0 '.general::get_status_for_filter('brand').'
				GROUP BY
					brand.id
				order by brand.sort';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	 */
	function newAction () {
	
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id'],0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			
			mysql::just_query("DELETE FROM serie_brand WHERE (id_brand='".$_POST['id']."')");
			if(isset($_POST['SERIES']) AND count($_POST['SERIES']) > 0)
			{
				foreach($_POST['SERIES'] AS $s)
				{
					if($s != 0 AND $s != NULL)
						mysql::just_query("INSERT INTO serie_brand (id_brand,id_serie) VALUES ('".$_POST['id']."','".$s."')");
				}
			}
			
			// загружаем фото
			forms::multy_update_photo_brand(
				HOST.IMG_BRAND_PATH,
				$_POST['id']
			);
			$this_series = mysql::query("SELECT * FROM serie_brand WHERE id_brand=".$_POST['id']);
			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {
			$this_series = mysql::query("SELECT * FROM serie_brand WHERE id_brand=".$_GET['id']);
			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];			
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder,
				'series'=>mysql::query("SELECT * FROM serie WHERE status=1 ORDER BY sort, id DESC"),
				'this_series'=>$this_series
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	/**
	 * удаляем фото новости
	 */
	function deletephotoAction () {

		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete_photo'));

		// имя файла на удаление
		$_name= array (
			$_GET['id'].'.jpg'
		);
		
		// удаляем
		forms::delete_photo(HOST.IMG_BRAND_PATH, $_name, 0);
		
		// выводим новость
		return self::newAction();
		
	}	
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {
		
		// удаляем из таблицы NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// удаляем фото новости
		self::deletephotoAction();
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * добавляем новый материал
	 */
	function addAction () {
		
		if ($_POST) {
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			
			// записываем в базу
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			$id = mysql_insert_id();
			if(isset($_POST['SERIES']) AND count($_POST['SERIES']) > 0)
			{
				foreach($_POST['SERIES'] AS $s)
				{
					mysql::just_query("INSERT INTO serie_brand (id_brand,id_serie) VALUES ('".$id."','".$s."')");
				}
			}
			
			// загружаем фото
			forms::multy_update_photo_brand(
				HOST.IMG_BRAND_PATH,
				$id
			);
			
			header('Location: /backend/'.$this->tpl_folder.'/index');
		
		}	
		
	
		return system::show_tpl(
			array(
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder,
				'series'=>mysql::query("SELECT * FROM serie WHERE status=1 ORDER BY sort, id DESC")
			),$this->tpl_folder.'/add.php');		

	
	}	
	
} 

?>