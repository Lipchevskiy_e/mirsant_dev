<?php

class seo_templatesController extends Controller {

    public $tpl_folder = 'seo_templates';
    public $tablename = 'seo_templates';    

    function indexAction () {
        $result = mysql::query('select * from '.$this->tablename.' order by id desc');
        return system::show_tpl(
            array(
                'result'=>$result,
                'tpl_folder'=>$this->tpl_folder
            ),$this->tpl_folder.'/index.php');
    }

    function newAction () {
        if ($_POST) {
            forms::multy_update_form($this->tablename, $_POST['id']);
            $_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];
        } else {
            $_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];
        }
        $result=mysql::query_one($_sql);
        return system::show_tpl(
            array(
                'obj'=>$result,
                'tpl_folder'=>$this->tpl_folder
            ),$this->tpl_folder.'/new.php');     
    }
    
}