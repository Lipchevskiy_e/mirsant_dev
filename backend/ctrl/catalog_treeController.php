<?php

class catalog_treeController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// ширина / высота фото
	public $_width=0;
	public $_height=0;
	public $_width_b=0;
	public $_height_b=0;	
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='catalog_tree';
	public $tablename='catalog_tree';	
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {
		
		if(isset($_POST['name_group'])) { $_SESSION['name_group']=$_POST['name_group']; }
		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }
		
		// строим запрос
		$_sql='SELECT * FROM '.$this->tablename.' '.general::get_status_for_filter($this->tablename).' order by sort';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	 */
	function newAction () {
	
		if ($_POST) {
			
			$this->_width=mysql::query_findpole('select zna from config where id=27','zna');
			$this->_height=mysql::query_findpole('select zna from config where id=28','zna');
			$this->_width_b=mysql::query_findpole('select zna from config where id=27','zna');
			$this->_height_b=mysql::query_findpole('select zna from config where id=28','zna');
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			$_sql='SELECT count(*) as count FROM '.$this->tablename.' where id!='.$_POST['id'].' and url="'.$_POST['FORM']['url'].'"';			
			// выполняем запрос + при необходимости выводим сам запрос
			if (mysql::query_findpole($_sql,'count',0)>0) {
				$_POST['FORM']['url'].='_new';
			}

			$_POST['FORM']['id_parent'] = (int) $_POST['FORM']['id_parent'];
			$_POST['FORM']['skidka'] = (int) $_POST['FORM']['skidka'];

			// записываем в базу
			forms::multy_update_form($this->tablename,$_POST['id']);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			// загружаем фото
			forms::multy_update_photo_catalog_grupa(
				HOST.IMG_CATALOG_TREE_PATH,
				1,
				$_POST['id'],
				$this->_width,
				$this->_height,
				$this->_width_b,
				$this->_height_b
			);
			
			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];			

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];			
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg, 
				'select'=>dbh::get_catolog_tree(),
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	/**
	 * удаляем фото новости
	 */
	function deletephotoAction () {

		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete_photo'));

		// имя файла на удаление
		$_name= array (
			"_".$_GET['id'].'.jpg',
			$_GET['id'].'.jpg'
		);
		
		// удаляем
		forms::delete_photo(HOST.IMG_CATALOG_TREE_PATH, $_name, 0);
		
		// выводим новость
		return self::newAction();
		
	}	
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {
		
		// удаляем из таблицы NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// удаляем фото новости
		self::deletephotoAction();
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * добавляем новый материал
	 */
	function addAction () {
		
		if ($_POST) {
			
			$this->_width=mysql::query_findpole('select zna from config where id=27','zna');
			$this->_height=mysql::query_findpole('select zna from config where id=28','zna');
			$this->_width_b=mysql::query_findpole('select zna from config where id=27','zna');
			$this->_height_b=mysql::query_findpole('select zna from config where id=28','zna');
			
			// проверяем на checkbox
			forms::check_box (array('status'));
			$_POST['FORM']['skidka'] = (int) $_POST['FORM']['skidka'];
			$_sql='SELECT count(*) as count FROM '.$this->tablename.' where url="'.$_POST['FORM']['url'].'"';			
			// выполняем запрос + при необходимости выводим сам запрос
			if (mysql::query_findpole($_sql,'count',0)>0) {
				$_POST['FORM']['url'].='_new';
			}
			// записываем в базу
			forms::multy_insert_form($this->tablename,0);
			$this->msg=general::messages(1,v::getI18n('backend_after_save'));
			
			// загружаем фото
			forms::multy_update_photo_catalog_grupa(
				HOST.IMG_CATALOG_TREE_PATH,
				1,
				mysql_insert_id(),
				$this->_width,
				$this->_height,
				$this->_width_b,
				$this->_height_b
			);
		
		}	
		
	
		return system::show_tpl(
			array(
				'select'=>dbh::get_catolog_tree(),
				'msg'=>$this->msg,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');		

	
	}	
	
} 

?>