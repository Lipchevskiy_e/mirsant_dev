<?php

class reklamaController extends Controller {

	public $msg=false;
	public $tpl=false;
	
	// имя основной таблицы + имя папки с шаблонами
	public $tpl_folder='reklama';
	public $tablename='reklama';	
	
	/**
	 * отображаем весь список материалов
	 */
	function indexAction () {
	
		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }
		if(isset($_POST['category'])) { $_SESSION['category']=$_POST['category']; }

		// строим запрос
		$_sql='SELECT * FROM `'.$this->tablename.'` where id>0 '.general::get_status_for_filter($this->tablename).' order by sort';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
		
		// массив типов
		global $_type_banner;	
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'type_banner'=>$_type_banner,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'_category'=>isset($_POST['category']) ? $_POST['category'] : 100,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/index.php');
		
	}
	
	
	/**
	 * редактируем выбранный материал
	 */
	function newAction () {
	
		if ($_POST) {
		
		    if (!empty($_FILES['file1']['tmp_name'])) {
			
				// проверяем на тип банера
				$size = getimagesize($_FILES['file1']['tmp_name']);
							
				// проверяем на тип (jpeg/gif/png/swf) баннера
				if (banners::check_type_banner($size['mime'])==0 or banners::check_type_banner($size['mime'])==1) {

					// проверяем на checkbox
					forms::check_box (array('status'));

					// определяем имя файла
					$_filename=$_POST['id'].'.'.end(explode(".", $_FILES['file1']['name']));	
					
					// имя картинки
					$_POST['FORM']['image']=$_filename;
					
					// записываем в базу
					forms::multy_update_form($this->tablename,$_POST['id'],0);
		
					// загружаем баннер
					move_uploaded_file($_FILES['file1']['tmp_name'], HOST.IMG_REKLAMA_PATH.'/'.$_filename);
					
					// сообщение
					$this->msg=general::messages(1,v::getI18n('backend_after_save'));
					
				} else {
				
					// сообщение error
					$this->msg=general::messages(0,v::getI18n('backend_upload_reklama_error_type'));
				
				}
			
			
			} else {
			
					// проверяем на checkbox
					forms::check_box (array('status'));
			
					// записываем в базу
					forms::multy_update_form($this->tablename,$_POST['id'],0);
			
					// сообщение
					$this->msg=general::messages(1,v::getI18n('backend_after_save'));			

			}
			

			//  строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_POST['id'];				

		} else {

			// строим запрос
			$_sql='SELECT * FROM '.$this->tablename.' where id='.$_GET['id'];			
		
		}
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
				
		// массив типов
		global $_type_banner;
		
		return system::show_tpl(
			array(
				'obj'=>$result, 
				'msg'=>$this->msg,
				'select'=>$_type_banner,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/new.php');
	
	}
	
	
	/**
	 * удаляем фото новости
	 */
	function deletephotoAction () {

		// строим запрос
		$_sql='SELECT * FROM `'.$this->tablename.'` where id='.$_GET['id'];
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);	
		
		if ($result->image) {
	
			// сообщение
			$this->msg=general::messages(1,v::getI18n('backend_after_delete_photo'));

			// имя файла на удаление
			$_name= array (
				$result->image
			);
			
			// удаляем
			forms::delete_photo(HOST.IMG_REKLAMA_PATH, $_name, 0);
			
			$_POST['FORM']['image']='';
			
			// записываем в базу
			forms::multy_update_form($this->tablename,$_GET['id'],0);
			
			unset($_POST);
			
			// выводим новость
			return self::newAction();
		
		} else {
		
			// выводим новость
			return self::newAction();		
		
		}
		
	}	
	
	
	/**
	 * удаляем материал
	 */
	function deleteAction () {

		// удаляем фото новости
		self::deletephotoAction();
	
		// удаляем из таблицы NEWS
		$_sql='DELETE FROM `'.$this->tablename.'` WHERE (`id`="'.$_GET['id'].'")';			
		$result=mysql::just_query($_sql,0);
		
		// сообщение
		$this->msg=general::messages(1,v::getI18n('backend_after_delete'));		
		
		return self::indexAction();		
					
	}	

	
	/**
	 * добавляем новый материал
	 */
	function addAction () {
		
		if ($_POST) {
		
			if (!empty($_FILES['file1']['tmp_name'])) {
			
				// проверяем на тип банера
				$size = getimagesize($_FILES['file1']['tmp_name']);
							
				// проверяем на тип (jpeg/gif/png/swf) баннера
				if (banners::check_type_banner($size['mime'])==0 or banners::check_type_banner($size['mime'])==1) {
				
					// проверяем на checkbox
					forms::check_box (array('status'));
					
					// записываем в базу
					forms::multy_insert_form($this->tablename,0);				
					
					// определяем имя файла
					$_filename=mysql_insert_id().'.'.end(explode(".", $_FILES['file1']['name']));	
		
					// загружаем баннер
					move_uploaded_file($_FILES['file1']['tmp_name'], HOST.IMG_REKLAMA_PATH.'/'.$_filename);
					
					// имя картинки
					$_POST['FORM']['image']=$_filename;
					
					// update имени картинки
					forms::multy_update_form($this->tablename,mysql_insert_id(),0);				

					$this->msg=general::messages(1,v::getI18n('backend_after_save'));			
					
				} else {
				
					// сообщение error
					$this->msg=general::messages(0,v::getI18n('backend_upload_reklama_error_type'));
				
				}
			
			} else {
			
					// проверяем на checkbox
					forms::check_box (array('status'));
			
					// записываем в базу
					forms::multy_insert_form($this->tablename,0);
			
					// сообщение
					$this->msg=general::messages(1,v::getI18n('backend_after_save'));			

			}
		
		}	
		
	
		// массив типов
		global $_type_banner;
				
		return system::show_tpl(
			array(
				'msg'=>$this->msg, 
				'select'=>$_type_banner,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/add.php');			

	
	}

	/**
	 * отображаем весь список материалов
	 */
	function hitsAction () {
	
		if(isset($_POST['status'])) { $_SESSION['status']=$_POST['status']; }
		if(isset($_POST['category'])) { $_SESSION['category']=$_POST['category']; }

		// строим запрос
		$_sql='SELECT * FROM `'.$this->tablename.'` where id>0 '.general::get_status_for_filter($this->tablename).' order by sort';
		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);		
		
		// массив типов
		global $_type_banner;	
		
		return system::show_tpl(
			array(
				'result'=>$result,
				'msg'=>$this->msg,
				'type_banner'=>$_type_banner,
				'_status'=>isset($_POST['status']) ? $_POST['status'] : 2,
				'_category'=>isset($_POST['category']) ? $_POST['category'] : 100,
				'tpl_folder'=>$this->tpl_folder
			),$this->tpl_folder.'/hits.php');
		
	}	
	
} 

?>