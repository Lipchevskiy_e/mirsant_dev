<?php

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class banners {

		/**
		* возвращает картинку или объект flash
		*
		* @param id баннера $id		
		*/
		static function get_reklama ($id) {
			
			// строим запрос
			$_sql='SELECT * FROM reklama where id='.$id;
			
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);

            if ($result->image) {			

				// проверяем на тип банера
				$size = getimagesize(HOST.IMG_REKLAMA_PATH.'/'.$result->image);
				
				// проверяем на тип (jpeg/gif/png/swf) баннера
				if (self::check_type_banner($size['mime'])==0) {
					$images="<a href='".$result->url."' class='banners".$result->id."' id='".$result->id."' target='_blank'><img src='/images/_reklama/".$result->image."' alt='".$result->name."' border=0></a>";
					// картинка
					return $images;				
				} else if (self::check_type_banner($size['mime'])==1) {
					// swf banner
					$_str_temp1='
					<div id="banner_'.$result->id.'"></div>
					<script type="text/javascript" src="js/swfobject.js"></script>
					<script type="text/javascript">
							// <![CDATA[
							var so = new SWFObject("/images/_reklama/'.$result->image.'", "sotester", "'.$size[0].'", "'.$size[1].'", "8");
							so.addParam("wmode", "transparent");
							so.write("banner_'.$result->id.'");
							// ]]>
					</script> 
					';				
					return $_str_temp1;
				} else {
				
					$images="<a href='".$result->url."' class='banners".$result->id."' id='".$result->id."' target='_blank'>".$result->url."</a>";
					// картинка
					return $images;	
					
				}
			
			} else {
			
					$images="<a href='".$result->url."' class='banners".$result->id."' id='".$result->id."' target='_blank'>".$result->url."</a>";
					// картинка
					return $images;	
					
			}
			
		}		
		
		/**
		* возвращает картинку или объект flash
		*
		* @param id баннера $id		
		*/
		static function get_reklama_backend ($id) {
			
			// строим запрос
			$_sql='SELECT * FROM reklama where id='.$id;
			
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);	

            if ($result->image) {			

				// проверяем на тип банера
				$size = getimagesize(HOST.IMG_REKLAMA_PATH.'/'.$result->image);
				
				// проверяем на тип (jpeg/gif/png/swf) баннера
				if (self::check_type_banner($size['mime'])==0) {
					// картинка
					return "<a href='".$result->url."' target='_blank'><img src='/images/_reklama/".$result->image."' width='100px' alt='".$result->name."' border=0></a>";				
				} else if (self::check_type_banner($size['mime'])==1) {
					// swf banner
					$_str_temp1='
					<div id="banner_'.$result->id.'"></div>
					<script type="text/javascript">
							// <![CDATA[
							var so = new SWFObject("/images/_reklama/'.$result->image.'", "sotester", "'.$size[0].'", "'.$size[1].'", "8");
							so.addParam("wmode", "transparent");
							so.write("banner_'.$result->id.'");
							// ]]>
					</script> 
					';				
					return $_str_temp1;
					
				} else {
				
					// ошибка
					return "<a href='".$result->url."' target='_blank'>".$result->url."</a>";
					
				}
			
			} else {
			
				// ошибка
				return "<a href='".$result->url."' target='_blank'>".$result->url."</a>";
			
			}
			
		}

	// выводим указанное кол-во баннеров
	static  function get_banner($limit) {

		// запрос
		$_sql="SELECT * from reklama where status=1 order by rand() limit ".$limit;
				  		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
			
		$_res='';
		foreach ($result as $obj) {
			$_res.=self::get_reklama($obj->id);
		}
		
		return $_res;

	}	
	
	// выводим указанное кол-во баннеров из указанной позиции
	static  function get_banner_category($category,$limit=1000) {

		// запрос
		$_sql="SELECT * from reklama where status=1 and category=".$category." order by sort limit ".$limit;
				  		
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);
			
		$_res='';
		foreach ($result as $obj) {
			$_res.=self::get_reklama($obj->id);
		}
		
		return $_res;

	}
	
	
	// проверяем тип (jpeg/gif/png/swf) баннера
	static function  check_type_banner($type) {
	
		if ($type=='image/gif' or $type=='image/png' or $type=='image/jpeg') {
			return 0;
		} else if ($type=='application/x-shockwave-flash') {
			return 1;
		}

		return 3;
	
	}		
	
}


?>