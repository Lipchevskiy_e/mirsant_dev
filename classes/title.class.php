<?php

// вывод ключевых тегов и их сортировка
function show_title() {

    global $action;
    f_mysql_connect();
    
    if (isset($_GET['action'])) {
        /*
         * специфические разделы для выбора ключей
         */
        if ($_GET['action'] == "news") {

            if (isset($_GET['url'])) {
                // нашли поля
                $_arr = ___findarray('select zag,text from news where url="' . $_GET['url'] . '"');
                $_title = $_arr['zag'] . ': превосходная статья для вас от интернет-магазина «Мир Сантехники»';
                $_description = 'Интернет-магазин «Мир Сантехники» представляет статью о ' . $_arr['zag'] . '. Все лучшее для Вас от нашей компании. Тел. (095) 101-77-26';
                $_keywords = get_stoka($_arr['zag'], 50, ', ') . _KEYWORDS;
            } else {

                if (TYPE_NEWS == 0 or ! isset($_GET['type'])) {

                    // нашли поля
                    $_arr = ___findarray('select title,description,keywords from ceo where id=1');
                    $_title = $_arr['title'];
                    $_description = $_arr['description'];
                    $_keywords = $_arr['keywords'];
                } else {


                    // нашли поля
                    switch ($_GET['type']) {
                        case 1:
                            $_idd = 6;
                            break;
                        case 2:
                            $_idd = 7;
                            break;
                        default:
                            $_idd = 1;
                            break;
                    }

                    //ищем
                    $_arr = ___findarray('select title,description,keywords from ceo where id=' . $_idd);

                    $_title = $_arr['title'];
                    $_description = $_arr['description'];
                    $_keywords = $_arr['keywords'];
                }
            }

            //$_title = 'Новости - интернет-магазин сантехнических товаров «Мир Сантехники»';
            //$_description = 'Интернет-магазин сантехнических товаров «Мир Сантехники»: Новости -  качественная сантехника на любой вкус по доступным ценам! Тел. (095) 101-77-26';
        } else if ($_GET['action'] == "gallery" or $_GET['action'] == "arhiv") {

            $_arr = ___findarray('select title,description,keywords from ceo where id=2');
            if (isset($_GET['id'])) {
                // нашли поля
                $_arr1 = ___findarray('select pole from gallery_grupa where id="' . $_GET['id'] . '"');
                $_title = $_arr['title'] . ' - ' . $_arr1['pole'] . ' — ' . NAME_FIRMS;
                if ($_arr1['text'] == '') {
                    $_description = _DESCRIPTION;
                } else {
                    $_description = get_stoka($_arr1['text'], 50, ' ');
                }
                $_keywords = get_stoka($_arr1['pole'], 50, ', ') . _KEYWORDS;
            } else {
                // нашли поля
                $_arr = ___findarray('select title,description,keywords from ceo where id=2');
                $_title = $_arr['title'] . ' - ' . NAME_FIRMS;
                $_description = $_arr['description'];
                $_keywords = $_arr['keywords'];
            }


            // раздел каталог
        } else if ($_GET['action'] == "catalog") {

            // нашли поля

            /*
              $_arr=___findarray('select title,description,keywords from ceo where id=3');
              $_title=$_arr['title'];
              $_description=$_arr['description'];
             */

            $_keywords = $_arr['keywords'];
//            var_dump($_keywords);
//            die();

            // если есть Группа
            if (isset($_GET['grupa'])) {

                // строим запрос
                $_sql = 'SELECT * FROM catalog_tree where url="' . $_GET['grupa'] . '"';
                // выполняем запрос + при необходимости выводим сам запрос
                $result = mysql::query_one($_sql, 0);

                /*
                 * определяем переменные
                 */

                /* if(!empty($result->title)){
                  $_title			.=	" - ".$result->title;
                  $_title			.=	" - ".NAME_FIRMS;
                  } else{ */
                $_title = $result->pole . ' купить в Киеве и Одессе: цены - «Мир Сантехники»';
                //}

                /* if(!empty($result->_description)){
                  $_description			.=	" - ".$result->_description;
                  $_description			.=	" - ".NAME_FIRMS;
                  } else{ */
                $_description = 'Интернет-магазин «Мир Сантехники»: Предлагает ' . $result->pole . ' отличного качества по умеренным ценам. Доставка по Киеву, Одессе и всей Украине. Тел. (095) 101-77-26';
                //}

                if (isset($_GET['filter']) && (strpos($_GET['filter'], 'brands_') !== false || strpos($_GET['filter'], 'type_') !== false)) {
                    $count = count(explode(';', $_GET['filter']));
                    if ($count == 1) {
                        $brand = explode('_', $_GET['filter']);
                        $brand_id = $brand[1];

                        switch ($brand[0]) {
                            case 'brands':
                                $table = 'brand';
                                $name = 'pole';
                                break;
                            case 'type':
                                $table = 'type_tovar';
                                $name = 'name';
                                break;
                        }

                        $brand = mysql::query('SELECT * FROM ' . $table . ' WHERE id = "' . $brand_id . '"');

                        if ($brand_id == 32) {
                            $_title = 'Акриловые ванны Kolpa San – купить акриловую ванну Kolpa San по доступной цене в Киеве | «Мир Сантехники»';
                            $_description = 'Акриловые ванны Kolpa San – интернет-магазин сантехники «СантехМир» ✔Большой выбор акриловых ванн Kolpa San по доступной цене в Киеве – mirsant.com.ua ✔ Доставка по Киеву и Украине ✔ ☎ +38 (095) 101-77-26';
                        } else {
                            $_title = $result->pole . ' ' . $brand[0]->$name . ' купить в Киеве, цены - «Мир Сантехники»';
                            $_description = 'Интернет магазин «Мир Сантехники»: Рекомендует ' . $result->pole . ' ' . $brand[0]->$name . ' по доступным ценам, отличного качества! Доставка по Киеву и Украине. Тел. (095) 101-77-26';
                        }
                    }
                }
                /* 04/05/16 */
                $_keywords = $result->keywords;

                /*
                 * определяем переменные
                 */
                $_keywords .= ", " . NAME_FIRMS;
            } else if (isset($_GET['goods'])) {

                // строим запрос
                $_sql = 'SELECT * FROM catalog where url="' . $_GET['goods'] . '" and status=1';
                // выполняем запрос + при необходимости выводим сам запрос
                $result = mysql::query_one($_sql, 0);

                if ($_GET['goods'] == 'shkafchik_podvesnoj_aquaform_flex_60_sm') {
                    if ($result->valuta == 2) {
                        $cost = $result->cost * EVRO;
                    } elseif ($result->valuta == 3) {
                        $cost = $result->cost * DOLLAR;
                    } else {
                        $cost = $result->cost;
                    }
                    $_title = 'Шкафчик подвесной Aquaform Flex 60*20 см. – купить по выгодной цене в Киеве и Украине | «Мир Сантехники»';
                    $_description = 'Купить шкафчик подвесной Aquaform Flex 60*20 см. – интернет-магазин сантехники «СантехМир» ✔ Купить шкафчик подвесной Aquaform Flex 60*20 см. по выгодной цене ' . ceil($cost) . ' грн.  на сайте – mirsant.com.ua ✔ Доставка по Киеву и Украине ✔ ☎ +38 (095) 101-77-26';
                } else {
                    /*
                     * определяем переменные
                     */
                    if ($result->title == '')
                        $_title .=" - Купить " . get_stoka($result->name, 50, ' ') . " - " . NAME_FIRMS;
                    else
                        $_title .=" - " . $result->title . " - " . NAME_FIRMS;

                    if ($result->keywords == '')
                        $_keywords .=", " . get_stoka($result->text_full, 150, ' ') . ", " . NAME_FIRMS;
                    else
                        $_keywords = $result->keywords;

                    if ($result->description == '')
                        $_description .=", " . get_stoka($result->text_full, 150, ' ') . ", " . NAME_FIRMS;
                    else
                        $_description = $result->description;

                    $_title = $result->name . ' (Арт: ' . $result->artikul . ') – купить по выгодной цене в Киеве и Украине | Интернет магазин «Мир Сантехники»';
                    $_description = $result->name . ' (Арт: ' . $result->artikul . ') в интернет магазине «Мир Сантехники» ✔ ' . $result->name . ' (Арт: ' . $result->artikul . ') – купить по доступной цене в Киеве – mirsant.com.ua ✔ Доставка по Киеву и Украине ✔ ☎ +38 (095) 101-77-26';
                }
            }

            // раздел каталог
        } else if ($_GET['action'] == "brand") {

            // нашли поля
            $_arr = ___findarray('select title,description,keywords from ceo where id=9');
            $_title = $_arr['title'];
            $_description = $_arr['description'];
            $_keywords = $_arr['keywords'];

            // конкретный бренд
            if (isset($_GET['id'])) {

                // строим запрос
                $_sql = 'SELECT * FROM brand where id=' . $_GET['id'];
                // выполняем запрос + при необходимости выводим сам запрос
                $result = mysql::query_one($_sql, 0);

                /*
                 * определяем переменные
                 */
                $_title .= " - " . $result->pole;
                $_description .= ", " . $result->pole;
                $_keywords .= ", " . $result->pole;

                /*
                 * определяем переменные
                 */
                $_title .= " - " . NAME_FIRMS;
                $_description .= ", " . NAME_FIRMS;
                $_keywords .= ", " . NAME_FIRMS;
            }


            // раздел каталог
        } else if ($_GET['action'] == "basket") {

            // нашли поля
            $_arr = ___findarray('select title,description,keywords from ceo where id=10');
            $_title = $_arr['title'];
            $_description = $_arr['description'];
            $_keywords = $_arr['keywords'];

            /*
             * определяем переменные
             */
            $_title .= " - " . NAME_FIRMS;
            $_description .= ", " . NAME_FIRMS;
            $_keywords .= ", " . NAME_FIRMS;


            // раздел каталог
        } else if ($_GET['action'] == "mycabinet") {

            // нашли поля
            $_arr = ___findarray('select title,description,keywords from ceo where id=11');
            $_title = $_arr['title'];
            $_description = $_arr['description'];
            $_keywords = $_arr['keywords'];

            /*
             * определяем переменные
             */
            $_title .= " - " . NAME_FIRMS;
            $_description .= ", " . NAME_FIRMS;
            $_keywords .= ", " . NAME_FIRMS;


            // раздел каталог
        } else if ($_GET['action'] == "filter") {

            // имя текущего раздела
            $_res = filter::get_name_curent_filter();

            $_title = $_res;
            $_description = $_res;
            $_keywords = $_res;

            // конкретный бренд
            if (isset($_GET['id'])) {

                /*
                 * определяем переменные
                 */
                $_title .= " - " . NAME_FIRMS;
                $_description .= ", " . NAME_FIRMS;
                $_keywords .= ", " . NAME_FIRMS;
            }
        } else if ($_GET['action'] == "search") {

            // нашли поля
            $_arr = ___findarray('select title,description,keywords from ceo where id=13');
            $_title = NAME_FIRMS . ' - ' . $_arr['title'];
            $_description = $_arr['description'];
            $_keywords = $_arr['keywords'];
        } else if ($_GET['action'] == "voting") {

            // нашли поля
            $_arr = ___findarray('select title,description,keywords from ceo where id=12');
            $_title = NAME_FIRMS . ' - ' . $_arr['title'];

            if (isset($_GET['arhiv'])) {
                $_title.=' - Архив голосований';
            }

            $_description = $_arr['description'];
            $_keywords = $_arr['keywords'];
        } else if ($_GET['action'] == "registration") {

            // нашли поля
            $_arr = ___findarray('select title,description,keywords from ceo where id=8');
            $_title = NAME_FIRMS . ' - ' . $_arr['title'];
            $_description = $_arr['description'];
            $_keywords = $_arr['keywords'];
        } else {

            $_str = "where action='" . $_GET['action'] . "' and status=1";
            //echo "<br>".$_str."<br>";
            $result = f_mysql_select("content", $_str);

            if (mysql_numrows($result) > 0) {
                $find = mysql_fetch_array($result);
                /*
                 * определяем переменные
                 */
                $_title = $find['title'] . ' - ' . NAME_FIRMS;
                $_description = $find['description'];
                $_keywords = $find['keywords'];
            } else {
                $_title = _TITLE . ' - ' . NAME_FIRMS;
                $_description = _DESCRIPTION;
                $_keywords = _KEYWORDS;
            }
        }
    }

    if (!isset($_GET['action']) or $_title == '') {
        //$result=f_mysql_select("title","where id=1");
        //$find=mysql_fetch_array($result);
        /*
         * определяем переменные
         */
        $_title = _TITLE . ' - ' . NAME_FIRMS;
        $_description = _DESCRIPTION;
        $_keywords = _KEYWORDS;
    }

    $seo = mysql::query('SELECT * FROM seo_optimise WHERE uri = "' . SEO_S::$uri . '"');

    $seo = $seo[0];

    if ($seo->title) {
        $_title = $seo->title;
    }

    if ($seo->desc) {
        $_description = $seo->desc;
    }

    // массив значений
    $_res = array($_title, $_description, $_keywords);
    //print_r($_res);
    return $_res;
}

// получаем заголовок раздела - если необходимо НОВАЯ ВЕРСИЯ

function get_navigator() {

    // если каталог!!!
    if ($_GET['action'] == 'catalog') {
        return get_navigator_catalog();
    }

    global $zag_radel;
    global $dbname;

    $_res = '';
    if (isset($_GET['action']) and $_GET['action'] != 'index') {
        $_res = '<a itemprop="url" href="/"><span itemprop="title">Сантехника</span></a> ';
    }
    if (isset($_GET['action']) and $_GET['action'] == 'news' and isset($_GET['url'])) {
        if (TYPE_NEWS) {
            $curr_type = isset($_GET['type']) ? (int) $_GET['type'] : 0;
            $_res.=' <a itemprop="url" href="/news/type/' . $curr_type . '"><span itemprop="title">' . $_type_news[$curr_type] . '</span></a> ';
        } else {
            $_res.=' <span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/news"><span itemprop="title">Новости</span></a></span> ';
        }
    }

    if (isset($_GET['action']) and $_GET['action'] == 'gallery') {

        if (isset($_GET['url'])) {

            $_res.=' <span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/gallery"><span itemprop="title">Галерея</span></a></span> ';

            $_id_gallery_grupa = dbh::get_gallery_grupa_id_parent($_GET['url']);
            // строим запрос
            $_sql = 'SELECT * FROM gallery_grupa where id=' . $_id_gallery_grupa . ' and status=1 order by sort';
            $result = mysql::query($_sql, 0);
            if ($result) {
                // выводим группы товаров
                $_res.=system::show_tpl(array('result' => $result), 'frontend/gallery/bread.php');
            }
        }
    }

    if ($_GET['action'] == 'voting') {
        if (isset($_GET['arhiv'])) {
            $_res.=' <span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/voting"><span itemprop="title">Голосование</span></a></span> ';
        }
    }


    /*
     * проверяем на разделы
     * если они есть, то выводим родительский  раздел
     */

    // проверяем на наличие подразделов данного раздела
    $_sql = "select id_parent, name
				 from content
				 where action='" . $_GET['action'] . "' and id_parent!=0";
    //echo $_sql."<br>";
    $result = mysql_db_query($dbname, $_sql);

    $_res_n = array();
    if (mysql_numrows($result) > 0) {
        // это дочерний раздел, т.е. у него есть родитель, ищем его имя и ссылку
        $find = mysql_fetch_array($result);
        //findpole_universal_full($poisk,$table,$name_field,$nomerpole,$whatdoing)
        $_id_parent_name = findpole_universal_full($find['id_parent'], "content", "id", 1, "return");
        $_id_parent_action = findpole_universal_full($find['id_parent'], "content", "id", 2, "return");
        //$_res='<a href="'.MAIN_PATH.'/'.$_id_parent_action.'.php">'.$_id_parent_name.'</a> / '.$_res;
        $_res_n[0] = '<span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="' . MAIN_PATH . '/' . $_id_parent_action . '"><span itemprop="title">' . $_id_parent_name . '</span></a></span> ';

        for ($i = 1; $i < 20; $i++) {
            // ищем следующий уровень
            $_sql = "select id_parent, name
					 from content
					 where action='" . $_id_parent_action . "' and id_parent!=0";
            //echo $_sql."<br>";
            $result = mysql_db_query($dbname, $_sql);
            if (mysql_numrows($result) > 0) {
                // это дочерний раздел, т.е. у него есть родитель, ищем его имя и ссылку
                $find = mysql_fetch_array($result);
                //findpole_universal_full($poisk,$table,$name_field,$nomerpole,$whatdoing)
                $_id_parent_name = findpole_universal_full($find['id_parent'], "content", "id", 1, "return");
                $_id_parent_action = findpole_universal_full($find['id_parent'], "content", "id", 2, "return");
                //$_res='<a href="'.MAIN_PATH.'/'.$_id_parent_action.'.php">'.$_id_parent_name.'</a> / '.$_res;
                $_res_n[$i] = '<span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="' . MAIN_PATH . '/' . $_id_parent_action . '"><span itemprop="title">' . $_id_parent_name . '</span></a></span> ';
            } else {
                break;
            }
        }
    }

    // строим в нужном порядке и выводим
    $_res_n = array_reverse($_res_n);
    for ($i = 0; $i < count($_res_n); $i++) {
        $_res.=$_res_n[$i];
    }

    if (isset($_GET['action']) and $_GET['action'] != 'index') {

        return $_res . ' <span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">' . get_h1() . '</span></span>';
    }
}

// получаем заголовок раздела - если необходимо для ХЛЕБНЫХ КРОШЕК!

function get_navigator_catalog() {

    global $zag_radel;
    global $dbname;

    $_res = "";

    if (isset($_GET['grupa'])) {

        // каталог / группа
        $_res = '<a itemprop="url" href="' . MAIN_PATH . '"><span itemprop="title">Сантехника</span></a> ';

        $__id = mirsant::get_id_grupa_by_url();
        // строим запрос
        $_sql = 'SELECT * FROM catalog_tree where id=' . $__id . ' and status=1 order by sort desc';
        // выполняем запрос + при необходимости выводим сам запрос
        $result = mysql::query($_sql, 0);

        $_res1 = '';
        if ($result) {
            // выводим группы товаров
            $_res1 = system::show_tpl(array('result' => $result, 'ls' => true), 'frontend/catalog/bread.php') . $_res1;
        }

        $_res.=$_res1;
    } else if (isset($_GET['goods'])) {
        $__id = mirsant::get_id_goods_by_url();
        // строим запрос
        $_sql = 'SELECT * FROM catalog where id=' . $__id;
        // выполняем запрос + при необходимости выводим сам запрос
        $result = mysql::query_one($_sql, 0);

        if ($result) {
            if ($result->h1 == '')
                $_res0 = $result->name;
            else
                $_res0 = $result->h1;
        } else {
            return 'такого товара нет';
        }

        $_res0 = '<span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">' . $_res0 . '</span></span>';


        // каталог / группа
        $_res = '<a itemprop="url" href="' . MAIN_PATH . '"><span itemprop="title">Сантехника</span></a> ';

        // строим запрос
        $_sql = 'SELECT * FROM catalog_tree where id=' . $result->id_parent . ' and status=1 order by sort desc';
        // выполняем запрос + при необходимости выводим сам запрос
        $result = mysql::query($_sql, 0);

        $_res1 = '';
        if ($result) {
            // выводим группы товаров
            $_res1 = system::show_tpl(array('result' => $result), 'frontend/catalog/bread.php') . $_res1;
            $_res1 = str_replace('<a', '<a itemprop="url"', $_res1);
            //$_res1 = str_replace('">', '"><span itemprop="title">', $_res1);
            $_res1 = preg_replace('#(href=".+" >)#', '$1<span itemprop="title">', $_res1);
            $_res1 = str_replace('</a>', '</span></a>', $_res1);
            $_res1 = '<span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">' . $_res1 . '</span>';
        }

        $_res.=$_res1 . $_res0;
    } else if ($_GET['action'] == "catalog") {
        $_res = '<span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="' . MAIN_PATH . '"><span itemprop="title">Главная</a></span></span> <span itemprop="" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">Каталог продукции</span></span>';
    }


    return $_res;
}

// парсим $_SERVER['REQUEST_URI'] для определения адресной строки

function get_url() {

    $tmp = explode("?", $_SERVER ['REQUEST_URI']);

    if (stripos($tmp[0], '/grupa/')) {

        $filter_pos = stripos($tmp[0], '/filter/');
        if ($filter_pos) {
            $filter_and_further = substr($tmp[0], $filter_pos);

            $page_pos = stripos($tmp[0], '/page/');
            if ($page_pos) {
                $arka = explode('/page/', $filter_and_further);
                $only_filter = $arka[0];
            } else {
                $only_filter = $filter_and_further;
            }

            $tmp[0] = str_replace($only_filter, '', $tmp[0]);

            $only_filter = str_replace('/filter/', '', $only_filter);
            $_GET['filter'] = str_replace('/', ';', $only_filter);
        }
    }


    $_arr = explode("/", $tmp[0]);
    //echo "<pre>".print_r($_arr,1)."</pre>";;
    // определяем action
    $_perem = (preg_replace('/.*\//', '', $_arr[1]));
    $_action = (preg_replace('/\..*/', '', $_perem));

    // проверяем что имеем
    if ($_action != "" and $_action != "index") {
        $_GET['action'] = $_action;

        // проверяем на расширение файла, если есть - 404
        system::isset_extension(end($_arr));

        // -----------------------
        // проверяем на функционал
        // -----------------------		
        if ($_action == "news" or $_action == "gallery") {

            if (isset($_arr[2]) and $_arr[2] != "") {
                for ($i = 2; $i < count($_arr); $i++) {
                    if ($i == 2 and $_arr[2] != 'page' and $_arr[2] != 'type') {
                        $_perem = (preg_replace('/.*\//', '', $_arr[2]));
                        $_action = (preg_replace('/\..*/', '', $_perem));
                        $_GET['url'] = $_action;
                    } else if (isset($_arr[$i]) and $_arr[$i] != "") {
                        $y = $i + 1;
                        $_GET[$_arr[$i]] = $_arr[$y];
                        //system::error(404);
                        $i++;
                    }
                }
            }
        } else if ($_action == "basket") {

            if (isset($_arr[2]) and $_arr[2] == 'finish') {
                $_GET['finish'] = 1;
            }
        } else {
            for ($i = 2; $i < count($_arr); $i++) {
                if (isset($_arr[$i]) and $_arr[$i] != "") {
                    $y = $i + 1;
                    $_GET[$_arr[$i]] = $_arr[$y];
                }
                $i++;
            }

            $uriArr = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
            if ($_action == 'catalog' && count($uriArr) == 3 && $uriArr[1] != 'grupa' && $uriArr[1] != 'goods') {
                header('HTTP/1.1 404 Not Found');
                include '404.html';
                exit;
            }
            //break;
        }
        // -----------------------
        // -----------------------
    } else {
        // если index
        // проверяем на расширение файла, должно быь только php иначе - 404
        system::isset_php($_perem);
    }

    // очищаем GET
    system::clear_get();
}

// получаем заголовок раздела - если необходимо

function get_h1() {

    global $zag_radel;
    global $dbname;

    // ищем в table ACTION сам action и если он есть то выхватываем имя раздела
    //$_res=findpole_universal_full($_GET['action'],"content","action",1,"return");
    $_res = ___findpole('select content.h1 from content where action="' . $_GET['action'] . '"', 'h1');
    //echo "----_res-----".$_res."<br>";

    if (!$_res) {


        if (isset($_GET['grupa'])) {
            if (isset($_GET['filter']) && strpos($_GET['filter'], ',') === false) {
                $brand = explode('_', $_GET['filter']);
                $brand_id = $brand[1];
                if ($brand_id == 32) {
                    $_res = 'Акриловые ванны Kolpa San';
                } else {
                    // строим запрос
                    $_sql = 'SELECT * FROM catalog_tree where url="' . $_GET['grupa'] . '"';
                    // выполняем запрос + при необходимости выводим сам запрос
                    $result = mysql::query_one($_sql, 0);

                    if ($result) {
                        $_res = $result->pole;
                    } else {
                        header('HTTP/1.1 404 Not Found');
                        $_res = 'Такого товара нет!';
                    }
                }
            } else {
                // строим запрос
                $_sql = 'SELECT * FROM catalog_tree where url="' . $_GET['grupa'] . '"';
                // выполняем запрос + при необходимости выводим сам запрос
                $result = mysql::query_one($_sql, 0);

                if ($result) {
                    $_res = $result->pole;
                } else {
                    header('HTTP/1.1 404 Not Found');
                    $_res = 'Такого товара нет!';
                }
            }
            //echo "----_res-----".$_res."<br>";
        } else if (isset($_GET['goods']) and $_GET['action'] == "catalog") {

            // строим запрос
            $_sql = 'SELECT * FROM catalog where url="' . $_GET['goods'] . '" and status=1';
            // выполняем запрос + при необходимости выводим сам запрос
            $result = mysql::query_one($_sql, 0);
            if ($_GET['goods'] == 'shkafchik_podvesnoj_aquaform_flex_60_sm') {
                if ($result->valuta == 2) {
                    $cost = $result->cost * EVRO;
                } elseif ($result->valuta == 3) {
                    $cost = $result->cost * DOLLAR;
                } else {
                    $cost = $result->cost;
                }
                $_res = 'Акриловые ванны Kolpa San ';
            } else {
                if ($result) {
                    if ($result->h1 == '')
                        $_res = $result->name . ' ' . $result->artikul;
                    else
                        $_res = $result->h1;
                } else {
                    $_res = 'Такого товара нет!';
                }
            }
        } else if (isset($_GET['action'])) {

            if ($_GET['action'] == 'news') {

                if (!isset($_GET['url'])) {

                    if (TYPE_NEWS == 0 or ! isset($_GET['type'])) {

                        // нашли поля
                        $_arr = ___findarray('select h1, title from ceo where id=1');
                    } else {

                        // нашли поля
                        switch ($_GET['type']) {
                            case 1:
                                $_idd = 6;
                                break;
                            case 2:
                                $_idd = 7;
                                break;
                            default:
                                $_idd = 1;
                                break;
                        }

                        // нашли поля
                        $_arr = ___findarray('select h1, title from ceo where id=' . $_idd);
                    }


                    if ($_arr['h1'] != '') {
                        $_res = $_arr['h1'];
                    } else {
                        $_res = $_arr['title'];
                    }
                } else {

                    $_res = ___findpole('select zag from news where url="' . $_GET['url'] . '"', 'zag');
                }
            } else if ($_GET['action'] == 'gallery') {

                if (!isset($_GET['url'])) {

                    $_arr = ___findarray('select h1, title from ceo where id=2');
                    if ($_arr['h1'] != '') {
                        $_res = $_arr['h1'];
                    } else {
                        $_res = $_arr['title'];
                    }

                    if (isset($_GET['id'])) {
                        $_res.=', ' . ___findpole('select pole from gallery_grupa where id=' . $_GET['id'], 'pole');
                    }
                } else {

                    $_res = ___findpole('select pole from gallery_grupa where url="' . $_GET['url'] . '"', 'pole');
                }
            } else if ($_GET['action'] == 'search') {


                $_arr = ___findarray('select h1, title from ceo where id=13');
                if ($_arr['h1'] != '') {
                    $_res = $_arr['h1'];
                } else {
                    $_res = $_arr['title'];
                }
            } else if ($_GET['action'] == 'voting') {


                $_arr = ___findarray('select h1, title from ceo where id=12');
                if ($_arr['h1'] != '') {
                    $_res = $_arr['h1'];
                    if (isset($_GET['arhiv'])) {
                        $_res = 'Архив голосований';
                    }
                } else {
                    $_res = $_arr['title'];
                }
            } else if ($_GET['action'] == 'catalog') {


                $_arr = ___findarray('select h1, title from ceo where id=3');
                if ($_arr['h1'] != '') {
                    $_res = $_arr['h1'];
                } else {
                    $_res = $_arr['title'];
                }
            } else if ($_GET['action'] == 'brand') {


                $_arr = ___findarray('select h1, title from ceo where id=9');
                if ($_arr['h1'] != '') {
                    $_res = $_arr['h1'];
                } else {
                    $_res = $_arr['title'];
                }
            } else if ($_GET['action'] == 'basket') {


                $_arr = ___findarray('select h1, title from ceo where id=10');
                if ($_arr['h1'] != '') {
                    $_res = $_arr['h1'];
                } else {
                    $_res = $_arr['title'];
                }
            } else if ($_GET['action'] == 'mycabinet') {


                $_arr = ___findarray('select h1, title from ceo where id=11');
                if ($_arr['h1'] != '') {
                    $_res = $_arr['h1'];
                } else {
                    $_res = $_arr['title'];
                }
            } else if ($_GET['action'] == 'filter') {

                // имя текущего раздела
                $_res = filter::get_name_curent_filter();
            } else if ($_GET['action'] == 'registration') {


                $_arr = ___findarray('select h1, title from ceo where id=8');
                if ($_arr['h1'] != '') {
                    $_res = $_arr['h1'];
                } else {
                    $_res = $_arr['title'];
                }
            } else {


                /*
                 * костыль для 3 новостей
                 */
                if (isset($_GET['type']) and $_GET['type'] == 1) {
                    $_res = $zag_radel['news1'];
                } else if (isset($_GET['type']) and $_GET['type'] == 2) {
                    $_res = $zag_radel['news2'];
                }
            }
        } else {

            $_res = $zag_radel['main'];
        }
    }

    if (isset($_GET['filter']) && (strpos($_GET['filter'], 'brands_') !== false || strpos($_GET['filter'], 'type_') !== false)) {
        $count = count(explode(';', $_GET['filter']));
        if ($count == 1) {
            $filter = explode('_', $_GET['filter']);
            $type = $filter[0];
            $value = $filter[1];

            if (strpos($value, ',') === false) {
                $id = $value;

                switch ($type) {
                    case 'brands':
                        $table = 'brand';
                        $name = 'pole';
                        break;

                    case 'type':
                        $table = 'type_tovar';
                        $name = 'name';
                        break;
                }

                $h1 = mysql::query('SELECT * FROM ' . $table . ' WHERE id = "' . $id . '"');
                /* if (empty($h1)) {

                  header('HTTP/1.1 404 Not Found');
                  include '404.html';
                  exit;
                  } */
                $_res .= ' ' . $h1[0]->$name;
            }
        }
    }

    $seo = mysql::query('SELECT * FROM seo_optimise WHERE uri = "' . SEO_S::$uri . '"');
    $seo = $seo[0];

    if ($seo->h1) {
        $_res = $seo->h1;
    }

    if (isset($_GET['grupa'])) {
        $_res = preg_replace('/[(]{1}.*[)]{1}[, ]+/', '', $_res);
    }

    return $_res;
}

/*
 * 	2012
 */

class title {

    public $title = _TITLE;
    public $keywords = _KEYWORDS;
    public $description = _DESCRIPTION;
    public $h1 = _H1;

    function get_title() {
        return $this->title;
    }

    function get_keywords() {
        return $this->keywords;
    }

    function get_description() {
        return $this->description;
    }

    function get_h1() {
        return $this->h1;
    }

}

?>