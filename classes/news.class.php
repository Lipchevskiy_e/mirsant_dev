<?php

/*
	* загрузка фото НАЧАЛО
	*/
	
	function multy_update_photo_news ($_table_name,$_dir_name, $_count_photo, $id) {
		
			
			//echo "<pre>".print_r($_FILES,1)."</pre>";

	
			// проверяем на картинку
		   for ($i=1; $i<=$_count_photo; $i++) {
	    		// имя картинки
			    $file_name="file".$i;

			    if ($_FILES[$file_name]['name']!=""){
			    	
		    		//$name_file_destination=$id.".jpg";
		    		$name_file_destination=$id.".jpg";
		
					// -------------------------------------------------------------------
		     		$fileuplo = $_FILES[$file_name]['tmp_name'];    		
		     		move_uploaded_file($fileuplo, HOST.'/images/'.$_dir_name.'/'.md5($_FILES[$file_name]['name']));
		     		$fileuplo = HOST.'/images/'.$_dir_name.'/'.md5($_FILES[$file_name]['name']);
					// -------------------------------------------------------------------
		
		    		$filename=HOST.'/images/'.$_dir_name.'/'.$name_file_destination;
		    		copy_and_resize_image ($fileuplo, $filename, 250, 250);
		
		    		$filename1=HOST.'/images/'.$_dir_name.'/_'.$name_file_destination;
		    		copy_and_resize_image_new ($filename, $filename1, _NEWS_WIDTH, _NEWS_HEIGHT);
		    		
		
					// удаляем загруженное фото
					if (file_exists($fileuplo)) {
						unlink($fileuplo);
					}
					
			    }
			}
			
	}		

/*
* core_2011
*/

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class news {


		/**
		* 	get news
		*	@param  int		$limit 		- count of news for return
		* 			text	$name_file 	- name of tamplate (plus path '/tpl/....') 
		*/	

		static function show_news($limit,$name_file,$type='') {
		
			if ($type) {
				$_sql='SELECT * FROM news WHERE status=1 and type="'.$type.'" order by date_news desc limit '.$limit;
			} else {			
				if ((TYPE_NEWS==0) or (!isset($_GET['type']))) {
					$_sql='SELECT * FROM news where status=1 order by date_news desc limit '.$limit;
				} else {
					system::isset_numeric($_GET['type']);
					$_sql='SELECT * FROM news where status=1 and type="'.$_GET['type'].'" order by date_news desc limit '.$limit;
				}
			}

			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query($_sql,0);

			// выполняем tpl
			return system::show_tpl(array('news'=>$result),$name_file);
			
		}
		

		
		/**
		* 	get count of the comments to news
		*	@param  int		$id 		- id news
		*/	
		
		static function count_comment($id) {
	
			// запрос
			$_sql="SELECT count(*) as count from news_gb where id_news=".$id;
				  		
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}		
		
		
		/**
		 * get count news at page
		 *
		 */
		static function count_news_at_page() {
	
			/*
			* проверяем на вібор пользователем отображения кол-ва на одной странице (НАЧАЛО)
			*/
			if (isset($_GET['news_at_page']))  {
				system::isset_numeric($_GET['news_at_page']); 
				$_SESSION['news_at_page']=$_GET['news_at_page'];
			}        
			
			/*
			* кол-во записей на одной странице 
			*/
			if (isset($_SESSION['news_at_page']))  {
				define ('NEWS_AT_PAGE',$_SESSION['news_at_page']);
			} else {
				define ('NEWS_AT_PAGE',FIRST_NEWS_AT_PAGE);
			}
	
		}
		

		/**
		* 	get count of news
		*/	
		
		static function count_news() {
	
			// запрос
			if (TYPE_NEWS==0 or !isset($_GET['type'])) {
				$_sql='SELECT count(*) as count FROM news where status=1';
			} else {
				system::isset_numeric($_GET['type']);
				$_sql='SELECT count(*) as count FROM news where status=1 and type="'.$_GET['type'].'"';
			}			
				  		
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}	

		
		/**
		 * get one news we need
		 *
		 * @param 	varchar 		$name_file 		- name of tamplate (plus path '/tpl/....')  
		 */
		
		static function show_url ($name_file) {	
			
			// запрос
			$_sql='SELECT * FROM news where status=1 and url="'.mysql_escape_string($_GET['url']).'"';
			
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);
			
			if (!$result) {
				die (get_tpl('/tpl/frontend/mainpage/404.tpl.php'));
			}
			
			// выполняем tpl
			return system::show_tpl(array('obj'=>$result),$name_file);	
			
		}
		
		/**
		 * show form to add comment
		 *
		 * @param 	varchar 		$name_file 		- name of tamplate (plus path '/tpl/....')  
		 */
		
		static function comment_add ($name_file) {
			
			// выполняем tpl
			return system::show_tpl(array('url'=>$_GET['url']),$name_file);				
			
		}
		
		
		/**
		 * comments of news
		 *
		 * @param 	int	 			$id				- id of news
		 * @param 	varchar 		$name_file 		- name of tamplate (plus path '/tpl/....')  
		 */
		
		static function comment_show($limit,$id,$name_file) {

			// выбираем
			$_sql='SELECT * FROM news_gb where status=1 and id_news='.$id.' order by id desc limit '.$limit;;
			
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query($_sql,0);

			// выполняем tpl
			return system::show_tpl(array('comment'=>$result),$name_file);			

		}
		
		
		/**
		 * get in news by url
		 *
		 * @param 		varchar		 $url	-	url of news
		 */
		
		static function get_news_id($url) {
			
			// выбираем
			$_sql='SELECT id FROM news where url="'.mysql_escape_string($_GET['url']).'"';			
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'id',0);			
			
		}
		static function get_news_type($url) {

			// выбираем
			$_sql='SELECT type FROM news where url="'.mysql_escape_string($_GET['url']).'"';

			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'type',0);

		}
		
		/**
		* 	get count of comment to news
		*/	
		
		static function count_comment_news($id) {
	
			// запрос
			$_sql="SELECT count(*) as count FROM news_gb where status=1 and id_news=".$id;
				  		
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}	

		
		/**
		 * get count comments to news at page
		 *
		 */
		static function count_comment_news_at_page() {
	
			/*
			* проверяем на вібор пользователем отображения кол-ва на одной странице (НАЧАЛО)
			*/
			if (isset($_GET['comment_news_at_page']))  {
				system::isset_numeric($_GET['comment_news_at_page']); 
				$_SESSION['comment_news_at_page']=$_GET['comment_news_at_page'];
			}        
			
			/*
			* кол-во записей на одной странице 
			*/
			if (isset($_SESSION['comment_news_at_page']))  {
				define ('COMMENT_NEWS_AT_PAGE',$_SESSION['comment_news_at_page']);
			} else {
				define ('COMMENT_NEWS_AT_PAGE',FIRST_COMMENT_NEWS_AT_PAGE);
			}
	
		}			
		
		/**
		 * print news we need
		 *
		 * @param 	varchar 		$name_file 		- name of tamplate (plus path '/tpl/....')  
		 */
		
		static function show_id ($name_file,$id) {	
			
			// запрос
			$_sql='SELECT * FROM news where status=1 and id='.$id;
			
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);
			
			// выполняем tpl
			return system::show_tpl(array('obj'=>$result),$name_file);	
			
		}		
		
		
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

?>