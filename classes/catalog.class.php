<?php

/* * ***********************************************************************************
 * *************** 2011 ****************************************************************
 * *********************************************************************************** */

class catalog {

    static function get_catalog_menu($name_file) {

        // строим запрос
        $_sql = "SELECT * FROM catalog_tree where id_parent=0 and status=1 order by sort";

        // выполняем запрос + при необходимости выводим сам запрос
        $result = mysql::query($_sql, 0);

        return system::show_tpl(array('result' => $result), $name_file);
    }
	
	static function get_night_time() {

        // строим запрос
        $time_from = strtotime(mysql::query_findpole('select zna from config where id=1022','zna'));
		$time_to = strtotime(mysql::query_findpole('select zna from config where id=1023','zna'));
		$time_night = mysql::query_findpole('select zna from config where id=1024','zna');
		
		$time['time_from'] = $time_from;
		$time['time_to'] = $time_to;
		$time['night'] = $time_night;

        return $time;
    }

    /**
     * get count news at page
     *
     */
    static function count_at_page() {

        /*
         * проверяем на вібор пользователем отображения кол-ва на одной странице (НАЧАЛО)
         */
        if (isset($_GET['at_page'])) {
            system::isset_numeric($_GET['at_page']);
            $_SESSION['at_page'] = $_GET['at_page'];
        }

        /*
         * кол-во записей на одной странице 
         */
        if (isset($_SESSION['at_page'])) {
            define('CATALOG_AT_PAGE', $_SESSION['at_page']);
        } else {
            define('CATALOG_AT_PAGE', FIRST_CATALOG_AT_PAGE);
        }
    }

    /**
     * 	get count 
     */
    static function get_count() {

        // фильтрация
        //$_filter=catalog::get_filter();			
        /* Фильтры */

        $s = 0;
        $_c = 0;
        $_b = 0;
        $_w = 0;
        $_h = 0;
        $_t = 0;
        $_s = 0;

        $_tray_depth = 0;
        $_cabin_form = 0;
        $_color = 0;
        $_mounting_type = 0;
        $_mixer_style = 0;
        $_mixer_type = 0;
        $_bathtub_form = 0;
        $_toilet_mount = 0;

        if (isset($_GET['filter'])) {
            $filter = explode(';', $_GET['filter']);
            foreach ($filter as $f) {
                $arr = explode('_', $f);
                switch ($arr[0]) {
                    case 'sort':
                        $sort = ' ORDER BY cost ';
                        if ($arr[1] == 'max-min')
                            $sort .= ' DESC';
                        $s = 1;
                        break;

                    case 'cost':
                        $_c = 1;
                        $c = explode('-', $arr[1]);
                        $cost = ' AND ((catalog.cost>=' . $c[0] . ' and catalog.cost<=' . $c[1] . ' and catalog.valuta=1) OR (catalog.cost>=' . $c[0] / EVRO . ' and catalog.cost<=' . $c[1] / EVRO . ' and catalog.valuta=2) OR (catalog.cost>=' . $c[0] / DOLLAR . ' and catalog.cost<=' . $c[1] / DOLLAR . ' and catalog.valuta=3)) ';
                        break;

                    case 'brands':
                        $_b = 1;
                        $brands = ' AND catalog.brand IN(' . $arr[1] . ') ';
                        break;

                    case 'series':
                        $_s = 1;
                        $brands = ' AND catalog.serie IN(' . $arr[1] . ') ';
                        break;

                    case 'type':
                        $_t = 1;
                        $type = ' AND catalog.has_type IN(' . $arr[1] . ') ';
                        break;

                    case 'width':
                        $_w = 1;
                        $width = ' AND catalog.width=' . $arr[1] . ' ';
                        break;

                    case 'length':
                        $_h = 1;
                        $length = ' AND catalog.length=' . $arr[1] . ' ';
                        break;

                    case 'tray-depth':
                        $_tray_depth = 1;
                        $tray_depth = ' AND catalog.tray_depth IN (' . $arr[1] . ') ';
                        break;

                    case 'cabin-form':
                        $_cabin_form = 1;
                        $cabin_form = ' AND catalog.cabin_form IN(' . $arr[1] . ') ';
                        break;

                    case 'color':
                        $_color = 1;
                        $color = ' AND catalog.color IN (' . $arr[1] . ') ';
                        break;

                    case 'mounting-type':
                        $_mounting_type = 1;
                        $mounting_type = ' AND catalog.mounting_type IN(' . $arr[1] . ') ';
                        break;

                    case 'mixer-style':
                        $_mixer_style = 1;
                        $mixer_style = ' AND catalog.mixer_style IN (' . $arr[1] . ') ';
                        break;

                    case 'mixer-type':
                        $_mixer_type = 1;
                        $mixer_type = ' AND catalog.mixer_type IN(' . $arr[1] . ') ';
                        break;

                    case 'bathtub-form':
                        $_bathtub_form = 1;
                        $bathtub_form = ' AND catalog.bathtub_form IN(' . $arr[1] . ') ';
                        break;

                    case 'toilet-mount':
                        $_toilet_mount = 1;
                        $toilet_mount = ' AND catalog.toilet_mount IN(' . $arr[1] . ') ';
                        break;

                    case 'forma-gidromassazhnoj-vanny':
                        $_gidr_van = 1;
                        $gidr_van = ' AND catalog.gidr_van IN(' . $arr[1] . ') ';
                        break;

                    case 'tip-montazha-tumby-dlja-vannoj':
                        $_tumb_van = 1;
                        $tumb_van = ' AND catalog.tumb_van IN(' . $arr[1] . ') ';
                        break;

                    case 'tip-montazha-penalov-dlja-vannoj':
                        $_penal_van = 1;
                        $penal_van = ' AND catalog.penal_van IN(' . $arr[1] . ') ';
                        break;

                    case 'tip-montazha-bide':
                        $_bide_mont = 1;
                        $bide_mont = ' AND catalog.bide_mont IN(' . $arr[1] . ') ';
                        break;

                    case 'tip-montazha-kuhonnyh-moek':
                        $_kuh_mont = 1;
                        $kuh_mont = ' AND catalog.kuh_mont IN(' . $arr[1] . ') ';
                        break;

                    case 'kolichestvo-sektsij-kuhonnoj-mojki':
                        $_kuh_count = 1;
                        $kuh_count = ' AND catalog.kuh_count IN(' . $arr[1] . ') ';
                        break;
                }
            }
        }

        if ($s == 0)
            $sort = ' ORDER BY sort ';

        if ($_c == 0)
            $cost = ' ';

        if ($_b == 0)
            $brands = ' ';

        if ($_t == 0)
            $type = ' ';

        if ($_w == 0)
            $width = ' ';

        if ($_h == 0)
            $length = ' ';

        if ($_s == 0)
            $series = ' ';

        if ($_tray_depth == 0)
            $tray_depth = ' ';
        if ($_cabin_form == 0)
            $cabin_form = ' ';
        if ($_color == 0)
            $color = ' ';
        if ($_mounting_type == 0)
            $mounting_type = ' ';
        if ($_mixer_style == 0)
            $mixer_style = ' ';
        if ($_mixer_type == 0)
            $mixer_type = ' ';
        if ($_bathtub_form == 0)
            $bathtub_form = ' ';
        if ($_toilet_mount == 0)
            $toilet_mount = ' ';
        /* Конец фильтра */
        $__id = mirsant::get_id_grupa_by_url();
        // запрос
        $_sql = 'SELECT count(*) as count FROM catalog where id_parent=' . $__id . ' and status=1 ' . $brands . $cost . $series . $width . $length . $type . $tray_depth . $cabin_form . $color . $mounting_type . $mixer_style . $mixer_type . $bathtub_form . $toilet_mount . $gidr_van . $tumb_van . $penal_van . $bide_mont . $kuh_mont. $kuh_count;

        // выполняем запрос + при необходимости выводим сам запрос
        return mysql::query_findpole($_sql, 'count');
    }

    static function get_type($name_file, $limit, $grupa = '') {

        $_filter = '';
        if ($grupa != '') {

            $sql = "SELECT * FROM catalog_tree WHERE url='" . $grupa . "'";
            $res = mysql::query_one($sql);

            $_filter = " and catalog.id_parent=" . $res->id;
        }

        // строим запрос
        $_sql = 'SELECT
						type_tovar.*
					FROM
						catalog
					Inner Join type_tovar ON catalog.has_type = type_tovar.id
					WHERE
						catalog.id IS NOT NULL and type_tovar.status=1 and type_tovar.id IS NOT NULL ' . $_filter . '
					GROUP BY
						type_tovar.id
					ORDER BY type_tovar.sort, id DESC
					LIMIT ' . $limit;

        // выполняем запрос + при необходимости выводим сам запрос
        $result = mysql::query($_sql, 0);
        $result['cat_uri'] = $grupa;

        return system::show_tpl(array('result' => $result), $name_file);
    }

    static function getCatFeatures($category) {

        $show_filter = true; // TO DO count products in category
        if (!$show_filter) {
            return array();
        }

        $filters = array();
        $products_sql = 'SELECT catalog.brand,
									catalog.serie,
									catalog.has_type,
									catalog.tray_depth,
									catalog.cabin_form,
									catalog.color,
									catalog.mounting_type,
									catalog.mixer_style,
									catalog.mixer_type,
									catalog.toilet_mount,
									catalog.bathtub_form
							FROM catalog
							WHERE catalog.status = \'1\' AND catalog.id_parent = ' . $category->id;
        $products_res = mysql::query($products_sql, 0);
        if (!count($products_res)) {
            return array();
        }

        $brands_ids = array();
        $series_ids = array();
        $types_ids = array();
        $bathtub_form_ids = array();
        $toilet_mount_ids = array();
        $tray_depth_ids = array();
        $cabin_form_ids = array();
        $color_ids = array();
        $mounting_type_ids = array();
        $mixer_style_ids = array();
        $mixer_type_ids = array();
        foreach ($products_res as $product) {
            if ($product->brand) {
                $brands_ids[$product->brand] = $product->brand;
            }
            if ($product->serie) {
                $series_ids[$product->serie] = $product->serie;
            }
            if ($product->has_type) {
                $types_ids[$product->has_type] = $product->has_type;
            }

            if ($product->bathtub_form) {
                $bathtub_form_ids[$product->bathtub_form] = $product->bathtub_form;
            }
            if ($product->toilet_mount) {
                $toilet_mount_ids[$product->toilet_mount] = $product->toilet_mount;
            }

            if ($product->tray_depth) {
                $tray_depth_ids[$product->tray_depth] = $product->tray_depth;
            }
            if ($product->cabin_form) {
                $cabin_form_ids[$product->cabin_form] = $product->cabin_form;
            }

            if ($product->color) {
                $color_ids[$product->color] = $product->color;
            }
            if ($product->mounting_type) {
                $mounting_type_ids[$product->mounting_type] = $product->mounting_type;
            }
            if ($product->mixer_style) {
                $mixer_style_ids[$product->mixer_style] = $product->mixer_style;
            }
            if ($product->mixer_type) {
                $mixer_type_ids[$product->mixer_type] = $product->mixer_type;
            }
        }

        if (count($brands_ids)) {
            $brands_sql = 'SELECT brand.id, brand.pole as name FROM brand
					WHERE brand.status = \'1\' AND brand.id IN (' . implode(',', $brands_ids) . ')';
            $brands = mysql::query($brands_sql, 0);
            if (count($brands)) {
                $filters['brands'] = $brands;
            }
        }

        if (count($series_ids)) {
            $series_sql = 'SELECT serie.id, serie.name, serie_brand.id_brand FROM serie
 									LEFT JOIN serie_brand ON serie_brand.id_serie = serie.id
									WHERE serie.status = \'1\' AND serie_brand.id_brand IS NOT NULL AND serie.id IN (' . implode(', ', $series_ids) . ')';
            $series = mysql::query($series_sql, 0);
            if (count($series)) {
                foreach ($series as $series_item) {
                    $filters['series'][$series_item->id_brand][] = $series_item;
                }
            }
        }

        if ($category->has_type) {
            if (count($types_ids)) {
                $types_sql = 'SELECT id, name FROM type_tovar WHERE status = \'1\' AND id IN (' . implode(', ', $types_ids) . ')';
                $types = mysql::query($types_sql, 0);
                if (count($types)) {
                    $filters['type'] = $types;
                }
            }
        }

        if ($category->has_bathtub) {
            if (count($bathtub_form_ids)) {
                $bathtub_form_sql = 'SELECT id, name FROM dict_bathtub_form WHERE status =  \'1\' AND id IN (' . implode(', ', $bathtub_form_ids) . ')';
                $bathtub_forms = mysql::query($bathtub_form_sql, 0);
                if (count($bathtub_forms)) {
                    $filters['bathtub-form'] = $bathtub_forms;
                }
            }
        }

        if ($category->has_toilet) {
            if (count($toilet_mount_ids)) {
                $toilet_mount_sql = 'SELECT id, name FROM dict_toilet_mount WHERE status =  \'1\' AND id IN (' . implode(', ', $toilet_mount_ids) . ')';
                $toilet_mounts = mysql::query($toilet_mount_sql, 0);
                if (count($toilet_mounts)) {
                    $filters['toilet-mount'] = $toilet_mounts;
                }
            }
        }

        if ($category->has_shower) {
            if (count($tray_depth_ids)) {
                $tray_depth_sql = 'SELECT id, name FROM dict_tray_depth WHERE status =  \'1\' AND id IN (' . implode(', ', $tray_depth_ids) . ')';
                $tray_depths = mysql::query($tray_depth_sql, 0);
                if (count($tray_depths)) {
                    $filters['tray-depth'] = $tray_depths;
                }
            }
            //-----------------------------
            if (count($cabin_form_ids)) {
                $cabin_form_sql = 'SELECT id, name FROM dict_cabin_form WHERE status =  \'1\' AND id IN (' . implode(', ', $cabin_form_ids) . ')';
                $cabin_forms = mysql::query($cabin_form_sql, 0);
                if (count($cabin_forms)) {
                    $filters['cabin-form'] = $cabin_forms;
                }
            }
        }

        if ($category->has_mixer) {
            if (count($color_ids)) {
                $color_sql = 'SELECT id, name FROM dict_colors WHERE status = \'1\' AND id IN (' . implode(', ', $color_ids) . ')';
                $colors = mysql::query($color_sql, 0);
                if (count($colors)) {
                    $filters['color'] = $colors;
                }
            }

            if (count($mounting_type_ids)) {
                $mounting_type_sql = 'SELECT id, name FROM dict_mounting_type WHERE status = \'1\' AND id IN (' . implode(', ', $mounting_type_ids) . ')';
                $mounting_types = mysql::query($mounting_type_sql, 0);
                if (count($mounting_types)) {
                    $filters['mounting-type'] = $mounting_types;
                }
            }

            if (count($mixer_style_ids)) {
                $mixer_style_sql = 'SELECT id, name FROM dict_mixer_style WHERE status = \'1\' AND id IN (' . implode(', ', $mixer_style_ids) . ')';
                $mixer_styles = mysql::query($mixer_style_sql, 0);
                if (count($mixer_styles)) {
                    $filters['mixer-style'] = $mixer_styles;
                }
            }

            if (count($mixer_type_ids)) {
                $mixer_type_sql = 'SELECT id, name FROM dict_mixer_type WHERE status = \'1\' AND id IN (' . implode(', ', $mixer_type_ids) . ')';
                $mixer_types = mysql::query($mixer_type_sql, 0);
                if (count($mixer_types)) {
                    $filters['mixer-type'] = $mixer_types;
                }
            }
        }

        return $filters;
    }

    static function get_filter_block($tpl_name) {
        if (!$_GET['grupa']) {
            return false;
        }
        $category = mysql::query_one('SELECT * FROM catalog_tree WHERE url = "' . $_GET['grupa'] . '"');
        if (!$category) {
            return false;
        }

        $show_filter = true; // TO DO count products in category
        if (!$show_filter) {
            return false;
        }

        $filters = array();
        $chosen = array();
        if (isset($_GET['filter'])) {
            $arr = explode(';', $_GET['filter']);
            if (count($arr)) {
                foreach ($arr as $value) {
                    $tmp = explode('_', $value);
                    if ($tmp[0] == 'cost') {
                        $chosen[$tmp[0]] = explode('-', $tmp[1]);
                    } else {
                        $chosen[$tmp[0]] = explode(',', $tmp[1]);
                    }
                }
            }
        }

        $products_sql = 'SELECT catalog.brand,
									catalog.has_type,
									catalog.tray_depth,
									catalog.cabin_form,
									catalog.color,
									catalog.mounting_type,
									catalog.mixer_style,
									catalog.mixer_type,
									catalog.toilet_mount,
									catalog.bathtub_form
							FROM catalog
							WHERE catalog.status = \'1\' AND catalog.id_parent = ' . $category->id;
        $products_res = mysql::query($products_sql, 0);
        if (!count($products_res)) {
            return false;
        }

        $brands_ids = array();
        $types_ids = array();
        $bathtub_form_ids = array();
        $toilet_mount_ids = array();
        $tray_depth_ids = array();
        $cabin_form_ids = array();
        $color_ids = array();
        $mounting_type_ids = array();
        $mixer_style_ids = array();
        $mixer_type_ids = array();
        foreach ($products_res as $product) {
            if ($product->brand) {
                $brands_ids[$product->brand] = $product->brand;
            }
            if ($product->has_type) {
                $types_ids[$product->has_type] = $product->has_type;
            }

            if ($product->bathtub_form) {
                $bathtub_form_ids[$product->bathtub_form] = $product->bathtub_form;
            }
            if ($product->toilet_mount) {
                $toilet_mount_ids[$product->toilet_mount] = $product->toilet_mount;
            }

            if ($product->tray_depth) {
                $tray_depth_ids[$product->tray_depth] = $product->tray_depth;
            }
            if ($product->cabin_form) {
                $cabin_form_ids[$product->cabin_form] = $product->cabin_form;
            }

            if ($product->color) {
                $color_ids[$product->color] = $product->color;
            }
            if ($product->mounting_type) {
                $mounting_type_ids[$product->mounting_type] = $product->mounting_type;
            }
            if ($product->mixer_style) {
                $mixer_style_ids[$product->mixer_style] = $product->mixer_style;
            }
            if ($product->mixer_type) {
                $mixer_type_ids[$product->mixer_type] = $product->mixer_type;
            }
        }

        $brands_sql = 'SELECT brand.id, brand.pole as name FROM brand
					WHERE brand.status = \'1\' AND brand.id IN (' . implode(',', $brands_ids) . ')
					ORDER BY brand.sort';
        $brands = mysql::query($brands_sql, 0);
        if (count($brands)) {
            $filters['brands']['name'] = 'Производители';
            $filters['brands']['features'] = $brands;
        }

        if (count($chosen['brands'])) {
            // строим запрос
            $series_ids_sql = 'SELECT catalog.serie FROM catalog
					WHERE catalog.status= \'1\' AND catalog.serie IS NOT NULL AND catalog.brand IN (' . implode(', ', $chosen['brands']) . ') AND catalog.id_parent = ' . $category->id . '
					GROUP BY catalog.serie';
            $series_ids_result = mysql::query($series_ids_sql, 0);
            if (count($series_ids_result)) {
                $series_ids = array();
                foreach ($series_ids_result as $value) {
                    $series_ids[] = $value->serie;
                }
                $series_sql = 'SELECT id, name FROM serie WHERE status = \'1\' AND id IN (' . implode(', ', $series_ids) . ') ORDER BY sort, id DESC';
                $series = mysql::query($series_sql, 0);
                if (count($series)) {
                    $filters['series']['name'] = 'Серия';
                    $filters['series']['features'] = $series;
                }
            }
        }

        if ($category->has_type) {
            if (count($types_ids)) { // $types_ids_result
                $types_sql = 'SELECT id, name FROM type_tovar WHERE status = \'1\' AND id IN (' . implode(', ', $types_ids) . ') ORDER BY sort, id DESC';
                $types = mysql::query($types_sql, 0);
                if (count($types)) {
                    $filters['type']['name'] = 'Предназначение';
                    $filters['type']['features'] = $types;
                }
            }
        }

        if ($category->has_bathtub) {
            if (count($bathtub_form_ids)) { // $bathtub_form_ids_result
                $bathtub_form_sql = 'SELECT id, name FROM dict_bathtub_form WHERE status = \'1\' AND id IN (' . implode(', ', $bathtub_form_ids) . ') ORDER BY sort DESC';
                $bathtub_forms = mysql::query($bathtub_form_sql, 0);
                if (count($bathtub_forms)) {
                    $filters['bathtub-form']['name'] = 'Форма ванны';
                    $filters['bathtub-form']['features'] = $bathtub_forms;
                }
            }
        }

        if ($category->has_toilet) {
            if (count($toilet_mount_ids)) { // $toilet_mount_ids_result
                $toilet_mount_sql = 'SELECT id, name FROM dict_toilet_mount WHERE status = \'1\' AND id IN (' . implode(', ', $toilet_mount_ids) . ') ORDER BY sort DESC';
                $toilet_mounts = mysql::query($toilet_mount_sql, 0);
                if (count($toilet_mounts)) {
                    $filters['toilet-mount']['name'] = 'Тип монтажа унитаза';
                    $filters['toilet-mount']['features'] = $toilet_mounts;
                }
            }
        }

        if ($category->has_shower) {
            if (count($tray_depth_ids)) { // $tray_depth_ids_result
                $tray_depth_sql = 'SELECT id, name FROM dict_tray_depth WHERE status = \'1\' AND id IN (' . implode(', ', $tray_depth_ids) . ') ORDER BY sort DESC';
                $tray_depths = mysql::query($tray_depth_sql, 0);
                if (count($tray_depths)) {
                    $filters['tray-depth']['name'] = 'Глубина поддона';
                    $filters['tray-depth']['features'] = $tray_depths;
                }
            }

            //-----------------------------

            if (count($cabin_form_ids)) { // $cabin_form_ids_result
                $cabin_form_sql = 'SELECT id, name FROM dict_cabin_form WHERE status = \'1\' AND id IN (' . implode(', ', $cabin_form_ids) . ') ORDER BY sort DESC';
                $cabin_forms = mysql::query($cabin_form_sql, 0);
                if (count($cabin_forms)) {
                    $filters['cabin-form']['name'] = 'Форма кабины';
                    $filters['cabin-form']['features'] = $cabin_forms;
                }
            }
        }

        if ($category->has_mixer) {

            if (count($color_ids)) { // $color_ids_result
                $color_sql = 'SELECT id, name FROM dict_colors WHERE status = \'1\' AND id IN (' . implode(', ', $color_ids) . ') ORDER BY sort DESC';
                $colors = mysql::query($color_sql, 0);
                if (count($colors)) {
                    $filters['color']['name'] = 'Цвет';
                    $filters['color']['features'] = $colors;
                }
            }

            if (count($mounting_type_ids)) { // $mounting_type_ids_result
                $mounting_type_sql = 'SELECT id, name FROM dict_mounting_type WHERE status = \'1\' AND id IN (' . implode(', ', $mounting_type_ids) . ') ORDER BY sort DESC';
                $mounting_types = mysql::query($mounting_type_sql, 0);
                if (count($mounting_types)) {
                    $filters['mounting-type']['name'] = 'Тип монтажа';
                    $filters['mounting-type']['features'] = $mounting_types;
                }
            }

            if (count($mixer_style_ids)) { // $mixer_style_ids_result
                $mixer_style_sql = 'SELECT id, name FROM dict_mixer_style WHERE status = \'1\' AND id IN (' . implode(', ', $mixer_style_ids) . ') ORDER BY sort DESC';
                $mixer_styles = mysql::query($mixer_style_sql, 0);
                if (count($mixer_styles)) {
                    $filters['mixer-style']['name'] = 'Стиль смесителя';
                    $filters['mixer-style']['features'] = $mixer_styles;
                }
            }

            if (count($mixer_type_ids)) { // $mixer_type_ids_result
                $mixer_type_sql = 'SELECT id, name FROM dict_mixer_type WHERE status = \'1\' AND id IN (' . implode(', ', $mixer_type_ids) . ') ORDER BY sort DESC';
                $mixer_types = mysql::query($mixer_type_sql, 0);
                if (count($mixer_types)) {
                    $filters['mixer-type']['name'] = 'Тип смесителя';
                    $filters['mixer-type']['features'] = $mixer_types;
                }
            }
        }

        return system::show_tpl(array(
                    'category' => $category,
                    'filters' => $filters,
                    'chosen' => $chosen,
                        ), $tpl_name);
    }

    static function generateInput($key, $feature, $category_alias, $chosen = array()) {
        $input = '';
        if ($key == 'brands') {
            $input .= '<div class="s_inp">';
            $input .= '<a class="filter-link" id="' . $key . $feature->id . '" href="' . self::generateLinkWithFilter($key, $feature->id, $category_alias, $chosen) . '">';
            $input .= '<input type="checkbox" data-id="' . $feature->id . '" data-filter="' . $key . '" class="checkboxFilter" class="checkboxFilter" ' . catalog::isCheckedFilter($key, $feature->id, $chosen) . ' />';
            $input .= '<label class="brandAjax" data-id="' . $feature->id . '">' . $feature->name . '<span class="vsplivayka"></span></label>';
            $input .= '</a>';
            $input .= '</div>';
        } else {
            $input .= '<div class="s_inp">';
            $input .= '<a class="filter-link" id="' . $key . $feature->id . '" href="' . self::generateLinkWithFilter($key, $feature->id, $category_alias, $chosen) . '">';
            $input .= '<input type="checkbox" data-id="' . $feature->id . '" data-filter="' . $key . '" class="checkboxFilter" ' . catalog::isCheckedFilter($key, $feature->id, $chosen) . ' />';
            $input .= '<label>' . $feature->name . '</label>';
            $input .= '</a>';
            $input .= '</div>';
        }

        return $input;
    }

    static function isCheckedFilter($key, $feature_id, $chosen = array()) {
        if (isset($chosen[$key])) {
            if (in_array($feature_id, $chosen[$key])) {
                return 'checked';
            }
        }
        return '';
    }

    static function generateLinkWithFilter($key, $feature_id, $category_alias, $chosen = array()) {
        $filter = array();
        $add_new = false;

        if (count($chosen)) {

            if (isset($chosen[$key]) && in_array($feature_id, $chosen[$key])) {
                
            } else {
                $add_new = true;
            }

            foreach ($chosen as $alias => $chosen_ones) {
                if (count($chosen_ones)) {
                    foreach ($chosen_ones as $sub_one) {
                        if ($alias == $key) {
                            if ($feature_id == $sub_one) {
                                
                            } else {
                                $filter[$alias][] = $sub_one;
                            }
                        } else {
                            $filter[$alias][] = $sub_one;
                        }
                    }
                }
            }
            if ($add_new) {
                $filter[$key][] = $feature_id;
            }
        } else {
            $filter[$key][] = $feature_id;
        }

        if (count($filter)) {
            $filter_link = self::sortFilter($filter);
            return '/catalog/grupa/' . $category_alias . '/filter/' . $filter_link;
        } else {
            return '/catalog/grupa/' . $category_alias;
        }
    }

    static function sortFilter($filter = array()) {
        $features_order = array(
            1 => 'brands',
            2 => 'series',
            3 => 'type',
            4 => 'bathtub-form',
            5 => 'toilet-mount',
            6 => 'tray-depth',
            7 => 'cabin-form',
            8 => 'color',
            9 => 'mounting-type',
            10 => 'mixer-style',
            11 => 'mixer-type',
            12 => 'cost',
            13 => 'width',
            14 => 'length',
        );
        $filter_sorted = array();
        foreach ($features_order as $order_item_alias) {
            if ($filter[$order_item_alias]) {
                $filter_sorted[$order_item_alias] = $filter[$order_item_alias];
                unset($filter[$order_item_alias]);
            }
        }
        if (count($filter)) {
            foreach ($filter as $key => $values) {
                $filter_sorted[$key] = $values;
            }
        }
        $filter_links = array();
        foreach ($filter_sorted as $key => $values) {

            if ($key == 'cost') {
                $filter_links[] = $key . '_' . implode('-', $values);
            } else {
                $filter_links[] = $key . '_' . implode(',', $values);
            }
        }
        return implode('/', $filter_links);
    }

    /**
     * выводим товары в специальном блоке / блоках
     *
     * @param имя шаблона для вывода 		$name_file
     * @param кол-во элементов в блоке 		$limit
     * @param статус элемента в блоке 		$what_find
     */
    static function get_goods_for_special_block($name_file, $limit, $what_find) {

        // строим запрос
        $_sql = 'SELECT * FROM catalog where status=1 and ' . $what_find . ' order by rand() limit ' . $limit;

        // выполняем запрос + при необходимости выводим сам запрос
        $result = mysql::query($_sql, 0);

        return system::show_tpl(array('result' => $result), $name_file);
    }

    static function get_catalog_ass_new($name_file, $limit, $id) {

        $_sql = 'SELECT * FROM catalog_ass WHERE id_catalog_who="' . $id . '"';
        $result = mysql::query($_sql, 0);

        $arr = array();
        foreach ($result as $r) {
            $arr[] = $r->id_catalog_with;
        }
        $_arr = implode(',', $arr);

        if ($_arr == '')
            $_arr = '0';

        $sql = "SELECT * FROM catalog WHERE id IN (" . $_arr . ") AND status=1 ORDER BY rand() LIMIT " . $limit;
        $res = mysql::query($sql, 0);

        return system::show_tpl(array('result' => $res), $name_file);
    }

    /**
     * выводим список брендов на главной
     *
     * @param имя шаблона для вывода 		$name_file
     * @param кол-во элементов в блоке 		$limit
     */
    static function get_brand($name_file, $limit, $grupa = '') {

        $_filter = '';
        if ($grupa != '') {

            $sql = "SELECT * FROM catalog_tree WHERE url='" . $grupa . "'";
            $res = mysql::query_one($sql);

            $_filter = " and catalog.id_parent=" . $res->id;
        }

        // строим запрос
        $_sql = 'SELECT brand.*
                    FROM
                            catalog
                    Inner Join brand ON catalog.brand = brand.id
                    WHERE
                            catalog.id IS NOT NULL and brand.status=1 and brand.id IS NOT NULL ' . $_filter . '
                    GROUP BY
                            brand.id
                    ORDER BY brand.sort
                    LIMIT ' . $limit;

        // выполняем запрос + при необходимости выводим сам запрос
        $result = mysql::query($_sql, 0);
        $result['cat_uri'] = $grupa;

        return system::show_tpl(array('result' => $result), $name_file);
    }

    static function get_series($name_file, $limit, $series) {

        return system::show_tpl(array('series' => $series), $name_file);
    }

    /**
     * выводит активный класс для значения кол-ва отображаемых записей на странице
     *
     * @param с чем сравниваем 			 $zna
     */
    function get_curent_at_page($zna) {

        if (isset($_SESSION['at_page']) and $_SESSION['at_page'] == $zna) {
            return 'class="cur"';
        } else {
            return '';
        }
    }

    /**
     * выводит активный класс для типа сортировки
     *
     * @param с чем сравниваем 			 $zna
     */
    function get_curent_type_sort($zna) {

        if (isset($_SESSION['type_sort']) and $_SESSION['type_sort'] == $zna) {
            return 'class="cur"';
        } else {
            return '';
        }
    }

    /**
     * ВЫВОДИМ тип сортировки и поле сортировки
     */
    function get_sort_at_page() {


        // тип сортироки
        if (!isset($_SESSION['type_sort'])) {
            $_SESSION['type_sort'] = "1";
        }

        if (isset($_GET['type_sort']) and intval($_GET['type_sort']) > 0) {
            $_SESSION['type_sort'] = $_GET['type_sort'];
        }

        switch ($_SESSION['type_sort']) {
            case 1:
                return "catalog.name ASC";
                break;
            case 2:
                return "catalog.name DESC";
                break;
            case 3:
                return "catalog.cost ASC";
                break;
            case 4:
                return "catalog.cost DESC";
                break;
            default:
                return "catalog.name ASC";
                break;
        }
    }

    /**
     * включаем фильтр товаров в данной группе
     */
    function get_filter() {

        if (isset($_GET['brand']) and intval($_GET['brand']) > 0) {
            $_SESSION['brand'] = $_GET['brand'];
        } else if (isset($_GET['brand']) and intval($_GET['brand']) == 0) {
            unset($_SESSION['brand']);
        }

        if (isset($_SESSION['brand'])) {
            return ' and catalog.brand=' . $_SESSION['brand'];
        } else {
            return '';
        }
    }

    /**
     * выводит активный класс для типа сортировки
     *
     * @param с чем сравниваем 			 $zna
     */
    function get_curent_brand($zna) {

        if (isset($_SESSION['brand']) and $_SESSION['brand'] == $zna) {
            return 'class="cur"';
        } else if (!isset($_SESSION['brand']) and $zna == 0) {
            // все бренды
            return 'class="cur"';
        } else {
            return '';
        }
    }

    public static function notice_change_price($item, $newItem) {
        if ($item->cost == $newItem->cost && $item->valuta == $newItem->valuta) {
            return false;
        }

        $_arr = mysql::query_one('select * from message where id=11');
        // елементы для замены
        $mass_element_for_parsing = array(
            '%product%',
            '%url%',
            '%site%',
            '%date%',
            '%old_price%',
            '%old_currency%',
            '%new_price%',
            '%new_currency%');
        // заменяеміе значения
        $url = '<a href="http://' . $_SERVER['HTTP_HOST'] . '/backend/catalog/new/id/' . $item->id . '" title="">Перейти к товару</a>';

        if ($item->valuta == 1) {
            $old_currency = 'грн';
        } elseif ($item->valuta == 2) {
            $old_currency = 'евро';
        } elseif ($item->valuta == 3) {
            $old_currency = 'доллар';
        }

        if ($newItem->valuta == 1) {
            $new_currency = 'грн';
        } elseif ($newItem->valuta == 2) {
            $new_currency = 'евро';
        } elseif ($newItem->valuta == 3) {
            $new_currency = 'доллар';
        }

        $mass_result_for_parsing = array(
            $item->name,
            $url,
            ADRESS_SITE,
            date('d-m-Y H:i:s'),
            $item->cost,
            $old_currency,
            $newItem->cost,
            $new_currency,
        );
        // парсим данные
        $message = parsing_data($mass_element_for_parsing, $mass_result_for_parsing, $_arr->text);

        // парсим заголовок письма
        $subject = replace_data($_arr->zag, '%site%', ADRESS_SITE);

        // отправляем мыло
        sent_email_new($GLOBALS['mailadmin'], nl2br($message), ADRESS_SITE, $subject);
    }

}

?>