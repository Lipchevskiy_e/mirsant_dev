<?php

/*
* core_2012
*/

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class gallery {


		/**
		* 	get gallery
		*	@param  int		$limit 		- count of news for return
		* 			text	$name_file 	- name of tamplate (plus path '/tpl/....') 
		*/	

		static function get_group_gallery($name_file, $limit=false) {
		
			$__add='';
			if (!isset($_GET['url'])) {
				$__add=' and id_parent=0';
			} else {
				// получаем группу галереи
				$_id_gallery_grupa=dbh::get_gallery_grupa_id($_GET['url']);
				$__add=' and id_parent='.$_id_gallery_grupa;
			}
			
			$__limit='';
			if ($limit) {
				$__limit='limit '.$limit;
			}
		
			$_sql='SELECT * 
					FROM gallery_grupa 
					WHERE status=1 '.$__add.' order by sort'.
					$__limit
				;

			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query($_sql,0);

			if($result) {
				return system::show_tpl(array('gallery'=>$result),$name_file);
			} else {
				return __('Нет альбомов');
			}
			
			
		}
		
		
		/**
		* 	get gallery
		*	@param  int		$limit 		- count of news for return
		* 			text	$name_file 	- name of tamplate (plus path '/tpl/....') 
		*/	

		static function get_gallery($limit=false, $name_file) {
		
			// получаем группу галереи
			$_id_gallery_grupa=dbh::get_gallery_grupa_id($_GET['url']);
			$__add=' and id_parent='.$_id_gallery_grupa;
			
			$__limit='';
			if ($limit) {
				$__limit=' limit '.$limit;
			}
			
			$_sql='SELECT * 
					FROM gallery
					WHERE status=1 '.$__add.' order by sort'.
					$__limit
				;

			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query($_sql,0);
			
			if($result) {
				return system::show_tpl(array('gallery'=>$result),$name_file);
			} else {
				return __('В данном альбоме нет изображений');
			}
			
		}		

		
		/**
		* 	get gallery first page
		*	@param  int		$limit 		- count of news for return
		* 			text	$name_file 	- name of tamplate (plus path '/tpl/....') 
		*/	

		static function get_gallery_first_page($name_file, $limit=false) {
		
			$__limit='';
			if ($limit) {
				$__limit='limit '.$limit;
			}
			
			$_sql='SELECT gallery.*, gallery_grupa.url 
					FROM gallery, gallery_grupa
					WHERE gallery.id_parent=gallery_grupa.id and gallery.status=1 and gallery.firstpage=1 
					order by rand() '.
					$__limit
				;

			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query($_sql,0);
			
			if($result) {
				return system::show_tpl(array('gallery'=>$result),$name_file);
			} else {
				return '';
			}
			
		}	
		
		
/**
		 * get count news at page
		 *
		 */
		static function count_gallery_at_page() {
	
			/*
			* проверяем на вібор пользователем отображения кол-ва на одной странице (НАЧАЛО)
			*/
			if (isset($_GET['gallery_at_page']))  {
				system::isset_numeric($_GET['gallery_at_page']); 
				$_SESSION['gallery_at_page']=$_GET['gallery_at_page'];
			}        
			
			/*
			* кол-во записей на одной странице 
			*/
			if (isset($_SESSION['gallery_at_page']))  {
				define ('GALLERY_AT_PAGE',$_SESSION['gallery_at_page']);
			} else {
				define ('GALLERY_AT_PAGE',FIRST_GALLERY_AT_PAGE);
			}
	
		}
		

		/**
		* 	get count of news
		*/	
		
		static function count_gallery() {
	
			// получаем группу галереи
			$_id_gallery_grupa=dbh::get_gallery_grupa_id($_GET['url']);
			
			if ($_id_gallery_grupa) {
			
				$_sql='SELECT count(*) as count FROM gallery where status=1 and id_parent='.$_id_gallery_grupa;
							
				// выполняем запрос + при необходимости выводим сам запрос
				return mysql::query_findpole($_sql,'count',0);
				
			}
			
		}			

}

?>