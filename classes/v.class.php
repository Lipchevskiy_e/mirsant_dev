<?php

/**
 *   @author Titenko Alexander (e-mail: alexvantiko@yandex.ru)
 */
class v {

    public static $i18n = array();
    public static $config = array();
    public static $models = array();

    /**
     * 	Get string in current languge.
     * 	@param  string		$key
     * 	@return string
     */
    static function getI18n($key) {
        return isset(self::$i18n[$key]) ? self::$i18n[$key] : $key;
    }

    /**
     * 	Get config value from $config array.
     * 	@param  string	$key	   	value key
     * 	@param  mixed	$default	default value
     * 	@return string
     */
    static function getConfig($key, $default = '') {
        return isset(self::$config[$key]) ? self::$config[$key] : v::getFrom(self::$config, $key, $default);
    }

    /**
     * 	Set system (config) value.
     * 	@param  string	$key	   	value key
     * 	@param  mixed	$value	value
     */
    static function setConfig($key, $value) {
        v::setTo(self::$config, $key, $value);
    }

    /**
     * 	Get array element value from by dir like key.
     * 	@param  array	$arr	   	array
     * 	@param  string	$key	   	value key
     * 	@param  mixed	$default	default value
     * 	@return string
     */
    static function getFrom($arr, $key, $default = '') {
        $path = explode('/', $key);
        $p = &$arr;

        foreach ($path as $s) {
            if (!isset($p[$s])) {
                return $default;
            }

            $p = &$p[$s];
        }

        return $p;
    }

    /**
     * 	Set array element value from by dir like key.
     * 	@param  array	$arr	   	array
     * 	@param  string	$key	   	key
     * 	@param  mixed	$value	value
     */
    static function setTo(&$arr, $key, $value) {
        $path = explode('/', $key);
        $p = &$arr;

        foreach ($path as $s) {
            if (!isset($p[$s])) {
                $p[$s] = '';
            }

            $p = &$p[$s];
        }

        $p = $value;
    }

    /**
     * 	Convert to valid string.
     * 	@param  string  	$value	value to convert
     * 	@param  string	$default	default value
     * 	@return string
     */
    static function toStr($value, $default = '') {
        // clean tags
        $str = strip_tags($value, '<b><i><u><br><p><em><strong><strike><ol><ul><li><a><img><blockquote><sub><sup><hr><font><div><span><table><tbody><tr><th><td>');
        $str = (!get_magic_quotes_gpc()) ? addslashes($str) : $str;

        return $str != '' ? $str : $default;
    }

    /**
     * 	Convert to valid word.
     * 	@param  string  	$value	value to convert
     * 	@param  string	$default	default value
     * 	@return string
     */
    static function toWord($value, $default = '') {
        $value = preg_replace('/\W/', '', $value);

        return $value != '' ? $value : $default;
    }

    /**
     * 	Convert to valid slug.
     * 	@param  string  $value	value to convert
     * 	@param  string	$default	default value
     * 	@return string
     */
    static function toSlug($value, $default = 'n-a') {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $value);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = v::translit($text);

        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        return $text != '' ? $text : $default;
    }

    /**
     * 	Convert to valid number.
     * 	@param  string  $value		value to convert
     * 	@param  int		$default	default value
     * 	@return int
     */
    static function toNum($value, $default = 0) {
        $value = preg_replace('/[^0-9\,\.\-]/', '', $value);

        if ($value !== '') {
            if (eregi('[\.\,]', $value)) {
                $value = floatval($value);
            } else {
                $value = intval($value);
            }
        } else {
            $value = $default;
        }

        return $value;
    }

    /**
     * 	Convert to array if it isn't.
     * 	@param  mixed	$value	value to convert
     * 	@return array
     */
    static function toArray($value) {
        return is_array($value) ? $value : array($value);
    }

    /**
     * 	Output data in json format.
     * 	@param  mixed	$value	value to convert
     * 	@return string
     */
    static function toJson($value) {
        return json_encode($value);
    }

    /**
     * 	Output var dump to string.
     * 	@param  array	$var	variable
     * 	@param  string	$title	variable title
     * 	@param  bool	$sort	if array than sort?
     * 	@param  bool	$first	for static function use only
     * 	@return string
     */
    static function dump($var, $title = '', $sort = false, $first = true) {
        $str = '';
        $size = '';

        if (is_object($var)) {
            $var = (array) $var;
        }

        if (is_array($var)) {
            $size = count($var);
        } else
        if (is_string($var)) {
            $size = strlen($var);

            if ($size > 200) {
                $var = v::cutStr($var, 20);
            }
        }

        $str .= '<li style="padding-left: 20px">' . ($title ? '[ <b style="color:#009">' . $title . '</b> ]' : '') . ' ( ' . gettype($var) . ' ' . ($size) . ') ';

        if (is_array($var)) {
            if ($sort) {
                asort($var);
            }

            if (count($var)) {
                $str .= '<ul style="padding-left: 20px">';
                foreach ($var as $k => $v) {
                    $str .= v::dump($v, is_numeric($k) ? $k + 1 : $k, $sort, false);
                }
                $str .= '</ul>';
            }
        } else {
            if (!is_object($var) and $var) {
                $str .= ' <b style="color:#090">' . htmlspecialchars(str_replace('=[', '\=\[', $var)) . '</b>';
            }
        }

        $str .= '</li>';

        if ($first && $str) {
            $str = '<ul style="padding: 0px">' . $str . '</ul>';
        }

        return $str;
    }

    /**
     *   TRANSLIT FUNCTION.
     *   Use to translit rus to eng chars
     *   @param  string	$str	string
     *   @return string
     */
    static function translit($str) {
        $str = mb_strtolower($str, 'utf8');

        $tlit = Array(
            Array("а", "a"),
            Array("б", "b"),
            Array("в", "v"),
            Array("г", "g"),
            Array("д", "d"),
            Array("е", "e"),
            Array("ё", "yo"),
            Array("ж", "zh"),
            Array("з", "z"),
            Array("и", "i"),
            Array("й", "j"),
            Array("к", "k"),
            Array("л", "l"),
            Array("м", "m"),
            Array("н", "n"),
            Array("о", "o"),
            Array("п", "p"),
            Array("р", "r"),
            Array("с", "s"),
            Array("т", "t"),
            Array("у", "u"),
            Array("ф", "f"),
            Array("х", "x"),
            Array("ц", "c"),
            Array("ч", "ch"),
            Array("ш", "sh"),
            Array("щ", "shh"),
            Array("ъ", ""),
            Array("ы", "y"),
            Array("ь", ""),
            Array("э", "je"),
            Array("ю", "ju"),
            Array("я", "ja")
        );

        for ($i = 0; $i < count($tlit); $i++)
            $str = str_replace($tlit[$i][0], $tlit[$i][1], $str);

        //$str = preg_replace('/ /', '_', $str);
        //$str = preg_replace('/[^\w\d]/', '', $str);

        return $str;
    }

    /**
     * 	Repair broken html tags
     * 	@param	string
     * 	@return string
     */
    static function pairTag($string) {
        $tags = Array('em', 'i', 'b', 'strong', 'div', 'span', 'p');

        $head = '';
        $tail = '';

        //array of tags we will keep paired, you may add another
        foreach ($tags as $tag) {
            $opentag = substr_count($string, '</' . $tag);

            // I don't put > in the opentag in case they may have an attribute
            $closetag = substr_count($string, '<' . $tag);

            $count = $opentag - $closetag;

            if ($count > 0) {
                $head = str_repeat('<' . $tag . '>', $count) . $head;
            }
        }

        //array of tags we will keep paired, you may add another
        foreach ($tags as $tag) {
            $opentag = substr_count($string, '<' . $tag);

            // I don't put > in the opentag in case they may have an attribute
            $closetag = substr_count($string, '</' . $tag . '>');

            $count = $opentag - $closetag;

            if ($count > 0) {
                $tail .= str_repeat('</' . $tag . '>', $count);
            }
        }

        return $head . $string . $tail;
    }

    /**
     * 	Cut string by words
     */
    static function cutStr($str, $words = 10, $end = '...') {
        $w = explode(' ', $str);
        $w = array_slice($w, 0, $words);
        return implode(' ', $w) . ' ' . $end;
    }

}

/**
 * Обертка для перевода
 * Добавлена 18.05.2011 (Amber)
 *
 */
if (!function_exists('__')) {

    function __($string) {
        return v::getI18n($string);
    }

}