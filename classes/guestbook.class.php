<?php

/*
* core_2012
*/

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class guestbook {


		/**
		* 	get guestbook
		*	@param  int		$limit 		- count of news for return
		* 			text	$name_file 	- name of tamplate (plus path '/tpl/....') 
		*/	

		static function get_guestbook($limit=false, $name_file) {
		
			$__limit='';
			if ($limit) {
				$__limit=' limit '.$limit;
			}
			
			$_sql='SELECT * 
					FROM guestbook
					WHERE status=1 order by id DESC'.
					$__limit
				;

			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query($_sql,0);
			
			if($result) {
				return system::show_tpl(array('guestbook'=>$result),$name_file);
			} else {
				return __('На данный момент отзывов нет');
			}
			
		}		

		
		/**
		* 	get guestbook first page
		*	@param  int		$limit 		- count of news for return
		* 			text	$name_file 	- name of tamplate (plus path '/tpl/....') 
		*/	

		static function get_guestbook_first_page($name_file, $limit=false) {
		
			$__limit='';
			if ($limit) {
				$__limit='limit '.$limit;
			}
			
			$_sql='SELECT *
					FROM guestbook
					WHERE status=1 
					order by rand() '.
					$__limit
				;

			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query($_sql,0);
			
			if($result) {
				return system::show_tpl(array('gallery'=>$result),$name_file);
			} else {
				return '';
			}
			
		}	
		
		
		/**
		 * get count news at page
		 *
		 */
		static function count_guestbook_at_page() {
	
			/*
			* проверяем на вібор пользователем отображения кол-ва на одной странице (НАЧАЛО)
			*/
			if (isset($_GET['guestbook_at_page']))  {
				system::isset_numeric($_GET['guestbook_at_page']); 
				$_SESSION['guestbook_at_page']=$_GET['guestbook_at_page'];
			}        
			
			/*
			* кол-во записей на одной странице 
			*/
			if (isset($_SESSION['guestbook_at_page']))  {
				define ('GUESTBOOK_AT_PAGE',$_SESSION['guestbook_at_page']);
			} else {
				define ('GUESTBOOK_AT_PAGE',FIRST_GUESTBOOK_AT_PAGE);
			}
	
		}
		

		/**
		* 	get count of news
		*/	
		
		static function count_guestbook() {
	
			$_sql='SELECT count(*) as count FROM guestbook where status=1';
						
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
				
		}			

}

?>