<?php

class general {

		/**
		 * вывод сообщения
		 *
		 * @param 0/1 $type
		 * @param текст сообщения $message
		 */
		static function messages ($type, $message){
			
			return system::show_tpl(array('type'=>$type,'msg'=>$message),'general/message.php');
			
		}
	
		/**
		 * show full link
		 * @param text $link
		 * @param text $text
		 * @return unknown
		 */
		
		static function link_to ($link, $text, $options=''){
			
			return '<a href="'.APPLICATION.'/'.$link.'" '.$options.'>'.$text.'</a>';
			
		}

		/**
		 * show link
		 * @param text $link
		 */
		
		static function link ($link){
			
			return APPLICATION.'/'.$link;
			
		}	
		
		
		/**
		 * проверяем на суперадмина
		 */
		static function sadmin () {	
			
			return $_SESSION['user_backend'] == 'superadmin' ? true : false;
			
		}
		
		/**
		 * префикс для tpl
		 */
		static function sadmin_tpl () {	
			
			return $_SESSION['user_backend'] == 'superadmin' ? '_sadmin' : '';
			
		}
		
		/**
		 * авторизация - суперадмин
		 */
		static function access() {
			if (isset($_GET['user_backend'])) {
				$_SESSION['user_backend']=$_GET['user_backend'];
			}
		}
		
		/**
		* переворачиваем дату для записи в таблицу
		 * @param date $_date
		*/
		static function date_to_database($_date) {
			return substr($_date,6,4)."-".substr($_date,3,2)."-".substr($_date,0,2);		
		}	
		
		/**
		* переворачиваем дату для вывода из таблицы на страницу
		* @param date $_date* 
		*/
		static function date_from_database($_date) {
			return substr($_date,8,2).".".substr($_date,5,2).".".substr($_date,0,4);		
		}
		
		
		/**
		 * проверяем на статус 0/1/2
		 * админка, Action=index 
		 */
		static function get_status_for_filter ($table_name) {	
			
			if (isset($_GET['menu'])) {
				unset($_SESSION['status']);
				unset($_SESSION['name']);
				unset($_SESSION['artikul']);
				unset($_SESSION['category']);
				unset($_SESSION['block_new']);
				unset($_SESSION['block_lider']);
				unset($_SESSION['id_parent']);
				unset($_SESSION['brand']);
				unset($_SESSION['photo']);
				unset($_SESSION['sklad']);
				unset($_SESSION['city']);				
			}
			$_res='';
			
			// для каталога
			if (isset($_SESSION['status']) and $_SESSION['status']<2) {
				$_res .= ' and '.$table_name.'.status="'.$_SESSION['status'].'"';
			}			
			
			// для позиции
			if (isset($_SESSION['category']) and $_SESSION['category']<100) {
				$_res .= ' and '.$table_name.'.category='.$_SESSION['category'];
			}			
			
			if (isset($_SESSION['name']) and $_SESSION['name']!='') {
				$_res .= ' and ('.$table_name.'.name LIKE "%'.$_SESSION['name'].'%" or '.$table_name.'.text_short LIKE "%'.$_SESSION['name'].'%")';
			}

			if (isset($_SESSION['artikul']) and $_SESSION['artikul']!='') {			
				$_res .= ' and '.$table_name.'.artikul="'.$_SESSION['artikul'].'"';
			}
			if (isset($_SESSION['block_new'])) {			
				$_res .= ' and '.$table_name.'.block_new='.$_SESSION['block_new'];
			}
			if (isset($_SESSION['block_lider'])) {			
				$_res .= ' and '.$table_name.'.block_lider='.$_SESSION['block_lider'];
			}
			if (isset($_SESSION['id_parent']) and $_SESSION['id_parent']!=0) {			
				$_res .= ' and '.$table_name.'.id_parent='.$_SESSION['id_parent'];
			}
			if (isset($_SESSION['brand']) and $_SESSION['brand']!=0) {			
				$_res .= ' and '.$table_name.'.brand='.$_SESSION['brand'];
			}
			if (isset($_SESSION['photo']) and $_SESSION['photo']<2) {			
				$_res .= ' and '.$table_name.'.is_photo='.$_SESSION['photo'];
			}
			if (isset($_SESSION['sklad']) and $_SESSION['sklad']<3) {			
				$_res .= ' and '.$table_name.'.sklad='.$_SESSION['sklad'];
			} 
			if (isset($_SESSION['city']) and $_SESSION['city']!=0) {			
				$_res .= ' and '.$table_name.'.id_city='.$_SESSION['city'];
			} 			
			
			// для групп каталога
			if (isset($_POST['name_group']) and $_POST['name_group']!='') {
				$_res .= ' and ('.$table_name.'.pole LIKE "%'.$_POST['name_group'].'%" or '.$table_name.'.text LIKE "%'.$_POST['name_group'].'%")';
			}		

			// для заказов
			if (isset($_POST['status_orders']) and $_POST['status_orders']<3) {
				$_res .= ' and '.$table_name.'.status='.$_POST['status_orders'];
			}
			if (isset($_POST['date_s']) and $_POST['date_s']!='') {
				$_res .= ' and '.$table_name.'.created_at>="'.$_POST['date_s'].'"';
			}
			if (isset($_POST['date_po']) and $_POST['date_po']!='') {
				$_res .= ' and '.$table_name.'.created_at<="'.$_POST['date_po'].'"';
			}			
			
			return $_res;
			
		}
		
		
		/**
		 * проверяем на фильтры
		 */
		static function get_for_filter ($table_name, $_data_post) {	
			
			if (isset($_POST[$_data_post])) {
				if ($_POST[$_data_post]==0) {
					return '';
				} else {
					return ' and '.$table_name.'.'.$_data_post.'='.$_POST[$_data_post];
				}
			} else {
				return '';
			}
			
			
		}		
		
		
		/**
		 * если в сессии есть сообщения - выводим его
		 */
		static function global_massage () {	
			
			$_global_message='';
			
			if (isset($_SESSION['GLOBAL_MESSAGE'])) {
				$_global_message=$_SESSION['GLOBAL_MESSAGE'];
				unset($_SESSION['GLOBAL_MESSAGE']);
			}			
			
			return $_global_message;
			
		}
		
		
		
		
		/**
		 * если в сессии есть сообщения - выводим его
		 */
		static function center () {	
		
			$_str=array();
	
			// проверяем action
			if (isset($_GET['action']) and $_GET['action']=="contact") {

				require HOST."/ctrl/contact/contact.php";						
					
			} else if (isset($_GET['action']) and file_exists(HOST."/ctrl/".$_GET['action']."/".$_GET['action'].".php")) {
					
					// не пускаей в разделы которые - не активный в данной версии CMS
					/*
					$_id=___findpole('select id from menu where target="'.$_GET['action'].'"', 'id');
					if (intval($_id)>0) {
						$__status=___findpole('select status from menu where id="'.$_id.'"', 'status');
						if ($__status==0) {
							die (get_tpl('/tpl/frontend/mainpage/404.tpl.php'));
							//echo "<meta http-equiv='refresh' content='0;URL=$putt/index.php'>";
							//die();
						}
					}
					*/
					// универсальное правило для стандартных модулей
					require HOST."/ctrl/".$_GET['action']."/".$_GET['action'].".php";
					
	
			} else if(@if_content($_GET['action'])) {
					
					// проверяем на Content разделы
					// т.е. выводим админразделы если таковые имеются	
					
					// добавляем возможность распечатать
					if (_PRINT==1) {
						$_str.=get_tpl("/tpl/mainpage/print.tpl.html");				
					}
					
					/*
					* вставляем проверку на доступ к разделам, которые заблокированы
					*/
					if (!get_access()) {
						$_str.='<span class="red">Доступ запрещен!</span>';
					} else {
						$_str=get_content($_GET['action']);
					}
					
			} else {
				header('HTTP/1.1 404 Not Found');
				die (get_tpl('/tpl/frontend/mainpage/404.tpl.php'));
			}

			return $_str;		

		}
		
		
		
		/**
		 * В шаблонах может использоваться механизм выделения
		 * активного раздела меню, в виде стиля
		 * этот класс специально создан для того, чтобы парсить метки со стилями
		 * не засоряя код файла index.php
		 *
		 * Amber (20-05-2011)
		 * Добавлена подсветка для типов новостей
		 * Пример:
		 * echo general::active_menu('news', 0);
		 * echo general::active_menu('news', 1);
		 *
		 * AntiX (09-08-2011)
		 * Добавлена подсветка меню для нескольких действий
		 * Пример:
		 * echo general::active_menu('arhiv,gallery');
		 */
		function active_menu ($act_str, $_type=0)
		{
			if (!isset($_GET['action'])) {
				return '';
			}
			$arr=explode(',',$act_str);
			for ($i=0;$i<count($arr);$i++)
			{
			 $_punkt_menu=$arr[$i];
			 $action = $_GET['action'];

			 // Проверяем на подразделы
			 $_sql = "SELECT `id_parent` FROM `content` WHERE `action`='$action'";
			 $rs1  = mysql_db_query($GLOBALS["dbname"], $_sql);
			 if (mysql_numrows($rs1)) {
		 		$f1   = mysql_fetch_array($rs1);
	 			$_sql = "SELECT `action` FROM `content` WHERE `id`=".$f1['id_parent'];
				$rs1  = mysql_db_query($GLOBALS["dbname"], $_sql);
				$f1   = mysql_fetch_array($rs1);
				if ($f1) {
					$action = $f1['action'];
				}
			 }
			 
			 if ($action==$_punkt_menu)
			 {
			  if (isset($_GET['type']))
			  {
			   if ($_GET['type']==$_type)
			   {
			    return ' class="cur"';
			   }
			  } else return ' class="cur"';
			 }
		    }
		}



		// получаем расширение файла и возвращаем соответсввующую иконку
		static function get_icon_for_file($file_name) {
				
				$ext=end(explode(".", $file_name));
				
				switch($ext) // переключающее выражение
				{
				   case 'doc': 
					  $_img_name='word.png';
				   break;
				   
				   case 'xls': 
					  $_img_name='excel.png';
				   break;

				   case 'jpg': 
					  $_img_name='jpg.png';
				   break;
				   
				   case 'gif': 
					  $_img_name='gif.png';
				   break;

				   case 'png': 
					  $_img_name='png.png';
				   break;			   

				   case 'pdf': 
					  $_img_name='pdf.png';
				   break;			   
				   
				   default:		 
					  $_img_name='no.png';
				}
				
				return '<img src="/images/_icon/'.$_img_name.'" class="middle">';
		
		}
		
		
		// получаем расширение файла и возвращаем соответсввующую иконку
		static function get_file_ext($file_name) {
				
				return end(explode(".", $file_name));
				
		}	


		/*
		*	выводим все подпункты согласно ID
		*/
		function get_all_punkt_menu($id_main_menu)
		{

			// выполняем запрос + при необходимости выводим сам запрос
			$_sql = "SELECT * FROM `content` WHERE `id_parent`=$id_main_menu AND `status`=1 ORDER BY `sort`";
			$result=mysql::query($_sql,0);
			
			if($result) {
				return system::show_tpl(array('result'=>$result),'frontend/mainpage/get_all_punkt_menu.php');
			} else {
				return false;
			}

		}
		
		
}


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------






?>