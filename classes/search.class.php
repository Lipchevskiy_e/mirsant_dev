<?php

class Search {

    public function __construct($word = false) {
        $this->word = $word;
    }

    function get_content() {
        $this->change_search();

        if ($this->get_data_search()) {
            return system::show_tpl(array(), 'frontend/search/yessearch.php') . $this->get_data_search();
        } else {
            return system::show_tpl(array(), 'frontend/search/nosearch.php');
        }
    }

    protected function change_search() {
        if (isset($_GET['word']) and $_GET['word'] != $this->word) {
            $this->word = $_GET['word'];
        }
    }

    function get_data_search($str = '') {
        $str .= $this->get_items();
        return $str;
    }

    function get_items() {
        return system::show_tpl(array('items' => $this->search2('catalog', array('name', 'text_full', 'text_short', 'charstik'))), 'frontend/search/items.php');
    }

    function search2($table_name, $fields) {
        $f = $this->fields($fields);

        $sql = 'SELECT `catalog`.*
                    FROM `catalog`
                    WHERE `catalog`.`name` LIKE "%' . $this->word . '%"
                    OR `catalog`.`artikul` LIKE "%'. $this->word .'%"    
                    AND `catalog`.`status` = 1 LIMIT 10'; // LIMIT '.$limit;

        $res = mysql::query($sql, 0);
        return $res;
    }

    function fields($fields, $str = '') {
        foreach ($fields as $field) {
            $str .= ' catalog.' . $field . ' like "%' . $this->word . '%" or ';
        }

        $str = substr($str, count($str) - 1, count($str) - 4);

        return $str;
    }

    function get_counts_search() {
        return system::show_tpl(
                        array(
                        ), 'frontend/search/count_search.php');
    }

    private $word;

    static function no_search() {
        return 'Не указаны параметры поиска!';
    }

}