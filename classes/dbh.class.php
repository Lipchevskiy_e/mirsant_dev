<?php

class dbh {

		/**
		*  получаем имя родителя контент
		*	@param  int parent_id
		*/	
		
		static function content_parent_name ($parent_id){
			
			return mysql::get_parent_name('content',$parent_id,'name');
			
		}
		
		
		
		
		/**
		*  получаем кол-во детей контент
		*	@param  int parent_id
		*/	
		
		static function content_count_child ($parent_id){
			
			$_sql='SELECT count(*) as count FROM content where id_parent='.$parent_id;
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}	
		
		
		
		/**
		*  получаем кол-во детей группы галереи
		*	@param  int parent_id
		*/	
		
		static function gallery_grupa_count_child ($parent_id){
			
			$_sql='SELECT count(*) as count FROM gallery_grupa where id_parent='.$parent_id;
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}			

		/**
		*  получаем имя родителя галереи
		*	@param  int parent_id
		*/	
		
		static function gallery_gb_parent_name ($parent_id){
			
			return mysql::get_parent_name('gallery_grupa',$parent_id,'pole');
			
		}		
		
		
		/**
		 * получаем детей для меню
		*	@param  int parent_id
		 */
		static function menu_get_child($id_parent) {
			
			// только для СуперАдмина
			if (!general::sadmin()) { return general::messages(0,v::getI18n('backend_orror_access')); }
			
			// строим запрос
			$_sql="SELECT * FROM menu where id_parent=".$id_parent." order by zindex";
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);
			
		}
		

		
		/**
		 * получаем все id таблицы menu в виде массива
		 */
		static function menu_get_id() {
			
			// только для СуперАдмина
			if (!general::sadmin()) { return general::messages(0,v::getI18n('backend_orror_access')); }
			
			// строим запрос
			$_sql="SELECT id FROM menu order by zindex";
			
			$result=mysql::query_only($_sql,0);
			
			$a=array();
			while ($obj = mysql_fetch_array($result)) {
				$a[]=$obj['id'];
			}			
			
			return $a;
			
		}		
		
		
		/**
		*  получаем кол-во детей групп товаров
		*	@param  int parent_id
		*/	
		
		static function catalog_tree_count_child ($parent_id){
			
			$_sql='SELECT count(*) as count FROM catalog_tree where id_parent='.$parent_id;
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}		
		

		
		/**
		*  получаем кол-во детей товаров
		*	@param  int parent_id
		*/	
		
		static function catalog_count_child ($parent_id){
			
			$_sql='SELECT count(*) as count FROM catalog where id_parent='.$parent_id;
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}				
		
		
		
		/**
		 * получаем детей для групп каталога
		*	@param  int parent_id
		 */
		static function catalog_tree_get_child($id_parent) {
			
			// строим запрос
			//$_sql='SELECT * FROM catalog_tree where id_parent='.$id_parent.' '.general::get_status_for_filter('catalog_tree').' order by sort';
			$_sql='SELECT * FROM catalog_tree where id_parent='.$id_parent.' and status=1 order by sort';
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);
			
		}
		
		static function catalog_tree_get_child2($id_parent) {
			
			// строим запрос
			//$_sql='SELECT * FROM catalog_tree where id_parent='.$id_parent.' '.general::get_status_for_filter('catalog_tree').' order by sort';
			$_sql='SELECT * FROM catalog_tree where id_parent='.$id_parent.' order by sort';
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);
			
		}
		
		
		/**
		 * получаем детей для групп каталога
		*	@param  int parent_id
		 */
		static function gallery_grupa_get_child($id_parent) {
			
			// строим запрос
			$_sql='SELECT * FROM gallery_grupa where id_parent='.$id_parent.' and status=1 order by sort';
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);
			
		}

		
		/**
		*  получаем имя группы каталога товаров
		*	@param  int parent_id
		*/	
		
		static function catalog_tree_name ($parent_id){
			
			return mysql::get_parent_name('catalog_tree',$parent_id,'pole');
			
		}		
		

		
		/**
		*  получаем список брендов
		*/	
		
		static function get_brand (){
			
			// строим запрос
			$_sql="SELECT id, pole FROM brand where status=1 order by sort";			

			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);			
			
		}	


		/**
		*  получаем список брендов
		*/	
		
		static function get_city (){
			
			// строим запрос
			$_sql="SELECT id, pole FROM city where status=1 order by sort";			

			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);			
			
		}	
		
		

		/**
		*  получаем дерево
		*/	
		
		static function get_catolog_tree (){
			
			// строим запрос
			$_sql="SELECT * FROM catalog_tree where id_parent=0 order by sort desc";
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);				
			
		}	

				/**
		*  получаем дерево
		*/	
		
		static function get_gallery_grupa_tree (){
			
			// строим запрос
			$_sql="SELECT * FROM gallery_grupa where id_parent=0 order by sort";
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);				
			
		}	
		
		
		
		/**
		 * получаем родителей для групп каталога
		*	@param  int parent_id
		 */
		static function catalog_tree_get_parent($id_parent) {
			
			// строим запрос
			$_sql="SELECT * FROM catalog_tree where id=".$id_parent." order by sort";
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);
			
		}
		
		
		/**
		 * получаем родителей для групп каталога
		*	@param  int parent_id
		 */
		static function gallery_grupa_get_parent($id_parent) {
			
			// строим запрос
			$_sql="SELECT * FROM gallery_grupa where id=".$id_parent." order by sort";
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);
			
		}		
		

		
		/**
		 * получаем детей для раздела "Содержание"
		*	@param  int parent_id
		 */
		static function content_get_child($id_parent) {
			
			// строим запрос
			$_sql='SELECT * FROM content where id_parent='.$id_parent.' '.general::get_status_for_filter('content').' order by sort';
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);
			
		}		
		
		
		/**
		 * получаем кол-во товаров в каталоге
		 */
		static function catalog_get_total_count() {
			
			// строим запрос
			$_sql='SELECT count(*) as count FROM catalog where id>0 '.general::get_status_for_filter('catalog');
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);
			
			return $result->count;
		
			
		}		
		
		
		/**
		 * получаем кол-во в таблице где статус=0
		 */
		static function get_count_status_off($tablename) {
			
			// строим запрос
			$_sql='SELECT count(*) as count FROM '.$tablename.' where status=0';
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);
			
			return $result->count;
		
			
		}		
		
		
		/**
		 * получаем поле статус
		 */
		static function get_tablename_status($tablename) {
			
			// строим запрос
			$_sql='SELECT status FROM '.$tablename;
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);
			
			return $result->status;
		
			
		}	
		
		
		
		/**
		 * получаем поле статус
		 */
		static function get_gallery($id_gallerygrupa) {
			
			// строим запрос
			$_sql='SELECT * FROM gallery WHERE id_parent='.$id_gallerygrupa.' order by sort';
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);
			
			
		}			
	
		/**
		* проверяем есть ли фото у данного товара 
		*/
		static function is_good_has_photo($id) {
		
			if (file_exists(HOST.IMG_CATALOG_PATH.'/01/'.$id.'_1.jpg')) {
				$_is=true;			
			} else {
				$_is=false;
			}
			
			// update поле is_photo
			if ($_is) {
				$_sql="UPDATE `catalog` SET `is_photo`='1' WHERE (`id`='".$id."')";
			} else {
				$_sql="UPDATE `catalog` SET `is_photo`='0' WHERE (`id`='".$id."')";
			}
			$select=mysql::just_query($_sql,0);		
			
		}
		
		/**
		 * кол-во галерей в контентовом разделе 
		 */
		static function get_content_gallery($id_content, $id_gallerygrupa) {
			
			// строим запрос
			$_sql='SELECT count(*) as count FROM content_gallerygrupa WHERE id_content='.$id_content.' and id_gallerygrupa='.$id_gallerygrupa;
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}	

		static function max_hit ($table_name){
			
			$_sql='SELECT max(hits) as max FROM '.$table_name;
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'max',0);
			
		}				
		
		static function min_hit ($table_name){
			
			$_sql='SELECT min(hits) as min FROM '.$table_name;
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'min',0);
			
		}				
	
		
		/**
		 * получаем id группы галереи по URL
		 */
		static function get_gallery_grupa_id($url) {
			
			// строим запрос
			$_sql='SELECT id FROM gallery_grupa WHERE url="'.mysql_escape_string($url).'"';
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'id',0);
			
			
		}	
		
		
		/**
		 * получаем id родителя группы галереи по URL
		 */
		static function get_gallery_grupa_id_parent($url) {
			
			// строим запрос
			$_sql='SELECT id_parent FROM gallery_grupa WHERE url="'.mysql_escape_string($url).'"';
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'id_parent',0);
			
			
		}		
		
		/**
		 * получаем кол-во товаров в каталоге
		 */
		static function gallery_get_total_count() {
			
			// строим запрос
			$_sql='SELECT count(*) as count FROM gallery';
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);
			
			return $result->count;
		
			
		}		
		
		/**
		 * получаем кол-во товаров в каталоге
		 */
		static function news_get_total_count() {
			
			// строим запрос
			$_sql='SELECT count(*) as count FROM news';
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);
			
			return $result->count;
		
			
		}		
	
		/**
		 * получаем детей для раздела "Голосование"
		*	@param  int parent_id
		 */
		static function voting_get_child($id_parent) {
			
			// строим запрос
			$_sql='SELECT * FROM voting where id_parent='.$id_parent.' '.general::get_status_for_filter('voting').' order by sort';
			
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query($_sql,0);
			
		}		
		
		/**
		*  получаем кол-во детей контент
		*	@param  int parent_id
		*/	
		
		static function voting_count_child ($parent_id){
			
			$_sql='SELECT count(*) as count FROM voting where id_parent='.$parent_id;
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}	
		
		/**
		*  получаем метатеги для текущего контроллера
		*	@param  int parent_id
		*/		
		static function get_title() {
			return ___findarray('select title,description,keywords, h1 from ceo where action="'.mysql_escape_string($_GET['action']).'"');
		}
	
		
}


?>