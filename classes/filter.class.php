<?php

/*************************************************************************************
**************** 2011 ****************************************************************
*************************************************************************************/

class filter {
	
		/**
		 * включаем фильтр товаров в данной группе
		 */
		function get_filter() {
	


			if (isset($_GET['id']) and intval($_GET['id'])>0)  {
				$_SESSION['filter_id']=$_GET['id'];
			} else if (isset($_GET['id']) and intval($_GET['id'])==0)  {
				unset($_SESSION['filter_id']);
			}
			
			/*
			* значения filter_id
			* 1 - block_new - новинки
			* 2 - block_spec - спецпредложения
			* 3 - block_lider - лидеры продаж
			*/
			
			if (isset($_SESSION['filter_id'])) {
				switch ($_SESSION['filter_id']) {
		   			case 1:
						$_res = " and catalog.block_new=1";
		       			break;
		   			case 2:
						$_res = " and catalog.block_spec=1";
		       			break;
		   			case 3:
						$_res = " and catalog.block_lider=1";
		       			break;       			
				}				
			} else {
				$_res = '';
			}
			
			
			
			// проверяем на поиск
			if (isset($_POST['poisk'])) { 
				$_SESSION['poisk']=mysql_escape_string($_POST['poisk']);
			}
			
			if (isset($_SESSION['poisk'])) {
				$_res = " and (catalog.name LIKE '%".$_SESSION['poisk']."%' or catalog.text_short LIKE '%".$_SESSION['poisk']."%' or catalog.text_full LIKE '%".$_SESSION['poisk']."%')";
			}
			
			return $_res;

		}	

		
		/**
		* 	get count 
		*/	
				
		static function get_count() {
			
			// фильтрация
			$_filter=self::get_filter();			
	
			// запрос
			$_sql='SELECT count(*) as count FROM catalog where status=1 '.$_filter;
				  		
			// выполняем запрос + при необходимости выводим сам запрос
			return mysql::query_findpole($_sql,'count',0);
			
		}
		
		
		/*
		* получаем текстовое описание текущего раздела
		*/
		static function get_name_curent_filter() {		
		
			if (isset($_GET['id'])) {
				switch ($_GET['id']) {
			   		case 1:
						return "Новинки";
			    		break;
			   		case 2:
						return "Спецпредложения";
			    		break;
			   		case 3:
						return "Лидеры продаж";
			    		break;   
				}
			} else {
				return "Поиск";
			}
		}
		

////////////////////////////////////////////////////////////////////////////////

	/**
	 * 17.05.2011 (Amber)
	 * Фильтр для вывода данных в форму, отправку на почту
	 *
	 */
	public static function varpost($value, $tags = TRUE, $chars = TRUE)
	{
		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		}
		if ($tags) {
			$value = strip_tags($value);
		}
		$value = trim($value);
		return $chars ? htmlspecialchars($value) : $value;
	}
		
}

	
?>