<?php

class SEO {

    public $_seo = array();
    private $group = NULL;
    private $group_vin = NULL;
    private $brand = NULL;
    private $serie = NULL;
    private $features = '';
    private $template = NULL;

    public static function factory() {
        return new SEO();
    }

    public function __construct() {
        if ($_GET['action'] != 'catalog') {
            return false;
        }
        if (!isset($_GET['grupa'])) {
            return false;
        }
        $result = mysql::query_one('SELECT pole AS name, vin FROM catalog_tree WHERE url = "' . $_GET['grupa'] . '"');
        if (!$result) {
            return false;
        }
        $this->group = $result->name;


        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/ss/morphyClient/MorphyClient.php')) {

            include($_SERVER['DOCUMENT_ROOT'] . '/ss/morphyClient/MorphyClient.php');

            $morphy = new MorphyClient(); // Получаем объект класса
            $result = $morphy->init(
                    '{fraze}', // шаблон Купить {fraze} недорого в Киеве
                    array($result->name), // массив слов array
                    'ВН', // Падеж
                    false                             // false => Единственное число, true => Множественное число
            );

            $res = current(current($result));
            $this->group_vin = mb_strtolower($res['after'], 'utf-8');
        } else {
            $this->group_vin = $result->vin ? $result->vin : $result->name;
        }


        if (isset($_GET['filter']) && strpos($_GET['filter'], ',') === false) {
            $arr = explode(';', $_GET['filter']);
            $filter = array();
            foreach ($arr as $value) {
                $tmp = explode('_', $value);
                $filter[$tmp[0]] = $tmp[1];
            }

            $features_ar = array();
            if (isset($filter['type'])) {
                $f_result = mysql::query_one('SELECT name FROM type_tovar WHERE id = "' . $filter['type'] . '"');
                if ($f_result) {
                    $features_ar[] = $f_result->name;
                }
            }
            if (isset($filter['bathtub-form'])) {
                $f_result = mysql::query_one('SELECT name FROM dict_bathtub_form WHERE id = "' . $filter['bathtub-form'] . '"');
                if ($f_result) {
                    $features_ar[] = $f_result->name;
                }
            }
            if (isset($filter['cabin-form'])) {
                $f_result = mysql::query_one('SELECT name FROM dict_cabin_form WHERE id = "' . $filter['cabin-form'] . '"');
                if ($f_result) {
                    $features_ar[] = $f_result->name;
                }
            }
            if (isset($filter['colors'])) {
                $f_result = mysql::query_one('SELECT name FROM dict_colors WHERE id = "' . $filter['colors'] . '"');
                if ($f_result) {
                    $features_ar[] = $f_result->name;
                }
            }
            if (isset($filter['mixer-style'])) {
                $f_result = mysql::query_one('SELECT name FROM dict_mixer_style WHERE id = "' . $filter['mixer-style'] . '"');
                if ($f_result) {
                    $features_ar[] = $f_result->name;
                }
            }
            if (isset($filter['mixer-type'])) {
                $f_result = mysql::query_one('SELECT name FROM dict_mixer_type WHERE id = "' . $filter['mixer-type'] . '"');
                if ($f_result) {
                    $features_ar[] = $f_result->name;
                }
            }
            if (isset($filter['mounting-type'])) {
                $f_result = mysql::query_one('SELECT name FROM dict_mounting_type WHERE id = "' . $filter['mounting-type'] . '"');
                if ($f_result) {
                    $features_ar[] = $f_result->name;
                }
            }
            if (isset($filter['toilet-mount'])) {
                $f_result = mysql::query_one('SELECT name FROM dict_toilet_mount WHERE id = "' . $filter['toilet-mount'] . '"');
                if ($f_result) {
                    $features_ar[] = $f_result->name;
                }
            }
            if (isset($filter['tray-depth'])) {
                $f_result = mysql::query_one('SELECT name FROM dict_tray_depth WHERE id = "' . $filter['tray-depth'] . '"');
                if ($f_result) {
                    $features_ar[] = $f_result->name;
                }
            }
            $this->features = implode(', ', $features_ar);

            if (isset($filter['brands'])) {
                $result = mysql::query_one('SELECT pole AS name FROM brand WHERE id = "' . $filter['brands'] . '"');
                if (!$result) {
                    $this->group();
                    return true;
                }
                $this->brand = $result->name;
                if (isset($filter['series'])) {
                    $result = mysql::query_one('SELECT name FROM serie WHERE id = "' . $filter['series'] . '"');
                    if (!$result) {
                        $this->group_brand();
                        return true;
                    }
                    $this->serie = $result->name;
                    $this->group_brand_serie();
                } else {
                    $this->group_brand();
                }
            } else {
                $this->group();
            }
        } elseif (!isset($_GET['filter'])) {
            $this->group();
        }
    }

    private function group() {  // add name to group
        $this->get_template(2);
        if (!$this->template) {
            return false;
        }
        $super_replacer = $this->group;

        if ($this->features) {
            $super_replacer.= ', ' . $this->features;
        }

        $from = array('{group}', '{group_vin}');
        $to = array($super_replacer, $this->group_vin);
        $this->_seo['h1'] = str_replace($from, $to, $this->template->h1);
        $this->_seo['title'] = str_replace($from, $to, $this->template->title);
        $this->_seo['keywords'] = str_replace($from, $to, $this->template->keywords);
        $this->_seo['description'] = str_replace($from, $to, $this->template->description);
    }

    private function group_brand() {  // add name to brand
        if ($this->features) {
            $this->get_template(1);
        } else {
            $this->get_template(4);
        }

        if (!$this->template) {
            return false;
        }

        $super_replacer = $this->brand;
        $originalBrand = $this->brand;
        if ($this->features) {
            $super_replacer.= ', ' . $this->features;
        }
        $from = array('{group}', '{group_vin}', '{brand}', '{original_brand}', '{features}', '{features_small}');
        $to = array($this->group, $this->group_vin, $super_replacer, $originalBrand, $this->features, mb_strtolower($this->features, 'UTF-8'));

        if ($_GET['grupa'] == 'akrilovie_vanni' AND isset($_GET['filter']) AND strpos($_GET['filter'], 'bathtub') !== FALSE) {
            $arr = explode(';', $_GET['filter']);
            $this->template->title = '{features} {original_brand} - купить в Киеве, цена на акриловые {features_small} {original_brand} | «Мир Сантехники»';
            $this->template->h1 = 'Акриловые {features_small} {original_brand}';
            $this->template->description = 'Купить акриловые {features_small} {original_brand} в интернет-магазине «Мир Сантехники» ✔ Доступная цена ✔ Доставка по Киеву и Украине ☎ +38 (095) 101-77-26';
            $clearBrand = TRUE;
        }

        if ($_GET['grupa'] == 'iskusstvennyj_mramor' AND isset($_GET['filter']) AND strpos($_GET['filter'], 'bathtub') !== FALSE) {
            $arr = explode(';', $_GET['filter']);
            $this->template->title = '{features} {original_brand} - купить в Киеве, цена на {features_small} из искуственного камня {original_brand} | «Мир Сантехники»';
            $this->template->h1 = '{features} {original_brand}';
            $this->template->description = 'Купить {features_small} из мрамора и камня {original_brand} в интернет-магазине «Мир Сантехники» ✔ Доступная цена ✔ Доставка по Киеву и Украине ☎ +38 (095) 101-77-26';
            $clearBrand = TRUE;
        }

        if ($_GET['grupa'] == 'dushevie_boksi' AND isset($_GET['filter']) AND strpos($_GET['filter'], 'cabin-form') !== FALSE) {
            $arr = explode(';', $_GET['filter']);
            $this->template->title = '{features} душевые боксы {original_brand} - купить в Киеве, цена на {features_small} душевые боксы {original_brand} | «Мир Сантехники»';
            $this->template->h1 = '{features} душевые боксы {original_brand}';
            $this->template->description = 'Купить {features_small} душевые боксы {original_brand} в интернет-магазине «Мир Сантехники» ✔ Доступная цена ✔ Доставка по Киеву и Украине ☎ +38 (095) 101-77-26';
            $clearBrand = TRUE;
        }

        if ($_GET['grupa'] == 'dushevye_kabiny' AND isset($_GET['filter']) AND strpos($_GET['filter'], 'cabin-form') !== FALSE) {
            $arr = explode(';', $_GET['filter']);

            if (strpos($arr[1], '5') !== FALSE OR strpos($arr[1], '4') !== FALSE) {
                $this->template->title = 'Душевые кабины {original_brand} <-{features_small}-> - купить в Киеве, цена на {original_brand} <-{features_small}-> | «Мир Сантехники»';
                $this->template->h1 = 'Душевые кабины {original_brand} <-{features_small}->';
                $this->template->description = 'Купить душевую кабину {original_brand} <-{features_small}-> в интернет-магазине «Мир Сантехники» ✔ Доступная цена ✔ Доставка по Киеву и Украине ☎ +38 (095) 101-77-26';
            } else {
                $this->template->title = '{features} душевые кабины {original_brand} - купить в Киеве, цена на <-{features_small}-> душевые кабины {original_brand} | «Мир Сантехники»';
                $this->template->h1 = '{features} душевые кабины {original_brand}';
                $this->template->description = 'Купить <-{features_small}-> душевые кабины {original_brand} в интернет-магазине «Мир Сантехники» ✔ Доступная цена ✔ Доставка по Киеву и Украине ☎ +38 (095) 101-77-26';
            }
            $clearBrand = TRUE;
        }

        if ($_GET['grupa'] == 'unitazi' AND isset($_GET['filter']) AND strpos($_GET['filter'], 'toilet-mount') !== FALSE) {
            $arr = explode(';', $_GET['filter']);
            $this->template->title = 'Унитаз {original_brand} <-{features_small}-> - купить в Киеве, цена унитаза {original_brand} <-{features_small}-> | «Мир Сантехники»';
            $this->template->h1 = 'Унитазы {original_brand} <-{features_small}->';
            $this->template->description = 'Купить унитаз {original_brand} <-{features_small}-> в интернет-магазине «Мир Сантехники» ✔ Доступная цена ✔ Доставка по Киеву и Украине ☎ +38 (095) 101-77-26';
            $clearBrand = TRUE;
        }

        $this->_seo['h1'] = str_replace($from, $to, $this->template->h1);
        $this->_seo['title'] = str_replace($from, $to, $this->template->title);
        $this->_seo['keywords'] = str_replace($from, $to, $this->template->keywords);
        $this->_seo['description'] = str_replace($from, $to, $this->template->description);

        if ($clearBrand == TRUE) {
            $this->_seo['h1'] = preg_replace('/[(]{1}.*[)]{1}/', '', str_replace('серия', '', $this->_seo['h1']));
            $this->_seo['title'] = preg_replace('/\((.+?)\)/', '', str_replace('серия', '', $this->_seo['title']));
            $this->_seo['keywords'] = preg_replace('/\((.+?)\)/', '', str_replace('серия', '', $this->_seo['keywords']));
            $this->_seo['description'] = preg_replace('/\((.+?)\)/', '', str_replace('серия', '', $this->_seo['description']));
        }

        $this->_seo['h1'] = str_replace(array('<-', '->'), array('(', ')'), $this->_seo['h1']);
        $this->_seo['title'] = str_replace(array('<-', '->'), array('(', ')'), $this->_seo['title']);
        $this->_seo['keywords'] = str_replace(array('<-', '->'), array('(', ')'), $this->_seo['keywords']);
        $this->_seo['description'] = str_replace(array('<-', '->'), array('(', ')'), $this->_seo['description']);
    }

    private function group_brand_serie() {  // add name to serie
        $this->get_template(3);
        if (!$this->template) {
            return false;
        }
        $super_replacer = $this->serie;
        if ($this->features) {
            $super_replacer.= ', ' . $this->features;
        }

        $from = array('{group}', '{group_vin}', '{brand}', '{serie}');


        if (isset($_GET['grupa']) AND $_GET['grupa'] == 'mebel_dlya_vannoi' AND isset($_GET['filter'])) {
            $this->template->title = 'Мебель {brand} {serie} - купить в Киеве, цена {brand} {serie} | «Мир Сантехники»';
            $this->template->description = 'Купить мебель для ванной {brand} серии {serie} в интернет-магазине «Мир Сантехники» ✔ Доступная цена ✔ Доставка по Киеву и Украине ☎ +38 (095) 101-77-26';
            $this->template->h1 = 'Мебель для ванной {brand} {serie}';

            $clearBrand = TRUE;
        }

        if (isset($_GET['grupa']) AND $_GET['grupa'] == 'plitka' AND isset($_GET['filter'])) {
            $this->template->title = 'Плитка {brand} {serie} - купить в Киеве, цена кафеля {brand} {serie} | «Мир Сантехники»';
            $this->template->description = 'Купить плитку {brand} серии {serie} в интернет-магазине «Мир Сантехники» ✔ Доступная цена ✔ Доставка по Киеву и Украине ☎ +38 (095) 101-77-26';
            $this->template->h1 = 'Плитка {brand} {serie}';

            $clearBrand = TRUE;
        }

        if (isset($_GET['grupa']) AND $_GET['grupa'] == 'umivalniki' AND isset($_GET['filter'])) {
            $this->template->title = 'Умывальник {brand} {serie} - купить в Киеве, цена раковины {brand} {serie} | «Мир Сантехники»';
            $this->template->description = ' Купить раковину {brand} серии {serie} в интернет-магазине «Мир Сантехники» ✔ Доступная цена ✔ Доставка по Киеву и Украине ☎ +38 (095) 101-77-26';
            $this->template->h1 = 'Умывальник {brand} {serie}';

            $clearBrand = TRUE;
        }

        $to = array($this->group, $this->group_vin, trim(preg_replace('/[(]{1}.*[)]/', '', $this->brand)), $super_replacer);
        $this->_seo['h1'] = str_replace($from, $to, $this->template->h1);
        $this->_seo['title'] = str_replace($from, $to, $this->template->title);
        $this->_seo['keywords'] = str_replace($from, $to, $this->template->keywords);
        $this->_seo['description'] = str_replace($from, $to, $this->template->description);

        if ($clearBrand == TRUE) {
            $this->_seo['h1'] = preg_replace('/[(]{1}.*[)]{1}[, ]+/', '', str_replace('серия', '', $this->_seo['h1']));
            $this->_seo['title'] = str_replace('серия', '', $this->_seo['title']);
            $this->_seo['keywords'] = str_replace('серия', '', $this->_seo['keywords']);
            $this->_seo['description'] = str_replace('серия', '', $this->_seo['description']);
        }
    }

    private function get_template($id) {
        $this->template = mysql::query_one('SELECT title, keywords, description, h1 FROM seo_templates WHERE id = "' . $id . '"');
    }

}
