<?php

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class captcha {
	
		/**
		 * get captcha
		 *
		 */
		
		static function get_captcha () {
			
			return system::show_tpl(array(),'/frontend/captcha/captcha.php');
			
		}	
		
		/**
		 * check entered captcha
		 *
		 */
		static function check_captcha () {
			
		    require_once  PLAGINS_PATH.'/captcha/securimage.php';
		    $image = new Securimage();
	    
		    return !$image->check($_POST['hislo']) ? false : true;
			
		}
		
	
}

?>