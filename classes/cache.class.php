<?php

/*
*********************************
* РАБОТА С КЕШЕМ, начало
*********************************
*/

// создаем имя КЕШ файла
// НЕ ИСПОЛЬЗУЕТС ПОКА 
/*
function get_name_file_keh() {

		// определяем позицию начала цикла
		/*
		* если local - то =1, если inet = 0
		*/

	/*
		if (_SET=="local") {
			$_begin=1;
		} else {
			$_begin=0;
		}
		
		// получаем адресную строку, запихуем ее в массив
		$_arr=explode("/",$_SERVER [ 'REQUEST_URI']);
		//print_r($_arr);	
		$_name_file_keh="";
		for ($i=$_begin; $i<count($_arr); $i++) {
			//echo $i."-".$_arr[$i]."<br />";
			$_name_file_keh.=$_arr[$i]."_";
			
		}
		
		// дописіваем к имени расширение
		$_name_file_keh.=".keh";
		
		// debug ceche
		if (DEBUG_KEH==1) {
			echo "<br>Начало - ".$_name_file_keh;
		}
		
		return $_name_file_keh;
		
	*/
/*	
}
*/


function use_keh() {
	
		// получаем имя файла
		//$_name_file_keh=get_name_file_keh();
		
		$_name_file_keh=parse_for_url(get_post_session()).".keh";
		// debug ceche
		if (DEBUG_KEH==1) {		
			echo " Начало - ".$_name_file_keh;
		}
		
	
		// ищем сгенеренный кеш
		if (file_exists(HOST.'/cache/'.$_GET['action'].'/'.$_name_file_keh)) {
			//echo "keh!!!";
			$_str.=get_tpl('/cache/'.$_GET['action'].'/'.$_name_file_keh);
			// выводим содержимое
			

			/*
			* парсим банер
			*/
			// елементы для замены
			$mass_element_for_parsing=array(
				"%banner%",
	                     
				);
				
			$mass_result_for_parsing=array(
				_BANNER,
				
				);		
			// парсим данные
			$_str=parsing_data($mass_element_for_parsing,$mass_result_for_parsing,$_str);				
			
			// debug ceche
			if (DEBUG_KEH==1) {
				echo " | keh!!!";
			}

			die($_str);
		} else {

			// debug ceche
			if (DEBUG_KEH==1) {
				echo " | NO keh!!!";
			}

		}

}

// генерим и пишем файлы в КЕШ
function create_keh($_str) {

		// получаем имя файла
		//$_name_file_keh=get_name_file_keh();
		
		$_name_file_keh=parse_for_url(get_post_session()).".keh";	
		
		// debug ceche
		if (DEBUG_KEH==1) {
			echo " | Конец - ".$_name_file_keh;
		}
		
		
		// если не создан каталог по имени ACTION - создаем его!
		create_dir();
		// пишем в кеш
		write_keh(HOST.'/cache/'.$_GET['action'].'/'.$_name_file_keh, $_str);

	
}

/*
* получаем все данные из GET + POST + SESSION
*/
function get_post_session() {
	
	$_res="";
	
	if (isset($_GET)) {
  			foreach($_GET as $key => $value) {
  				$_res.=$key.'/'.$value.'/';
  			}
   }
   
	if (isset($_POST)) {
  			foreach($_POST as $key => $value) {
  				$_res.=$key.'/'.$value.'/';
  			}
   }

   
	if (isset($_SESSION)) {
  			foreach($_SESSION as $key => $value) {
  				$_res.=$key.'/'.$value.'/';
  			}
   }
   
   

   return $_res;
 
}

/*
* создаем каталог
*/
function create_dir() {
	
  $_dir=@opendir(HOST."/cache/".$_GET['action']);
  
 
  if (!$_dir) {
	  $flag = mkdir (HOST."/cache/".$_GET['action'], "0700");
	  chmod(HOST."/cache/".$_GET['action'], 0777);
		  if($flag)
		  {

		  	// debug ceche
			if (DEBUG_KEH==1) {
		    	echo " | Каталог успешно создан!";
			}
			
		  }
		  else
		  {

		  	// debug ceche
			if (DEBUG_KEH==1) {
		    	echo " | Ошибка создания каталога!";
			}

		  }
  } else {

		  	// debug ceche
			if (DEBUG_KEH==1) {
		    	echo " | Каталог существует!";
			}
  	
  }
	
	
}

/*
*
*/
function parse_for_url($_url) {
	
	// получаем адресную строку, запихуем ее в массив
	$_arr=explode("/",$_url);
	//print_r($_arr);	
	$_res="";
	for ($i=2; $i<count($_arr); $i++) {
		//echo $i."-".$_arr[$i]."<br />";
		$_res.=$_arr[$i]."_";
		
	}
	
	
	return $_res;
	
}


/*
*********************************
* РАБОТА С КЕШЕМ, конец
*********************************
*/


/*
* работа с папками , удаление указанной папки
*/
function removedir ($directory) {
	$dir = opendir($directory);
	while(($file = readdir($dir)))
	{
		if ( is_file ($directory."/".$file))
		{
			unlink ($directory."/".$file);
		}
		else if ( is_dir ($directory."/".$file) & ($file != ".") & ($file != ".."))
		{
			removedir ($directory."/".$file);
		}
	}
	closedir ($dir);
	@rmdir ($directory);
	return TRUE;  
}

/*
* узнаем время изминения файла в секундах
*/

function get_date_last_modify_file_in_second ($filename) {

	// текущее время
	$time_sec=time();
	// время изменения файла
	if (file_exists($filename)) {
		$time_file=filemtime($filename);
	} else {
		$time_file=$time_sec;
	}
	// тепрь узнаем сколько прошло времени (в секундах)
	$time=$time_sec-$time_file;  
	
	return $time;

}

/*
* удаляем кеш
*/
function delete_all_cache () {	
	

	require_once HOST."/classes/cache.class.php";		
	// удаляем весь каталог 
	removedir (HOST."/cache");
	// создаем новій каталог, устанавливаем права
	$flag = @mkdir (HOST."/cache", "0700");
  	@chmod(HOST."/cache", 0777);
  	
}  	



?>