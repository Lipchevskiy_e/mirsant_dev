<?php

//------------------------------------

$_1_2=array(" ","одна ","две ");
$_1_19=array(" ","одна ","две ","три ","четыре ","пять ","шесть ","семь ","восемь ","девять ","десять ","одиннадцать ","двенадцать ",
"тринадцать ","четырнадцать ","пятнадцать ","шестнадцать ","семнадцать ","восемнадцать ",
"девятьнадцать ");
$des=array(" "," ","двадцать ","тридцать ","сорок ","пятьдесят ","шестьдесят ","семьдесят ","восемьдесят ","девяносто ");
$hang=array(" ","сто ","двести ","триста ","четыреста ","пятьсот ","шестьсот ","семьсот ","восемьсот ","девятьсот ");
$nametho=array(" ","тысяча ","тысячи ","тысяч ");
$namemil=array(" ","миллион ","миллиона ","миллионов ");
$namemrd=array(" ","мильярд ","мильярда ","мильярдов ");
 function num2str($L){
global $_1_2, $_1_19, $des, $hang, $namerub, $nametho, $namemil, $namemrd, $kopeek;

$s=" ";
$s1=" ";
$s2=" ";
#$kop=intval( ( $L*100 - intval( $L )*100 ));
$kop=substr(sprintf("%.2f", $L ),-2,2);
$L=intval($L);
if($L>=1000000000){
$many=0;
semantic(intval($L / 1000000000),$s1,$many,3);
$s.=$s1.$namemrd[$many];
$L%=1000000000;
}

if($L >= 1000000){
$many=0;
semantic(intval($L / 1000000),$s1,$many,2);
$s.=$s1.$namemil[$many];
$L%=1000000;
if($L==0){
$s.="рублей. ";
}
}

if($L >= 1000){
$many=0;
semantic(intval($L / 1000),$s1,$many,1);
$s.=$s1.$nametho[$many];
$L%=1000;
if($L==0){
$s.="рублей. ";
}
}

if($L != 0){
$many=0;
semantic($L,$s1,$many,0);
#$s.=$s1.$namerub[$many];
$s.=$s1."рублей. ";
}
#if($L == 0){
if($many == 0){
$many=0;
$s.="нуль рублей. ";
}

if($kop > 0){
$many=0;
#semantic($kop,$s1,$many,1);
#$s.=$s1.$kopeek[$many];
#$s.=$s1."КОП ";
$s.=$kop." копеек. ";
}
else {
$s.=" 00 копеек.";
}

return $s;
}

function semantic($i,&$words,&$fem,$f){
global $_1_2, $_1_19, $des, $hang, $namerub, $nametho, $namemil, $namemrd;
$words="";
$fl=0;
if($i >= 100){
$jkl = intval($i / 100);
$words.=$hang[$jkl];
$i%=100;
}
if($i >= 20){
$jkl = intval($i / 10);
$words.=$des[$jkl];
$i%=10;
$fl=1;
}
switch($i){
case 1: $fem=1; break;
case 2:
case 3:
case 4: $fem=2; break;
default: $fem=3; break;
}
if( $i ){
if( $i < 3 && $f > 0 ){
if ( $f >= 2 ) {
$words.=$_1_19[$i];
}
else {
$words.=$_1_2[$i];
}
}
else {
$words.=$_1_19[$i];
}
}
}

?>