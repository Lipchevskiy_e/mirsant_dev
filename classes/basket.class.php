<?php

/*******************************************
// core 2011 *******************************
/********************************************/
		
class basket {
	
	/**
	 * всего товаров в корзине
	 */
	static function get_Count_Goods () {	

		if (isset($_SESSION['basket'])) {
			$_count=0;
			foreach($_SESSION['basket'] as $key => $value) {
				$_count+=$value['kolvo'];
			}
			return $_count;
		} else {
			return 0;
		}
		
	}	
	
	
	/**
	 * всего товаров на сумму
	 */
	static function get_Total_Goods () {	

		if (isset($_SESSION['basket'])) {
			$_count=0;
			foreach($_SESSION['basket'] as $key => $value) {
				$_count+=$value['cost']*$value['kolvo'];
			}
			return $_count;
		} else {
			return 0;
		}
		
	}	
	
	
	/**
	 * выводим всю корзину
	 */
	static function show_Basket () {	

		if (isset($_SESSION['basket'])) {
			return $_SESSION['basket'];
		} else {
			return false;
		}
		
	}	
	
	
	/**
	 * выбираем данные о товаре
	 */
	static function get_Good_Data ($id) {	

		// строим запрос
		$_sql='SELECT catalog.*, brand.pole as brand_name 
				FROM catalog, brand 
				WHERE catalog.brand=brand.id and catalog.id='.intval($id).' and catalog.status=1';
		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);
			
		if ($result) {
			return $result;
		} else {
			return false;
		}
		
	}	
	
	
	/**
	 * форматируем цену
	 */
	static function format_Cost ($cost) {	

		return number_format($cost,2,'.','');
		
	}	
	
	
	/**
	 * очищаем корзину
	 */
	static function clear_Basket () {	

		unset($_SESSION['basket']);
		
	}		
	

	/**
	 * всего товаров на сумму
	 */
	static function get_Cost_Deliver ($_id_user,$_total) {	

		//if (intval($_id_user<=0)) { return 0; }
		
		// ищем страну пользователя
		$_sql1='SELECT city FROM users WHERE id='.intval($_id_user);
		// выполняем запрос + при необходимости выводим сам запрос
		$result1=mysql::query_one($_sql1,0);
		
		if (!$result1) { return 'Договорная'; }
	
		if (registration::is_Autorize()) {
			$_sql='SELECT * 
				FROM deliver 
				WHERE 
				id_city='.$result1->city.' 
				and status=1 
				and cost_order_to>'.$_total.' 
				and cost_order<'.$_total;
			// выполняем запрос + при необходимости выводим сам запрос
			$result=mysql::query_one($_sql,0);
			return $result ? $result->cost_deliver : 'Договорная';
			;
		} else {
			return 'Договорная';
		}
		
	}		
	
	
	
	
	
	
}

	
?>