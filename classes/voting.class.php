<?php

/*
* core_2011
*/

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class voting {

	/*
	* выводит вопрос 
	*/
	static function get_vopros($name_file) {
	
		$_limit=1;
		if (isset($_GET['arhiv'])) {
			$_limit=100;
		}

		// выбираем вопрос
		$_sql="SELECT
			voting.id,
			voting.id_parent,
			voting.`text`,
			voting.kolvo
			FROM
				voting
			WHERE
				voting.status = 1 and
				voting.id_parent = 0
			order by voting.id DESC
			limit ".$_limit
			;			

		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);

		// выполняем tpl
		return system::show_tpl(array('vopros'=>$result),$name_file);		  

	}	

	/*
	* выводит вопрос + ответы
	*/
	static function get_otvet($name_file, $id_parent) {

		// выбираем вопрос
		$_sql="SELECT
			voting.id,
			voting.id_parent,
			voting.`text`,
			voting.kolvo,
			ROUND(voting.kolvo*100/voting.summa) as summa
			FROM
				voting
			join (select SUM(voting.kolvo) as summa from voting WHERE voting.status = 1 and voting.id_parent = ".intval($id_parent).") as voting
			WHERE
				voting.status = 1 and
				voting.id_parent = ".intval($id_parent)."
			order by voting.sort
			";			

		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query($_sql,0);

		// выполняем tpl
		return system::show_tpl(array('otvet'=>$result),$name_file);		  

	}	

	/*
	* выводит кол-во голосов
	*/
	static function get_kolvo($id_parent) {

		// выбираем вопрос
		$_sql="SELECT 
			SUM(voting.kolvo) as sum_kolvo
			FROM
				voting
			WHERE
				voting.status = 1 and
				voting.id_parent = ".intval($id_parent)
			;			

		// выполняем запрос + при необходимости выводим сам запрос
		$result=mysql::query_one($_sql,0);

		// выполняем tpl
		return $result->sum_kolvo;		  

	}	
	
	
	/*
	* записываем результаты голосования в базу
	*/	
	function set_voting	() {
		
		// ищем голосовали ли сегодня
		$_sql="SELECT count(*) as count 
				FROM voting_ip 
				WHERE 
				  date='".date('Y-m-d')."' and ip='".$_SERVER['REMOTE_ADDR']."'";			

 		$result=mysql::query_one($_sql,0);
 		
 		if ($result->count==0) {
			
			// update
			$_sql="UPDATE `voting` SET `kolvo`=kolvo+1 WHERE (`id`=".intval($_POST['id_voting']).")";			
			$result=mysql::query_one($_sql,0);
			
			// insert to voting_IP
			$_sql="INSERT INTO `voting_ip` (`id`,`ip`,`date`) VALUES (NULL,'".$_SERVER['REMOTE_ADDR']."','".date('Y-m-d')."')";
			$result=mysql::query_one($_sql,0);
			
 		} else {
			
			return '<div style="font-weight:bold; color: red;">Вы уже голосовали сегодня!</div>';
		
		}
		
	}	
	
		
}		

?>