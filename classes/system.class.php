<?php

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class system {

    /**
     * 	check ctrl
     * 	@param  int		$ctr	name of ctrl
     */
    static function isset_ctrl($ctrl, $msg = '') {

        //$this->action=$action;

        if (isset($_GET[$ctrl])) {

            return true;
        } else {
            system::error(404, 'Action <b>' . $_GET['action'] . '</b> not found');
        }
    }

    /**
     * 	check is_numeric
     * 	@param  int		$id		числовое значение
     */
    static function isset_numeric($id, $msg = '') {

        //$this->action=$action;

        if (is_numeric($id)) {

            return true;
        } else {

            system::error(404, 'Parametrs <b>' . $id . '</b> is not a numeric');
        }
    }

    /**
     * 	check parametrs > 2
     * @param  int		$_count		числовое значение* - кол-во 
     */
    static function count_parametr($_count, $msg = '') {

        $_arr = explode("/", $_SERVER ['REQUEST_URI']);
        //print_r($_arr);

        if (count($_arr) > $_count) {
            system::error(404, 'There are to many parametrs at url. Page - <b>' . $_GET['action'] . '</b>');
        } else {
            return true;
        }
    }

    /**
     * 	check action
     * 	@param string		$action		name of action with extention
     */
    static function isset_extension($action) {

        $_arr = explode(".", $action);

        // echo  count($_arr);


        if (count($_arr) == 1) {

            return true;
        } else {

            system::error(404);
        }
    }

    /**
     * 	check action
     * 	@param string		$action		name of action with extention
     */
    static function isset_php($action) {

        $_arr = explode(".", $action);

        if (!$action or $_arr[1] == 'php') {

            return true;
        } else {

            system::error(404);
        }
    }

    /**
     * 	Redirect to error page.
     * 	@param  int		$code	error code [404 | 500]
     * 	@param  string	$msg		message
     */
    static function error($code = 404, $msg = '') {
        ob_clean();
        
        // set header
        switch ($code) {
            case 404:
                if (DEBUG_SITE == 1) {
                    echo $msg;
                }
                $header = '404 Not Found';
                break;

            case 500:
                if (DEBUG_SITE == 1) {
                    echo $msg;
                }
                $header = '500 Internal Server Error';
                break;
        }

        // save to log file
        /*
          $log_file = LOG_PATH.'/'.date('Y-m-d', time()).'.html';
          error_log(nl2br('<b>['.date('d.m.Y H:i:s').'] '.request::getIP().'<br />'.$header.'</b><br />'.$_SERVER['REQUEST_URI'].'<br /><br />'.$msg.'<br /><br />'), 3, $log_file);
         */
//        header($_SERVER['SERVER_PROTOCOL'] . ' ' . $header);
        die(get_tpl('/tpl/mainpage/' . $code . '.tpl.html'));
        //die ("<meta http-equiv='refresh' content='0;URL=/404.php'>");
    }

    /**
     * clear GET data 
     */
    static function clear_get() {


        if (isset($_GET)) {
            //echo "<pre>".print_r($_GET,1)."</pre>";
            foreach ($_GET as $key => $value) {

                //if (is_array($value)) {
                //del_sql_in();
                //} else {


                $value = str_replace(array('%27', 'union', 'insert', '%22', '&', ':', '.', '=', 'mysql'), '', $value);

                $_GET[$key] = $value;



                //}
            }
            //echo "<pre>".print_r($_GET,1)."</pre>";
        }
    }

    /**
     * show template
     * 	@param 	array			$result	 		- array of objects
     * 			varchar			$name_file 		- name of tamplate (plus path '/tpl/....') 
     */
    static function show_tpl($tpl_data, $name_file) {
        //print_r ($result);	

        ob_start();
        extract($tpl_data, EXTR_SKIP);

        if (file_exists(HOST . '/tpl/' . $name_file)) {
            //echo '<br>'.HOST.'/tpl/'.$name_file.'<br>';
            include(HOST . '/tpl/' . $name_file);
        } else {
            //echo '<br>'.HOST.APPLICATION.'/tpl/'.$name_file.'<br>';
            include(HOST . APPLICATION . '/tpl/' . $name_file);
        }

        return ob_get_clean();
    }

    /**
     * work with date
     * @param 			$data	- date
     */
    static function show_data($date_input, $time = false) {

        $monthes = array(
            '', __('January'), __('February'), __('March'), __('April'), __('May'), __('June'),
            __('July'), __('August'), __('September'), __('October'), __('November'), __('December')
        );
        $date = strtotime($date_input);

        //Время
        if ($time)
            $time = ' G:i';
        else
            $time = '';

        //Сегодня, вчера, завтра
        if (date('Y') == date('Y', $date)) {
            if (date('z') == date('z', $date)) {
                $result_date = __('today');
            } elseif (date('z') == date('z', mktime(0, 0, 0, date('n', $date), date('j', $date) + 1, date('Y', $date)))) {
                $result_date = __('yesterday');
            } elseif (date('z') == date('z', mktime(0, 0, 0, date('n', $date), date('j', $date) - 1, date('Y', $date)))) {
                $result_date = __('tomorrow');
            }

            if (isset($result_date))
                return $result_date;
        }

        //Месяца
        $month = $monthes[date('n', $date)];

        //Года
        if (date('Y') != date('Y', $date))
            $year = date('Y', $date) . ' ' . __('year') . '.';
        else
            $year = '';

        $result_date = date('j', $date) . ' ' . $month . ' ' . $year . ' ' . $time;
        return $result_date;
    }

    /**
     * 	Auto load classes.
     * 	FOR SYSTEM USE ONLY
     */
    static function classAutoLoad($class) {

        if (stripos($class, 'Controller') > 0) {
            if (file_exists(HOST . "/ctrl/" . $class . ".php")) {
                //echo '<br>'.HOST."/ctrl/".$class.".php<br>";
                include_once(HOST . "/ctrl/" . $class . ".php");
            } else {
                //echo '<br>'.HOST.APPLICATION."/ctrl/".$class.".php<br>";
                include_once(HOST . APPLICATION . "/ctrl/" . $class . ".php");
            }
        } else {
            if (file_exists(HOST . "/classes/" . $class . ".class.php")) {
                //echo '<br>'.HOST."/classes/".$class.".class.php<br>";
                include_once(HOST . "/classes/" . $class . ".class.php");
            } else {
                //echo '<br>'.HOST.APPLICATION."/classes/".$class.".class.php<br>";
                include_once(HOST . APPLICATION . "/classes/" . $class . ".class.php");
            }
        }
    }

    /**
     * if isset GET
     *
     */
    static function IsGet() {

        return $_SERVER['REQUEST_METHOD'] == 'GET';
    }

    /**
     * if isset POST
     *
     */
    static function IsPost() {

        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }

    static function _get($key, $default = '') {

        return isset($_GET[$key]) ? $_GET[$key] : $default;
    }

    static function _cfg($key, $default = '') {

        return isset($GLOBALS[$key]) ? $GLOBALS[$key] : $default;
    }

    static function _route() {


        // search alias
        // save for debug message
        $request_uri = $_SERVER['REQUEST_URI'];

        // clean request uri
        $request_uri = preg_replace('/\/.*\?/', '/', $request_uri);
        $request_uri = str_replace(array('&', '=',), '/', $request_uri);
        $request_uri = preg_replace('/^\/|ctrl\/|action\/|\/$/', '', $request_uri);

        if ($request_uri !== '') {


            // serach aliases
            foreach (system::_cfg('route_alias', array()) as $k => $v) {


                // string
                if (is_string($v)) {
                    if ($request_uri == $k) {
                        $request_uri = $v;
                        break;
                    }
                }
                // preg
                else {
                    $k = str_replace('/', '\/', $k);
                    $k = str_replace('.', '\.', $k);

                    foreach ($v as $rule => $vars) {
                        foreach ($vars as $key => $value) {
                            $k = str_replace('$' . ($key + 1), '(' . $value . ')', $k);
                        }

                        if (preg_match('/^' . $k . '$/', $request_uri)) {
                            $request_uri = preg_replace('/' . $k . '/', $rule, $request_uri);
                            break;
                        }
                    }
                }
            }
        }


        $_GET = array();


        $_arr = explode('/', $request_uri);

        $_GET['ctrl'] = v::toWord(v::getFrom($_arr, 0, DEFAULT_CTRL));
        $_GET['action'] = v::toWord(v::getFrom($_arr, 1, DEFAULT_ACTION));

        array_shift($_arr);
        array_shift($_arr);

        for ($i = 0; $i < count($_arr); $i+=2) {

            $_GET[$_arr[$i]] = v::toWord(v::getFrom($_arr, $i + 1, ''));
        }

        //print_r($_GET);
        //die();
    }

}

?>