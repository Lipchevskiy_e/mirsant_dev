<?php

@session_start();

/*
 * подключаем класс с системными функциями
 */
include_once "classes/system.class.php";

// подключаем класс работы с базами  данных
include_once "classes/mysql.class.php";

// настройки
include_once "putt.php";

if (file_exists('ss/seo_class.php')) {
    include_once('ss/seo_class.php');

    SEO_S::loader();
    SEO_S::index();
    SEO_S::lastSlash(-1);
}

/*
 * подгружаем автоматически классы
 */
spl_autoload_register('system::classAutoLoad');


// get i18n
//include CTRL_PATH.'/i18n/'.request::getUserLang(LANGS).'.php';								
include CTRL_PATH . '/i18n/' . LANGS . '.php';
v::$i18n = $_i18n_;
unset($_i18n_);


// подключаем класс вівода Title
require_once "classes/title.class.php";
// подключаем класс вівода Title
require_once "classes/pasing_tpl.class.php";
// ------ ДЕБАГЕР
require_once "classes/debug.class.php";
// подключаем класс работы с картинками
require_once "classes/image.class.php";


// ------ подключаем модуль MESSAGE
require_once "classes/message.class.php";
// ------ подключаем модуль ОТПРАВКИ МЫЛА
require_once "classes/mail.class.php";
// подключаем класс работы CONTENT
require_once "classes/content.class.php";
// модуль голосование
require_once "classes/voting.class.php";
// модуль голосование
require_once "classes/forms.class.php";


// валюта
$_currency = array("грн", "Доллар США");

// валюта
$_status_order = array("Новый", "В работе", "Проведенный");

// стандарт
$_standart = array("Нет", "Есть");

// склад
$_sklad = array('0' => 'Нет на складе', '1' => 'Есть в наличии', '2' => 'Уточняйте у менеджера');
$_sklad_color = array('0' => 'red', '1' => 'green', '2' => 'gray');

// таблицы

function head1() {

    $_str = "";

    /* if(strpos(SEO_S::$uri, '/page/')){
      $_str .= SEO_S::robots(0, 1);
      } else {

      }

      if(strpos(SEO_S::$uri, '/filter/')){
      $filters = substr(SEO_S::$uri, strpos(SEO_S::$uri, '/filter/'));
      $filters = explode('_', $filters);
      $filter = $filters[1];

      if(strpos($filter, ',')){
      $_str .= SEO_S::robots(0, 0);
      }
      } */
    if ($_SESSION['no_result'] != 1) {
        $r_index = true;
    } else {
        $r_index = false;
    }
    $r_follow = true;
    unset($_SESSION['no_result']);
    
    if (strpos(SEO_S::$uri, '/page/')) {
        $r_index = false;
    }

    $filter_keys = array();
    if (strpos(SEO_S::$uri, '/filter/')) {
        if ($_GET['filter']) {
            $filters_arr = explode(';', $_GET['filter']);
            //$filters = array();
            if (count($filters_arr)) {
                foreach ($filters_arr as $value) {
                    $filter = explode('_', $value);
                    $filter_keys[$filter[0]] = $filter[0];
                    //$filters[$filter[0]] = explode(',', $filter[1]);
                    if (strpos($value, ',')) {
                        $r_index = false;
                        $r_follow = false;
                        $show_canonical = true;
                    }
                    if ($filter[0] == 'cost') {
                        $r_index = false;
                        $r_follow = false;
                        $show_canonical = true;
                    }
                    if ($filter[0] == 'width') {
                        $r_index = false;
                        $r_follow = false;
                        $show_canonical = true;
                    }
                    if ($filter[0] == 'length') {
                        $r_index = false;
                        $r_follow = false;
                        $show_canonical = true;
                    }
                }
            }

            if (count($filters_arr) > 1 && $_GET['grupa'] == 'smesiteli') {
                $r_index = false;
                $r_follow = false;
            }
        }
    }

    $features_order = array(
        1 => 'brands',
        2 => 'series',
        3 => 'type',
        4 => 'bathtub-form',
        5 => 'toilet-mount',
        6 => 'tray-depth',
        7 => 'cabin-form',
        8 => 'color',
        9 => 'mounting-type',
        10 => 'mixer-style',
        11 => 'mixer-type',
        12 => 'cost',
        13 => 'width',
        14 => 'length',
    );

    if (isset($_GET['page']) && $_GET['page'] > 1) {
        $show_canonical = true;
    }

    if ($_GET['grupa'] == 'smesiteli') {
        if (count($filters_arr) > 1) {
            $show_canonical = true;
        }
    } else {
        if (isset($filter_keys) && count($filter_keys) > 1) {
            $current_filter_string = '';
            $ordered_filter_array = array();
            foreach ($filter_keys as $f_key => $f_value) {
                $current_filter_string .= $f_value;
            }
            foreach ($features_order as $order_key => $order_item_alias) {
                if ($filter_keys[$order_item_alias]) {
                    $ordered_filter_array[$order_key] = $order_item_alias;
                }
            }
            $ordered_filter_string = implode('', $ordered_filter_array);

            if ($current_filter_string != $ordered_filter_string) {
                $show_canonical = true;
                $r_index = false;
                $r_follow = false;
            }
        }
    }

    if ($show_canonical) {
        //$_str .= SEO_S::canonical('/catalog/grupa/'.$_GET['grupa']);
    }
    /* if(isset($filters_arr) && count($filters_arr)){
      //var_dump($filters_arr);
      //var_dump($_GET['page']);
      $_str .= SEO_S::canonical('/catalog/grupa/'.$_GET['grupa']);
      } */

    $_str .= SEO_S::robots($r_index, $r_follow);

    /*
     * для кеширования CSS и JS
     */
    $css_pack = array(
        '/css/jquery/jquery.alerts.css', // интернет магазин
        '/css/shop.css', // интернет магазин
    );

    $js_pack = array(
        '/js/shop-ru.js', /// интернет магазин
        '/js/jquery/jquery.alerts.js', /// интернет магазин
    );

    if (__CACHE_CSS == '0') {
        foreach ($css_pack as $v)
            $_str .= "\n" . '<link rel="stylesheet" type="text/css" media="all" href="' . $v . '" />';

        foreach ($js_pack as $v)
            $_str .= "\n" . '<script type="text/javascript" src="' . $v . '"></script>';
    } else {
        $_str .= '<link rel="stylesheet" type="text/css" media="all" href="/plagin/min/?f=' . implode(',', $css_pack) . '" />
			<script type="text/javascript" src="/plagin/min/?f=' . implode(',', $js_pack) . '"></script>';
    }






    return $_str;
}

?>