<?php
//ini_set('display_errors', 'on'); // Display all errors on screen

$tmpRD = explode('/', $_SERVER['REQUEST_URI']);

if (preg_match('/[\/]{2,}/', $_SERVER['REQUEST_URI'], $matches)) {
    $url = preg_replace('/[\/]{2,}/', '/', $_SERVER['REQUEST_URI']);
    header('Location:http://' . $_SERVER['HTTP_HOST'] . $url);
    die();
}

if (strripos($_SERVER['REQUEST_URI'], ';') !== false) {
    $url = str_replace(';', '/', $_SERVER['REQUEST_URI']);
    header('Location:http://' . $_SERVER['HTTP_HOST'] . $url);
    die();
}

if (strripos($_SERVER['REQUEST_URI'], '/filter/series_') !== false) {
    $url = explode('/filter/', $_SERVER['REQUEST_URI']);
    header('Location:http://' . $_SERVER['HTTP_HOST'] . $url[0]);
    die();
}

if (in_array('catalog', $tmpRD) AND ! in_array('grupa', $tmpRD) AND ! in_array('goods', $tmpRD)) {

    $last = end($tmpRD);

    if (!empty($last)) {
        header('Location :http://mirsant.com.ua', TRUE, 301);
    }
}

if (in_array('catalog', $tmpRD) AND in_array('grupa', $tmpRD) AND in_array('akrilovie_vanni', $tmpRD) AND strpos($_SERVER['REQUEST_URI'], 'series') !== FALSE) {
    header('Location :http://mirsant.com.ua/catalog/grupa/akrilovie_vanni', TRUE, 301);
}
if ($_SERVER['HTTP_HOST'] == 'santmir.com.ua' OR $_SERVER['HTTP_HOST'] == 'www.santmir.com.ua') {
    $rd = array(
        '/' => '/',
        '/catalog/shtorki-na-vannu' => '/catalog/grupa/shtorki_na_vannu',
        '/catalog/installyaciya-dlya-unitaza' => '/catalog/grupa/installyacii',
        '/catalog/dushevye-kabiny' => '/catalog/grupa/dushevye_kabiny',
        '/catalog/akrilovye-vanny' => '/catalog/grupa/akrilovie_vanni',
        '/catalog/gidromassajnye-kabiny' => '/catalog/grupa/dushevie_boksi',
        '/catalog/smesiteli-ferro' => '/catalog/grupa/smesiteli',
        '/catalog/unitazy' => '/catalog/grupa/unitazi',
        '/catalog/mebel-dlya-vannoy-komnaty' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog/unitazy-i-umyvalniki' => '/',
        '/catalog/gidromassajnye-vanny' => '/catalog/grupa/gidromassajnie_vanni',
        '/catalog/akrilovye-vanny-ravak' => '/catalog/grupa/akrilovie_vanni/filter/brands_69',
        '/catalog/mebel-dlya-vannoy-moydodir' => '/catalog/grupa/mebel_dlya_vannoi/filter/brands_95',
        '/catalog/installyaciya-dlya-unitaza-cersanit' => '/catalog/grupa/installyacii/filter/brands_77',
        '/catalog/gidromassazhnye_vanny' => '/catalog/grupa/gidromassajnie_vanni',
        '/catalog/smesiteli' => '/catalog/grupa/smesiteli',
        '/catalog/akrilovye-vanny-cersanit' => '/catalog/grupa/akrilovie_vanni/filter/brands_33',
        '/catalog/dushevye-kabiny/' => '/catalog/grupa/dushevye_kabiny',
        '/catalog/mebel-dlya-vanni-aqua-rodos' => '/catalog/grupa/mebel_dlya_vannoi/filter/brands_90',
        '/catalog/installyaciya-cersanit-link' => '/catalog/grupa/installyacii',
        '/catalog/dushevye-kabiny-eger' => '/catalog/grupa/dushevye_kabiny/filter/brands_55',
        '/catalog/unitazy-kolo' => '/catalog/grupa/unitazi/filter/brands_74',
        '/catalog/dushevye-kabiny-ravak' => '/catalog/grupa/dushevye_kabiny/filter/brands_18',
        '/catalog/otoplenie-boileri' => '/',
        '/catalog/smesiteli-grohe' => '/catalog/grupa/smesiteli/filter/brands_16',
        '/catalog/vanny-kaldewei' => '/catalog/grupa/stalnie_vanni/filter/brands_164',
        '/catalog/dushevaya-stoyka-s-tropicheskim-dushem-temp' => '/catalog/grupa/smesiteli',
        '/catalog/akrilovye-vanny-kolo' => '/catalog/grupa/akrilovie_vanni/filter/brands_70',
        '/catalog/chugunnie-vanny-roca' => '/catalog/grupa/chugunnie_vanni/filter/brands_71',
        '/catalog/installyaciya-dlya-unitaza-grohe' => '/catalog/grupa/installyacii/filter/brands_78',
        '/catalog/dushevye-kabiny-radaway' => '/catalog/grupa/dushevye_kabiny/filter/brands_53',
        '/catalog/tumba-v-vannu-roca-victoria-60' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog/smesiteli-kfa' => '/catalog/grupa/smesiteli/filter/brands_193',
        '/catalog/mebel-dlya-vannoy-komnaty-uventa' => '/catalog/grupa/mebel_dlya_vannoi/filter/brands_97',
        '/catalog/dushevaya-kabina-eger-tisza-mely-599-187' => '/catalog/grupa/dushevye_kabiny/filter/brands_55',
        '/catalog/otoplenie' => '/',
        '/catalog/umivalniki_roca' => '/catalog/grupa/umivalniki/filter/brands_43',
        '/catalog/smesiteli-welle' => '/catalog/grupa/smesiteli/filter/brands_39',
        '/catalog/mebel-dlya-vannoy-komnaty-aquaform' => '/catalog/grupa/mebel_dlya_vannoi/filter/brands_94',
        '/catalog/dushevye-kabiny-miracle' => '/catalog/grupa/dushevye_kabiny',
        '/catalog/installyaciya-dlya-unitaza-kolo' => '/catalog/grupa/installyacii/filter/brands_76',
        '/catalog/mebel-dlya-vannoy-roca' => '/catalog/grupa/mebel_dlya_vannoi/filter/brands_91',
        '/catalog/akrilovye-vanny-kollerpool' => '/catalog/grupa/akrilovie_vanni/filter/brands_20',
        '/catalog/akrilovye-vanny-triton' => '/catalog/grupa/akrilovie_vanni/filter/brands_19',
        '/catalog/akrilovye-vanny-artemis' => '/catalog/grupa/akrilovie_vanni',
        '/catalog/unitazy-imex' => '/catalog/grupa/unitazi',
        '/catalog/dushevye-kabiny-aquaform' => '/catalog/grupa/dushevye_kabiny/filter/brands_54',
        '/catalog/vanny' => '/catalog/grupa/vanni',
        '/catalog/poddon-dlya-dusha-radawey-korfu-c-4c88400-03' => '/catalog/grupa/dushevye_kabiny',
        '/catalog/smesiteli-gustavsberg' => '/catalog/grupa/smesiteli',
        '/catalog/umivalniki_jacob_delafon' => '/catalog/grupa/umivalniki/filter/brands_49',
        '/catalog/umivalniki_villeroy_bosh' => '/catalog/grupa/umivalniki/filter/brands_47',
        '/catalog/vanna-akrilovaya-standart-130' => '/catalog/grupa/akrilovie_vanni',
        '/catalog/installyaciya-cersanit-siamp-2300' => '/catalog/grupa/installyacii',
        '/catalog/komplekt_installjatsii_cersanit_link_delfi_soft_close_5_v_1' => '/catalog/grupa/installyacii',
        '/catalog/smesiteli-aqua-rodos' => '/catalog/grupa/smesiteli',
        '/catalog/akrilovaya-vanna-bisante-mini-105-70' => '/catalog/grupa/akrilovie_vanni',
        '/catalog/mebel-dlya-vannoy-aquarodos' => '/catalog/grupa/mebel_dlya_vannoi/filter/brands_90',
        '/catalog/installyaciya-dlya-unitaza-geberit' => '/catalog/grupa/installyacii/filter/brands_80',
        '/catalog/smesiteli-hansgrohe' => '/catalog/grupa/smesiteli/filter/brands_14',
        '/catalog/umivalniki_kolo' => '/catalog/grupa/umivalniki/filter/brands_74',
        '/catalog/gidroboks-triton-alfa-150-85' => '/catalog/grupa/dushevie_boksi/filter/brands_19',
        '/catalog/gidroboks-aqualife-sidney-1-90-90' => '/catalog/grupa/dushevie_boksi',
        '/catalog/tumba-s-umivalnikom-estandar-capricho' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog/smesiteli-na_kuhnyu' => '/catalog/grupa/smesiteli/filter/type_3',
        '/catalog/unitazy-jacob-delafon' => '/catalog/grupa/unitazi/filter/brands_49',
        '/catalog/unitazy-villeroyboch' => '/catalog/grupa/unitazi/filter/brands_47',
        '/catalog/installyaciya-cersanit-dlya-bide' => '/catalog/grupa/installyacii',
        '/catalog/dushevye-kabiny-atlantis' => '/catalog/grupa/dushevye_kabiny/filter/brands_28',
        '/catalog/tumba-s-umivalnikom-80--belaya-roca-victoria' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog' => '/',
        '/catalog/shtorki-na-vannu-ravak' => '/catalog/grupa/shtorki_na_vannu/filter/brands_101',
        '/catalog/umivalniki_laufen' => '/catalog/grupa/umivalniki/filter/brands_46',
        '/catalog/vanny-stalnie-i-chugunnie' => '/catalog/grupa/vanni',
        '/catalog/installyaciya-kolo-slim-99235' => '/catalog/grupa/installyacii',
        '/catalog/dushevaya-kabina-eger-balaton-599-507' => '/catalog/grupa/dushevye_kabiny/filter/brands_55',
        '/catalog/dushevye-kabiny-koller-pool' => '/catalog/grupa/dushevye_kabiny/filter/brands_20',
        '/catalog/tumba-s-umivalnikom-podvesnaya-triton-nika' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog/dushevaya-kabina-eger-tisza-599-021' => '/catalog/grupa/dushevye_kabiny/filter/brands_55',
        '/catalog/vanna-cersanit-calabria-170' => '/catalog/grupa/vanni',
        '/catalog/smesiteli-devit' => '/catalog/grupa/smesiteli/filter/brands_151',
        '/catalog/unitaz-roca-khroma' => '/catalog/grupa/unitazi/filter/brands_43',
        '/catalog/vanna-cersanit-joanna-140x90-l-r' => '/catalog/grupa/vanni',
        '/catalog/dushevaja_kabina_zakrytaja_eger_sharkeza_9090' => '/catalog/grupa/dushevye_kabiny/filter/brands_55',
        '/catalog/smesiteli-dushevie_stoiki' => '/catalog/grupa/smesiteli/filter/type_6',
        '/catalog/vanna-prakrik-ravak-165-85' => '/catalog/grupa/vanni',
        '/catalog/dushevye-kabiny-cersanit' => '/catalog/grupa/dushevye_kabiny/filter/brands_162',
        '/catalog/gidroboks-apollo-ts-41w' => '/catalog/grupa/dushevie_boksi/filter/brands_25',
        '/catalog/unitaz-kompak-roca-victoria-34940200u' => '/catalog/grupa/unitazi/filter/brands_43',
        '/catalog/gidroboks-miracle-f77-3' => '/catalog/grupa/dushevie_boksi/filter/brands_21',
        '/catalog/installyaciya-siamp-genua' => '/catalog/grupa/unitazi',
        '/catalog/gidroboks-serena-ew-32130m' => '/catalog/grupa/dushevie_boksi/filter/brands_142',
        '/catalog/bide-ka-leyka-welle' => '/catalog/grupa/unitazi',
        '/catalog/akrilovaya-vanna-bisante-nika-120-70' => '/catalog/grupa/akrilovie_vanni',
        '/catalog/smesitel-termostat-na-vannu-optima' => '/catalog/grupa/smesiteli',
        '/catalog/mebel-dlya-vannoy-ravak' => '/catalog/grupa/mebel_dlya_vannoi/filter/brands_96',
        '/catalog/dushevye-kabiny-apollo' => '/catalog/grupa/dushevye_kabiny/filter/brands_25',
        '/catalog/gidroboks--atlantis-a008' => '/catalog/grupa/dushevie_boksi',
        '/catalog/smesiteli-dlya_bide' => '/catalog/grupa/smesiteli/filter/type_1',
        '/catalog/unitazy-am-pm' => '/catalog/grupa/unitazi/filter/brands_30',
        '/catalog/unitazy-devit' => '/catalog/grupa/unitazi/filter/brands_42',
        '/catalog/vanny-stalnie' => '/catalog/grupa/stalnie_vanni',
        '/catalog/dushevoy-boks-triton-sirius-120-85' => '/catalog/grupa/dushevie_boksi/filter/brands_19',
        '/catalog/gidroboks-tempesta-faoma-90-90' => '/catalog/grupa/dushevie_boksi',
        '/catalog/komplekt-mebeli-v-vannu-nicole' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog/duwevaya-kabina-aquaform-etna-120-85' => '/catalog/grupa/dushevye_kabiny/filter/brands_54',
        '/catalog/installyaciya-dlya-unitaza-grohe-38750' => '/catalog/grupa/installyacii',
        '/catalog/tumba-s-umivalnikom-crw-sp3309' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog/vanna-stalnaya-kaldewei-eurowa-170-70' => '/catalog/grupa/stalnie_vanni/filter/brands_164',
        '/catalog/mebel-dlya-vannoy-komnaty-mebel-ukraina' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog/komplekt-mebeli-v-vannu-natalie' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog/vanny-roca' => '/catalog/grupa/vanni',
        '/catalog/duchevaya-sistema-kludi-mono-shower-system-660810500' => '/catalog/grupa/smesiteli/filter/type_7',
        '/catalog/dushevaja_kabina_eger_tisza_8080' => '/catalog/grupa/dushevye_kabiny/filter/brands_55',
        '/catalog/komplekt_installjatsija_unitaz_tsersanit_tajger' => '/catalog/grupa/installyacii',
        '/catalog/tumba-s-umuvalnikom-roca-gap-70' => '/catalog/grupa/mebel_dlya_vannoi',
        '/catalog/umyvalniki' => '/catalog/grupa/umivalniki',
        '/catalog/unitazy-duravit' => '/catalog/grupa/unitazi/filter/brands_50',
        '/catalog/unitazy-laufen' => '/catalog/grupa/unitazi/filter/brands_46',
        '/catalog/vanna-artemis-zeugma-150-150' => '/catalog/grupa/vanni',
    );

    if (isset($rd[$_SERVER['REQUEST_URI']])) {
        header('Location :http://mirsant.com.ua' . $rd[$_SERVER['REQUEST_URI']], TRUE, 301);
    } else {
        header('Location :http://mirsant.com.ua', TRUE, 301);
    }

    exit;
}
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
define('APPLICATION', '');

$from = array(
    '/catalog/grupa/akrilovie_vanni/filter/brands_154',
    '/catalog/grupa/mebel_dlya_vannoi/filter/brands_148',
    '/catalog/grupa/mebel_dlya_vannoi/filter/brands_106',
);
$to = array(
    '/catalog/grupa/akrilovie_vanni',
    '/catalog/grupa/mebel_dlya_vannoi',
    '/catalog/grupa/mebel_dlya_vannoi',
);
$key = array_search($_SERVER['REQUEST_URI'], $from);
if ($key !== FALSE) {
    header('Location: ' . $to[$key], TRUE, 301);
    exit;
}

#Redirects from 
$redirects = array(
    '/catalog/goods/dushevaja_kabina_eger_tiza_599_021_90_90',
    '/catalog/goods/dushevoj_boks_golston_g_u695_115_115_220_belyj_tv',
    '/catalog/goods/gidroboks_golston_g_u6810_163_163_220_belyj_tv',
    '/catalog/goods/dushevoj_boks_golston_g_u688_150_150_220_belyj_tv',
    '/catalog/goods/gidroboks_golston_g_u688_150_150_220_chernyjtv',
    '/catalog/goods/gidroboks_golston_g_u688_150_150_220_belyj_',
    '/catalog/goods/gidroboks_golston_g_u870_106_106_220_belyj',
    '/catalog/goods/zerkalo_mojdodyr_parma_50_80',
    '/catalog/grupa/mebel_dlya_vannoi/filter/brand_96',
    '/catalog/goods/dushevaja_kabina_cersanit_tesalia_80_80',
    '/catalog/grupa/dushevye_kabiny/filter/brand_68',
    '/catalog/grupa/dushevie_boksi/filter/brand_25',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_klassik_80_sm',
    '/catalog/goods/tumba_dlja_rakoviny_roca_gap_856521577_40_sm',
    '/catalog/grupa/mebel_dlya_vannoi/filter/brand_97',
    '/catalog/grupa/mebel_dlya_vannoi/filter/brand_95',
    '/catalog/grupa/dushevye_kabiny/filter/brand_104',
    '/catalog/goods/zerkalo_mojdodyr_blondi_45_sm',
    '/catalog/grupa/umivalniki/filter/brand_42',
    '/catalog/grupa/dushevye_kabiny/filter/brand_18',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_nika_80_sm',
    '/catalog/grupa/mebel_dlya_vannoi/filter/brand_106',
    '/catalog/grupa/dushevye_kabiny/filter/brand_25',
    '/catalog/goods/unitaz_villeroy_boch_o_nova_5660h101_podvesnoj',
    '/catalog/grupa/dushevye_kabiny/filter/brand_163',
    '/catalog/grupa/dushevye_kabiny/filter/brand_162',
    '/catalog/grupa/shtorki_na_vannu/filter/brand_150',
    '/catalog/goods/zerkalo_mojdodyr_sjuita_64_sm',
    '/catalog/goods/tumbochka_podvesnaja_mojdodyr_blondi',
    '/catalog/goods/vanna_akrilovaja_kolo_mirra_xwp3340000_140_70',
    '/catalog/goods/gidroboks_atlantis_l_530_ax_130_85',
    '/catalog/grupa/gidromassajnie_vanni/filter/brand_57',
    '/catalog/goods/umyvalnik_gustavsberg_top_nordic_596_11l_90sm_levyj',
    '/catalog/grupa/plitka/filter/brand_122',
    '/catalog/grupa/plitka/filter/brand_124',
    '/catalog/goods/penal_podvesnoj_mojdodyr_sjuita',
    '/catalog/goods/unitaz_kompakt_roca_khroma_34265700080165a004_napolnyj_belyj',
    '/catalog/grupa/plitka/filter/brand_135',
    '/catalog/grupa/installyacii/filter/brand_79',
    '/catalog/grupa/akrilovie_vanni/filter/brand_33/page/63',
    '/catalog/goods/zerkalo_mojdodyr_bogatyr_195_100',
    '/catalog/goods/akrilovaja_vanna_cersanit_selena_120_70_45',
    '/catalog/goods/rakovina_gustavsberg_basic_590_4_55_sm',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_klassik_100_sm',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_nika_100_sm',
    '/catalog/grupa/akrilovie_vanni/filter/brand_33/page/62',
    '/catalog/goods/dushevoj_poddon_cersanit_tako_90_90_16',
    '/catalog/goods/akrilovaja_vanna_cersanit_selena_120_70',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_fokus_50_sm',
    '/catalog/goods/shkaf_podvesnoj_mojdodyr_sjuita_',
    '/catalog/grupa/akrilovie_vanni/filter/brand_33/page/65',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_orion_55_sm',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_sjuita_100_sm',
    '/catalog/goods/umyvalnik_laufen_gallery_14079_81_sm_pravyj',
    '/catalog/goods/vanna_akrilovaja_villeroy_boch_sabway_190_90',
    '/catalog/grupa/akrilovie_vanni/filter/brand_33/page/66',
    '/catalog/goods/umyvalnik_villeroy_boch_subway_7309_37_01_37_sm',
    '/catalog/grupa/gidromassazhnye_paneli/filter/brand_128',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_blondi_60_sm',
    '/catalog/goods/bide_roca_khroma_357657000_napolnoe_krasnyjbelyj',
    '/catalog/goods/smesitel_dlja_dusha_welle_ernest',
    '/catalog/goods/smesitel_dlja_vanny_grohe_euroeco_32743000_hrom',
    '/catalog/grupa/akrilovie_vanni/filter/brand_33/page/5',
    '/catalog/goods/unitaz_devit_smart_3020147_podvesnoj_',
    '/catalog/grupa/installyacii/filter/brand_84/page/6',
    '/catalog/goods/zerkalo_atoll_valensija_815_sm_ivory',
    '/catalog/grupa/mebel_dlya_vannoi/filter/brand_94',
    '/catalog/grupa/stalnie_vanni/filter/brand_164',
    '/catalog/goods/dushevaja_cersanit_toskana_90_90_kvadrat',
    '/catalog/goods/akrilovaja_vanna_fibrex_water_nory_130_130',
    '/catalog/goods/umyvalnik_laufen_vienna_comfort_13479_100_sm',
    '/catalog/goods/smesitel_dlja_vannydusha_vstroennyj_grohe_bau_flow_29045000_hrom',
    '/catalog/goods/dushevoj_boks_miracle_100_100',
    '/catalog/goods/umyvalnik_laufen_palace_12508_90_sm',
    '/catalog/goods/dushevoj_boks_atlantis_bh_r0008_90_90_215',
    '/catalog/goods/dekor_monopole_30_40_dulcinea_placer',
    '/catalog/grupa/umivalniki.',
    '/catalog/grupa/unitazi/filter/brand_43',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_parma_100_sm',
    '/catalog/goods/umyvalnik_villeroy_boch_o_nova_51608001_80_sm',
    '/catalog/goods/smesitel_dlja_rakoviny_kludi_objekta_mix_new_33_234_05_75_hrom',
    '/catalog/goods/umyvalnik_laufen_form_13673_90_sm',
    '/catalog/goods/dushevoj_boks_miracle_f7_100_100',
    '/catalog/goods/gidromassazhnyj_boks_caribe_x164_lr_rz_130_80_215_lr',
    '/catalog/goods/tumba_s_umyvalnikom_356_sm_gorenje_jazz_belyj',
    '/catalog/goods/penal_mojdodyr_blondi',
    '/catalog/goods/bide_laufen_gallery_30171_podvesnoe',
    '/catalog/goods/dushevoj_boks_grandehome_ws101s6_9090224_s_parom',
    '/catalog/goods/dushevoj_ugol_kolo_first_zkpg90222003_90_90',
    '/catalog/goods/akrilovaja_vanna_am_pm_bliss_150_150_s_karkasom_i_panelju',
    '/catalog/goods/bide_napolnoe_roca_victoria_357390000_belyj',
    '/catalog/goods/pedestal_roca_giralda_337461000_',
    '/catalog/goods/installjatsija_dlja_unitaza_grohe_38750_',
    '/catalog/goods/zerkalo_mojdodyr_nika_80_sm',
    '/catalog/goods/rakovina_villeroy_boch_verity_51735601_565_sm',
    '/catalog/goods/umyvalnik_villeroy_boch_o_nova_53604501_45_sm',
    '/catalog/goods/dushevaja_cersanit_toskana_80_80',
    '/catalog/goods/installjatsija_dlja_podvesnogo_unitaza_grohe_38775',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_bogatyr_120_sm',
    '/catalog/goods/unitaz_kompakt_devit_iven_3010141_napolnyj',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_orion_50_sm',
    '/catalog/goods/akrilovaja_vanna_artemis_nestor_150_100_44_lr',
    '/catalog/goods/rakovina_villeroy_boch_o_nova_692210_86_sm',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_zodiak_55_sm',
    '/catalog/goods/poddon_dushevoj_s_sideniem_cersanit_90_90_30',
    '/catalog/goods/umyvalnik_villeroy_boch_century_7173_60a1_r1_60_sm',
    '/catalog/goods/akrilovaja_vanna_kollerpool_karina_170110_lr',
    '/catalog/goods/dushevaja_kabina_aquaform_variabel_101_26910_80_90',
    '/catalog/goods/smesitel_dlja_rakoviny_akva_rodos_vita_90343_hrom',
    '/catalog/goods/dushevoj_boks_esk_8812c_120_120_215',
    '/catalog/goods/penal_podvesnoj_40_sm',
    '/catalog/goods/friz_napolnyj_cristalcer_jamajca_15_45_grecco',
    '/catalog/goods/gidroboks_esk_8810a_90_90_215',
    '/catalog/goods/chugunnaja_vanna_roca_continental_21291100r_170_70',
    '/catalog/goods/umyvalnik_villeroy_boch_subway_6116_xx_01_100_sm',
    '/catalog/goods/shkafchik_pod_umyvalnik_kolo_primo_89105_60_sm',
    '/catalog/goods/rakovina_laufen_palace_12615_150_sm_dvojnaja',
    '/catalog/goods/rakovina_duravit_vero_0454120000_120_sm_new',
    '/catalog/goods/poddon_kolo_first_xbn1690_90_90',
    '/catalog/goods/smesitel_dlja_kuhni_kludi_trendo_star_33_978_96_75_nerzhavejuschaja_stal',
    '/catalog/goods/smesitel_dlja_rakoviny_s_gidrozatvorom_grohe_avina_21092000_hrom',
    '/catalog/goods/plitka_nastennaja_metropol_agros_33_33_beige',
    '/catalog/goods/gidromassazhnaja_panel_presto_910',
    '/catalog/goods/rakovina_villeroy_boch_o_nova_633200_895_sm',
    '/catalog/goods/rakovina_ido_11169_55_sm',
    '/catalog/goods/smesitel_dlja_rakoviny_na_3_otverstija_grohe_allure_20144000_hrom',
    '/catalog/goods/akrilovaja_vanna_ravak_gentiana_150_150_lr',
    '/catalog/goods/smesitel_dlja_kuhni_kludi_mix_33_904_19_62_mokka',
    '/catalog/goods/smesitel_dlja_rakoviny_s_gidrozatvorom_grohe_arabesk_20701000_hrom_',
    '/catalog/goods/smesitel_dlja_kuhni_s_krafinom_i_filtrom_grohe_blue_33249000_hrom',
    '/catalog/goods/smesitel_dlja_dusha_s_termostatom_grohe_grohtherm_3000_34179000_hrom',
    '/catalog/goods/rakovina_laufen_living_18431_150_sm',
    '/catalog/goods/akrilovaja_vanna_ravak_evolution_180102_lr',
    '/pic/globus.swf',
    '/catalog/grupa/dushevye_kabiny/filter/brand_163',
    '/catalog/grupa/dushevie_boksi/filter/brand_25',
    '/catalog/grupa/installyacii/filter/brand_79',
    '/catalog/goods/vanna_stalnaja_kaldewei_saniform_plus_star_170_70',
    '/catalog/grupa/gidromassazhnye_paneli/filter/brands_128',
    '/catalog/goods/dushevoj_parovoj_boks_ws_118_150_150',
    '/catalog/goods/zerkalo_kolo_primo_88140_50_70',
    '/catalog/goods/vanna_s_gidro_i_aeromassazhem_170_96_4_lr',
    '/catalog/goods/zerkalnyj_shkafchik_mojdodyr_zodiak_55_sm',
    '/catalog/goods/unitaz_kompakt_am_pm_bliss_c558607wh_napolnyj',
    '/catalog/grupa/akrilovie_vanni/filter/brands_35',
    '/catalog/goods/umyvalnik_laufen_form_13675_120_sm',
    '/catalog/goods/gidroboks_miracle_90_90',
    '/catalog/goods/unitaz_podvesnoj_cersanit_yasmina_',
    '/catalog/goods/unitaz_ido_pozzi_ginori_easy_74421_01_pristavnoj',
    '/catalog/goods/unitaz_villeroy_boch_aveo_661210r1_podvesnoj',
    '/catalog/goods/shkaf_pod_umyvalnik_kolo_primo_89106_70_sm',
    '/catalog/goods/penal_kolo_twins_88323',
    '/catalog/goods/penal_kolo_primo_88137_',
    '/catalog/goods/akrilovaja_vanna_artemis_minevra_142_142_56',
    '/catalog/goods/unitaz_kompakt_ido_mozaik_39171_99_napolnyj',
    '/catalog/goods/akrilovaja_vanna_cersanit_adria_150_105_46_lr',
    '/catalog/goods/unitaz_villeroy_boch_aveo_661310r1_napolnyj',
    '/catalog/goods/dushevoj_poddon_cersanit_80_80_16',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_parma_80_sm',
    '/catalog/goods/smesitel_dlja_rakoviny_nastennyj_na_2_otverstija_grohe_bau_classic_20292000_hrom',
    '/catalog/goods/umyvalnik_laufen_gallery_14178_dvojnoj_135_sm',
    '/catalog/goods/bide_laufen_vienna_comfort_30471_napolnoe',
    '/catalog/goods/gidroboks_esk_8813_90_90_215',
    '/catalog/goods/tumba_s_umyvalnikom_mojdodyr_parma_60_sm',
    '/catalog/goods/poddon_kolo_first_xbn1680_80_80',
    '/catalog/goods/akrilovaja_vanna_artemis_gektor_170_80_44',
    '/catalog/goods/smesitel_dlja_rakoviny_s_gidrozatvorom_grohe_avina_21093000_hrom',
    '/catalog/goods/nabor_smesitelej_vanna_ravak_suzan',
    '/catalog/goods/smesitel_dlja_rakoviny_welle_hans_h16090d_hrom',
    '/catalog/goods/dushevoj_boks_eco_san_k44_120_120_215_',
    '/catalog/goods/dushevoj_boks_apollo_aw_5030_90_90_217',
    '/catalog/goods/umyvalnik_laufen_gallery_14078_81_sm_levyj',
    '/catalog/goods/unitaz_kompakt_gustavsberg_nordic_390_belyj',
    '/catalog/goods/smesitel_dlja_vanny_devit_vintage_23247122_hrom',
    '/catalog/goods/gidromassazhnaja_panel_presto_913',
    '/catalog/goods/akrilovaja_vanna_artemis_akva_170_110_lr',
    '/catalog/goods/gidroboks_miracle_f76_120_85',
    '/catalog/goods/unitaz_kompakt_gustavsberg_nordic_112320661205_podvesnoj',
    '/catalog/goods/smesitel_dlja_kuhni_devit_prestige_56163kt_hrom',
    '/catalog/goods/umyvalnik_laufen_mylife_10946_75_sm',
    '/catalog/goods/umyvalnik_laufen_palace_12518_90_sm',
    '/catalog/goods/panel_gidromassazhnaja_presto_916',
    '/catalog/goods/smesitel_dlja_dusha_vstroennyj_s_termostatom_hansgrohe_ecostat_e_31570000_hrom',
    '/catalog/goods/gidromassazhnyj_boks_esk_8810c_90_90_215',
    '/catalog/goods/shtora_dlja_vanny_aquaform_standard_1_170_04200',
    '/catalog/goods/gidromassazhnyj_boks_grandehome_ws103ls6_12090224_lr_s_parom',
    '/catalog/goods/smesitel_dlja_vanny_na_3_otverstija_grohe_eurodisk_33390001_hrom',
    '/catalog/goods/gidromassazhnaja_panel_presto_902',
    '/catalog/goods/dushevoj_nabor_grohe_eurosmart_116936_hrom',
    '/catalog/goods/akrilovaja_vanna_vagnerplast_minerva_1707042',
    '/catalog/goods/smesitel_dlja_rakoviny_devit_optima_16187130p_hrom',
    '/catalog/goods/umyvalnik_laufen_mimo_15552_45_sm',
    '/catalog/goods/unitaz_podvesnoj',
    '/catalog/goods/rakovina_laufen_vienna_10034_60_sm',
    '/catalog/grupa/mebel_dlya_vannoi/filter/brand_104;sort_min-max/page/136',
    '/catalog/grupa/akrilovie_vanni/filter/brands_103;sort_max-min/page/1',
    '/catalog/grupa/akrilovie_vanni/filter/brand_33;sort_min-max/page/68',
    '/catalog/grupa/akrilovie_vanni/filter/brands_139;sort_max-min/page/4',
    '/catalog/grupa/akrilovie_vanni/filter/brand_33;sort_max-min/page/73',
    '/catalog/grupa/akrilovie_vanni/page/73/filter/sort_min-max',
);

$key = array_search($_SERVER['REQUEST_URI'], $redirects);
if ($key !== FALSE) {
    $tmp = explode('/', $redirects[$key]);
    $k = array_search('grupa', $tmp);
    if ($k !== FALSE) {
        header('Location: /catalog/grupa/' . $tmp[$k + 1], TRUE, 301);
    } else {
        header('Location: /', TRUE, 301);
    }
    exit;
}
#End Redirects
#Redirect from page 1 & 0

$pArr = explode('/', $_SERVER['REQUEST_URI']);
$end = end($pArr);

if (in_array('page', $pArr) AND $end < 2) {
    $count = count($pArr);
    unset($pArr[$count - 1]);
    unset($pArr[$count - 2]);

    header('Location: ' . implode('/', $pArr), TRUE, 301);
    exit;
}

ob_start();

#header("Cache-Control: public");
#header("Expires: " . date("r", time() + 3600));
header('Content-Type: text/html; charset=UTF-8');
//echo "<h1>", date("H:i:s"), "</h1>";
// цепляем все функции
require "main.php";

if (DEBUG_SITE == 1) {
    // время выполнения скрипта
    require_once 'plagin/Benchmark/Profiler.php';
    global $profiler;
    $profiler = new Benchmark_Profiler(TRUE);
    $profiler->start();

    // время
    $profiler->enterSection('head');
}

// парсим адресную строку
get_url();

if (isset($_GET['gclid'])) {
    unset($_GET['gclid']);
}

if (DEBUG_SITE == 1) {
    // время
    $profiler->leaveSection('head');
}


if (!isset($_SESSION['currency'])) {
    $_SESSION['currency'] = SHOW_CURRENCY;
}

if (!isset($_GET['action'])) {
    $_GET['action'] = "index";
}

// проверяем на КЕШ
/*
 * если у нас есть закешированная страница - выводим ее
 * иначе генерим и создаем кеш страницу
 */

// используем КЕШ сайта	
if (USE_KEH == 1) {
    // подключаем класс работы CONTENT
    require "classes/cache.class.php";
    // если время с создания папки cache/index > 24 часов, удаляем ее
    if (get_date_last_modify_file_in_second(HOST . '/cache/index') > 86400) {
        /*
         * удаляем кеш
         */
        removedir(HOST . "/cache/index");
    }
    use_keh();
}


// если печатаем страницу
if (isset($_GET['action']) and $_GET['action'] == "print") {
    require HOST . "/ctrl/print/print.php";
    exit;
}

if (DEBUG_SITE == 1) {
    //error_reporting(E_ALL);
    ini_set('display_errors', 1);
}


if (DEBUG_SITE == 1) {
    // время
    $profiler->enterSection('center');
}

// выводим контентовую часть
$_str = general::center();

if (DEBUG_SITE == 1) {
    // время
    $profiler->leaveSection('center');
}


/*
 * данный блок используется ЕСЛИ НЕОБХОДИМО ПОДСТАВЛЯТЬ НЕСКОЛЬКО ШАБЛОНОВ (начало)
 */
// подставляем необходимый шаблон
/*
  $name_file="/tpl/index_full.tpl.html";


  if (isset($_GET['action'])) {
  switch ($_GET['action']) {

  /*
  case "catalog":

  if (isset($_GET['id'])) {
  $name_file="/tpl/catalog_page.tpl.html";
  //} else if (isset($_GET['main'])) {
  //$name_file="/tpl/catalog.tpl.html";
  } else {
  $name_file="/tpl/catalog_list.tpl.html";
  }

  break;
 */

/* 		
  case "index":
  $name_file="/tpl/index_full.tpl.html";
  break;
 */

/*
  default:
  //$name_file="/tpl/index.tpl.html";
  $name_file="/tpl/index_full.tpl.html";
  break;

  }

  }
 */


if (DEBUG_SITE == 1) {
    // время
    $profiler->enterSection('echo');
}


// создаем КЕШ сайта	
if (USE_KEH == 1) {
    create_keh($_str);
}

$a = show_title();

// генерим статику
$_statik = array(
    'TITLE' => is_array($_str) ? $_str['title'] : $a[0], // PERMANENT
    'KEYWORDS' => is_array($_str) ? $_str['keywords'] : $a[2], // PERMANENT
    'DESCRIPTION' => is_array($_str) ? $_str['description'] : $a[1], // PERMANENT
    'H1' => is_array($_str) ? $_str['h1'] : get_h1(), // PERMANENT
    'HEAD' => head1(), // PERMANENT
    'news_at_first' => mysql::query_findpole('select zna from config where id=46', 'zna'),
    'lider_at_first' => mysql::query_findpole('select zna from config where id=39', 'zna'),
    'copy' => mysql::query_findpole('select zna from config where id=32', 'zna'),
    'tel1' => mysql::query_findpole('select zna from config where id=30', 'zna'),
    'tel2' => mysql::query_findpole('select zna from config where id=31', 'zna'),
    'twitter' => mysql::query_findpole('select zna from config where id=33', 'zna'),
    'facebook' => mysql::query_findpole('select zna from config where id=34', 'zna')
);

echo system::show_tpl(
        array(
    'str' => is_array($_str) ? $_str['_str'] : $_str,
    'statik' => $_statik,
    'ecommerce' => $_ecommerce
        ), 'index_full.php');

// *******************************************************


if (DEBUG_SITE == 1) {
    // время
    $profiler->leaveSection('echo');
}


// вывод/невывод дебаггера
if (DEBUG_SITE == 1) {
    echo '<div id="debug" style="float: left; width: 100%; border: solid red 0px; background: #C0C0C0; ">';
    // время
    $profiler->stop();
    $profiler->display();

    // дебагер
    debug();
    echo "</div>";
}

// RSS
//require_once "classes/xml.class.php";
//do_xml();
?>